﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DGVPrinterHelper;

namespace Boon
{
    public partial class ClasswiseFee : Form
    {
        public ClasswiseFee()
        {
            InitializeComponent();
        }
        common c = new common();
        private void GetListBtn_Click(object sender, EventArgs e)
        {
            try
            {
                double FTerm = 0;
                double STerm = 0;
                
                StudCountTxt.Text = "Total students : 0";
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    this.dataGridView1.DataSource = null;
                    dataGridView1.Rows.Clear();
                    DataTable table1 = new DataTable();
                    DataTable MyTable = new DataTable();
                    MyTable.Columns.Add("First Term");
                    MyTable.Columns.Add("Second Term");
                    table1 = c.SelectData("SELECT ID,Name,RemaingFee,Std,OldRemFees from Boon.stud_p_primary WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'");
                    
                    foreach (DataRow row in table1.Rows)
                    {
                        DataTable tbl = c.SelectData("Select FirstTerm,SecondTerm from Boon.feesstructure where Class ='" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text+ "'");
                        if (tbl.Rows.Count > 0)
                        {
                            string a = tbl.Rows[0][0].ToString();
                            string b = tbl.Rows[0][1].ToString();
                            if (a != "" && a != " " && a != null)
                            {
                                FTerm = Convert.ToDouble(a);
                            }
                            if (b != "" && b != " " && b != null)
                            {
                                STerm = Convert.ToDouble(b);
                            }

                        }
                        int ID = Convert.ToInt32(row["ID"].ToString());
                        DataTable dt = c.SelectData("Select Sum(EducationFee),SUM(ExamFee) from Boon.receipts_pri_primary where ID='" + ID + "' AND AcdYear ='" + AcdYearBox.Text + "'");
                        if (dt.Rows.Count > 0)
                        {
                            String f1 = dt.Rows[0][0].ToString();
                            string f2 = dt.Rows[0][1].ToString();
                            if (f1 != "" && f1 != " " && f1 != null)
                            {
                                double FirstTerm = Convert.ToDouble(dt.Rows[0][0].ToString());
                                if (FTerm > 0)
                                {
                                    FTerm = FTerm - FirstTerm;
                                }
                            }
                            if (f2 != "" && f2 != " " && f2 != null)
                            {
                                double SecondTerm = Convert.ToDouble(dt.Rows[0][1].ToString());
                                if (STerm > 0)
                                {
                                    STerm = STerm - SecondTerm;
                                }
                            }
                        }
                        double oldrem = Convert.ToDouble(row["OldRemFees"].ToString());
                        double RemNew = Convert.ToDouble(row["RemaingFee"].ToString());
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = row["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = row["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = row["Std"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = FTerm.ToString();
                        dataGridView1.Rows[n].Cells[4].Value = STerm.ToString();
                        dataGridView1.Rows[n].Cells[5].Value = row["OldRemFees"].ToString();
                        dataGridView1.Rows[n].Cells[6].Value = oldrem + RemNew;
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                       
                    }
                   
                    
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    this.dataGridView1.DataSource = null;
                    dataGridView1.Rows.Clear();
                    DataTable table1 = new DataTable();

                    table1 = c.SelectData("SELECT ID,Name,RemaingFee,Std,OldRemFees  from Boon.students WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'");

                    foreach (DataRow row in table1.Rows)
                    {
                        DataTable tbl = c.SelectData("Select FirstTerm,SecondTerm from Boon.feesstructure where Class ='" + ClassBox.Text + "' AND AcdYear ='" + AcdYearBox.Text + "'");
                        if (tbl.Rows.Count > 0)
                        {
                            string a = tbl.Rows[0][0].ToString();
                            string b = tbl.Rows[0][1].ToString();
                            if (a != "" && a != " " && a != null)
                            {
                                FTerm = Convert.ToDouble(a);
                            }
                            if (b != "" && b != " " && b != null)
                            {
                                STerm = Convert.ToDouble(b);
                            }

                        }
                        int ID = Convert.ToInt32(row["ID"].ToString());
                        DataTable dt = c.SelectData("Select Sum(EducationFee),SUM(ExamFee) from Boon.receipts where ID='" + ID + "' AND AcdYear ='"+AcdYearBox.Text+"'");
                        if (dt.Rows.Count > 0)
                        {
                            String f1 = dt.Rows[0][0].ToString();
                            string f2 = dt.Rows[0][1].ToString();
                            if (f1 !="" && f1!=" " && f1 !=null)
                            {
                                double FirstTerm = Convert.ToDouble(dt.Rows[0][0].ToString());
                                if (FTerm > 0)
                                {
                                    FTerm = FTerm - FirstTerm;
                                }
                            }
                            if (f2 != "" && f2 != " " && f2 != null)
                            {
                                double SecondTerm = Convert.ToDouble(dt.Rows[0][1].ToString());
                                if (STerm > 0)
                                {
                                    STerm = STerm - SecondTerm;
                                }
                            }
                        }
                        double oldrem = Convert.ToDouble(row["OldRemFees"].ToString());
                        double RemNew = Convert.ToDouble(row["RemaingFee"].ToString());
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = row["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = row["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = row["Std"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = FTerm.ToString();
                        dataGridView1.Rows[n].Cells[4].Value =  STerm.ToString();
                        dataGridView1.Rows[n].Cells[5].Value = row["OldRemFees"].ToString();
                        dataGridView1.Rows[n].Cells[6].Value = oldrem + RemNew;
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                    }

                    //dataGridView1.DataSource = table1;
                    //dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                 
                    //dataGridView1.Columns[0].Width = 50;
                    //dataGridView1.Columns[1].Width = 300;
                    //dataGridView1.Columns[2].Width = 200;
                    //dataGridView1.Columns[3].Width = 131;
                    StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    this.dataGridView1.DataSource = null;
                    dataGridView1.Rows.Clear();
                    DataTable table1 = new DataTable();

                    table1 = c.SelectData("SELECT ID,Name,RemaingFee,Std,OldRemFees from Boon.students_high_school WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ");
                    foreach (DataRow row in table1.Rows)
                    {
                        DataTable tbl = c.SelectData("Select FirstTerm,SecondTerm from Boon.feesstructure where Class ='" + ClassBox.Text + "'AND AcdYear ='" + AcdYearBox.Text + "'");
                        if (tbl.Rows.Count > 0)
                        {
                            string a = tbl.Rows[0][0].ToString();
                            string b = tbl.Rows[0][1].ToString();
                            if (a != "" && a != " " && a != null)
                            {
                                FTerm = Convert.ToDouble(a);
                            }
                            if (b != "" && b != " " && b != null)
                            {
                                STerm = Convert.ToDouble(b);
                            }

                        }
                        int ID = Convert.ToInt32(row["ID"].ToString());
                        DataTable dt = c.SelectData("Select Sum(EducationFee),SUM(ExamFee) from Boon.receipts_high where ID='" + ID + "' AND AcdYear ='" + AcdYearBox.Text + "'");
                        if (dt.Rows.Count > 0)
                        {
                            String f1 = dt.Rows[0][0].ToString();
                            string f2 = dt.Rows[0][1].ToString();
                            if (f1 != "" && f1 != " " && f1 != null)
                            {
                                double FirstTerm = Convert.ToDouble(dt.Rows[0][0].ToString());
                                if (FTerm > 0)
                                {
                                    FTerm = FTerm - FirstTerm;
                                }
                            }
                            if (f2 != "" && f2 != " " && f2 != null)
                            {
                                double SecondTerm = Convert.ToDouble(dt.Rows[0][1].ToString());
                                if (STerm > 0)
                                {
                                    STerm = STerm - SecondTerm;
                                }
                            }
                        }
                        double oldrem = Convert.ToDouble(row["OldRemFees"].ToString());
                        double RemNew = Convert.ToDouble(row["RemaingFee"].ToString());

                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = row["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = row["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = row["Std"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = FTerm.ToString();
                        dataGridView1.Rows[n].Cells[4].Value = STerm.ToString();
                        dataGridView1.Rows[n].Cells[5].Value = row["OldRemFees"].ToString();
                        dataGridView1.Rows[n].Cells[6].Value = oldrem + RemNew;
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                    }
                }
            }
            catch { }
        }

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();
                
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    String Str = "select Distinct(AcdYear) from Boon.stud_p_primary where Std = '" + ClassBox.Text + "' ORDER BY AcdYear ";
                    DataTable dt = c.SelectData(Str);

                    foreach (DataRow row in dt.Rows)
                    {
                        AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    String Str = "select Distinct(AcdYear) from Boon.students where Std = '" + ClassBox.Text + "' ORDER BY AcdYear ";
                    DataTable dt = c.SelectData(Str);

                    foreach (DataRow row in dt.Rows)
                    {
                        AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String Str = "select Distinct(AcdYear) from Boon.students_high_school where Std = '" + ClassBox.Text + "' ORDER BY AcdYear ";
                    DataTable dt = c.SelectData(Str);

                    foreach (DataRow row in dt.Rows)
                    {
                        AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }              
                
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
        


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                //ClassBox.Items.Clear();
                //if (comboBox1.Text=="Pre-Primary")
                //{
                //    String Str = "select Distinct(Std) from Boon.stud_p_primary ORDER BY Std ";
                //DataTable dt = c.SelectData(Str);

                //foreach (DataRow row in dt.Rows)
                //{
                //    AcdYearBox.Items.Add(row["Std"].ToString());
                //}
                //}
                // if (comboBox1.Text=="Primary")
                //{
                //    String Str = "select Distinct(Std) from Boon.students ORDER BY Std ";
                //    DataTable dt = c.SelectData(Str);

                //    foreach (DataRow row in dt.Rows)
                //    {
                //        AcdYearBox.Items.Add(row["Std"].ToString());
                //    }
                // }
                // if (comboBox1.Text == "High School")
                //{
                //    String Str = "select Distinct(Std) from Boon.students_high_school ORDER BY Std ";
                //    DataTable dt = c.SelectData(Str);

                //    foreach (DataRow row in dt.Rows)
                //    {
                //        AcdYearBox.Items.Add(row["Std"].ToString());
                //    }
                // }
            
            }
            catch { }
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
    }
}
