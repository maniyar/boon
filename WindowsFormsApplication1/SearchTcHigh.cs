﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DGVPrinterHelper;

namespace Boon
{
    public partial class SearchTcHigh : Form
    {
        public SearchTcHigh()
        {
            InitializeComponent();
        }
        common c = new common();
        private void SearchByDateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                lblTotalTC.Text = "Total T.C : 0 ";
                dataGridView1.DataSource = null;
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_primary WHERE DateOfApplication Between '" + dateTimePicker1.Value + "' AND '" + dateTimePicker2.Value + "'AND Std = '" + ClassBox.Text + "' ";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");                        
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_high WHERE DateOfApplication Between '" + dateTimePicker1.Value + "' AND '" + dateTimePicker2.Value + "' AND Std = '" + ClassBox.Text + "' ";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
            
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Exception");
            }
        }

        private void GenSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                lblTotalTC.Text = "Total T.C : 0 ";
                dataGridView1.DataSource = null;
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_primary WHERE ID = '"+IdTxt.Text+"'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_high WHERE ID = '" + IdTxt.Text + "'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void NameSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                lblTotalTC.Text = "Total T.C : 0 ";
                dataGridView1.DataSource = null;
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_primary WHERE Name LIKE  '" + NameTxt.Text + "%'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_high WHERE Name LIKE  '" + NameTxt.Text + "%'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                lblTotalTC.Text = "Total T.C : 0 ";
                dataGridView1.DataSource = null;
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_primary WHERE TcNo =  '" + TcNoTxt.Text + "'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_high WHERE TcNo =  '" + TcNoTxt.Text + "'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void AllTcBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_primary order by TcNo";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.tc_high order by TcNo";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        dataGridView1.DataSource = table;
                        lblTotalTC.Text = "Total T.C : " + dataGridView1.Rows.Count;
                    }
                    else
                    {
                        MessageBox.Show("Data Not Available");
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
    }
}
