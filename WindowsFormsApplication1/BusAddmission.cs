﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class BusAddmission : Form
    {
        public BusAddmission()
        {
            InitializeComponent();
        }
      
        common c = new common();
        DataTable dt = new DataTable();
        String Address;
        long Contact;

        private void GetListBtn_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            try
            {
                if (StdBox.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    StdBox.Focus();
                    MessageBox.Show("Please select standard and Vehicle ID first");

                }
                else if (VehicleIDBox.Text == "")
                {
                    string myStringVariable3 = string.Empty;
                    VehicleIDBox.Focus();
                    MessageBox.Show("Please select Vehicle ID");
                }
                else if (StdBox.Text != "" & VehicleIDBox.Text != "")
                {
                    if (StdBox.Text == "Nursery" || StdBox.Text == "Sr.KG" || StdBox.Text == "Jr.KG")
                    {
                        String SelectQuery = "SELECT * from Boon.stud_p_primary WHERE Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";
                        DataTable table = new DataTable();
                        table = c.SelectData(SelectQuery);
                        foreach (DataRow item in table.Rows)
                        {
                            int n = dataGridView1.Rows.Add();
                            dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                            dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                            dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                            dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                            dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                        }
                    }
                    if (StdBox.Text == "1 st" || StdBox.Text == "2 nd" || StdBox.Text == "3 rd" || StdBox.Text == "4 th" || StdBox.Text == "5 th" || StdBox.Text == "6 th" || StdBox.Text == "7 th")
                    {
                        String SelectQuery = "SELECT * from Boon.students WHERE Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";
                        DataTable table = new DataTable();
                        table = c.SelectData(SelectQuery);
                        foreach (DataRow item in table.Rows)
                        {
                            int n = dataGridView1.Rows.Add();
                            dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                            dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                            dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                            dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                            dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                        }
                    }
                    if (StdBox.Text == "8 th" || StdBox.Text == "9 th" || StdBox.Text == "10 th")
                    {


                        String SelectQuery = "SELECT * from Boon.Students_high_school WHERE Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";
                        DataTable table = new DataTable();
                        table = c.SelectData(SelectQuery);
                        foreach (DataRow item in table.Rows)
                        {
                            int n = dataGridView1.Rows.Add();
                            dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                            dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                            dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                            dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                            dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                        }
                    }
                }
            }
            catch
            {

            }

        }



     
        public void PassValue()
        {
            try
            {
                int i = 0;
                int j = 0;
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {
                     
                        var SelectedID = row.Cells[0].Value.ToString();

                        String Name = row.Cells[1].Value.ToString();

                        Address = row.Cells[2].Value.ToString();
                        Contact = long.Parse(row.Cells[3].Value.ToString());
                        
                        String SelectQuery = "Select * from Boon.busaddmisions Where ID = '" + SelectedID + "' AND Std = '" + StdBox.Text + "' ";

                        dt = c.SelectData(SelectQuery);

                        if (dt.Rows.Count > 0)
                        {
                            String UpdateQuery = "UPDATE Boon.busaddmisions SET ID = '" + SelectedID + "' , Name_of_student = '" + Name + "' ,Std = '" + StdBox.Text + "',Contact = '" + Contact + "',Address = '" + Address + "' ,Vehicle_ID = '" + VehicleIDBox.Text + "'  Where ID = '" + SelectedID + "'  AND Std = '" + StdBox.Text + "'";

                            if (c.InsertData(UpdateQuery))
                            {
                              //  MessageBox.Show("Success the code for Update", "alert");
                            }

                            // MessageBox.Show("Success the code", "alert");

                        }
                        else
                        {
                            String InsertQuery = "INSERT INTO Boon.busaddmisions (ID,Name_of_student,Address,Contact,Std,Vehicle_ID) Values ('" + SelectedID + "','" + Name + "','" + Address + "','" + Contact + "','" + StdBox.Text + "','" + VehicleIDBox.Text + "')";

                            if (c.InsertData(InsertQuery))
                            {
                               // MessageBox.Show("Success the code for Insert", "alert");
                            }
                        }

                      
                        ++i;
                    }
                    else
                    {
                        ++j;
                    }


                    if (i + j == dataGridView1.Rows.Count)
                    {
                        MessageBox.Show("Updated succsessfully", "Alert");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BusIDLoad()
        {
            try
            {
                VehicleIDBox.Items.Clear();
                String Str = "select Vechile_ID from Boon.bus ";

                DataTable tb = c.SelectData(Str);
                if(tb.Rows.Count>0)
                {
                    foreach (DataRow row in tb.Rows)
                    {
                        VehicleIDBox.Items.Add(row["Vechile_ID"].ToString());
                    }
                }
                   

               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                
            }
        }


        private void BusAddmission_Load(object sender, EventArgs e)
        {
          //  BusIDLoad();
            try
            {
         
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
               
            }
        }

    

        private void button3_Click(object sender, EventArgs e)
        {

        }
        //
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
          

        }

    

        private void AddBtn_Click(object sender, EventArgs e)
        {
             int i1 = 0;
             foreach (DataGridViewRow row in this.dataGridView1.Rows)
             {
                 //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                 if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                 {
                     i1++;
                     if (i1 == dataGridView1.Rows.Count)
                     {
                         MessageBox.Show("You have to select at least one student", "Alert");
                     }
                     // MessageBox.Show("You have to select at least one student", "Alert");
                     // break;
                 }
                 else
                 {

                     if (MessageBox.Show("Do you want to update stud_p_primary?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                     {

                         PassValue();
                         break;
                     }
                 }
             }
        }

        

        private void StdBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void VehicleIDBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[5].Value = row.Cells[5].Value == null ? false : !(bool)row.Cells[5].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void StdBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                BusIDLoad();
                AcdYearBox.Items.Clear();
                if (StdBox.Text == "Nursery" || StdBox.Text == "Sr.KG" || StdBox.Text == "Jr.KG")
                    {
                String Str = "select Distinct(AcdYear) from Boon.stud_p_primary where Std = '" + StdBox.Text + "' order by  AcdYear";
                DataTable tbl = c.SelectData(Str);
                if (tbl.Rows.Count>0)
                {
                    foreach(DataRow row in tbl.Rows)
                    {
                    AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
                }
                 if (StdBox.Text == "1 st" || StdBox.Text == "2 nd" || StdBox.Text == "3 rd" || StdBox.Text == "4 th" || StdBox.Text == "5 th" || StdBox.Text == "6 th" || StdBox.Text == "7 th")
                    {
                String Str = "select Distinct(AcdYear) from Boon.students where Std = '" + StdBox.Text + "' order by  AcdYear";
                DataTable tbl = c.SelectData(Str);
                if (tbl.Rows.Count>0)
                {
                    foreach(DataRow row in tbl.Rows)
                    {
                    AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
                }
                if (StdBox.Text == "8 th" || StdBox.Text == "9 th" || StdBox.Text == "10 th")
                    {
                String Str = "select Distinct(AcdYear) from Boon.students_High_school where Std = '" + StdBox.Text + "' order by  AcdYear";
                DataTable tbl = c.SelectData(Str);
                if (tbl.Rows.Count>0)
                {
                    foreach(DataRow row in tbl.Rows)
                    {
                    AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
                }
            
            }
            catch { }
        }
        
    }
}

