﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class UnitTestOne : Form
    {

        MySqlConnection connection;
        common obj = new common();
        public UnitTestOne()
        {
            connection = new MySqlConnection(obj.ConnectionString);

            InitializeComponent();
        }
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        MySqlCommand command;
        public void Query(String query)
        {
            try
            {
                openConnection();
                command = new MySqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 0)
                {
                    // MessageBox.Show("Data is refeshed");
                }
                else
                { //MessageBox.Show("Data is not refe"); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }
        public void Load_Grid()
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();

            String SelectQuery = "SELECT * from Boon.Students WHERE Std = '" + comboBox1.Text + "';";
            DataTable table1 = new DataTable();
            MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
            adpater.Fill(table1);
            dataGridView1.DataSource = table1;
            dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Width = 250;
            dataGridView1.Columns[2].Width = 250;
            dataGridView1.Columns[3].Width = 150;
            Query(SelectQuery);
            adpater.Dispose();
        }
        private void GetList_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                string myStringVariable2 = string.Empty;
                comboBox1.Focus();
                MessageBox.Show("Please select standard of student first");

            }
            else
            {
                Load_Grid(); 
            }
        }
        
        public void ClearBoxes()
        {
            NameTxt.Text = null;
            IdTxt.Text = null;
            StdTxt.Text = null;
            MarksTxt.Text = null;
        }
     
        private void SaveBtn_Click(object sender, EventArgs e)
        {
           
            try
            {

                String insertQuery = "UPDATE Boon.Students SET Unit_1=@Unit_1 WHERE ID='" + IdTxt.Text + "'";

                connection.Open();

                command = new MySqlCommand(insertQuery, connection);
                command.Parameters.Add("@Unit_1", MySqlDbType.Float, 10);

                command.Parameters["@Unit_1"].Value = MarksTxt.Text;
                if (command.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Exam details saved successfull...");
                    Load_Grid();
                    ClearBoxes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message );
            }
            finally
            {
                closeConnection();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            IdTxt.Text = selectedRow.Cells[0].Value.ToString();
            NameTxt.Text = selectedRow.Cells[1].Value.ToString();
            StdTxt.Text = selectedRow.Cells[8].Value.ToString();
            MarksTxt.Text = selectedRow.Cells[19].Value.ToString();
        }

        private void SearchBtn_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                string myStringVariable2 = string.Empty;
                comboBox1.Focus();
                MessageBox.Show("Please select standard of student first");

            }
            else
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Name LIKE '%{0}%'", SearchBtn.Text);
            }
        }

        private void MarksTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }
      
    }
}
