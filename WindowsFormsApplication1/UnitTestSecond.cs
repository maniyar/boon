﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class UnitTestSecond : Form
    {
        MySqlConnection connection;
        MySqlConnection mysql;
        DataTable dt;
        common obj = new common();
        public UnitTestSecond()
        {
            connection = new MySqlConnection(obj.ConnectionString);
            bindgrid();
            InitializeComponent();
        }
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        MySqlCommand command;
        public void Query(String query)
        {
            try
            {
                openConnection();
                command = new MySqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 0)
                {
                    // MessageBox.Show("Data is refeshed");
                }
                else
                { //MessageBox.Show("Data is not refe"); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }
        public void Load_Grid()
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            
            String SelectQuery = "SELECT ID,Name,Std,StudentId,Unit_2 from Boon.Students WHERE Std = '" + comboBox1.Text + "';";
            DataTable table1 = new DataTable();
            MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
            adpater.Fill(table1);
            bindgrid();
            dataGridView1.DataSource = table1;
            dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
          
            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Width = 250;

            Query(SelectQuery);
            adpater.Dispose();
            //connection.Open();
            //mysql = new MySqlConnection();
            //mysql.ConnectionString = obj.ConnectionString;
            //mysql.Open();
            //DataTable dt = new DataTable();
            //MySqlCommand Cmd1 = new MySqlCommand();
            //Cmd1.CommandText = "Select * from Boon.students where std='" + comboBox1.Text + "'";
            //Cmd1.Connection = connection;
            //MySqlDataReader dr1 = Cmd1.ExecuteReader();
            //while (dr1.Read())
            //{
            //    dt.Rows.Add(dr1["ID"].ToString(), dr1["Name"].ToString(), dr1["Std"].ToString(), dr1["StudentId"].ToString(), dr1["Unit_2"].ToString());

            //}
            //dr1.Close();
            //// ID,Name,Std,StudentId,Unit_2

            //mysql.Close();
    
          
        }
     
        private void SearchBtn_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                string myStringVariable2 = string.Empty;
                comboBox1.Focus();
                MessageBox.Show("Please select standard of student first");

            }
            else
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Name LIKE '%{0}%'", SearchBtn.Text);
            }
        }
        public void ClearBoxes()
        {
            NameTxt.Text = null;
            IdTxt.Text = null;
            StdTxt.Text = null;
            MarksTxt.Text = null;
        }
        private void GetList_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                string myStringVariable2 = string.Empty;
                comboBox1.Focus();
                MessageBox.Show("Please select standard of student first");

            }
            else
            {   
             Load_Grid();
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {

                String insertQuery = "UPDATE Boon.Students SET Unit_2=@Unit_2 WHERE ID='" + IdTxt.Text + "'";

                connection.Open();

                command = new MySqlCommand(insertQuery, connection);
                command.Parameters.Add("@Unit_2", MySqlDbType.Float, 10);

                command.Parameters["@Unit_2"].Value = MarksTxt.Text;
                if (command.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Exam details saved successfull...");
                    Load_Grid();
                    ClearBoxes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        
        }
        private void bindgrid()
        {
            //try
            //{
            //    dt = new DataTable();
            //    dt.Columns.AddRange(new DataColumn[5]
            //{
            //new DataColumn("Id",typeof(string)),
            //new DataColumn("Name",typeof(string)),
            //new DataColumn("Standard",typeof(string)),
            //new DataColumn("StudentId",typeof(string)),
            //new DataColumn("Unit-II",typeof(string))
            ////new DataColumn("OMRP",typeof(string)),
            //new DataColumn("Profitamount",typeof(string)),
            //new DataColumn("Basicamount",typeof(string)),
            // new DataColumn("Vat",typeof(string)),
            //  new DataColumn("Vat.Amount",typeof(string)),
            //new DataColumn("Amount",typeof(string)),
            //new DataColumn("Date",typeof(string))
            //});
            //    dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
            //    dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
            //    this.dataGridView1.DataSource = dt;
            //    dataGridView1.Columns[0].Width = 100;
            //}
            //catch(Exception e)
            //{
            //    MessageBox.Show(e.Message);
            //}
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[index];
                IdTxt.Text = selectedRow.Cells[0].Value.ToString();
                NameTxt.Text = selectedRow.Cells[1].Value.ToString();
                StdTxt.Text = selectedRow.Cells[2].Value.ToString();
                MarksTxt.Text = selectedRow.Cells[4].Value.ToString();
                  // IdTxt.Text = dataGridView1.SelectedRows[0].Cells["Id"].Value + string.Empty;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UnitTestSecond_Load(object sender, EventArgs e)
        {
            
        }
    }
}
