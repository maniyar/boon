﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Net;

namespace Boon
{
    public partial class SendSms : Form
    {
        common c = new common();
        DataTable table = new DataTable();
        DataTable dt = new DataTable();
        MySqlConnection connection;

        public SendSms()
        {
            InitializeComponent();
            connection = new MySqlConnection(c.ConnectionString);
        }
        
        public void LoadYear()//YearBox
        {
            try
            {
                YearBox.Items.Clear();
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                //Cmd.CommandText =
                Cmd.CommandText = "select Distinct(AcdYear) from Boon.students ORDER BY AcdYear  ";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["AcdYear"].ToString();
                    YearBox.Items.Add(i);

                }
                dr.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }


        }

        private void SendSms_Load(object sender, EventArgs e)
        {
            LoadYear();

          
            try
            {
                AcdYearBox.Items.Clear();
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                //Cmd.CommandText =
                Cmd.CommandText = "select Distinct(AcdYear) from Boon.students ORDER BY AcdYear  ";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["AcdYear"].ToString();
                    AcdYearBox.Items.Add(i);

                }
                dr.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }
        }

        System.Net.WebClient client = new System.Net.WebClient();
        string username = "BOONSL",StudID;
        string pass = "Gateway@1";

       
        private Boolean SendResults()
        {
           
            try
            {
                int i = 0, j = 0;
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {
                    //    string message = MessageTxt.Text;
                        String message = "Dear Parents,                                    Today your child " + row.Cells[1].Value.ToString() + " is absent in school.Kindly inform the reason at school.";
                        var number = row.Cells[2].Value.ToString();
                        string no = ("+91" + number).ToString();
                        UriBuilder urlBuilder = new UriBuilder();
                        string createdURL = "http://mysms.gatewayinfotech.in/http-api.php?username=" + username + "&password=" + pass + "&senderid=BOONSL&route=2&number=" + no + "&message=" + message + "";
                        HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(createdURL, false));
                        httpReq.Timeout = 7200;
                        HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse());
                        ++i;
                        httpReq.Abort();
                    }  else
                    { ++j; }
                }
                if (i + j == dataGridView1.Rows.Count)
                {
                    MessageBox.Show("Message successfully sent ");
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                
            }
        }
        private void CheckBtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[3].Value = row.Cells[3].Value == null ? false : !(bool)row.Cells[3].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void ClasslistBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            try
            {
                if (ClassBox1.Text == "Nursery" || ClassBox1.Text == "Sr.KG" || ClassBox1.Text == "Jr.KG")
                {
                    String SelectQuery = "SELECT * from Boon.stud_p_primary WHERE Std = '" + ClassBox1.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' AND Contact > 0 ";

                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Contact"].ToString();

                    }
                }
                if (ClassBox1.Text == "1 st" || ClassBox1.Text == "2 nd" || ClassBox1.Text == "3 rd" || ClassBox1.Text == "4 th" || ClassBox1.Text == "5 th" || ClassBox1.Text == "6 th" || ClassBox1.Text == "7 th")
                {

                    String SelectQuery = "SELECT * from Boon.students WHERE Std = '" + ClassBox1.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' AND Contact > 0 ";

                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Contact"].ToString();

                    }

                }
                if (ClassBox1.Text == "8 th" || ClassBox1.Text == "9 th" || ClassBox1.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.students_high_school WHERE Std = '" + ClassBox1.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' AND Contact > 0";

                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Contact"].ToString();

                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }


        }
       // string message;
        int sec= 0;
        private Boolean sendsms()
        {          
            try
            {
                int i = 0, j = 0;
                foreach (DataGridViewRow row in this.dataGridView2.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check1"].Value) == true)
                    {
                        //    string message = MessageTxt.Text;
                        String message = MessageTxt.Text;
                        var number = row.Cells[2].Value.ToString();
                        string no = ("+91" + number).ToString();
                        UriBuilder urlBuilder = new UriBuilder();
                        string createdURL = "http://mysms.gatewayinfotech.in/http-api.php?username=" + username + "&password=" + pass + "&senderid=BOONSL&route=2&number=" + no + "&message=" + message + "";
                        HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(createdURL, false));
                        httpReq.Timeout = 7200;
                        HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse());
                        ++i;
                        httpReq.Abort();
                    }
                    else
                    { ++j; }
                }
                if (i + j == dataGridView2.Rows.Count)
                {
                    MessageBox.Show("Message successfully sent ");
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }
        private void SendBtn_Click(object sender, EventArgs e)
        {
            int i1 = 0;
            try
            {
                if (MessageBox.Show("Do you want to send sms?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in this.dataGridView2.Rows)
                    {

                        //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                        if (Convert.ToBoolean(row.Cells["Check1"].Value) == false)
                        {
                            ++i1;
                            if (i1 == dataGridView2.Rows.Count)
                            {
                                MessageBox.Show("You have to select atleat one student", "Warnning");
                                return;
                            }
                        }
                        else 
                        {
                            sendsms();
                            break;
                        }
                    }
                }
            
             

                   
            
    }

            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Alert");
            }
            
        }

        private void MessageTxt_TextChanged(object sender, EventArgs e)
        {
            if (MessageTxt.TextLength == 160)
            {
                MessageCount.Text = "-1 Message -";
            }
            if (MessageTxt.TextLength > 160)
            {
                MessageCount.Text = "-2 Message -";
            }
            if (MessageTxt.TextLength > 320)
            {
                MessageCount.Text = "-3 Message -";
            }
            if (MessageTxt.TextLength > 480)
            {
                MessageCount.Text = "-4 Message -";
            }
        }
        public void LoadAcdYear()
        {
            try
            {

                if (ClassBox.Text != "")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.stud_p_primary  WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        YearBox.Items.Add(i);

                    }
                    dr.Close();
                }
                else
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.stud_p_primary  WHERE Std = '" + ClassBox1.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        YearBox.Items.Add(i);

                    }
                    dr.Close();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }

        }
        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.stud_p_primary  WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        YearBox.Items.Add(i);

                    }
                    dr.Close();
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students  WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        YearBox.Items.Add(i);

                    }
                    dr.Close();
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students_high_school  WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        YearBox.Items.Add(i);

                    }
                    dr.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }

        }

        private void ClassBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ClassBox1.Text == "Nursery" || ClassBox1.Text == "Sr.KG" || ClassBox1.Text == "Jr.KG")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.stud_p_primary  WHERE Std = '" + ClassBox1.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        AcdYearBox.Items.Add(i);

                    }
                    dr.Close();
                }
                if (ClassBox1.Text == "1 st" || ClassBox1.Text == "2 nd" || ClassBox1.Text == "3 rd" || ClassBox1.Text == "4 th" || ClassBox1.Text == "5 th" || ClassBox1.Text == "6 th" || ClassBox1.Text == "7 th")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students  WHERE Std = '" + ClassBox1.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        AcdYearBox.Items.Add(i);

                    }
                    dr.Close();
                }
                if (ClassBox1.Text == "8 th" || ClassBox1.Text == "9 th" || ClassBox1.Text == "10 th")
                {
                    YearBox.Items.Clear();
                    AcdYearBox.Items.Clear();
                    // LoadTutionFee();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students_high_school  WHERE Std = '" + ClassBox1.Text + "' ORDER BY AcdYear";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        AcdYearBox.Items.Add(i);

                    }
                    dr.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }
        }
    
        private void YearBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();
            try
            {
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    String SelectQuery = "SELECT * from Boon.stud_p_primary WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + YearBox.Text + "' AND Contact > 0 Order By ID";

                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView2.Rows.Add();
                        dataGridView2.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView2.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView2.Rows[n].Cells[2].Value = item["Contact"].ToString();

                    }
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {

                    String SelectQuery = "SELECT * from Boon.students WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + YearBox.Text + "' AND Contact > 0 Order By ID ";

                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView2.Rows.Add();
                        dataGridView2.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView2.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView2.Rows[n].Cells[2].Value = item["Contact"].ToString();
                    }
                
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    String SelectQuery = "SELECT * from Boon.students_high_school WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + YearBox.Text + "' AND Contact > 0 Order By ID";

                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView2.Rows.Add();
                        dataGridView2.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView2.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView2.Rows[n].Cells[2].Value = item["Contact"].ToString();

                    }
                
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            
        }

        private void chkBtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView2.Rows)
                {
                    row.Cells[3].Value = row.Cells[3].Value == null ? false : !(bool)row.Cells[3].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SendBtn1_Click(object sender, EventArgs e)
        {
            int i1 = 0;
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                    {
                        i1++;
                        if (i1 == dataGridView1.Rows.Count)
                        {
                            MessageBox.Show("You have to select at least one student", "Alert");
                        }

                    }
                    else
                    {

                        if (MessageBox.Show("Do you want to send absent message?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            SendResults();
                           // PassValue();
                            break;
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Alert");
            }
        }

        private void myTimer_Tick(object sender, EventArgs e)
        {
           // myTimer. = sec.ToString();

                myTimer.Stop();
            
           
        }
    }
}
// username = "Boon";
//pass = "Gateway@1";
// int i = 0, j = 0;
// string message = MessageTxt.Text;
// try
// {
//     foreach (DataGridViewRow row in this.dataGridView2.Rows)
//     {
//         //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
//         if (Convert.ToBoolean(row.Cells["Check1"].Value) == true)
//         {

//             //  //  StudID = row.Cells[0].Value.ToString();
//             var number = row.Cells[2].Value.ToString();
//             string no = ("+91" + number).ToString();

//             UriBuilder urlBuilder = new UriBuilder();
//             string createdURL = "http://mysms.gatewayinfotech.in/http-api.php?username=" + username + "&password=" + pass + "&senderid=BOONSL&route=2&number=" + no + "&message=" + message + "";
//             HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(createdURL, false));
//             httpReq.Timeout = 7200;
//             HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse());

//             ++i;
//              httpReq.Abort();


//         }
//         else
//         { ++j; }


//     }

//     if (i + j == dataGridView2.Rows.Count)
//     {
//         MessageBox.Show("Successfully sent message");

//     }
//     return true;

// }
// catch (Exception ex)
// {
//    // ++j;

//     MessageBox.Show(ex.Message,"Alert");
//     return false;
// }
// finally
// {


// }