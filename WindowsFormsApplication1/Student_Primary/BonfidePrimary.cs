﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using MySql.Data.MySqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Configuration;


namespace Boon
{
    public partial class BonfidePrimary : Form
    {
        DataSet ds;
        MySqlConnection mysql;
        common c = new common();
        ErrorProvider errorProvider1 = new ErrorProvider();
        DataTable Dt = new DataTable();

        public BonfidePrimary()
        {
            mysql = new MySqlConnection();
            mysql.ConnectionString = c.ConnectionString;
           // mysql.Open();
            //StudentName();
            InitializeComponent();
        }
        private void GenrateBonfide()
        {
            try
            {
                MySqlCommand Cmd = new MySqlCommand();
                MySqlDataAdapter da = new MySqlDataAdapter(Cmd);

                DataTable table = new DataTable();


                Cmd.CommandText = "SELECT ID,Name,Std,B_Place,DOB,Profile from Boon.students WHERE Name = '" + NameTxt.Text + "' AND Std = '" + StdBox.Text + "'";

                Cmd.Connection = mysql;
                ds = new DataSet();
                MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                mda.Fill(ds, "student");
                PreBonafideReport cr = new PreBonafideReport();
                cr.SetDataSource(ds);
                crystalReportViewer1.ReportSource = cr;
            }
            catch { }
        }
        public void LoadAcdYear()
        {
            try
            {
                AcdYearBox.Items.Clear();
                mysql.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = mysql;
                //Cmd.CommandText =
                Cmd.CommandText = "select Distinct(AcdYear) from Boon.students where Std = '" + StdBox.Text + "' order by  AcdYear ";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["AcdYear"].ToString();
                    AcdYearBox.Items.Add(i);

                }
                dr.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                mysql.Close();
            }
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
          
        }

        private void BonfidePre_Load(object sender, EventArgs e)
        {
          //  StudentName();
            //try
            //{
            //    AcdYearBox.Items.Clear();
            //    mysql.Open();
            //    MySqlCommand Cmd = new MySqlCommand();
            //    Cmd.Connection = mysql;
            //    //Cmd.CommandText =
            //    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students ORDER BY AcdYear  ";
            //    MySqlDataReader dr = Cmd.ExecuteReader();
            //    while (dr.Read())
            //    {
            //        common i = new common();
            //        i.Text = dr["AcdYear"].ToString();
            //        AcdYearBox.Items.Add(i);

            //    }
            //    dr.Close();
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Alert");
            //}
            //finally
            //{
            //    mysql.Close();
            //}
            
        }

        private void StudentID()
        {
            //comboBox1.Format.coustomSource();
            try
            {
                IdTxtBox.Items.Clear();
                mysql.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = mysql;
                //Cmd.CommandText =
                Cmd.CommandText = "select ID from Boon.students Where Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["ID"].ToString();
                    IdTxtBox.Items.Add(i);

                }
                dr.Close();
            }
            catch
            {

            }
            mysql.Close();
        }


        private void StudentName()
        {
            try
            {
                mysql.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = mysql;
                Cmd.CommandText = "select Name from Boon.students Where Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                MySqlDataReader dr1 = Cmd.ExecuteReader();
                AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                while (dr1.Read())
                {
                    data.Add(dr1.GetString(0));

                }
                NameTxt.AutoCompleteCustomSource = data;
                dr1.Close();
            }
            catch { }
            finally
            {
                mysql.Close();
            }
        }
        

        private void NameTxt_TextChanged(object sender, EventArgs e)
        {
            IdTxtBox.Text = null;
            try
            {

         
            if (StdBox.Text == "" || AcdYearBox.Text == "")
            {
               
                NameTxt.Text = null;
                errorProvider1.SetError(AcdYearBox, "Select Acd.Year of student first ");
                errorProvider1.SetError(StdBox, "Please select standard of student ");
              

                return;
            }


            //String SelectQuery = "select ID from Boon.students where Name = '" + NameTxt.Text + "' AND Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'  ";
            //    DataTable table = c.SelectData(SelectQuery);
            //    if (table.Rows.Count > 0)
            //    {
            //        IdTxtBox.Text = table.Rows[0][0].ToString();
            //    }
            //    else
            //    {
            //        IdTxtBox.Text = null;
            //       // return;
            //    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //if (AcdYearBox.Text == "")
            //{
            //    MessageBox.Show("Please select Acd.Year of student","Warnning");
            //    AcdYearBox.Focus();
            //    NameTxt.Text = null;
                

            //}
                
        }

        String Name, Std, B_Place, DOB , y ,m ,d,date,sysdate,Tq,Dist,State;
        int  ID;


        public void LoadInfo()
        {
            try
            {
          String InsertQuery = "Insert into Boon.bonafied (ID,Name,Std,B_Place,DOB,AcdYear,Tq,Dist,State) values ('" + ID + "','" + Name + "','" + Std + "','" + B_Place + "','" + date + "','" + sysdate + "','"+Tq+"','"+Dist+"','"+State+"')";
                
                    if (c.InsertData(InsertQuery))
                    {

                        MySqlCommand Cmd = new MySqlCommand();

                        MySqlDataAdapter da = new MySqlDataAdapter(Cmd);

                        DataTable table = new DataTable();

                        Cmd.CommandText = "SELECT * from Boon.bonafied order by BonNo Desc LIMIT 1";

                        Cmd.Connection = mysql;
                        ds = new DataSet();
                        MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                        mda.Fill(ds, "bonafied");
                        PrimaryBonafideReport cr = new PrimaryBonafideReport();
                       // PreBonafideReport cr = new PreBonafideReport();
                        cr.SetDataSource(ds);
                        crystalReportViewer1.ReportSource = cr;



                    }
            }
            catch{}

                    
        }

        private void GenrateBonfideBtn_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (NameTxt.Text == "" )
                {

                    String SelectQuery = "SELECT ID,Name,Std,B_Place,DOB,AcdYear,Tq,Dist,State from Boon.students WHERE  ID ='" + IdTxtBox.Text + "'  ";
                    DataTable dt = new DataTable();
                    dt = c.SelectData(SelectQuery);
                    if (dt.Rows.Count > 0)
                    {
                        // Name = (NameTxt.Text).ToString();
                        var id = dt.Rows[0][0].ToString();
                        ID = int.Parse(id);
                        Name = dt.Rows[0][1].ToString();
                        Std = dt.Rows[0][2].ToString();
                        B_Place = dt.Rows[0][3].ToString();
                        DOB = dt.Rows[0][4].ToString();
                        y = DOB.Substring(0, 4);
                        m = DOB.Substring(5, 2);
                        d = DOB.Substring(8, 2);
                        date = d + "-" + m + "-" + y;


                        sysdate = dt.Rows[0][5].ToString();

                        Tq= dt.Rows[0][6].ToString();
                        Dist = dt.Rows[0][7].ToString();
                        State = dt.Rows[0][8].ToString();

                        LoadInfo();



                    }
                    else
                    {
                        MessageBox.Show("No record is found", "Warnnings");

                    }
                }
               else //if (IdTxtBox.Text == "")
                {

                    String SelectQuery = "SELECT ID,Name,Std,B_Place,DOB,AcdYear,Tq,Dist,State from Boon.students WHERE  Name ='" + NameTxt.Text + "' AND Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'  ";
                    DataTable dt = new DataTable();
                    dt = c.SelectData(SelectQuery);
                    if (dt.Rows.Count > 0)
                    {
                        // Name = (NameTxt.Text).ToString();
                        var id = dt.Rows[0][0].ToString();
                        ID = int.Parse(id);
                        Name = dt.Rows[0][1].ToString();
                        Std = dt.Rows[0][2].ToString();
                        B_Place = dt.Rows[0][3].ToString();
                        DOB = dt.Rows[0][4].ToString();
                        y = DOB.Substring(0, 4);
                        m = DOB.Substring(5, 2);
                        d = DOB.Substring(8, 2);
                        date = d + "-" + m + "-" + y;
                        Tq = dt.Rows[0][6].ToString();
                        Dist = dt.Rows[0][7].ToString();
                        State = dt.Rows[0][8].ToString();

                        sysdate = dt.Rows[0][5].ToString();



                        LoadInfo();



                    }
                    else
                    {
                        MessageBox.Show("No record found", "Warnnings");

                    }
                }


            }
            catch { }
        }


        private void StdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAcdYear();
            //errorProvider1.Clear();
            //if (AcdYearBox.Text == "")
            //{
                
            //  //  errorProvider1.SetError(AcdYearBox, "Select Acd.Year of student first ");
            //}
            //else
            //{
            //    errorProvider1.Clear();
            //    StudentName();
            //}
        }

        private void IdTxt_TextChanged(object sender, EventArgs e)
        {
            NameTxt.Text = null;
        }

        private void IdTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter numbers only", "Warnning");
                e.Handled = true;
            }
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (StdBox.Text == "")
            {

                errorProvider1.SetError(StdBox, "Select Std. of student first ");
            }
            else
            {
                StudentID();
                StudentName();
            }
        }

        private void NameTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter letters only", "Warnning");
                e.Handled = true;
            }
        }

        private void IdTxtBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            try
            {
                NameTxt.Text = null;
                //String SelectQuery = "select Name from Boon.students where ID = '" + IdTxtBox.Text + "' AND Std = '" + StdBox.Text + "' ";
                //Dt = c.SelectData(SelectQuery);
                //if (Dt.Rows.Count > 0)
                //{
                //NameTxt.Text = Dt.Rows[0][0].ToString();
                ////  NameTxt1.Text = Dt.Rows[0][0].ToString();
                //}


            }
            catch (Exception ex)
            {
               // return;
                MessageBox.Show(ex.Message);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void BonfidePrimary_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Control && e.KeyCode == Keys.P)
            {
                crystalReportViewer1.PrintReport();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void crystalReportViewer1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Control && e.KeyCode == Keys.P)
            {
                crystalReportViewer1.PrintReport();
            }
        }

        private void NameTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Control && e.KeyCode == Keys.P)
            {
                crystalReportViewer1.PrintReport();
            }
        }
    }
}
