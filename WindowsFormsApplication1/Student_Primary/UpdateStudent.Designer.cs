﻿namespace Boon
{
    partial class UpdateStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateStudent));
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.SearchTxt1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.MotherTongueTxt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.PreClassBox = new System.Windows.Forms.ComboBox();
            this.FatherTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IdTxt = new System.Windows.Forms.TextBox();
            this.CategoryBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.SubCastTxt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CastBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.StudentIDTxt = new System.Windows.Forms.TextBox();
            this.MotherTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DateTime1 = new System.Windows.Forms.DateTimePicker();
            this.PlaceTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.FemaleRbtn = new System.Windows.Forms.RadioButton();
            this.MaleRbtn = new System.Windows.Forms.RadioButton();
            this.PriviousTxt = new System.Windows.Forms.TextBox();
            this.AadharTxt = new System.Windows.Forms.TextBox();
            this.ContactTxt = new System.Windows.Forms.TextBox();
            this.AddressTxt = new System.Windows.Forms.TextBox();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.FirstClassBox = new System.Windows.Forms.ComboBox();
            this.DateTime2 = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CountryTxt = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.DistricTxt = new System.Windows.Forms.TextBox();
            this.TqTxt = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.StateBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ReligionTxt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.NationalityBox = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.GetListBtn = new System.Windows.Forms.Button();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.UploadBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(428, 759);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 32);
            this.button2.TabIndex = 29;
            this.button2.Text = "clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(268, 760);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 32);
            this.button1.TabIndex = 28;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1488, 10);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 18);
            this.label17.TabIndex = 70;
            this.label17.Text = "Search  :";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(1009, 66);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(300, 611);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            this.dataGridView2.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentDoubleClick);
            // 
            // SearchTxt1
            // 
            this.SearchTxt1.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTxt1.Location = new System.Drawing.Point(116, 6);
            this.SearchTxt1.Margin = new System.Windows.Forms.Padding(4);
            this.SearchTxt1.Name = "SearchTxt1";
            this.SearchTxt1.Size = new System.Drawing.Size(179, 28);
            this.SearchTxt1.TabIndex = 0;
            this.SearchTxt1.TextChanged += new System.EventHandler(this.SearchTxt1_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label20.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(1017, 24);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(104, 18);
            this.label20.TabIndex = 78;
            this.label20.Text = "Search Student :";
            // 
            // MotherTongueTxt
            // 
            this.MotherTongueTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTongueTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTongueTxt.Location = new System.Drawing.Point(491, 226);
            this.MotherTongueTxt.Name = "MotherTongueTxt";
            this.MotherTongueTxt.Size = new System.Drawing.Size(224, 29);
            this.MotherTongueTxt.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(362, 230);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(123, 22);
            this.label24.TabIndex = 119;
            this.label24.Text = "Mother Tongue :";
            // 
            // PreClassBox
            // 
            this.PreClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.PreClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PreClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreClassBox.FormattingEnabled = true;
            this.PreClassBox.Items.AddRange(new object[] {
            "Sr.KG",
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th"});
            this.PreClassBox.Location = new System.Drawing.Point(166, 572);
            this.PreClassBox.Name = "PreClassBox";
            this.PreClassBox.Size = new System.Drawing.Size(137, 30);
            this.PreClassBox.TabIndex = 24;
            // 
            // FatherTxt
            // 
            this.FatherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.FatherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FatherTxt.Location = new System.Drawing.Point(491, 152);
            this.FatherTxt.Name = "FatherTxt";
            this.FatherTxt.Size = new System.Drawing.Size(224, 29);
            this.FatherTxt.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(371, 155);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(117, 22);
            this.label19.TabIndex = 115;
            this.label19.Text = "Father\'s Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(14, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 22);
            this.label2.TabIndex = 113;
            this.label2.Text = "Gen.Reg.No.  :";
            // 
            // IdTxt
            // 
            this.IdTxt.BackColor = System.Drawing.SystemColors.Window;
            this.IdTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdTxt.Location = new System.Drawing.Point(166, 62);
            this.IdTxt.Name = "IdTxt";
            this.IdTxt.Size = new System.Drawing.Size(176, 29);
            this.IdTxt.TabIndex = 2;
            // 
            // CategoryBox
            // 
            this.CategoryBox.BackColor = System.Drawing.SystemColors.Window;
            this.CategoryBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategoryBox.FormattingEnabled = true;
            this.CategoryBox.Items.AddRange(new object[] {
            "Open",
            "OBC",
            "NT",
            "NT3",
            "SC",
            "ST",
            "VJNT",
            "Other"});
            this.CategoryBox.Location = new System.Drawing.Point(580, 399);
            this.CategoryBox.Name = "CategoryBox";
            this.CategoryBox.Size = new System.Drawing.Size(135, 30);
            this.CategoryBox.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(496, 400);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 22);
            this.label16.TabIndex = 112;
            this.label16.Text = "Category :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SubCastTxt
            // 
            this.SubCastTxt.BackColor = System.Drawing.SystemColors.Window;
            this.SubCastTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubCastTxt.Location = new System.Drawing.Point(580, 355);
            this.SubCastTxt.Name = "SubCastTxt";
            this.SubCastTxt.Size = new System.Drawing.Size(135, 29);
            this.SubCastTxt.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(491, 358);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 22);
            this.label14.TabIndex = 111;
            this.label14.Text = "Sub Caste :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CastBox
            // 
            this.CastBox.BackColor = System.Drawing.SystemColors.Window;
            this.CastBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CastBox.FormattingEnabled = true;
            this.CastBox.Items.AddRange(new object[] {
            "Muslim",
            "Crishshan",
            "Hindu",
            "Sikh",
            "Buddh",
            "Jain",
            "Other"});
            this.CastBox.Location = new System.Drawing.Point(166, 355);
            this.CastBox.Name = "CastBox";
            this.CastBox.Size = new System.Drawing.Size(137, 30);
            this.CastBox.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(14, 358);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 22);
            this.label13.TabIndex = 110;
            this.label13.Text = "Caste :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StudentIDTxt
            // 
            this.StudentIDTxt.BackColor = System.Drawing.SystemColors.Window;
            this.StudentIDTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentIDTxt.Location = new System.Drawing.Point(491, 572);
            this.StudentIDTxt.Name = "StudentIDTxt";
            this.StudentIDTxt.Size = new System.Drawing.Size(224, 29);
            this.StudentIDTxt.TabIndex = 25;
            // 
            // MotherTxt
            // 
            this.MotherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTxt.Location = new System.Drawing.Point(166, 152);
            this.MotherTxt.Name = "MotherTxt";
            this.MotherTxt.Size = new System.Drawing.Size(176, 29);
            this.MotherTxt.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(14, 155);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 22);
            this.label11.TabIndex = 109;
            this.label11.Text = "Mother\'s Name :";
            // 
            // DateTime1
            // 
            this.DateTime1.CalendarMonthBackground = System.Drawing.Color.NavajoWhite;
            this.DateTime1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime1.Location = new System.Drawing.Point(166, 225);
            this.DateTime1.Name = "DateTime1";
            this.DateTime1.Size = new System.Drawing.Size(137, 29);
            this.DateTime1.TabIndex = 8;
            this.DateTime1.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            // 
            // PlaceTxt
            // 
            this.PlaceTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PlaceTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceTxt.Location = new System.Drawing.Point(166, 268);
            this.PlaceTxt.Name = "PlaceTxt";
            this.PlaceTxt.Size = new System.Drawing.Size(137, 29);
            this.PlaceTxt.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(14, 271);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 22);
            this.label8.TabIndex = 108;
            this.label8.Text = "Place of birth";
            // 
            // FemaleRbtn
            // 
            this.FemaleRbtn.AutoSize = true;
            this.FemaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FemaleRbtn.Location = new System.Drawing.Point(263, 191);
            this.FemaleRbtn.Name = "FemaleRbtn";
            this.FemaleRbtn.Size = new System.Drawing.Size(77, 26);
            this.FemaleRbtn.TabIndex = 7;
            this.FemaleRbtn.Text = "Female";
            this.FemaleRbtn.UseVisualStyleBackColor = true;
            this.FemaleRbtn.CheckedChanged += new System.EventHandler(this.FemaleRbtn_CheckedChanged_1);
            // 
            // MaleRbtn
            // 
            this.MaleRbtn.AutoSize = true;
            this.MaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaleRbtn.Location = new System.Drawing.Point(166, 190);
            this.MaleRbtn.Name = "MaleRbtn";
            this.MaleRbtn.Size = new System.Drawing.Size(62, 26);
            this.MaleRbtn.TabIndex = 6;
            this.MaleRbtn.Text = "Male";
            this.MaleRbtn.UseVisualStyleBackColor = true;
            this.MaleRbtn.CheckedChanged += new System.EventHandler(this.MaleRbtn_CheckedChanged_1);
            // 
            // PriviousTxt
            // 
            this.PriviousTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PriviousTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PriviousTxt.Location = new System.Drawing.Point(166, 529);
            this.PriviousTxt.Name = "PriviousTxt";
            this.PriviousTxt.Size = new System.Drawing.Size(548, 29);
            this.PriviousTxt.TabIndex = 23;
            // 
            // AadharTxt
            // 
            this.AadharTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AadharTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AadharTxt.Location = new System.Drawing.Point(166, 443);
            this.AadharTxt.Name = "AadharTxt";
            this.AadharTxt.Size = new System.Drawing.Size(176, 29);
            this.AadharTxt.TabIndex = 20;
            this.AadharTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AadharTxt_KeyPress);
            // 
            // ContactTxt
            // 
            this.ContactTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ContactTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ContactTxt.Location = new System.Drawing.Point(491, 443);
            this.ContactTxt.Name = "ContactTxt";
            this.ContactTxt.Size = new System.Drawing.Size(224, 29);
            this.ContactTxt.TabIndex = 21;
            this.ContactTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ContactTxt_KeyPress);
            // 
            // AddressTxt
            // 
            this.AddressTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AddressTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressTxt.Location = new System.Drawing.Point(166, 486);
            this.AddressTxt.Name = "AddressTxt";
            this.AddressTxt.Size = new System.Drawing.Size(548, 29);
            this.AddressTxt.TabIndex = 22;
            // 
            // NameTxt
            // 
            this.NameTxt.BackColor = System.Drawing.SystemColors.Window;
            this.NameTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTxt.Location = new System.Drawing.Point(166, 108);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(549, 29);
            this.NameTxt.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(14, 532);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 22);
            this.label7.TabIndex = 91;
            this.label7.Text = "Previous school :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(14, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 22);
            this.label6.TabIndex = 89;
            this.label6.Text = "Gender :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(14, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 22);
            this.label5.TabIndex = 87;
            this.label5.Text = "Date of Birth :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(14, 448);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 22);
            this.label4.TabIndex = 85;
            this.label4.Text = "Aadhar No. :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(371, 448);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 22);
            this.label3.TabIndex = 83;
            this.label3.Text = "Contact No. :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(14, 489);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 22);
            this.label10.TabIndex = 82;
            this.label10.Text = "Address :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(14, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 22);
            this.label12.TabIndex = 79;
            this.label12.Text = "Name :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(14, 576);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 22);
            this.label23.TabIndex = 124;
            this.label23.Text = "Previous Std. :";
            // 
            // FirstClassBox
            // 
            this.FirstClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.FirstClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FirstClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstClassBox.FormattingEnabled = true;
            this.FirstClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th"});
            this.FirstClassBox.Location = new System.Drawing.Point(166, 616);
            this.FirstClassBox.Name = "FirstClassBox";
            this.FirstClassBox.Size = new System.Drawing.Size(137, 30);
            this.FirstClassBox.TabIndex = 26;
            // 
            // DateTime2
            // 
            this.DateTime2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime2.Location = new System.Drawing.Point(491, 616);
            this.DateTime2.Name = "DateTime2";
            this.DateTime2.Size = new System.Drawing.Size(224, 29);
            this.DateTime2.TabIndex = 27;
            this.DateTime2.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(341, 616);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(141, 22);
            this.label15.TabIndex = 123;
            this.label15.Text = "Date of Admission :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(14, 619);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(146, 22);
            this.label25.TabIndex = 120;
            this.label25.Text = "First Admitted Std. :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label26.Location = new System.Drawing.Point(371, 576);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(92, 22);
            this.label26.TabIndex = 125;
            this.label26.Text = "Student ID :";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.CountryTxt);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.DistricTxt);
            this.panel1.Controls.Add(this.TqTxt);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.IdTxt);
            this.panel1.Controls.Add(this.StateBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ReligionTxt);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.NationalityBox);
            this.panel1.Controls.Add(this.FirstClassBox);
            this.panel1.Controls.Add(this.NameTxt);
            this.panel1.Controls.Add(this.MotherTxt);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.DateTime2);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.FatherTxt);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.FemaleRbtn);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.PreClassBox);
            this.panel1.Controls.Add(this.MotherTongueTxt);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.StudentIDTxt);
            this.panel1.Controls.Add(this.CategoryBox);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.PriviousTxt);
            this.panel1.Controls.Add(this.AddressTxt);
            this.panel1.Controls.Add(this.AadharTxt);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.SubCastTxt);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.ContactTxt);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.MaleRbtn);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.CastBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.DateTime1);
            this.panel1.Controls.Add(this.PlaceTxt);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(51, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(727, 665);
            this.panel1.TabIndex = 0;
            // 
            // CountryTxt
            // 
            this.CountryTxt.BackColor = System.Drawing.SystemColors.Window;
            this.CountryTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CountryTxt.FormattingEnabled = true;
            this.CountryTxt.Items.AddRange(new object[] {
            "India"});
            this.CountryTxt.Location = new System.Drawing.Point(166, 399);
            this.CountryTxt.Name = "CountryTxt";
            this.CountryTxt.Size = new System.Drawing.Size(137, 30);
            this.CountryTxt.TabIndex = 18;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label27.Location = new System.Drawing.Point(528, 271);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 22);
            this.label27.TabIndex = 150;
            this.label27.Text = "Dist. :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label28.Location = new System.Drawing.Point(321, 271);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 22);
            this.label28.TabIndex = 149;
            this.label28.Text = "Tq :";
            // 
            // DistricTxt
            // 
            this.DistricTxt.BackColor = System.Drawing.SystemColors.Window;
            this.DistricTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DistricTxt.Location = new System.Drawing.Point(580, 268);
            this.DistricTxt.Name = "DistricTxt";
            this.DistricTxt.Size = new System.Drawing.Size(135, 29);
            this.DistricTxt.TabIndex = 12;
            // 
            // TqTxt
            // 
            this.TqTxt.BackColor = System.Drawing.SystemColors.Window;
            this.TqTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TqTxt.Location = new System.Drawing.Point(362, 268);
            this.TqTxt.Name = "TqTxt";
            this.TqTxt.Size = new System.Drawing.Size(135, 29);
            this.TqTxt.TabIndex = 11;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label29.Location = new System.Drawing.Point(14, 405);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(75, 22);
            this.label29.TabIndex = 148;
            this.label29.Text = "Country :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(307, 319);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 22);
            this.label22.TabIndex = 144;
            this.label22.Text = "State :";
            // 
            // StateBox
            // 
            this.StateBox.BackColor = System.Drawing.SystemColors.Window;
            this.StateBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StateBox.FormattingEnabled = true;
            this.StateBox.Items.AddRange(new object[] {
            "Andhra Pradesh",
            "Arunachal Pradesh",
            "Assam",
            "Bihar",
            "Chhattisgarh",
            "Goa",
            "Gujarat",
            "Haryana",
            "Himachal Pradesh",
            "Jammu & Kashmir",
            "Jharkhand",
            "Karnataka",
            "Kerala",
            "Madhya Pradesh",
            "Maharashtra",
            "Manipur",
            "Meghalaya",
            "Mizoram",
            "Nagaland",
            "Odisha",
            "Punjab",
            "Rajasthan",
            "Sikkim",
            "Tamil Nadu",
            "Telangana",
            "Tripura",
            "Uttarakhand",
            "Uttar Pradesh",
            "West Bengal"});
            this.StateBox.Location = new System.Drawing.Point(360, 311);
            this.StateBox.Name = "StateBox";
            this.StateBox.Size = new System.Drawing.Size(137, 30);
            this.StateBox.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(504, 319);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 22);
            this.label1.TabIndex = 142;
            this.label1.Text = "Religion :";
            // 
            // ReligionTxt
            // 
            this.ReligionTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ReligionTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReligionTxt.Location = new System.Drawing.Point(580, 311);
            this.ReligionTxt.Name = "ReligionTxt";
            this.ReligionTxt.Size = new System.Drawing.Size(135, 29);
            this.ReligionTxt.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(14, 319);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 22);
            this.label18.TabIndex = 141;
            this.label18.Text = "Nationality :";
            // 
            // NationalityBox
            // 
            this.NationalityBox.BackColor = System.Drawing.SystemColors.Window;
            this.NationalityBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NationalityBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NationalityBox.FormattingEnabled = true;
            this.NationalityBox.Items.AddRange(new object[] {
            "Indian",
            "Other"});
            this.NationalityBox.Location = new System.Drawing.Point(166, 311);
            this.NationalityBox.Name = "NationalityBox";
            this.NationalityBox.Size = new System.Drawing.Size(137, 30);
            this.NationalityBox.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.GetListBtn);
            this.panel3.Controls.Add(this.ClassBox);
            this.panel3.Controls.Add(this.AcdYearBox);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Location = new System.Drawing.Point(9, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(707, 51);
            this.panel3.TabIndex = 130;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(20, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 22);
            this.label21.TabIndex = 133;
            this.label21.Text = "Select Std :";
            // 
            // GetListBtn
            // 
            this.GetListBtn.BackColor = System.Drawing.Color.Lime;
            this.GetListBtn.Location = new System.Drawing.Point(603, 10);
            this.GetListBtn.Margin = new System.Windows.Forms.Padding(4);
            this.GetListBtn.Name = "GetListBtn";
            this.GetListBtn.Size = new System.Drawing.Size(82, 28);
            this.GetListBtn.TabIndex = 132;
            this.GetListBtn.Text = "Get List";
            this.GetListBtn.UseVisualStyleBackColor = false;
            this.GetListBtn.Click += new System.EventHandler(this.GetListBtn_Click);
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th"});
            this.ClassBox.Location = new System.Drawing.Point(125, 9);
            this.ClassBox.Margin = new System.Windows.Forms.Padding(4);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(175, 30);
            this.ClassBox.TabIndex = 131;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.AcdYearBox.BackColor = System.Drawing.SystemColors.Window;
            this.AcdYearBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Location = new System.Drawing.Point(422, 9);
            this.AcdYearBox.Margin = new System.Windows.Forms.Padding(4);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(160, 30);
            this.AcdYearBox.TabIndex = 130;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(321, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 22);
            this.label9.TabIndex = 134;
            this.label9.Text = "Acd.Year :";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(794, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 261);
            this.panel2.TabIndex = 127;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.SearchTxt1);
            this.panel4.Location = new System.Drawing.Point(1008, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(301, 41);
            this.panel4.TabIndex = 128;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.DeleteBtn);
            this.panel5.Controls.Add(this.UpdateBtn);
            this.panel5.Controls.Add(this.ClearBtn);
            this.panel5.Location = new System.Drawing.Point(784, 547);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(218, 130);
            this.panel5.TabIndex = 129;
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.DeleteBtn.Image = global::Boon.Properties.Resources.Delete_Icon1;
            this.DeleteBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.DeleteBtn.Location = new System.Drawing.Point(7, 91);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(202, 34);
            this.DeleteBtn.TabIndex = 2;
            this.DeleteBtn.Text = "Delete Student";
            this.DeleteBtn.UseVisualStyleBackColor = false;
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.UpdateBtn.Image = global::Boon.Properties.Resources.save;
            this.UpdateBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.UpdateBtn.Location = new System.Drawing.Point(7, 53);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(200, 32);
            this.UpdateBtn.TabIndex = 1;
            this.UpdateBtn.Text = "Update student";
            this.UpdateBtn.UseVisualStyleBackColor = false;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click_1);
            // 
            // ClearBtn
            // 
            this.ClearBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.ClearBtn.Image = global::Boon.Properties.Resources._1024px_Edit_clear_svg;
            this.ClearBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ClearBtn.Location = new System.Drawing.Point(7, 14);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(200, 33);
            this.ClearBtn.TabIndex = 0;
            this.ClearBtn.Text = "Clear Text Boxes";
            this.ClearBtn.UseVisualStyleBackColor = false;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.BackgroundImage = global::Boon.Properties.Resources.user_male_512;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(805, 32);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 176);
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // UploadBtn
            // 
            this.UploadBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.UploadBtn.Image = ((System.Drawing.Image)(resources.GetObject("UploadBtn.Image")));
            this.UploadBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.UploadBtn.Location = new System.Drawing.Point(805, 220);
            this.UploadBtn.Margin = new System.Windows.Forms.Padding(4);
            this.UploadBtn.Name = "UploadBtn";
            this.UploadBtn.Size = new System.Drawing.Size(177, 37);
            this.UploadBtn.TabIndex = 1;
            this.UploadBtn.Text = "Browse Image";
            this.UploadBtn.UseVisualStyleBackColor = false;
            this.UploadBtn.Click += new System.EventHandler(this.OpenImg_Click);
            // 
            // UpdateStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.UploadBtn);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UpdateStudent";
            this.Text = "Update Primary Student";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UpdateStudent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

    

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button UploadBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.TextBox SearchTxt1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox MotherTongueTxt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox PreClassBox;
        private System.Windows.Forms.TextBox FatherTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IdTxt;
        private System.Windows.Forms.ComboBox CategoryBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox SubCastTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox CastBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox StudentIDTxt;
        private System.Windows.Forms.TextBox MotherTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker DateTime1;
        private System.Windows.Forms.TextBox PlaceTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton FemaleRbtn;
        private System.Windows.Forms.RadioButton MaleRbtn;
        private System.Windows.Forms.TextBox PriviousTxt;
        private System.Windows.Forms.TextBox AadharTxt;
        private System.Windows.Forms.TextBox ContactTxt;
        private System.Windows.Forms.TextBox AddressTxt;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox FirstClassBox;
        private System.Windows.Forms.DateTimePicker DateTime2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox StateBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ReligionTxt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox NationalityBox;
        private System.Windows.Forms.ComboBox CountryTxt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox DistricTxt;
        private System.Windows.Forms.TextBox TqTxt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button GetListBtn;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.Label label9;


      
    }
}