﻿namespace Boon
{
    partial class Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Student));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.school = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.Nametxt1 = new System.Windows.Forms.TextBox();
            this.Addresstxt2 = new System.Windows.Forms.TextBox();
            this.Contacttxt3 = new System.Windows.Forms.TextBox();
            this.Aadhartxt4 = new System.Windows.Forms.TextBox();
            this.Privioustxt5 = new System.Windows.Forms.TextBox();
            this.MaleRbtn = new System.Windows.Forms.RadioButton();
            this.FemaleRbtn = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.UploadBtn = new System.Windows.Forms.Button();
            this.PlaceTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DateTime1 = new System.Windows.Forms.DateTimePicker();
            this.MotherTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.StudentIDTxt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CastBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.SubCastTxt = new System.Windows.Forms.TextBox();
            this.DateTime2 = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.CategoryBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.IDTxt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.NationalityBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.FatherTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.FirstClassBox = new System.Windows.Forms.ComboBox();
            this.ReligionTxt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TqTxt = new System.Windows.Forms.TextBox();
            this.DistricTxt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.PreClassBox = new System.Windows.Forms.ComboBox();
            this.std = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.MotherTongueTxt = new System.Windows.Forms.TextBox();
            this.OldGenTxt = new System.Windows.Forms.TextBox();
            this.reg = new System.Windows.Forms.Label();
            this.DateTime3 = new System.Windows.Forms.DateTimePicker();
            this.tc = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.OldTcTxt = new System.Windows.Forms.TextBox();
            this.SchoolCheked = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.AcdYearTxt = new System.Windows.Forms.TextBox();
            this.Yeslabel = new System.Windows.Forms.Label();
            this.Nolabel = new System.Windows.Forms.Label();
            this.NoCheckBox = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.StateBox = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.CountryTxt = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(211, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(214, 410);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Address :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(655, 362);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cont. No. :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(214, 362);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "Aadhar No. :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(211, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "Date of Birth :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(211, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 22);
            this.label6.TabIndex = 5;
            this.label6.Text = "Gender :";
            // 
            // school
            // 
            this.school.AutoSize = true;
            this.school.BackColor = System.Drawing.Color.Transparent;
            this.school.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.school.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.school.Location = new System.Drawing.Point(56, 28);
            this.school.Name = "school";
            this.school.Size = new System.Drawing.Size(63, 22);
            this.school.TabIndex = 6;
            this.school.Text = "School :";
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.SaveBtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Location = new System.Drawing.Point(7, 10);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(94, 34);
            this.SaveBtn.TabIndex = 0;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            this.SaveBtn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SaveBtn_KeyPress);
            // 
            // ClearBtn
            // 
            this.ClearBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.ClearBtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBtn.Location = new System.Drawing.Point(107, 10);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(94, 34);
            this.ClearBtn.TabIndex = 1;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = false;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // Nametxt1
            // 
            this.Nametxt1.BackColor = System.Drawing.SystemColors.Window;
            this.Nametxt1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nametxt1.Location = new System.Drawing.Point(335, 56);
            this.Nametxt1.Name = "Nametxt1";
            this.Nametxt1.Size = new System.Drawing.Size(618, 29);
            this.Nametxt1.TabIndex = 2;
            this.Nametxt1.TextChanged += new System.EventHandler(this.Nametxt1_TextChanged);
            this.Nametxt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nametxt1_KeyPress);
            // 
            // Addresstxt2
            // 
            this.Addresstxt2.BackColor = System.Drawing.SystemColors.Window;
            this.Addresstxt2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addresstxt2.Location = new System.Drawing.Point(335, 404);
            this.Addresstxt2.Name = "Addresstxt2";
            this.Addresstxt2.Size = new System.Drawing.Size(618, 29);
            this.Addresstxt2.TabIndex = 21;
            this.Addresstxt2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Addresstxt2_KeyPress);
            // 
            // Contacttxt3
            // 
            this.Contacttxt3.BackColor = System.Drawing.SystemColors.Window;
            this.Contacttxt3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contacttxt3.Location = new System.Drawing.Point(749, 359);
            this.Contacttxt3.Name = "Contacttxt3";
            this.Contacttxt3.Size = new System.Drawing.Size(204, 29);
            this.Contacttxt3.TabIndex = 20;
            this.Contacttxt3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Contacttxt3_KeyPress);
            // 
            // Aadhartxt4
            // 
            this.Aadhartxt4.BackColor = System.Drawing.SystemColors.Window;
            this.Aadhartxt4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Aadhartxt4.Location = new System.Drawing.Point(335, 359);
            this.Aadhartxt4.Name = "Aadhartxt4";
            this.Aadhartxt4.Size = new System.Drawing.Size(204, 29);
            this.Aadhartxt4.TabIndex = 19;
            this.Aadhartxt4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Aadhartxt4_KeyPress);
            // 
            // Privioustxt5
            // 
            this.Privioustxt5.BackColor = System.Drawing.SystemColors.Window;
            this.Privioustxt5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Privioustxt5.Location = new System.Drawing.Point(150, 25);
            this.Privioustxt5.Name = "Privioustxt5";
            this.Privioustxt5.Size = new System.Drawing.Size(622, 29);
            this.Privioustxt5.TabIndex = 0;
            this.Privioustxt5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Privioustxt5_KeyPress);
            // 
            // MaleRbtn
            // 
            this.MaleRbtn.AutoSize = true;
            this.MaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaleRbtn.Location = new System.Drawing.Point(335, 140);
            this.MaleRbtn.Name = "MaleRbtn";
            this.MaleRbtn.Size = new System.Drawing.Size(62, 26);
            this.MaleRbtn.TabIndex = 5;
            this.MaleRbtn.Text = "Male";
            this.MaleRbtn.UseVisualStyleBackColor = true;
            this.MaleRbtn.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            this.MaleRbtn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaleRbtn_KeyPress);
            // 
            // FemaleRbtn
            // 
            this.FemaleRbtn.AutoSize = true;
            this.FemaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FemaleRbtn.Location = new System.Drawing.Point(429, 141);
            this.FemaleRbtn.Name = "FemaleRbtn";
            this.FemaleRbtn.Size = new System.Drawing.Size(77, 26);
            this.FemaleRbtn.TabIndex = 6;
            this.FemaleRbtn.Text = "Female";
            this.FemaleRbtn.UseVisualStyleBackColor = true;
            this.FemaleRbtn.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(33, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 22);
            this.label9.TabIndex = 21;
            this.label9.Text = "Admitted Std. :";
            // 
            // UploadBtn
            // 
            this.UploadBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.UploadBtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UploadBtn.Location = new System.Drawing.Point(34, 180);
            this.UploadBtn.Name = "UploadBtn";
            this.UploadBtn.Size = new System.Drawing.Size(110, 33);
            this.UploadBtn.TabIndex = 0;
            this.UploadBtn.Text = "Upload";
            this.UploadBtn.UseVisualStyleBackColor = false;
            this.UploadBtn.Click += new System.EventHandler(this.OpenBtn_Click);
            this.UploadBtn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.UploadBtn_KeyPress);
            // 
            // PlaceTxt
            // 
            this.PlaceTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PlaceTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceTxt.Location = new System.Drawing.Point(587, 275);
            this.PlaceTxt.Name = "PlaceTxt";
            this.PlaceTxt.Size = new System.Drawing.Size(112, 29);
            this.PlaceTxt.TabIndex = 14;
            this.PlaceTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PlaceTxt_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(477, 278);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 22);
            this.label8.TabIndex = 29;
            this.label8.Text = "Place of birth :";
            // 
            // DateTime1
            // 
            this.DateTime1.CalendarMonthBackground = System.Drawing.Color.NavajoWhite;
            this.DateTime1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTime1.Location = new System.Drawing.Point(335, 275);
            this.DateTime1.Name = "DateTime1";
            this.DateTime1.Size = new System.Drawing.Size(112, 29);
            this.DateTime1.TabIndex = 13;
            this.DateTime1.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            this.DateTime1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime1_KeyPress);
            // 
            // MotherTxt
            // 
            this.MotherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTxt.Location = new System.Drawing.Point(335, 98);
            this.MotherTxt.Name = "MotherTxt";
            this.MotherTxt.Size = new System.Drawing.Size(204, 29);
            this.MotherTxt.TabIndex = 3;
            this.MotherTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MotherTxt_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(211, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 22);
            this.label11.TabIndex = 38;
            this.label11.Text = "Mother\'s Name :";
            // 
            // StudentIDTxt
            // 
            this.StudentIDTxt.BackColor = System.Drawing.SystemColors.Window;
            this.StudentIDTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentIDTxt.Location = new System.Drawing.Point(749, 15);
            this.StudentIDTxt.Name = "StudentIDTxt";
            this.StudentIDTxt.Size = new System.Drawing.Size(204, 29);
            this.StudentIDTxt.TabIndex = 1;
            this.StudentIDTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StudentIDTxt_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(626, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 22);
            this.label12.TabIndex = 40;
            this.label12.Text = "Online ID :";
            // 
            // CastBox
            // 
            this.CastBox.BackColor = System.Drawing.SystemColors.Window;
            this.CastBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CastBox.FormattingEnabled = true;
            this.CastBox.Items.AddRange(new object[] {
            "Muslim",
            "Crishshan",
            "Hindu",
            "Sikh",
            "Buddh",
            "Jain",
            "Other"});
            this.CastBox.Location = new System.Drawing.Point(841, 178);
            this.CastBox.Name = "CastBox";
            this.CastBox.Size = new System.Drawing.Size(112, 30);
            this.CastBox.TabIndex = 9;
            this.CastBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CastBox_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(757, 182);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 22);
            this.label13.TabIndex = 42;
            this.label13.Text = "Caste :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(211, 229);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 22);
            this.label14.TabIndex = 44;
            this.label14.Text = "Sub Caste :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SubCastTxt
            // 
            this.SubCastTxt.BackColor = System.Drawing.SystemColors.Window;
            this.SubCastTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubCastTxt.Location = new System.Drawing.Point(335, 224);
            this.SubCastTxt.Name = "SubCastTxt";
            this.SubCastTxt.Size = new System.Drawing.Size(112, 29);
            this.SubCastTxt.TabIndex = 10;
            this.SubCastTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SubCastTxt_KeyPress);
            // 
            // DateTime2
            // 
            this.DateTime2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTime2.Location = new System.Drawing.Point(660, 32);
            this.DateTime2.Name = "DateTime2";
            this.DateTime2.Size = new System.Drawing.Size(112, 29);
            this.DateTime2.TabIndex = 1;
            this.DateTime2.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            this.DateTime2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime2_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(511, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(141, 22);
            this.label15.TabIndex = 46;
            this.label15.Text = "Date of Admission :";
            // 
            // CategoryBox
            // 
            this.CategoryBox.BackColor = System.Drawing.SystemColors.Window;
            this.CategoryBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategoryBox.FormattingEnabled = true;
            this.CategoryBox.Items.AddRange(new object[] {
            "Open",
            "OBC",
            "NT",
            "NT3",
            "SC",
            "ST",
            "VJNT",
            "Other"});
            this.CategoryBox.Location = new System.Drawing.Point(587, 225);
            this.CategoryBox.Name = "CategoryBox";
            this.CategoryBox.Size = new System.Drawing.Size(112, 30);
            this.CategoryBox.TabIndex = 11;
            this.CategoryBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CategoryBox_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(503, 229);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 22);
            this.label16.TabIndex = 48;
            this.label16.Text = "Category :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // IDTxt
            // 
            this.IDTxt.BackColor = System.Drawing.SystemColors.Window;
            this.IDTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDTxt.Location = new System.Drawing.Point(335, 14);
            this.IDTxt.Name = "IDTxt";
            this.IDTxt.Size = new System.Drawing.Size(176, 29);
            this.IDTxt.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(211, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(109, 22);
            this.label17.TabIndex = 51;
            this.label17.Text = "Gen.Reg.No.  :";
            // 
            // NationalityBox
            // 
            this.NationalityBox.BackColor = System.Drawing.SystemColors.Window;
            this.NationalityBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NationalityBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NationalityBox.FormattingEnabled = true;
            this.NationalityBox.Items.AddRange(new object[] {
            "Indian",
            "Other"});
            this.NationalityBox.Location = new System.Drawing.Point(841, 225);
            this.NationalityBox.Name = "NationalityBox";
            this.NationalityBox.Size = new System.Drawing.Size(112, 30);
            this.NationalityBox.TabIndex = 12;
            this.NationalityBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NationalityBox_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(734, 325);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 22);
            this.label18.TabIndex = 53;
            this.label18.Text = "Country :";
            // 
            // FatherTxt
            // 
            this.FatherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.FatherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FatherTxt.Location = new System.Drawing.Point(749, 100);
            this.FatherTxt.Name = "FatherTxt";
            this.FatherTxt.Size = new System.Drawing.Size(204, 29);
            this.FatherTxt.TabIndex = 4;
            this.FatherTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FatherTxt_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(594, 101);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(117, 22);
            this.label19.TabIndex = 55;
            this.label19.Text = "Father\'s Name :";
            // 
            // FirstClassBox
            // 
            this.FirstClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.FirstClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FirstClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstClassBox.FormattingEnabled = true;
            this.FirstClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th"});
            this.FirstClassBox.Location = new System.Drawing.Point(154, 31);
            this.FirstClassBox.Name = "FirstClassBox";
            this.FirstClassBox.Size = new System.Drawing.Size(112, 30);
            this.FirstClassBox.TabIndex = 0;
            this.FirstClassBox.SelectedIndexChanged += new System.EventHandler(this.FirstClassBox_SelectedIndexChanged);
            this.FirstClassBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FirstClassBox_KeyPress);
            this.FirstClassBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FirstClassBox_MouseClick);
            // 
            // ReligionTxt
            // 
            this.ReligionTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ReligionTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReligionTxt.Location = new System.Drawing.Point(587, 179);
            this.ReligionTxt.Name = "ReligionTxt";
            this.ReligionTxt.Size = new System.Drawing.Size(112, 29);
            this.ReligionTxt.TabIndex = 8;
            this.ReligionTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ReligionTxt_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(511, 182);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 22);
            this.label20.TabIndex = 58;
            this.label20.Text = "Religion :";
            // 
            // TqTxt
            // 
            this.TqTxt.BackColor = System.Drawing.SystemColors.Window;
            this.TqTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TqTxt.Location = new System.Drawing.Point(841, 275);
            this.TqTxt.Name = "TqTxt";
            this.TqTxt.Size = new System.Drawing.Size(112, 29);
            this.TqTxt.TabIndex = 15;
            this.TqTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TqTxt_KeyPress);
            // 
            // DistricTxt
            // 
            this.DistricTxt.BackColor = System.Drawing.SystemColors.Window;
            this.DistricTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DistricTxt.Location = new System.Drawing.Point(335, 322);
            this.DistricTxt.Name = "DistricTxt";
            this.DistricTxt.Size = new System.Drawing.Size(112, 29);
            this.DistricTxt.TabIndex = 16;
            this.DistricTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DistricTxt_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(777, 278);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 22);
            this.label21.TabIndex = 61;
            this.label21.Text = "Tq :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(214, 325);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 22);
            this.label22.TabIndex = 62;
            this.label22.Text = "Dist. :";
            // 
            // PreClassBox
            // 
            this.PreClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.PreClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PreClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreClassBox.FormattingEnabled = true;
            this.PreClassBox.ItemHeight = 22;
            this.PreClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th"});
            this.PreClassBox.Location = new System.Drawing.Point(112, 75);
            this.PreClassBox.Name = "PreClassBox";
            this.PreClassBox.Size = new System.Drawing.Size(90, 30);
            this.PreClassBox.TabIndex = 1;
            this.PreClassBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PreClassBox_KeyPress);
            // 
            // std
            // 
            this.std.AutoSize = true;
            this.std.BackColor = System.Drawing.Color.Transparent;
            this.std.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.std.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.std.Location = new System.Drawing.Point(56, 79);
            this.std.Name = "std";
            this.std.Size = new System.Drawing.Size(45, 22);
            this.std.TabIndex = 63;
            this.std.Text = "Std. :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(995, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(156, 168);
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(211, 182);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(123, 22);
            this.label24.TabIndex = 66;
            this.label24.Text = "Mother Tongue :";
            // 
            // MotherTongueTxt
            // 
            this.MotherTongueTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTongueTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTongueTxt.Location = new System.Drawing.Point(335, 179);
            this.MotherTongueTxt.Name = "MotherTongueTxt";
            this.MotherTongueTxt.Size = new System.Drawing.Size(112, 29);
            this.MotherTongueTxt.TabIndex = 7;
            this.MotherTongueTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MotherTongueTxt_KeyPress);
            // 
            // OldGenTxt
            // 
            this.OldGenTxt.BackColor = System.Drawing.SystemColors.Window;
            this.OldGenTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OldGenTxt.Location = new System.Drawing.Point(289, 76);
            this.OldGenTxt.Name = "OldGenTxt";
            this.OldGenTxt.Size = new System.Drawing.Size(91, 29);
            this.OldGenTxt.TabIndex = 2;
            // 
            // reg
            // 
            this.reg.AutoSize = true;
            this.reg.BackColor = System.Drawing.Color.Transparent;
            this.reg.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reg.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.reg.Location = new System.Drawing.Point(209, 79);
            this.reg.Name = "reg";
            this.reg.Size = new System.Drawing.Size(73, 22);
            this.reg.TabIndex = 68;
            this.reg.Text = "Reg.No. :";
            // 
            // DateTime3
            // 
            this.DateTime3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTime3.Location = new System.Drawing.Point(660, 76);
            this.DateTime3.Name = "DateTime3";
            this.DateTime3.Size = new System.Drawing.Size(112, 29);
            this.DateTime3.TabIndex = 4;
            this.DateTime3.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            // 
            // tc
            // 
            this.tc.AutoSize = true;
            this.tc.BackColor = System.Drawing.Color.Transparent;
            this.tc.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tc.Location = new System.Drawing.Point(571, 79);
            this.tc.Name = "tc";
            this.tc.Size = new System.Drawing.Size(81, 22);
            this.tc.TabIndex = 70;
            this.tc.Text = "T.C. Date :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.OldTcTxt);
            this.groupBox1.Controls.Add(this.school);
            this.groupBox1.Controls.Add(this.std);
            this.groupBox1.Controls.Add(this.DateTime3);
            this.groupBox1.Controls.Add(this.Privioustxt5);
            this.groupBox1.Controls.Add(this.tc);
            this.groupBox1.Controls.Add(this.PreClassBox);
            this.groupBox1.Controls.Add(this.reg);
            this.groupBox1.Controls.Add(this.OldGenTxt);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(181, 557);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 117);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Previous School Info :";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(387, 79);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 22);
            this.label25.TabIndex = 72;
            this.label25.Text = "T.C No. :";
            // 
            // OldTcTxt
            // 
            this.OldTcTxt.BackColor = System.Drawing.SystemColors.Window;
            this.OldTcTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OldTcTxt.Location = new System.Drawing.Point(464, 76);
            this.OldTcTxt.Name = "OldTcTxt";
            this.OldTcTxt.Size = new System.Drawing.Size(91, 29);
            this.OldTcTxt.TabIndex = 3;
            // 
            // SchoolCheked
            // 
            this.SchoolCheked.AutoSize = true;
            this.SchoolCheked.BackColor = System.Drawing.Color.Transparent;
            this.SchoolCheked.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SchoolCheked.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SchoolCheked.ForeColor = System.Drawing.Color.Black;
            this.SchoolCheked.Location = new System.Drawing.Point(334, 519);
            this.SchoolCheked.Name = "SchoolCheked";
            this.SchoolCheked.Size = new System.Drawing.Size(392, 30);
            this.SchoolCheked.TabIndex = 22;
            this.SchoolCheked.Text = "Is student belongs from another school ?";
            this.SchoolCheked.UseVisualStyleBackColor = false;
            this.SchoolCheked.CheckedChanged += new System.EventHandler(this.SchoolCheked_CheckedChanged);
            this.SchoolCheked.Click += new System.EventHandler(this.SchoolCheked_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.FirstClassBox);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.DateTime2);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(181, 444);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(795, 73);
            this.groupBox2.TabIndex = 74;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Current Addmission Info. :";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.UploadBtn);
            this.panel3.Location = new System.Drawing.Point(984, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(178, 223);
            this.panel3.TabIndex = 75;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(980, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 22);
            this.label10.TabIndex = 77;
            this.label10.Text = "Acd.Year :";
            // 
            // AcdYearTxt
            // 
            this.AcdYearTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AcdYearTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearTxt.Location = new System.Drawing.Point(1059, 267);
            this.AcdYearTxt.Name = "AcdYearTxt";
            this.AcdYearTxt.Size = new System.Drawing.Size(103, 29);
            this.AcdYearTxt.TabIndex = 24;
            this.AcdYearTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AcdYearTxt_KeyPress);
            // 
            // Yeslabel
            // 
            this.Yeslabel.AutoSize = true;
            this.Yeslabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Yeslabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Yeslabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Yeslabel.Location = new System.Drawing.Point(704, 543);
            this.Yeslabel.Name = "Yeslabel";
            this.Yeslabel.Size = new System.Drawing.Size(35, 22);
            this.Yeslabel.TabIndex = 78;
            this.Yeslabel.Text = "Yes";
            this.Yeslabel.Click += new System.EventHandler(this.Yeslabel_Click);
            // 
            // Nolabel
            // 
            this.Nolabel.AutoSize = true;
            this.Nolabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Nolabel.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nolabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Nolabel.Location = new System.Drawing.Point(752, 543);
            this.Nolabel.Name = "Nolabel";
            this.Nolabel.Size = new System.Drawing.Size(32, 22);
            this.Nolabel.TabIndex = 79;
            this.Nolabel.Text = "No";
            this.Nolabel.Click += new System.EventHandler(this.Nolabel_Click);
            // 
            // NoCheckBox
            // 
            this.NoCheckBox.AutoSize = true;
            this.NoCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.NoCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.NoCheckBox.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoCheckBox.ForeColor = System.Drawing.Color.Black;
            this.NoCheckBox.Location = new System.Drawing.Point(758, 527);
            this.NoCheckBox.Name = "NoCheckBox";
            this.NoCheckBox.Size = new System.Drawing.Size(15, 14);
            this.NoCheckBox.TabIndex = 23;
            this.NoCheckBox.UseVisualStyleBackColor = false;
            this.NoCheckBox.CheckedChanged += new System.EventHandler(this.NoCheckBox_CheckedChanged);
            this.NoCheckBox.Click += new System.EventHandler(this.NoCheckBox_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.SaveBtn);
            this.panel4.Controls.Add(this.ClearBtn);
            this.panel4.Location = new System.Drawing.Point(984, 617);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(211, 57);
            this.panel4.TabIndex = 80;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(532, 325);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 22);
            this.label7.TabIndex = 82;
            this.label7.Text = "State :";
            // 
            // StateBox
            // 
            this.StateBox.BackColor = System.Drawing.SystemColors.Window;
            this.StateBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StateBox.FormattingEnabled = true;
            this.StateBox.Items.AddRange(new object[] {
            "Andhra Pradesh",
            "Arunachal Pradesh",
            "Assam",
            "Bihar",
            "Chhattisgarh",
            "Goa",
            "Gujarat",
            "Haryana",
            "Himachal Pradesh",
            "Jammu & Kashmir",
            "Jharkhand",
            "Karnataka",
            "Kerala",
            "Madhya Pradesh",
            "Maharashtra",
            "Manipur",
            "Meghalaya",
            "Mizoram",
            "Nagaland",
            "Odisha",
            "Punjab",
            "Rajasthan",
            "Sikkim",
            "Tamil Nadu",
            "Telangana",
            "Tripura",
            "Uttarakhand",
            "Uttar Pradesh",
            "West Bengal"});
            this.StateBox.Location = new System.Drawing.Point(587, 321);
            this.StateBox.Name = "StateBox";
            this.StateBox.Size = new System.Drawing.Size(112, 30);
            this.StateBox.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(717, 229);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 22);
            this.label23.TabIndex = 84;
            this.label23.Text = "Nationality :";
            // 
            // CountryTxt
            // 
            this.CountryTxt.BackColor = System.Drawing.SystemColors.Window;
            this.CountryTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CountryTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CountryTxt.FormattingEnabled = true;
            this.CountryTxt.Items.AddRange(new object[] {
            "Indian",
            "Other"});
            this.CountryTxt.Location = new System.Drawing.Point(841, 321);
            this.CountryTxt.Name = "CountryTxt";
            this.CountryTxt.Size = new System.Drawing.Size(112, 30);
            this.CountryTxt.TabIndex = 18;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Location = new System.Drawing.Point(1277, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(85, 741);
            this.pictureBox2.TabIndex = 151;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(85, 741);
            this.pictureBox3.TabIndex = 152;
            this.pictureBox3.TabStop = false;
            // 
            // Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(3, 3);
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.CountryTxt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.StateBox);
            this.Controls.Add(this.NoCheckBox);
            this.Controls.Add(this.Nolabel);
            this.Controls.Add(this.Yeslabel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.SchoolCheked);
            this.Controls.Add(this.AcdYearTxt);
            this.Controls.Add(this.MotherTongueTxt);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.DistricTxt);
            this.Controls.Add(this.TqTxt);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.ReligionTxt);
            this.Controls.Add(this.FatherTxt);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.NationalityBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.IDTxt);
            this.Controls.Add(this.CategoryBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.SubCastTxt);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.CastBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.StudentIDTxt);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.MotherTxt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.DateTime1);
            this.Controls.Add(this.PlaceTxt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.FemaleRbtn);
            this.Controls.Add(this.MaleRbtn);
            this.Controls.Add(this.Aadhartxt4);
            this.Controls.Add(this.Contacttxt3);
            this.Controls.Add(this.Addresstxt2);
            this.Controls.Add(this.Nametxt1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Name = "Student";
            this.Text = "New Admission Primary";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Student_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label school;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.TextBox Nametxt1;
        private System.Windows.Forms.TextBox Addresstxt2;
        private System.Windows.Forms.TextBox Contacttxt3;
        private System.Windows.Forms.TextBox Aadhartxt4;
        private System.Windows.Forms.TextBox Privioustxt5;
        private System.Windows.Forms.RadioButton MaleRbtn;
        private System.Windows.Forms.RadioButton FemaleRbtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button UploadBtn;
        private System.Windows.Forms.TextBox PlaceTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker DateTime1;
        private System.Windows.Forms.TextBox MotherTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox StudentIDTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CastBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox SubCastTxt;
        private System.Windows.Forms.DateTimePicker DateTime2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox CategoryBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox IDTxt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox NationalityBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox FatherTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox FirstClassBox;
        private System.Windows.Forms.TextBox ReligionTxt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TqTxt;
        private System.Windows.Forms.TextBox DistricTxt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox PreClassBox;
        private System.Windows.Forms.Label std;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox MotherTongueTxt;
        private System.Windows.Forms.TextBox OldGenTxt;
        private System.Windows.Forms.Label reg;
        private System.Windows.Forms.DateTimePicker DateTime3;
        private System.Windows.Forms.Label tc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox SchoolCheked;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox AcdYearTxt;
        private System.Windows.Forms.Label Yeslabel;
        private System.Windows.Forms.Label Nolabel;
        private System.Windows.Forms.CheckBox NoCheckBox;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox StateBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox CountryTxt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox OldTcTxt;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}