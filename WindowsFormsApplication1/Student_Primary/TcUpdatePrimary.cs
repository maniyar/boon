﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Boon
{
    public partial class TcUpdatePrimary : Form
    {
        common c = new common();
        public TcUpdatePrimary()
        {
            InitializeComponent();
        }

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();

                String Str = "select Distinct(AcdYear) from Boon.tc_primary where Std = '" + ClassBox.Text + "' Order by AcdYear";
                DataTable dt = c.SelectData(Str);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
                else { MessageBox.Show("No data found","Alert"); }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GenNoBox.Items.Clear();

                String Str = "select ID from Boon.tc_primary where Std = '" + ClassBox.Text + "' AND AcdYear='"+AcdYearBox.Text+"' order By ID";
                DataTable dt = c.SelectData(Str);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        GenNoBox.Items.Add(row["ID"].ToString());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
        string FAcdYear, OldId;
        private void GetBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTxt();
                FAcdYear = "";
                OldId = "";
                String SelectQuery = "SELECT * from Boon.tc_primary WHERE  ID = '" + GenNoBox.Text + "'";
                    DataTable table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        Nametxt.Text = table.Rows[0][2].ToString();
                        AddressTxt.Text = table.Rows[0][3].ToString();
                        ContactTxt.Text = table.Rows[0][4].ToString();
                        AadharTxt.Text = table.Rows[0][5].ToString();
                        PriviousTxt.Text = table.Rows[0][6].ToString();
                        var GetGender = table.Rows[0][7].ToString();
                        if (GetGender == "Male")
                        {
                            MaleRbtn.Checked = true;
                            FemaleRbtn.Checked = false;
                        }
                        else
                        {
                            FemaleRbtn.Checked = true;
                            MaleRbtn.Checked = false;
                        }
                        TxtDOB.Text= table.Rows[0][8].ToString();
                        PlaceTxt.Text = table.Rows[0][10].ToString();
                        MotherTxt.Text = table.Rows[0][11].ToString();
                        FatherTxt.Text = table.Rows[0][12].ToString();
                        StudentIDTxt.Text = table.Rows[0][13].ToString();
                        CastBox.Text = table.Rows[0][14].ToString();
                        SubCastTxt.Text = table.Rows[0][15].ToString();
                        CategoryBox.Text = table.Rows[0][16].ToString();
                       
                        TxtDOAD.Text = table.Rows[0][17].ToString();
                        var Acd = TxtDOAD.Text;
                        if (TxtDOAD.Text != "")
                        {
                            int Fyear = Convert.ToInt32((Acd).Substring(8, 2).ToString());
                            FAcdYear = ("20" + (Fyear) + "-" + (Fyear + 1));
                        }
                        NationalityBox.Text = table.Rows[0][18].ToString();
                        DateOfLeaving.Text = table.Rows[0][20].ToString();
                        ConductTxt.Text = table.Rows[0][21].ToString();
                        ReasonTxt.Text = table.Rows[0][22].ToString();
                        RemarkTxt.Text = table.Rows[0][23].ToString();
                        MotherTongueTxt.Text = table.Rows[0][24].ToString();
                        ReligionTxt.Text = table.Rows[0][25].ToString();
                        ProgressTxt.Text = table.Rows[0][26].ToString();
                        PreClassBox.Text = table.Rows[0][27].ToString();
                        FirstClassBox.Text = table.Rows[0][28].ToString();
                        StateBox.Text = table.Rows[0][29].ToString();
                        TqTxt.Text = table.Rows[0][32].ToString();
                        DistricTxt.Text = table.Rows[0][33].ToString();
                        StateBox.Text = table.Rows[0][34].ToString();
                        CountryTxt.Text = table.Rows[0][35].ToString();
                        OldTcTxt.Text = table.Rows[0][36].ToString();
                        TxtTcDate.Text = table.Rows[0][37].ToString();
                        DateOfLeaving.Text = table.Rows[0][20].ToString();
                        DateOfApplication.Text= table.Rows[0][31].ToString();
                        TdateTxt.Text = table.Rows[0][39].ToString();
                        OldGenTxt.Text = table.Rows[0][40].ToString();
                    }
                    else
                    {
                        MessageBox.Show("No record found", "Alert");
                     
                    }
            }
            catch(Exception ex)
            { MessageBox.Show(ex.Message,"Exception"); }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        { 
            try
            {
                var DateInWords = common.DateToWords(TxtDOB.Text);
                String UpdateQuery = "Update Boon.tc_primary SET Name='" + Nametxt.Text + "',Address='" + AddressTxt.Text + "',Contact='" + ContactTxt.Text + "',Previous_school='" + PriviousTxt.Text + "',Gender='" + Gender + "',DOB='" + TxtDOB.Text + "',Std='" + ClassBox.Text + "',B_Place='" + PlaceTxt.Text + "',Mother_Name='" + MotherTxt.Text + "',Father_Name='" + FatherTxt.Text + "',StudentId='" + StudentIDTxt.Text + "',Cast='" + CastBox.Text + "',SubCast='" + SubCastTxt.Text + "',D_O_A='" + TxtDOAD.Text + "',Nationality='" + NationalityBox.Text + "',Date_of_leaving='" + DateOfLeaving.Text + "',Conduct='" + ConductTxt.Text + "',Reason='" + ReasonTxt.Text + "',Remark='" + RemarkTxt.Text + "',MotherTongue='" + MotherTongueTxt.Text + "',Religion='" + ReligionTxt.Text + "',Progress='" + ProgressTxt.Text + "',Pre_Std='" + PreClassBox.Text + "',FirstStd='" + FirstClassBox.Text + "',AcdYear='" + AcdYearBox.Text + "',DateInWords='" + DateInWords + "',DateOfApplication='" + DateOfApplication.Text + "',State='" + StateBox.Text + "',Tq='" + TqTxt.Text + "',Dist='" + DistricTxt.Text + "',Country='" + CountryTxt.Text + "',OldTcNo='" + OldTcTxt.Text + "',TcDate='" + TxtTcDate.Text + "',Category='" + CategoryBox.Text + "',Aadhar='" + AadharTxt.Text + "',Tdate='" + TdateTxt.Text + "',AcdYearFirst = '" + FAcdYear + "',OldId='" + OldGenTxt.Text + "'  WHERE ID='" + GenNoBox.Text + "'";
                if (MessageBox.Show("Do you want to update T.C. ?", "Update T.C Application", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (c.InsertData(UpdateQuery))
                    {
                        MessageBox.Show("TC Updated...", "Alert");
                        ClearTxt();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }
        String Gender = " ";
      
        private void MaleRbtn_CheckedChanged(object sender, EventArgs e)
        {
        Gender= "Male";   
        }

        private void FemaleRbtn_CheckedChanged(object sender, EventArgs e)
        {
        Gender= "Female"; 
        }
        public void ClearTxt()
        {
            Nametxt.Text = null;
            AddressTxt.Text = null;
            ContactTxt.Text = null;
            AadharTxt.Text = null;
            PriviousTxt.Text = null;
            MaleRbtn.Checked = false;
            FemaleRbtn.Checked = false;
            TxtDOB.Text = null;
            PlaceTxt.Text = null;
            MotherTxt.Text = null;
            FatherTxt.Text = null;
            StudentIDTxt.Text = null;
            CastBox.Text = null;
            SubCastTxt.Text = null;
            CategoryBox.Text = null;
            TxtDOAD.Text = null;
            NationalityBox.Text = null;
            DateOfLeaving.Text = null;
            ConductTxt.Text = null;
            ReasonTxt.Text = null;
            RemarkTxt.Text = null;
            MotherTongueTxt.Text = null;
            ReligionTxt.Text = null;
            ProgressTxt.Text = null;
            PreClassBox.Text = null;
            FirstClassBox.Text = null;
            StateBox.Text = null;
            TqTxt.Text = null;
            DistricTxt.Text = null;
            StateBox.Text = null;
            CountryTxt.Text = null;
            OldGenTxt.Text = null;
            OldTcTxt.Text = null;
            DateOfLeaving.Text = null;
            DateOfApplication.Text = null;
            TxtTcDate.Text = null;
            OldTcTxt.Text = null;
            TdateTxt.Text = null;
        }
    }

}
