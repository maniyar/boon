﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Boon
{
    public partial class StudMarks : Form
    {
        ErrorProvider errorProvider1 = new ErrorProvider();
        common c = new common();
        MySqlConnection connection ;
        public StudMarks()
        {
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
        
      
        DataTable dt = new DataTable();
        Double Total;
        MySqlCommand command = new MySqlCommand();
        int Daily, OralA, OralB, Test, Project, Practical, Arts, Work, Other, PracticalA, Writting ;
       // int t1, t2, t3, t4, t5, t6, t7, t8, t9;
      //  Total1, Total2


        private void StudentID()
        {
           
            try
            {
                IdBox.Items.Clear();
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;

                Cmd.CommandText = "select ID from Boon.students Where Std = '" + ClassBox.Text + "'AND AcdYear = '" + AcdYearBox.Text + "';";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["ID"].ToString();
                    IdBox.Items.Add(i);

                }
                dr.Close();
            }
            catch
            {

            }
            connection.Close();
        }

        
        public void LoadName()
        {
            try
            {
                String SelectQuery = "select Name from Boon.students where ID = '" + IdBox.Text + "' AND Std = '"+ClassBox.Text+"' ";

                dt = c.SelectData(SelectQuery);

                NameTxt.Text = dt.Rows[0][0].ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

           }
        public void LoadID()
        {
            try
            {
                String SelectQuery = "select ID from Boon.students where Name = '" + NameTxt.Text + "' ";

                dt = c.SelectData(SelectQuery);

                IdBox.Text = dt.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public void Totalcount()
        {
            var marks = 0;
            var marks1 = 0;
           // int Daily, OralA, OralB, Test, Project, Practical, Arts, Work, Other, OralB, PracticalA,Writting ;
            try
            {
                if (DailyTxt.Text != "" )
                {
                    Daily = int.Parse(DailyTxt.Text);
                    marks = marks + Daily;
                }
                if ( OralTxt.Text != "")
                {
                    OralA = int.Parse(OralTxt.Text);
                    marks = marks + OralA;
                }
                if ( OralTxt1.Text !="")
                {
                    OralB = int.Parse(OralTxt1.Text);
                    marks1 = marks1 + OralB;
                }
                if (TestTxt.Text != "")
                {
                    Test = int.Parse(TestTxt.Text);
                    marks = marks + Test;
                }
                if ( ProjectTxt.Text != "")
                {
                    Project = int.Parse(ProjectTxt.Text);
                    marks = marks + Project;
                }

                if ( ArtsTxt.Text != "")
                {
                    Arts = int.Parse(ArtsTxt.Text);
                    marks = marks + Arts;
                }

                if ( WorkTxt.Text != "")
                {
                    Work = int.Parse(WorkTxt.Text);
                    marks = marks + Work;
                }
                if ( OtherTxt.Text != "")
                {
                    Other = int.Parse(OtherTxt.Text);
                    marks = marks + Other;
                }
                if ( PracticalTxt.Text != "")
                {
                    Practical = int.Parse(PracticalTxt.Text);
                    marks = marks + Practical;
                }
                if ( PracticalTxt1.Text != "")
                {
                    PracticalA = int.Parse(PracticalTxt1.Text);
                    marks1 = marks1 + PracticalA;
                }
                if (WritingTxt.Text != "")
                {
                    Writting = int.Parse(WritingTxt.Text);
                    marks1 = marks1 + Writting;
                }
               
           //   var  Total1 = Daily + OralA + Test + Project + Practical + Arts + Work + Other;
           //  var   Total2 = OralB + PracticalA + Writting;
                TotalTxt.Text = marks.ToString();
                TotalTxt1.Text = marks1.ToString();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
      
        public void TotalMarks()
        {
            try
            {
                var totA = Double.Parse(TotalTxt.Text).ToString();
                var totB = Double.Parse(TotalTxt1.Text).ToString();
                Double a = Double.Parse(totA);
                Double b = Double.Parse(totB);
                Total = a + b;
            }
            catch { }
        }
       

        private void StudentNameLoad()
        {
            try
            {
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                Cmd.CommandText = "select Name from Boon.students Where Std = '" + ClassBox.Text + "'  AND AcdYear = '"+AcdYearBox.Text+"'";
                MySqlDataReader dr1 = Cmd.ExecuteReader();
                AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                while (dr1.Read())
                {
                    data.Add(dr1.GetString(0));

                }
                NameTxt.AutoCompleteCustomSource = data;
                dr1.Close();
            }
            catch
            {

            }
            connection.Close();
        }
        public void ClearTxtBoxes()
        {
            DailyTxt.Text = null;
            OralTxt.Text = null;
            OralTxt1.Text = null;
            PracticalTxt.Text = null;
            ProjectTxt.Text = null;
            TotalTxt.Text = null;
            TotalTxt1.Text = null;
            TestTxt.Text = null;
            ArtsTxt.Text = null;
            WorkTxt.Text = null;
            OtherTxt.Text = null;
            PracticalTxt1.Text = null;
            WritingTxt.Text = null;
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Do you want to save the student marks?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                TotalMarks();
                try
                {
                    if (SubBox.Text == "English")
                    {

                        String InsertQuery = "INSERT into Boon.English (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "' , '"+AcdYearBox.Text+"')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "' AND Class = '" + ClassBox.Text + "' ";
                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET English = '" + Total + "' where ID = '" + IdBox.Text + "' AND Std = '"+ClassBox.Text+"'";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,English,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }

                    }
                    if (SubBox.Text == "Hindi")
                    {

                        String InsertQuery = "INSERT into Boon.Hindi (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "'AND Class = '" + ClassBox.Text + "' ";
                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Hindi = '" + Total + "' where ID = '" + IdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' AND Class = '" + ClassBox.Text + "'";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Hindi,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Urdu")
                    {

                        String InsertQuery = "INSERT into Boon.Urdu (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','"+AcdYearBox.Text+"')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "' AND Class = '"+ClassBox.Text+"' ";
                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Urdu = '" + Total + "' where ID = '" + IdBox.Text + "' AND Class = '" + ClassBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Urdu,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Marathi")
                    {

                        String InsertQuery = "INSERT into Boon.Marathi (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "' AND Class = '" + ClassBox.Text + "' ";
                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Marathi = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Marathi,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Math")
                    {

                        String InsertQuery = "INSERT into Boon.Math (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "'AND Class = '" + ClassBox.Text + "' ";

                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Math = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Math,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "G Science")
                    {

                        String InsertQuery = "INSERT into Boon.G_Science (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "' AND Class = '" + ClassBox.Text + "' ";

                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET G_Science = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,G_Science,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Social Science")
                    {

                        String InsertQuery = "INSERT into Boon.Social_Science (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "'AND Class = '" + ClassBox.Text + "' ";

                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Social_Science = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Social_Science,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Arts")
                    {

                        String InsertQuery = "INSERT into Boon.Arts (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "' AND Class = '" + ClassBox.Text + "' ";

                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Arts = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Arts,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Work Exp")
                    {

                        String InsertQuery = "INSERT into Boon.Work_Exp (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "'AND Class = '" + ClassBox.Text + "' ";

                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Work_Exp = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Work_Exp,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                    if (SubBox.Text == "Physical Edu")
                    {

                        String InsertQuery = "INSERT into Boon.Physical_Edu (ID,Name,Class,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            String SelectQuery = "SELECT ID from Boon.exammarks where ID = '" + IdBox.Text + "' AND Class = '" + ClassBox.Text + "' ";

                            dt = c.SelectData(SelectQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String UpdateQuery = "Update Boon.exammarks SET Physical_Edu = '" + Total + "' where ID = '" + IdBox.Text + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    MessageBox.Show("Data Updated");
                                }
                            }
                            else
                            {
                                String InsertQuery1 = "INSERT into Boon.exammarks (ID,Name,Class,Physical_Edu,AcdYear) VALUES ('" + IdBox.Text + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + Total + "','" + AcdYearBox.Text + "')";
                                if (c.InsertData(InsertQuery1))
                                {
                                    MessageBox.Show("Data saved");
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
         }

        public void LoadTxtBoxes()
        {
            try
            {
                String SelectQuery = "Select * from Boon.examtructure Where ClassSub Like  '%" + SubBox.Text + "%'  AND Class = '" + ClassBox.Text + "' ";
                DataTable table = new DataTable();
                table = c.SelectData(SelectQuery);
                DTxt.Text = table.Rows[0][2].ToString();
                O1Txt.Text = table.Rows[0][3].ToString();
                P1Txt.Text = table.Rows[0][4].ToString();
                ATxt.Text = table.Rows[0][5].ToString();
                PrTxt.Text = table.Rows[0][6].ToString();
                TTxt.Text = table.Rows[0][7].ToString();
                WTxt.Text = table.Rows[0][8].ToString();
                OTxt.Text = table.Rows[0][9].ToString();
                T1Txt.Text = table.Rows[0][10].ToString();
                O2Txt.Text = table.Rows[0][11].ToString();
                P2Txt.Text = table.Rows[0][12].ToString();
                W1Txt.Text = table.Rows[0][13].ToString();
                T2Txt.Text = table.Rows[0][14].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Alert ");
            }

        }

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTxtBoxes();
           // StudentNameLoad();
          //  StudentID();
            
        }

        private void IdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTxtBoxes();
            LoadName();


        }

        private void SubBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClassBox.Text == "")
            {
                ClearTxtBoxes();
                MessageBox.Show("Select class", "Warnning");
            }
            else
            {
                ClearTxtBoxes();
                LoadTxtBoxes();
                ReadOnlyTxt();
            }
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            Totalcount();
            //ClassBox.Text = null;
            //SubBox.Text = null;
            //IdBox.Text = null;
            //NameTxt.Text = null;
            //ClearTxtBoxes();
        }

        private void NameTxt_Leave(object sender, EventArgs e)
        {
            LoadID();
        }

        private void DailyTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(DTxt.Text);
                int b = int.Parse(DailyTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    DailyTxt.Focus();
                }
            }
            catch
            {
                
            }
        }

        private void OralTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(O1Txt.Text);
                int b = int.Parse(OralTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    OralTxt.Focus();
                }
            }
            catch { }
        }

        private void PracticalTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(P1Txt.Text);
                int b = int.Parse(PracticalTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    PracticalTxt.Focus();
                }
            }
            catch { }
        }

        private void ArtsTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(ATxt.Text);
                int b = int.Parse(ArtsTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    ArtsTxt.Focus();
                }
            }
            catch { }
        }

        private void ProjectTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(PrTxt.Text);
                int b = int.Parse(ProjectTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    ProjectTxt.Focus();
                }
            }
            catch { }
        }

        private void TestTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(TTxt.Text);
                int b = int.Parse(TestTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    TestTxt.Focus();
                }
            }
            catch { }
        }

        private void WorkTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(WTxt.Text);
                int b = int.Parse(WorkTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    WorkTxt.Focus();
                }
            }
            catch { }
        }

        private void OtherTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(OTxt.Text);
                int b = int.Parse(OtherTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    OtherTxt.Focus();
                }
            }
            catch { }
        }

        private void TotalTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(T1Txt.Text);
                int b = int.Parse(TotalTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    TotalTxt.Focus();
                }
            }
            catch { }
        }

        private void OralTxt1_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(O2Txt.Text);
                int b = int.Parse(OralTxt1.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    OralTxt1.Focus();
                }
            }
            catch { }
        }

        private void PracticalTxt1_Leave(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(P2Txt.Text);
                int b = int.Parse(PracticalTxt1.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    PracticalTxt1.Focus();
                }
            }
            catch { }
        }

        private void WritingTxt_Leave(object sender, EventArgs e)
        {

            try
            {
                int a = int.Parse(W1Txt.Text);
                int b = int.Parse(WritingTxt.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    WritingTxt.Focus();
                  
                }
            }
               
            catch { }
        }

        private void TotalTxt1_Leave(object sender, EventArgs e)
        {

            try
            {
                int a = int.Parse(T2Txt.Text);
                int b = int.Parse(TotalTxt1.Text);
                if (b > a)
                {
                    MessageBox.Show("Enter valid Marks", "Alert");
                    TotalTxt1.Focus();
                }
            }
            catch { }
        }

        private void DailyTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only","Alert");
                e.Handled = true;
                
            }
        }

        private void OralTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
             
            }
        }

        private void PracticalTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
               
            }
        }

        private void ArtsTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
                
            }
        }

        private void ProjectTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
               
            }
        }

        private void TestTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
               
            }
        }

        private void WorkTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
                
            }
        }

        private void OtherTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
                
            }
        }

        private void TotalTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }
        }

        private void OralTxt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
                
            }
        }

        private void PracticalTxt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
                
            }
        }

        private void WritingTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
               
            }
        }

        private void TotalTxt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }
        }

        private void StudMarks_Load(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                //Cmd.CommandText =
                Cmd.CommandText = "select Distinct(AcdYear) from Boon.students ORDER BY AcdYear  ";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["AcdYear"].ToString();
                    AcdYearBox.Items.Add(i);

                }
                dr.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }
 
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClassBox.Text == "" && SubBox.Text == "")
            {

                MessageBox.Show("Select Class & Subject first", "Warnning");

            }
            else
            {
               
                StudentNameLoad();
                StudentID();
            }
        }

        private void DailyTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void PracticalTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void OralTxt_TextChanged(object sender, EventArgs e)
        {
           Totalcount();
        }

        private void ArtsTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void ProjectTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void TestTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void WorkTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void OtherTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void OralTxt1_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void PracticalTxt1_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void WritingTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void P1Txt_TextChanged(object sender, EventArgs e)
        {
            ReadOnlyTxt();
        }
        public void ReadOnlyTxt()
        {
            if (P1Txt.Text == "0")
            {
                PracticalTxt.ReadOnly = true;
            }
            else
            {
                PracticalTxt.ReadOnly = false;
            }

            if (DTxt.Text == "0")
            {
                DailyTxt.ReadOnly = true;
            }
            else
            {
                DailyTxt.ReadOnly = false;
            }
            if (OTxt.Text == "0")
            {
                OtherTxt.ReadOnly = true;
            }
            else
            {
                OtherTxt.ReadOnly = false;
            }
            if (O1Txt.Text == "0")
            {
                OralTxt.ReadOnly = true;
            }
            else
            {
                OralTxt.ReadOnly = false;
            }
            if (PrTxt.Text == "0")
            {
                ProjectTxt.ReadOnly = true;
            }
            else
            {
                ProjectTxt.ReadOnly = false;
            }
            if (W1Txt.Text == "0")
            {
                WritingTxt.ReadOnly = true;
            }
            else
            {
                WritingTxt.ReadOnly = false;
            }
            if (P2Txt.Text == "0")
            {
                PracticalTxt1.ReadOnly = true;
            }
            else
            {
                PracticalTxt1.ReadOnly = false;
            }
            if (WTxt.Text == "0")
            {
                WorkTxt.ReadOnly = true;
            }
            else
            {
                WorkTxt.ReadOnly = false;
            }
            if (O2Txt.Text == "0")
            {
                OralTxt1.ReadOnly = true;
            }
            else
            {
                OralTxt1.ReadOnly = false;
            }
            if (TTxt.Text == "0")
            {
                TestTxt.ReadOnly = true;
            }
            else
            {
                TestTxt.ReadOnly = false;
            }
            if (ATxt.Text == "0")
            {
                ArtsTxt.ReadOnly = true;
            }
            else
            {
                ArtsTxt.ReadOnly = false;
            }
        }
    }
}
