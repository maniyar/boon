﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DGVPrinterHelper;

namespace Boon
{
    public partial class SearchFormPPrimary : Form
    {
        common c = new common();
        public SearchFormPPrimary()
        {
            InitializeComponent();
        }

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (StudType.Text == "")
                {
                    MessageBox.Show("You have select Student type", "Alert");
                    StudType.Focus();
                    return;
                }
                if (StudType.Text == "New")
                {
                    AcdYearBox.Items.Clear();
                    String SelectQuery = "select Distinct(AcdYear) from Boon.stud_p_primary WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear  ";
                    DataTable dt1 = c.SelectData(SelectQuery);
                    if (dt1.Rows.Count > 0)
                    {
                        AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                        foreach (DataRow row in dt1.Rows)
                        {
                           

                            common i = new common();
                            i.Text = row["AcdYear"].ToString();
                            AcdYearBox.Items.Add(i);

                        }
                       
                    }
                }
                if (StudType.Text == "Old")
                {
                    AcdYearBox.Items.Clear();
                    String SelectQuery = "select Distinct(AcdYear) from Boon.stud_p_primary_copy WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear  ";
                    DataTable dt1 = c.SelectData(SelectQuery);
                    if (dt1.Rows.Count > 0)
                    {
                        AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                        foreach (DataRow row in dt1.Rows)
                        {


                            common i = new common();
                            i.Text = row["AcdYear"].ToString();
                            AcdYearBox.Items.Add(i);

                        }

                    }
                }
            }
            catch { }
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClassBox.Text == "")
            {
                MessageBox.Show("Select Class", "Alert");

                ClassBox.Focus();
                return;
            }
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            StudCountTxt.Text = "Total students : 0";
            // dataGridView1.DataSource =null;
            try
            {
                if (ClassBox.Text == "")
                {


                    MessageBox.Show("Please select standard first");
                    ClassBox.Focus();
                    return;

                }

                else
                {
                    if (StudType.Text == "New")
                    {
                        String SelectQuery = "SELECT ID,Name,Address,Contact,Std from Boon.stud_p_primary WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' Order BY ID";
                        DataTable table = new DataTable();
                        table = c.SelectData(SelectQuery);
                        dataGridView1.DataSource = table;
                        dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
                        dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                        dataGridView1.Columns[0].Width = 50;
                        dataGridView1.Columns[1].Width = 300;
                        dataGridView1.Columns[2].Width = 350;
                        dataGridView1.Columns[3].Width = 130;
                        dataGridView1.Columns[4].Width = 100;
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                        
                    }
                    else
                    {
                        String SelectQuery = "SELECT ID,Name,Address,Contact,Std from Boon.stud_p_primary_copy WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' Order BY ID";
                        DataTable table = new DataTable();
                        table = c.SelectData(SelectQuery);
                        dataGridView1.DataSource = table;
                        dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
                        dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                        dataGridView1.Columns[0].Width = 50;
                        dataGridView1.Columns[1].Width = 300;
                        dataGridView1.Columns[2].Width = 350;
                        dataGridView1.Columns[3].Width = 130;
                        dataGridView1.Columns[4].Width = 100;
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void NameTxt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Name LIKE '%{0}%'", NameTxt.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (StudType.Text == "New")
               {
                    this.MaleRbtn.Checked = false;
                this.FemaleRbtn.Checked = false;

                int index = e.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[index];
              String  IdTxt = selectedRow.Cells[0].Value.ToString();
              SNameTxt.Text = selectedRow.Cells[1].Value.ToString();

                DataTable table = new DataTable();
                String SelectQuery = "SELECT * from Boon.stud_p_primary WHERE ID = '" + IdTxt + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                table = c.SelectData(SelectQuery);
                AddressTxt.Text = table.Rows[0][2].ToString();
                ContactTxt.Text = table.Rows[0][3].ToString();
                AadharTxt.Text = table.Rows[0][4].ToString();
                PriviousTxt.Text = table.Rows[0][5].ToString();
                
                var GetGender = table.Rows[0][6].ToString();

                DateTime1.Text = table.Rows[0][7].ToString();



                PlaceTxt.Text = table.Rows[0][9].ToString() + " Tq. " + table.Rows[0][35].ToString() + " Dist. " + table.Rows[0][36].ToString();


                MotherTxt.Text = table.Rows[0][11].ToString();
                FatherTxt.Text = table.Rows[0][12].ToString();
                StudentIDTxt.Text = table.Rows[0][13].ToString();
                CastBox.Text = table.Rows[0][14].ToString();
                SubCastTxt.Text = table.Rows[0][15].ToString();
                CategoryBox.Text = table.Rows[0][16].ToString();
                DateTime2.Text = table.Rows[0][17].ToString();
                NationalityBox.Text = table.Rows[0][18].ToString();
                ReligionTxt.Text = table.Rows[0][24].ToString();
                MotherTongueTxt.Text = table.Rows[0][25].ToString();
                PreClassBox.Text = table.Rows[0][26].ToString();
                FirstClassBox.Text = table.Rows[0][27].ToString();
                RemFeeTxt.Text = table.Rows[0][33].ToString();
                TutionFeeTxt.Text = table.Rows[0][23].ToString();
              
                if (GetGender.ToString() != "")
                {
                    if (GetGender.ToString() == "Male")
                    {
                        this.FemaleRbtn.Checked = false;
                        this.MaleRbtn.Checked = true;
                    }
                    else
                    {
                        this.MaleRbtn.Checked = false;
                        this.FemaleRbtn.Checked = true;
                    }
                }
                tabControl1.SelectTab("tabPage2");
               }
                else{
               
               
                 this.MaleRbtn.Checked = false;
                this.FemaleRbtn.Checked = false;

                int index = e.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[index];
              String  IdTxt = selectedRow.Cells[0].Value.ToString();
              SNameTxt.Text = selectedRow.Cells[1].Value.ToString();

                DataTable table = new DataTable();
                String SelectQuery = "SELECT * from Boon.stud_p_primary_copy WHERE ID = '" + IdTxt + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                table = c.SelectData(SelectQuery);
                AddressTxt.Text = table.Rows[0][2].ToString();
                ContactTxt.Text = table.Rows[0][3].ToString();
                AadharTxt.Text = table.Rows[0][4].ToString();
                PriviousTxt.Text = table.Rows[0][5].ToString();
                
                var GetGender = table.Rows[0][6].ToString();

                DateTime1.Text = table.Rows[0][7].ToString();
               PlaceTxt.Text = table.Rows[0][9].ToString() + " Tq. " + table.Rows[0][35].ToString() + " Dist. " + table.Rows[0][36].ToString();
                MotherTxt.Text = table.Rows[0][11].ToString();
                FatherTxt.Text = table.Rows[0][12].ToString();
                StudentIDTxt.Text = table.Rows[0][13].ToString();
                CastBox.Text = table.Rows[0][14].ToString();
                SubCastTxt.Text = table.Rows[0][15].ToString();
                CategoryBox.Text = table.Rows[0][16].ToString();
                DateTime2.Text = table.Rows[0][17].ToString();
                NationalityBox.Text = table.Rows[0][18].ToString();
                ReligionTxt.Text = table.Rows[0][24].ToString();
                MotherTongueTxt.Text = table.Rows[0][25].ToString();
                PreClassBox.Text = table.Rows[0][26].ToString();
                FirstClassBox.Text = table.Rows[0][27].ToString();
                RemFeeTxt.Text = table.Rows[0][33].ToString();
                TutionFeeTxt.Text = table.Rows[0][23].ToString();
              
                if (GetGender.ToString() != "")
                {
                    if (GetGender.ToString() == "Male")
                    {
                        this.FemaleRbtn.Checked = false;
                        this.MaleRbtn.Checked = true;
                    }
                    else
                    {
                        this.MaleRbtn.Checked = false;
                        this.FemaleRbtn.Checked = true;
                    }
                }
                tabControl1.SelectTab("tabPage2");}
            }
            catch { }
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();
                this.dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
                StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                NameTxt.Text = null;
                AddressTxt.Text = null;
                ContactTxt.Text = null;
                AadharTxt.Text = null;
                PriviousTxt.Text = null;
                PlaceTxt.Text = null;
                DateTime1.Text = null;
                DateTime2.Text = null;
                SNameTxt.Text = null;

                NationalityBox.Text = null;
                MotherTxt.Text = null;
                FatherTxt.Text = null;
                ClassBox.Text = null;
                this.FemaleRbtn.Checked = false;
                this.MaleRbtn.Checked = false;
                CastBox.Text = null;
                SubCastTxt.Text = null;
                CategoryBox.Text = null;
                TutionFeeTxt.Text = null;
                RemFeeTxt.Text = null;
                MotherTongueTxt.Text = null;
                ReligionTxt.Text = null;
                FirstClassBox.Text = null;
                PreClassBox.Text = null;
                StudentIDTxt.Text = null;
            }
            catch { }
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
    }
}
