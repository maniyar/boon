﻿namespace Boon
{
    partial class StudMarks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.OralTxt1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.PracticalTxt1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.WritingTxt = new System.Windows.Forms.TextBox();
            this.TotalTxt1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.SubBox = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.IdBox = new System.Windows.Forms.ComboBox();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.PrTxt = new System.Windows.Forms.TextBox();
            this.TTxt = new System.Windows.Forms.TextBox();
            this.ATxt = new System.Windows.Forms.TextBox();
            this.WTxt = new System.Windows.Forms.TextBox();
            this.P1Txt = new System.Windows.Forms.TextBox();
            this.OTxt = new System.Windows.Forms.TextBox();
            this.O1Txt = new System.Windows.Forms.TextBox();
            this.T1Txt = new System.Windows.Forms.TextBox();
            this.DTxt = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ProjectTxt = new System.Windows.Forms.TextBox();
            this.TestTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ArtsTxt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.WorkTxt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.PracticalTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.OtherTxt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.OralTxt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TotalTxt = new System.Windows.Forms.TextBox();
            this.DailyTxt = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.O2Txt = new System.Windows.Forms.TextBox();
            this.P2Txt = new System.Windows.Forms.TextBox();
            this.W1Txt = new System.Windows.Forms.TextBox();
            this.T2Txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkKhaki;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel2.Controls.Add(this.OralTxt1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label20, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.PracticalTxt1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label19, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.WritingTxt, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.TotalTxt1, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.label23, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label24, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(880, 281);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(299, 96);
            this.tableLayoutPanel2.TabIndex = 78;
            // 
            // OralTxt1
            // 
            this.OralTxt1.Location = new System.Drawing.Point(7, 52);
            this.OralTxt1.Name = "OralTxt1";
            this.OralTxt1.Size = new System.Drawing.Size(38, 25);
            this.OralTxt1.TabIndex = 0;
            this.OralTxt1.TextChanged += new System.EventHandler(this.OralTxt1_TextChanged);
            this.OralTxt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OralTxt1_KeyPress);
            this.OralTxt1.Leave += new System.EventHandler(this.OralTxt1_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(211, 2);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 18);
            this.label20.TabIndex = 22;
            this.label20.Text = "Total";
            // 
            // PracticalTxt1
            // 
            this.PracticalTxt1.Location = new System.Drawing.Point(53, 52);
            this.PracticalTxt1.Name = "PracticalTxt1";
            this.PracticalTxt1.Size = new System.Drawing.Size(64, 25);
            this.PracticalTxt1.TabIndex = 1;
            this.PracticalTxt1.TextChanged += new System.EventHandler(this.PracticalTxt1_TextChanged);
            this.PracticalTxt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PracticalTxt1_KeyPress);
            this.PracticalTxt1.Leave += new System.EventHandler(this.PracticalTxt1_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(133, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 18);
            this.label19.TabIndex = 21;
            this.label19.Text = "Writing ";
            // 
            // WritingTxt
            // 
            this.WritingTxt.Location = new System.Drawing.Point(133, 52);
            this.WritingTxt.Name = "WritingTxt";
            this.WritingTxt.Size = new System.Drawing.Size(64, 25);
            this.WritingTxt.TabIndex = 2;
            this.WritingTxt.TextChanged += new System.EventHandler(this.WritingTxt_TextChanged);
            this.WritingTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WritingTxt_KeyPress);
            this.WritingTxt.Leave += new System.EventHandler(this.WritingTxt_Leave);
            // 
            // TotalTxt1
            // 
            this.TotalTxt1.Location = new System.Drawing.Point(211, 52);
            this.TotalTxt1.Name = "TotalTxt1";
            this.TotalTxt1.ReadOnly = true;
            this.TotalTxt1.Size = new System.Drawing.Size(64, 25);
            this.TotalTxt1.TabIndex = 3;
            this.TotalTxt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TotalTxt1_KeyPress);
            this.TotalTxt1.Leave += new System.EventHandler(this.TotalTxt1_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 2);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 18);
            this.label23.TabIndex = 13;
            this.label23.Text = "Oral";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(53, 2);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 18);
            this.label24.TabIndex = 14;
            this.label24.Text = "Practical";
            // 
            // SubBox
            // 
            this.SubBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SubBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SubBox.BackColor = System.Drawing.Color.DarkKhaki;
            this.SubBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubBox.FormattingEnabled = true;
            this.SubBox.Items.AddRange(new object[] {
            "English",
            "Marathi",
            "Hindi",
            "Urdu",
            "Math",
            "G Science",
            "Social Science",
            "Arts",
            "Work Exp",
            "Physical Edu"});
            this.SubBox.Location = new System.Drawing.Point(334, 17);
            this.SubBox.Name = "SubBox";
            this.SubBox.Size = new System.Drawing.Size(121, 30);
            this.SubBox.TabIndex = 1;
            this.SubBox.SelectedIndexChanged += new System.EventHandler(this.SubBox_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GrayText;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1208, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(162, 750);
            this.panel2.TabIndex = 81;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GrayText;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(162, 750);
            this.panel1.TabIndex = 80;
            // 
            // IdBox
            // 
            this.IdBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.IdBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.IdBox.BackColor = System.Drawing.Color.DarkKhaki;
            this.IdBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdBox.FormattingEnabled = true;
            this.IdBox.Location = new System.Drawing.Point(115, 19);
            this.IdBox.Name = "IdBox";
            this.IdBox.Size = new System.Drawing.Size(139, 30);
            this.IdBox.TabIndex = 0;
            this.IdBox.SelectedIndexChanged += new System.EventHandler(this.IdBox_SelectedIndexChanged);
            // 
            // NameTxt
            // 
            this.NameTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.NameTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.NameTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.NameTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTxt.Location = new System.Drawing.Point(115, 71);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(388, 29);
            this.NameTxt.TabIndex = 1;
            this.NameTxt.Leave += new System.EventHandler(this.NameTxt_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 18);
            this.label3.TabIndex = 70;
            this.label3.Text = "Gen.Reg.No.  :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 18);
            this.label4.TabIndex = 72;
            this.label4.Text = "Stud. Name  :";
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.Color.DarkKhaki;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th",
            "8 th",
            "9 th",
            "10 th"});
            this.ClassBox.Location = new System.Drawing.Point(113, 17);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 30);
            this.ClassBox.TabIndex = 0;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(254, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 18);
            this.label2.TabIndex = 75;
            this.label2.Text = "Select Sub :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 18);
            this.label1.TabIndex = 74;
            this.label1.Text = "Selecte Class :";
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.SaveBtn.Location = new System.Drawing.Point(31, 16);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(96, 35);
            this.SaveBtn.TabIndex = 2;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkKhaki;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.NameTxt);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.IdBox);
            this.panel3.Location = new System.Drawing.Point(431, 113);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(547, 137);
            this.panel3.TabIndex = 84;
            // 
            // ClearBtn
            // 
            this.ClearBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.ClearBtn.Location = new System.Drawing.Point(139, 16);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(96, 35);
            this.ClearBtn.TabIndex = 3;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = false;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // PrTxt
            // 
            this.PrTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.PrTxt.Location = new System.Drawing.Point(296, 5);
            this.PrTxt.Name = "PrTxt";
            this.PrTxt.ReadOnly = true;
            this.PrTxt.Size = new System.Drawing.Size(68, 25);
            this.PrTxt.TabIndex = 4;
            // 
            // TTxt
            // 
            this.TTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.TTxt.Location = new System.Drawing.Point(373, 5);
            this.TTxt.Name = "TTxt";
            this.TTxt.ReadOnly = true;
            this.TTxt.Size = new System.Drawing.Size(58, 25);
            this.TTxt.TabIndex = 5;
            // 
            // ATxt
            // 
            this.ATxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ATxt.Location = new System.Drawing.Point(223, 5);
            this.ATxt.Name = "ATxt";
            this.ATxt.ReadOnly = true;
            this.ATxt.Size = new System.Drawing.Size(65, 25);
            this.ATxt.TabIndex = 3;
            // 
            // WTxt
            // 
            this.WTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.WTxt.Location = new System.Drawing.Point(439, 5);
            this.WTxt.Name = "WTxt";
            this.WTxt.ReadOnly = true;
            this.WTxt.Size = new System.Drawing.Size(66, 25);
            this.WTxt.TabIndex = 6;
            // 
            // P1Txt
            // 
            this.P1Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.P1Txt.Location = new System.Drawing.Point(145, 5);
            this.P1Txt.Name = "P1Txt";
            this.P1Txt.ReadOnly = true;
            this.P1Txt.Size = new System.Drawing.Size(68, 25);
            this.P1Txt.TabIndex = 2;
            this.P1Txt.TextChanged += new System.EventHandler(this.P1Txt_TextChanged);
            // 
            // OTxt
            // 
            this.OTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.OTxt.Location = new System.Drawing.Point(513, 5);
            this.OTxt.Name = "OTxt";
            this.OTxt.ReadOnly = true;
            this.OTxt.Size = new System.Drawing.Size(60, 25);
            this.OTxt.TabIndex = 7;
            // 
            // O1Txt
            // 
            this.O1Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.O1Txt.Location = new System.Drawing.Point(65, 5);
            this.O1Txt.Name = "O1Txt";
            this.O1Txt.ReadOnly = true;
            this.O1Txt.Size = new System.Drawing.Size(68, 25);
            this.O1Txt.TabIndex = 1;
            // 
            // T1Txt
            // 
            this.T1Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.T1Txt.Location = new System.Drawing.Point(581, 5);
            this.T1Txt.Name = "T1Txt";
            this.T1Txt.ReadOnly = true;
            this.T1Txt.Size = new System.Drawing.Size(68, 25);
            this.T1Txt.TabIndex = 8;
            // 
            // DTxt
            // 
            this.DTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.DTxt.Location = new System.Drawing.Point(7, 5);
            this.DTxt.Name = "DTxt";
            this.DTxt.ReadOnly = true;
            this.DTxt.Size = new System.Drawing.Size(50, 25);
            this.DTxt.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.DarkKhaki;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel3.ColumnCount = 10;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel3.Controls.Add(this.DTxt, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.T1Txt, 9, 0);
            this.tableLayoutPanel3.Controls.Add(this.O1Txt, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.OTxt, 8, 0);
            this.tableLayoutPanel3.Controls.Add(this.P1Txt, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.WTxt, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.ATxt, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.TTxt, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.PrTxt, 5, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(16, 155);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(677, 40);
            this.tableLayoutPanel3.TabIndex = 85;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(145, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 18);
            this.label12.TabIndex = 14;
            this.label12.Text = "Practical";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(65, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 18);
            this.label11.TabIndex = 13;
            this.label11.Text = "Oral";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 36);
            this.label7.TabIndex = 12;
            this.label7.Text = "Daily Plan";
            // 
            // ProjectTxt
            // 
            this.ProjectTxt.Location = new System.Drawing.Point(296, 52);
            this.ProjectTxt.Name = "ProjectTxt";
            this.ProjectTxt.Size = new System.Drawing.Size(68, 25);
            this.ProjectTxt.TabIndex = 4;
            this.ProjectTxt.TextChanged += new System.EventHandler(this.ProjectTxt_TextChanged);
            this.ProjectTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ProjectTxt_KeyPress);
            this.ProjectTxt.Leave += new System.EventHandler(this.ProjectTxt_Leave);
            // 
            // TestTxt
            // 
            this.TestTxt.Location = new System.Drawing.Point(373, 52);
            this.TestTxt.Name = "TestTxt";
            this.TestTxt.Size = new System.Drawing.Size(58, 25);
            this.TestTxt.TabIndex = 5;
            this.TestTxt.TextChanged += new System.EventHandler(this.TestTxt_TextChanged);
            this.TestTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TestTxt_KeyPress);
            this.TestTxt.Leave += new System.EventHandler(this.TestTxt_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(373, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 18);
            this.label13.TabIndex = 15;
            this.label13.Text = "Test";
            // 
            // ArtsTxt
            // 
            this.ArtsTxt.Location = new System.Drawing.Point(223, 52);
            this.ArtsTxt.Name = "ArtsTxt";
            this.ArtsTxt.Size = new System.Drawing.Size(65, 25);
            this.ArtsTxt.TabIndex = 3;
            this.ArtsTxt.TextChanged += new System.EventHandler(this.ArtsTxt_TextChanged);
            this.ArtsTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ArtsTxt_KeyPress);
            this.ArtsTxt.Leave += new System.EventHandler(this.ArtsTxt_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(439, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 18);
            this.label14.TabIndex = 16;
            this.label14.Text = "Work";
            // 
            // WorkTxt
            // 
            this.WorkTxt.Location = new System.Drawing.Point(439, 52);
            this.WorkTxt.Name = "WorkTxt";
            this.WorkTxt.Size = new System.Drawing.Size(66, 25);
            this.WorkTxt.TabIndex = 6;
            this.WorkTxt.TextChanged += new System.EventHandler(this.WorkTxt_TextChanged);
            this.WorkTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WorkTxt_KeyPress);
            this.WorkTxt.Leave += new System.EventHandler(this.WorkTxt_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(513, 2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 18);
            this.label15.TabIndex = 17;
            this.label15.Text = "Other";
            // 
            // PracticalTxt
            // 
            this.PracticalTxt.Location = new System.Drawing.Point(145, 52);
            this.PracticalTxt.Name = "PracticalTxt";
            this.PracticalTxt.Size = new System.Drawing.Size(68, 25);
            this.PracticalTxt.TabIndex = 2;
            this.PracticalTxt.TextChanged += new System.EventHandler(this.PracticalTxt_TextChanged);
            this.PracticalTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PracticalTxt_KeyPress);
            this.PracticalTxt.Leave += new System.EventHandler(this.PracticalTxt_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(581, 2);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 18);
            this.label16.TabIndex = 18;
            this.label16.Text = "Total";
            // 
            // OtherTxt
            // 
            this.OtherTxt.Location = new System.Drawing.Point(513, 52);
            this.OtherTxt.Name = "OtherTxt";
            this.OtherTxt.Size = new System.Drawing.Size(60, 25);
            this.OtherTxt.TabIndex = 7;
            this.OtherTxt.TextChanged += new System.EventHandler(this.OtherTxt_TextChanged);
            this.OtherTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OtherTxt_KeyPress);
            this.OtherTxt.Leave += new System.EventHandler(this.OtherTxt_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(223, 2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 18);
            this.label21.TabIndex = 23;
            this.label21.Text = "Art\'s";
            // 
            // OralTxt
            // 
            this.OralTxt.Location = new System.Drawing.Point(65, 52);
            this.OralTxt.Name = "OralTxt";
            this.OralTxt.Size = new System.Drawing.Size(68, 25);
            this.OralTxt.TabIndex = 1;
            this.OralTxt.TextChanged += new System.EventHandler(this.OralTxt_TextChanged);
            this.OralTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OralTxt_KeyPress);
            this.OralTxt.Leave += new System.EventHandler(this.OralTxt_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(296, 2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 18);
            this.label22.TabIndex = 24;
            this.label22.Text = "Project";
            // 
            // TotalTxt
            // 
            this.TotalTxt.Location = new System.Drawing.Point(581, 52);
            this.TotalTxt.Name = "TotalTxt";
            this.TotalTxt.ReadOnly = true;
            this.TotalTxt.Size = new System.Drawing.Size(68, 25);
            this.TotalTxt.TabIndex = 8;
            this.TotalTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TotalTxt_KeyPress);
            this.TotalTxt.Leave += new System.EventHandler(this.TotalTxt_Leave);
            // 
            // DailyTxt
            // 
            this.DailyTxt.Location = new System.Drawing.Point(7, 52);
            this.DailyTxt.Name = "DailyTxt";
            this.DailyTxt.Size = new System.Drawing.Size(50, 25);
            this.DailyTxt.TabIndex = 0;
            this.DailyTxt.TextChanged += new System.EventHandler(this.DailyTxt_TextChanged);
            this.DailyTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DailyTxt_KeyPress);
            this.DailyTxt.Leave += new System.EventHandler(this.DailyTxt_Leave);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DarkKhaki;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel1.Controls.Add(this.DailyTxt, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TotalTxt, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.label22, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.OralTxt, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label21, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.OtherTxt, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.label16, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.PracticalTxt, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label15, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.WorkTxt, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.ArtsTxt, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.TestTxt, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.ProjectTxt, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(191, 279);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(677, 96);
            this.tableLayoutPanel1.TabIndex = 76;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.DarkKhaki;
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel4.Controls.Add(this.O2Txt, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.P2Txt, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.W1Txt, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.T2Txt, 4, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(880, 420);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(299, 40);
            this.tableLayoutPanel4.TabIndex = 86;
            // 
            // O2Txt
            // 
            this.O2Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.O2Txt.Location = new System.Drawing.Point(7, 5);
            this.O2Txt.Name = "O2Txt";
            this.O2Txt.ReadOnly = true;
            this.O2Txt.Size = new System.Drawing.Size(38, 25);
            this.O2Txt.TabIndex = 0;
            // 
            // P2Txt
            // 
            this.P2Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.P2Txt.Location = new System.Drawing.Point(53, 5);
            this.P2Txt.Name = "P2Txt";
            this.P2Txt.ReadOnly = true;
            this.P2Txt.Size = new System.Drawing.Size(64, 25);
            this.P2Txt.TabIndex = 1;
            // 
            // W1Txt
            // 
            this.W1Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.W1Txt.Location = new System.Drawing.Point(133, 5);
            this.W1Txt.Name = "W1Txt";
            this.W1Txt.ReadOnly = true;
            this.W1Txt.Size = new System.Drawing.Size(64, 25);
            this.W1Txt.TabIndex = 2;
            // 
            // T2Txt
            // 
            this.T2Txt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.T2Txt.Location = new System.Drawing.Point(211, 5);
            this.T2Txt.Name = "T2Txt";
            this.T2Txt.ReadOnly = true;
            this.T2Txt.Size = new System.Drawing.Size(64, 25);
            this.T2Txt.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(471, 388);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 22);
            this.label5.TabIndex = 87;
            this.label5.Text = "--- OUT OF ---";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(974, 390);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 22);
            this.label6.TabIndex = 88;
            this.label6.Text = "--- OUT OF ---";
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AcdYearBox.BackColor = System.Drawing.Color.DarkKhaki;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Items.AddRange(new object[] {
            "English",
            "Marathi",
            "Hindi",
            "Urdu",
            "Math",
            "G Science",
            "Social Science",
            "Arts",
            "Work Exp",
            "Physical Edu"});
            this.AcdYearBox.Location = new System.Drawing.Point(554, 17);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(121, 30);
            this.AcdYearBox.TabIndex = 89;
            this.AcdYearBox.SelectedIndexChanged += new System.EventHandler(this.AcdYearBox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(474, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 18);
            this.label8.TabIndex = 90;
            this.label8.Text = "Acd.Year :";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.AcdYearBox);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.ClassBox);
            this.panel4.Controls.Add(this.SubBox);
            this.panel4.Location = new System.Drawing.Point(341, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(689, 62);
            this.panel4.TabIndex = 91;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.tableLayoutPanel3);
            this.panel5.Location = new System.Drawing.Point(174, 264);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1022, 214);
            this.panel5.TabIndex = 92;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.SaveBtn);
            this.panel6.Controls.Add(this.ClearBtn);
            this.panel6.Location = new System.Drawing.Point(849, 497);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(272, 70);
            this.panel6.TabIndex = 93;
            // 
            // StudMarks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1370, 750);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StudMarks";
            this.Text = "Student Marks";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.StudMarks_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox OralTxt1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox PracticalTxt1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox WritingTxt;
        private System.Windows.Forms.TextBox TotalTxt1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox SubBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox IdBox;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.TextBox PrTxt;
        private System.Windows.Forms.TextBox TTxt;
        private System.Windows.Forms.TextBox ATxt;
        private System.Windows.Forms.TextBox WTxt;
        private System.Windows.Forms.TextBox P1Txt;
        private System.Windows.Forms.TextBox OTxt;
        private System.Windows.Forms.TextBox O1Txt;
        private System.Windows.Forms.TextBox T1Txt;
        private System.Windows.Forms.TextBox DTxt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ProjectTxt;
        private System.Windows.Forms.TextBox TestTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox ArtsTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox WorkTxt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox PracticalTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox OtherTxt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox OralTxt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TotalTxt;
        private System.Windows.Forms.TextBox DailyTxt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox O2Txt;
        private System.Windows.Forms.TextBox P2Txt;
        private System.Windows.Forms.TextBox W1Txt;
        private System.Windows.Forms.TextBox T2Txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
    }
}