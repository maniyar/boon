﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace Boon
{
    public partial class BusDetails : Form
    {
        public BusDetails()
        {
            InitializeComponent();
        }
        common c = new common();

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            try
            {
                if (VehicleIDBox.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    VehicleIDBox.Focus();
                    MessageBox.Show("Please select Vehicle ID first");
                    return;

                }


                String SelectQuery = "SELECT * from Boon.busaddmisions WHERE Vehicle_ID = '" + VehicleIDBox.Text + "'";
                DataTable table = new DataTable();
                table = c.SelectData(SelectQuery);
                foreach (DataRow item in table.Rows)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                    dataGridView1.Rows[n].Cells[1].Value = item["Name_of_student"].ToString();
                    dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                    dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                    dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                }
            }

            catch { }


        }

        private void BusDetails_Load(object sender, EventArgs e)
        {
            try
            {
                VehicleIDBox.Items.Clear();
                String Str = "select Vechile_ID from Boon.bus ";

                DataTable tb = c.SelectData(Str);
                if (tb.Rows.Count > 0)
                {
                    foreach (DataRow row in tb.Rows)
                    {
                        VehicleIDBox.Items.Add(row["Vechile_ID"].ToString());
                    }
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[5].Value = row.Cells[5].Value == null ? false : !(bool)row.Cells[5].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private Boolean sendsms()
        {
            String username = "BOONSL";
            String pass = "Gateway@1";
            int i = 0, j = 0;
           
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {
                        string message = "Dear Parents, Your child " + row.Cells[1].Value.ToString() +" "+ MessageTxt.Text;
                        //  //  StudID = row.Cells[0].Value.ToString();
                        var number = row.Cells[3].Value.ToString();
                        string no = ("+91" + number).ToString();

                        UriBuilder urlBuilder = new UriBuilder();
                        string createdURL = "http://mysms.gatewayinfotech.in/http-api.php?username=" + username + "&password=" + pass + "&senderid=BOONSL&route=2&number=" + no + "&message=" + message + "";
                        HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(createdURL, false));
                        httpReq.Timeout = 7200;
                        HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse());



                        //string baseURL = "http://mysms.gatewayinfotech.in/http-api.php?username=" + username + "&password=" + pass + "&senderid=BOONSL&route=2&number=" + number + "&message=" + message + "";
                        //client.OpenRead(baseURL);
                        //  MessageBox.Show("Successfully sent message");  
                        ++i;
                        httpReq.Abort();


                    }
                    else
                    { ++j; }


                }

                if (i + j == dataGridView1.Rows.Count)
                {
                    MessageBox.Show("Successfully sent message");

                }
                return true;

            }
            catch (Exception ex)
            {
                // ++j;

                MessageBox.Show(ex.Message, "Alert");
                return false;
            }
            finally
            {


            }
        }
        private void SmsSend_Click(object sender, EventArgs e)
        {
            int i1 = 0;
            try
            {
                if (MessageBox.Show("Do you want to send sms?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in this.dataGridView1.Rows)
                    {

                        //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                        if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                        {
                            ++i1;
                            if (i1 == dataGridView1.Rows.Count)
                            {
                                MessageBox.Show("You have to select atleat one student", "Warnning");
                                return;
                            }
                        }
                        else
                        {
                            sendsms();
                            break;
                        }
                    }
                }

            }
            catch { }
        }
        public void PassValue()
        {
            try
            {
                int i = 0;
                int j = 0, Up = 0, a = 0;
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {
                        ++i;
                        var SelectedID = row.Cells[0].Value.ToString();
                        String Str = "Delete From Boon.busaddmisions where ID= '" + SelectedID + "'";
                        if (c.InsertData(Str))
                        {
                            ++j;
                        }
                        else { }
                    }

                }
                if (i + j == dataGridView1.Rows.Count)
                {
                    MessageBox.Show("Students deleted","Alert");
                    this.dataGridView1.DataSource = null;
                    dataGridView1.Rows.Clear();
                    String SelectQuery = "SELECT * from Boon.busaddmisions WHERE Vehicle_ID = '" + VehicleIDBox.Text + "'";
                    DataTable table = new DataTable();
                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name_of_student"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                    }
                }
            }
            catch { }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int i1=0;
                if (MessageBox.Show("Do you want to delete students?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in this.dataGridView1.Rows)
                    {
                        //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                        if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                        {
                            i1++;
                            if (i1 == dataGridView1.Rows.Count)
                            {
                                MessageBox.Show("You have to select at least one student", "Alert");
                            }

                        }
                        else
                        {
                            PassValue();
                            break;

                        }

                    }
                }
            }
            catch { }
        }

        private void MessageTxt_TextChanged(object sender, EventArgs e)
        {
            if (MessageTxt.TextLength < 160)
            {
                MessageCount.Text = "-1 Message -";
            }
            if (MessageTxt.TextLength > 160)
            {
                MessageCount.Text = "-2 Message -";
            }
            if (MessageTxt.TextLength > 320)
            {
                MessageCount.Text = "-3 Message -";
            }
            if (MessageTxt.TextLength > 480)
            {
                MessageCount.Text = "-4 Message -";
            }
            
        }
    }
}
