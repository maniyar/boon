﻿namespace Boon
{
    partial class IdSearchReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ReceiptsNoTxt = new System.Windows.Forms.TextBox();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IdTxt = new System.Windows.Forms.ComboBox();
            this.GenSearchBtn = new System.Windows.Forms.Button();
            this.NameSearchBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ReceiptNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EducationFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TCFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BonFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarkFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtherFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "Nursery",
            "Jr.KG",
            "Sr.KG",
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th",
            "8 th",
            "9 th",
            "10 th"});
            this.ClassBox.Location = new System.Drawing.Point(101, 15);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(115, 30);
            this.ClassBox.TabIndex = 55;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 22);
            this.label4.TabIndex = 8;
            this.label4.Text = "Class :";
            // 
            // ReceiptsNoTxt
            // 
            this.ReceiptsNoTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceiptsNoTxt.Location = new System.Drawing.Point(1043, 22);
            this.ReceiptsNoTxt.Name = "ReceiptsNoTxt";
            this.ReceiptsNoTxt.Size = new System.Drawing.Size(109, 29);
            this.ReceiptsNoTxt.TabIndex = 7;
            // 
            // NameTxt
            // 
            this.NameTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.NameTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.NameTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTxt.Location = new System.Drawing.Point(489, 22);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(303, 29);
            this.NameTxt.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(944, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 22);
            this.label3.TabIndex = 4;
            this.label3.Text = "Receipt No. :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 22);
            this.label2.TabIndex = 3;
            this.label2.Text = "Reg.No. :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(370, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Student Name :";
            // 
            // SearchBtn
            // 
            this.SearchBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.SearchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SearchBtn.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.ForeColor = System.Drawing.Color.White;
            this.SearchBtn.Location = new System.Drawing.Point(1169, 19);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(104, 34);
            this.SearchBtn.TabIndex = 0;
            this.SearchBtn.Text = "Search";
            this.SearchBtn.UseVisualStyleBackColor = false;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ReceiptNo,
            this.StudName,
            this.Class,
            this.Date,
            this.EducationFee,
            this.ExamFee,
            this.RegFee,
            this.TCFee,
            this.BonFee,
            this.ComFee,
            this.MarkFee,
            this.OtherFee,
            this.TotalFee});
            this.dataGridView1.Location = new System.Drawing.Point(8, 253);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1347, 476);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            // 
            // IdTxt
            // 
            this.IdTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.IdTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdTxt.FormattingEnabled = true;
            this.IdTxt.Location = new System.Drawing.Point(102, 21);
            this.IdTxt.Name = "IdTxt";
            this.IdTxt.Size = new System.Drawing.Size(115, 30);
            this.IdTxt.TabIndex = 56;
            // 
            // GenSearchBtn
            // 
            this.GenSearchBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.GenSearchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GenSearchBtn.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenSearchBtn.ForeColor = System.Drawing.Color.White;
            this.GenSearchBtn.Location = new System.Drawing.Point(234, 19);
            this.GenSearchBtn.Name = "GenSearchBtn";
            this.GenSearchBtn.Size = new System.Drawing.Size(104, 34);
            this.GenSearchBtn.TabIndex = 57;
            this.GenSearchBtn.Text = "Search";
            this.GenSearchBtn.UseVisualStyleBackColor = false;
            this.GenSearchBtn.Click += new System.EventHandler(this.GenSearchBtn_Click);
            // 
            // NameSearchBtn
            // 
            this.NameSearchBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.NameSearchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.NameSearchBtn.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameSearchBtn.ForeColor = System.Drawing.Color.White;
            this.NameSearchBtn.Location = new System.Drawing.Point(809, 19);
            this.NameSearchBtn.Name = "NameSearchBtn";
            this.NameSearchBtn.Size = new System.Drawing.Size(104, 34);
            this.NameSearchBtn.TabIndex = 58;
            this.NameSearchBtn.Text = "Search";
            this.NameSearchBtn.UseVisualStyleBackColor = false;
            this.NameSearchBtn.Click += new System.EventHandler(this.NameSearchBtn_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.ClassBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(304, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(754, 65);
            this.panel1.TabIndex = 56;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(620, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 34);
            this.button1.TabIndex = 60;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.ClassBtnSearch_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(437, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 22);
            this.label6.TabIndex = 59;
            this.label6.Text = "To :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(234, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 22);
            this.label5.TabIndex = 58;
            this.label5.Text = "From :";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(493, 18);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(109, 25);
            this.dateTimePicker2.TabIndex = 57;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(310, 18);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(109, 25);
            this.dateTimePicker1.TabIndex = 56;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.SearchBtn);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.IdTxt);
            this.panel2.Controls.Add(this.NameSearchBtn);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ReceiptsNoTxt);
            this.panel2.Controls.Add(this.GenSearchBtn);
            this.panel2.Controls.Add(this.NameTxt);
            this.panel2.Location = new System.Drawing.Point(31, 134);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1301, 73);
            this.panel2.TabIndex = 59;
            // 
            // ReceiptNo
            // 
            this.ReceiptNo.HeaderText = "Receipt No";
            this.ReceiptNo.Name = "ReceiptNo";
            this.ReceiptNo.ReadOnly = true;
            // 
            // StudName
            // 
            this.StudName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StudName.HeaderText = "Student Name";
            this.StudName.Name = "StudName";
            this.StudName.ReadOnly = true;
            // 
            // Class
            // 
            this.Class.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Class.HeaderText = "Class";
            this.Class.Name = "Class";
            this.Class.ReadOnly = true;
            this.Class.Width = 65;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // EducationFee
            // 
            this.EducationFee.HeaderText = "First Term Fee";
            this.EducationFee.Name = "EducationFee";
            this.EducationFee.ReadOnly = true;
            // 
            // ExamFee
            // 
            this.ExamFee.HeaderText = "Second Term Fee";
            this.ExamFee.Name = "ExamFee";
            this.ExamFee.ReadOnly = true;
            // 
            // RegFee
            // 
            this.RegFee.HeaderText = "Registration Fee";
            this.RegFee.Name = "RegFee";
            this.RegFee.ReadOnly = true;
            // 
            // TCFee
            // 
            this.TCFee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TCFee.HeaderText = "T.C. Fee";
            this.TCFee.Name = "TCFee";
            this.TCFee.ReadOnly = true;
            this.TCFee.Width = 72;
            // 
            // BonFee
            // 
            this.BonFee.HeaderText = "Bonafide Fee";
            this.BonFee.Name = "BonFee";
            this.BonFee.ReadOnly = true;
            // 
            // ComFee
            // 
            this.ComFee.HeaderText = "Computer Fee";
            this.ComFee.Name = "ComFee";
            this.ComFee.ReadOnly = true;
            // 
            // MarkFee
            // 
            this.MarkFee.HeaderText = "Mark Memo Fee";
            this.MarkFee.Name = "MarkFee";
            this.MarkFee.ReadOnly = true;
            // 
            // OtherFee
            // 
            this.OtherFee.HeaderText = "Other Fee";
            this.OtherFee.Name = "OtherFee";
            this.OtherFee.ReadOnly = true;
            // 
            // TotalFee
            // 
            this.TotalFee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TotalFee.HeaderText = "Total";
            this.TotalFee.Name = "TotalFee";
            this.TotalFee.ReadOnly = true;
            this.TotalFee.Width = 64;
            // 
            // IdSearchReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "IdSearchReceipt";
            this.Text = "Serch Receipt";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ReceiptsNoTxt;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.ComboBox IdTxt;
        private System.Windows.Forms.Button GenSearchBtn;
        private System.Windows.Forms.Button NameSearchBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiptNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Class;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn EducationFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn TCFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn BonFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarkFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn OtherFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalFee;
    }
}