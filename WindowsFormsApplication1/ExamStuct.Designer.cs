﻿namespace Boon
{
    partial class ExamStuct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.DailyTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.OralTxt = new System.Windows.Forms.TextBox();
            this.PracticalTxt = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TotalTxt = new System.Windows.Forms.TextBox();
            this.OtherTxt = new System.Windows.Forms.TextBox();
            this.WorkTxt = new System.Windows.Forms.TextBox();
            this.ArtsTxt = new System.Windows.Forms.TextBox();
            this.TestTxt = new System.Windows.Forms.TextBox();
            this.ProjectTxt = new System.Windows.Forms.TextBox();
            this.SubBox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.OralTxt1 = new System.Windows.Forms.TextBox();
            this.PracticalTxt1 = new System.Windows.Forms.TextBox();
            this.WritingTxt = new System.Windows.Forms.TextBox();
            this.TotalTxt1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.SaveBtn.Location = new System.Drawing.Point(958, 323);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(96, 35);
            this.SaveBtn.TabIndex = 0;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Selecte Class :";
            // 
            // DailyTxt
            // 
            this.DailyTxt.Location = new System.Drawing.Point(7, 52);
            this.DailyTxt.Name = "DailyTxt";
            this.DailyTxt.Size = new System.Drawing.Size(56, 25);
            this.DailyTxt.TabIndex = 2;
            this.DailyTxt.TextChanged += new System.EventHandler(this.DailyTxt_TextChanged);
            this.DailyTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DailyTxt_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(179, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select Sub :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 36);
            this.label7.TabIndex = 12;
            this.label7.Text = "Daily Plan";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(71, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 18);
            this.label11.TabIndex = 13;
            this.label11.Text = "Oral";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(151, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 18);
            this.label12.TabIndex = 14;
            this.label12.Text = "Practical";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(379, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 18);
            this.label13.TabIndex = 15;
            this.label13.Text = "Test";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(445, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 18);
            this.label14.TabIndex = 16;
            this.label14.Text = "Work";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(519, 2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 18);
            this.label15.TabIndex = 17;
            this.label15.Text = "Other";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(587, 2);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 18);
            this.label16.TabIndex = 18;
            this.label16.Text = "Total";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(139, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 18);
            this.label19.TabIndex = 21;
            this.label19.Text = "Writing ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(217, 2);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 18);
            this.label20.TabIndex = 22;
            this.label20.Text = "Total";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(229, 2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 18);
            this.label21.TabIndex = 23;
            this.label21.Text = "Art\'s";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(302, 2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 18);
            this.label22.TabIndex = 24;
            this.label22.Text = "Project";
            // 
            // OralTxt
            // 
            this.OralTxt.Location = new System.Drawing.Point(71, 52);
            this.OralTxt.Name = "OralTxt";
            this.OralTxt.Size = new System.Drawing.Size(68, 25);
            this.OralTxt.TabIndex = 25;
            this.OralTxt.TextChanged += new System.EventHandler(this.OralTxt_TextChanged);
            this.OralTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OralTxt_KeyPress);
            // 
            // PracticalTxt
            // 
            this.PracticalTxt.Location = new System.Drawing.Point(151, 52);
            this.PracticalTxt.Name = "PracticalTxt";
            this.PracticalTxt.Size = new System.Drawing.Size(68, 25);
            this.PracticalTxt.TabIndex = 26;
            this.PracticalTxt.TextChanged += new System.EventHandler(this.PracticalTxt_TextChanged);
            this.PracticalTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PracticalTxt_KeyPress);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DarkKhaki;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel1.Controls.Add(this.DailyTxt, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TotalTxt, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.label22, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.OralTxt, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label21, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.OtherTxt, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.label16, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.PracticalTxt, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label15, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.WorkTxt, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.ArtsTxt, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.TestTxt, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.ProjectTxt, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(465, 78);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(677, 96);
            this.tableLayoutPanel1.TabIndex = 27;
            // 
            // TotalTxt
            // 
            this.TotalTxt.Location = new System.Drawing.Point(587, 52);
            this.TotalTxt.Name = "TotalTxt";
            this.TotalTxt.ReadOnly = true;
            this.TotalTxt.Size = new System.Drawing.Size(68, 25);
            this.TotalTxt.TabIndex = 33;
            // 
            // OtherTxt
            // 
            this.OtherTxt.Location = new System.Drawing.Point(519, 52);
            this.OtherTxt.Name = "OtherTxt";
            this.OtherTxt.Size = new System.Drawing.Size(60, 25);
            this.OtherTxt.TabIndex = 32;
            this.OtherTxt.TextChanged += new System.EventHandler(this.OtherTxt_TextChanged);
            this.OtherTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OtherTxt_KeyPress);
            // 
            // WorkTxt
            // 
            this.WorkTxt.Location = new System.Drawing.Point(445, 52);
            this.WorkTxt.Name = "WorkTxt";
            this.WorkTxt.Size = new System.Drawing.Size(66, 25);
            this.WorkTxt.TabIndex = 31;
            this.WorkTxt.TextChanged += new System.EventHandler(this.WorkTxt_TextChanged);
            this.WorkTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WorkTxt_KeyPress);
            // 
            // ArtsTxt
            // 
            this.ArtsTxt.Location = new System.Drawing.Point(229, 52);
            this.ArtsTxt.Name = "ArtsTxt";
            this.ArtsTxt.Size = new System.Drawing.Size(65, 25);
            this.ArtsTxt.TabIndex = 28;
            this.ArtsTxt.TextChanged += new System.EventHandler(this.ArtsTxt_TextChanged);
            this.ArtsTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ArtsTxt_KeyPress);
            // 
            // TestTxt
            // 
            this.TestTxt.Location = new System.Drawing.Point(379, 52);
            this.TestTxt.Name = "TestTxt";
            this.TestTxt.Size = new System.Drawing.Size(58, 25);
            this.TestTxt.TabIndex = 30;
            this.TestTxt.TextChanged += new System.EventHandler(this.TestTxt_TextChanged);
            this.TestTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TestTxt_KeyPress);
            // 
            // ProjectTxt
            // 
            this.ProjectTxt.Location = new System.Drawing.Point(302, 52);
            this.ProjectTxt.Name = "ProjectTxt";
            this.ProjectTxt.Size = new System.Drawing.Size(68, 25);
            this.ProjectTxt.TabIndex = 29;
            this.ProjectTxt.TextChanged += new System.EventHandler(this.ProjectTxt_TextChanged);
            this.ProjectTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ProjectTxt_KeyPress);
            // 
            // SubBox
            // 
            this.SubBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SubBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SubBox.BackColor = System.Drawing.Color.DarkKhaki;
            this.SubBox.FormattingEnabled = true;
            this.SubBox.Items.AddRange(new object[] {
            "English",
            "Marathi",
            "Hindi",
            "Urdu",
            "Math",
            "G Science",
            "Social Science",
            "Arts",
            "Work Exp",
            "Physical Edu"});
            this.SubBox.Location = new System.Drawing.Point(316, 129);
            this.SubBox.Name = "SubBox";
            this.SubBox.Size = new System.Drawing.Size(121, 26);
            this.SubBox.TabIndex = 34;
            this.SubBox.SelectedIndexChanged += new System.EventHandler(this.SubBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkKhaki;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel2.Controls.Add(this.OralTxt1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label20, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.PracticalTxt1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label19, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.WritingTxt, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.TotalTxt1, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.label23, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label24, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(653, 215);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(299, 96);
            this.tableLayoutPanel2.TabIndex = 35;
            // 
            // OralTxt1
            // 
            this.OralTxt1.Location = new System.Drawing.Point(7, 52);
            this.OralTxt1.Name = "OralTxt1";
            this.OralTxt1.Size = new System.Drawing.Size(44, 25);
            this.OralTxt1.TabIndex = 2;
            this.OralTxt1.TextChanged += new System.EventHandler(this.OralTxt1_TextChanged);
            this.OralTxt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OralTxt1_KeyPress);
            // 
            // PracticalTxt1
            // 
            this.PracticalTxt1.Location = new System.Drawing.Point(59, 52);
            this.PracticalTxt1.Name = "PracticalTxt1";
            this.PracticalTxt1.Size = new System.Drawing.Size(64, 25);
            this.PracticalTxt1.TabIndex = 25;
            this.PracticalTxt1.TextChanged += new System.EventHandler(this.PracticalTxt1_TextChanged);
            this.PracticalTxt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PracticalTxt1_KeyPress);
            // 
            // WritingTxt
            // 
            this.WritingTxt.Location = new System.Drawing.Point(139, 52);
            this.WritingTxt.Name = "WritingTxt";
            this.WritingTxt.Size = new System.Drawing.Size(64, 25);
            this.WritingTxt.TabIndex = 26;
            this.WritingTxt.TextChanged += new System.EventHandler(this.WritingTxt_TextChanged);
            this.WritingTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WritingTxt_KeyPress);
            // 
            // TotalTxt1
            // 
            this.TotalTxt1.Location = new System.Drawing.Point(217, 52);
            this.TotalTxt1.Name = "TotalTxt1";
            this.TotalTxt1.ReadOnly = true;
            this.TotalTxt1.Size = new System.Drawing.Size(64, 25);
            this.TotalTxt1.TabIndex = 28;
            this.TotalTxt1.TextChanged += new System.EventHandler(this.TotalTxt1_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 2);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 18);
            this.label23.TabIndex = 13;
            this.label23.Text = "Oral";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(59, 2);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 18);
            this.label24.TabIndex = 14;
            this.label24.Text = "Practical";
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.Color.DarkKhaki;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th",
            "8 th",
            "9 th",
            "10 th"});
            this.ClassBox.Location = new System.Drawing.Point(316, 83);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 30);
            this.ClassBox.TabIndex = 36;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GrayText;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(162, 702);
            this.panel1.TabIndex = 65;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GrayText;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1208, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(162, 702);
            this.panel2.TabIndex = 66;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(168, 375);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1034, 301);
            this.dataGridView1.TabIndex = 67;
            // 
            // ExamStuct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1370, 702);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ClassBox);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.SubBox);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SaveBtn);
            this.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ExamStuct";
            this.Text = "Exam Stucture";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ExamStuct_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DailyTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox OralTxt;
        private System.Windows.Forms.TextBox PracticalTxt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox ArtsTxt;
        private System.Windows.Forms.TextBox ProjectTxt;
        private System.Windows.Forms.TextBox TestTxt;
        private System.Windows.Forms.TextBox WorkTxt;
        private System.Windows.Forms.TextBox OtherTxt;
        private System.Windows.Forms.TextBox TotalTxt;
        private System.Windows.Forms.ComboBox SubBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox OralTxt1;
        private System.Windows.Forms.TextBox PracticalTxt1;
        private System.Windows.Forms.TextBox WritingTxt;
        private System.Windows.Forms.TextBox TotalTxt1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}