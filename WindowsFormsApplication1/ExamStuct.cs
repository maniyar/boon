﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Boon
{
    public partial class ExamStuct : Form
    {
        public ExamStuct()
        {
              InitializeComponent();
        }
        common c = new common();
        int Daily, OralA, OralB, Test, Project, Practical, Arts, Work, Other, PracticalA, Writting;

        //English Marathi Hindi Urdu Math G Science Social Science Arts Work Exp Physical Edu
        String Class;
        Double Total;
        public void ClassBoxName()
        {
            if (ClassBox.Text == "1 st")
            {
               Class  = "First";
        
            }
            if (ClassBox.Text == "2 nd")
            {
                Class = "Second";
            }
            if (ClassBox.Text == "3 rd")
            {
                Class = "Third";
            }
            if (ClassBox.Text == "4 th")
            {
                Class = "Fourth";
            }
            if (ClassBox.Text == "5 th")
            {
                Class = "Fifth";
            }
            if (ClassBox.Text == "6 th")
            {
                Class = "Sixth";
            }
            if (ClassBox.Text == "7 th")
            {
                Class = "Seventh";
            }
        }

        public void TotalMarks()
        {
            var totA = Double.Parse(TotalTxt.Text).ToString();
            var totB = Double.Parse(TotalTxt1.Text).ToString();
            Double a = Double.Parse(totA);
            Double b = Double.Parse(totB);
            Total = a + b;
        }

        public void Load_Grid()
        {
            ClassBoxName();
          
            
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();

            String SelectQuery = "SELECT * from Boon.examtructure where ClassSub Like '"+Class+"%'";
            DataTable table1 = new DataTable();

            table1 = c.SelectData(SelectQuery);
            dataGridView1.DataSource = table1;
            //dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
            //dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
            //dataGridView1.Columns[0].Width = 100;
            //dataGridView1.Columns[1].Width = 250;
            //dataGridView1.Columns[2].Width = 250;
            //dataGridView1.Columns[3].Width = 150;
           
        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (TotalTxt.Text == "0" && TotalTxt.Text == "0")
            {
                MessageBox.Show("Fill the information","Warnning");
            }
            else if (MessageBox.Show("Do you want to save?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    ClassBoxName();
                    TotalMarks();
                    String SelectQuery = "Select * from Boon.examtructure Where ClassSub Like  '%" + SubBox.Text + "%'  AND Class = '" + ClassBox.Text + "' ";
                    DataTable table = new DataTable();
                    table = c.SelectData(SelectQuery);
                    if (table.Rows.Count > 0)
                    {
                        String UpdateQuery = "Update Boon.examtructure Set Class = '" + ClassBox.Text + "',ClassSub = '" + Class + "''" + SubBox.Text + "',DailyPlan = '" + DailyTxt.Text + "',OralA = '" + OralTxt.Text + "',PracticalA = '" + PracticalTxt.Text + "',Arts = '" + ArtsTxt.Text + "',Project = '" + ProjectTxt.Text + "',Test = '" + TestTxt.Text + "',Work = '" + WorkTxt.Text + "',Other = '" + OtherTxt.Text + "',TotalA = '" + TotalTxt.Text + "',OralB = '" + OralTxt1.Text + "',PracticalB = '" + PracticalTxt1.Text + "',Writing = '" + WritingTxt.Text + "',TotalB = '" + TotalTxt1.Text + "',TotalAB = '" + Total + "'";
                        if (c.InsertData(UpdateQuery))
                        {
                            MessageBox.Show("Successfully updated..");
                        }

                    }
                    else
                    {

                        String InsertQuery = "INSERT into Boon.examtructure (Class,ClassSub,DailyPlan,OralA,PracticalA,Arts,Project,Test,Work,Other,TotalA,OralB,PracticalB,Writing,TotalB,TotalAB) VALUES ('" + ClassBox.Text + "','" + Class + "''" + SubBox.Text + "','" + DailyTxt.Text + "','" + OralTxt.Text + "','" + PracticalTxt.Text + "','" + ArtsTxt.Text + "','" + ProjectTxt.Text + "','" + TestTxt.Text + "','" + WorkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + OralTxt1.Text + "','" + PracticalTxt1.Text + "','" + WritingTxt.Text + "','" + TotalTxt1.Text + "','" + Total + "')";
                        if (c.InsertData(InsertQuery))
                        {
                            MessageBox.Show("Successfully saved..");
                        }
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    ClearTxtBoxes();
                }
            }
        }

        public void Totalcount()
        {
            var marks = 0;
            var marks1 = 0;
            // int Daily, OralA, OralB, Test, Project, Practical, Arts, Work, Other, OralB, PracticalA,Writting ;
            try
            {
                if (DailyTxt.Text != "")
                {
                    Daily = int.Parse(DailyTxt.Text);
                    marks = marks + Daily;
                }
                if (OralTxt.Text != "")
                {
                    OralA = int.Parse(OralTxt.Text);
                    marks = marks + OralA;
                }
                if (OralTxt1.Text != "")
                {
                    OralB = int.Parse(OralTxt1.Text);
                    marks1 = marks1 + OralB;
                }
                if (TestTxt.Text != "")
                {
                    Test = int.Parse(TestTxt.Text);
                    marks = marks + Test;
                }
                if (ProjectTxt.Text != "")
                {
                    Project = int.Parse(ProjectTxt.Text);
                    marks = marks + Project;
                }

                if (ArtsTxt.Text != "")
                {
                    Arts = int.Parse(ArtsTxt.Text);
                    marks = marks + Arts;
                }

                if (WorkTxt.Text != "")
                {
                    Work = int.Parse(WorkTxt.Text);
                    marks = marks + Work;
                }
                if (OtherTxt.Text != "")
                {
                    Other = int.Parse(OtherTxt.Text);
                    marks = marks + Other;
                }
                if (PracticalTxt.Text != "")
                {
                    Practical = int.Parse(PracticalTxt.Text);
                    marks = marks + Practical;
                }
                if (PracticalTxt1.Text != "")
                {
                    PracticalA = int.Parse(PracticalTxt1.Text);
                    marks1 = marks1 + PracticalA;
                }
                if (WritingTxt.Text != "")
                {
                    Writting = int.Parse(WritingTxt.Text);
                    marks1 = marks1 + Writting;
                }

                //   var  Total1 = Daily + OralA + Test + Project + Practical + Arts + Work + Other;
                //  var   Total2 = OralB + PracticalA + Writting;
                TotalTxt.Text = marks.ToString();
                TotalTxt1.Text = marks1.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ClearTxtBoxes()
        {
            DailyTxt.Text = null;
            OralTxt.Text = null;
            OralTxt1.Text = null;
            PracticalTxt.Text = null;
            ProjectTxt.Text = null;
            TotalTxt.Text = null;
            TotalTxt1.Text = null;
            TestTxt.Text = null;
            ArtsTxt.Text = null;
            WorkTxt.Text = null;
            OtherTxt.Text = null;
            PracticalTxt1.Text = null;
            WritingTxt.Text = null;
        }


        private void SubBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_Grid();
        }

        private void ExamStuct_Load(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void DailyTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void OralTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void PracticalTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void ArtsTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void ProjectTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void TestTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void WorkTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void OtherTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void OralTxt1_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void PracticalTxt1_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void WritingTxt_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void TotalTxt1_TextChanged(object sender, EventArgs e)
        {
            Totalcount();
        }

        private void DailyTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void OralTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void PracticalTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void ArtsTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void ProjectTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void TestTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void WorkTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void OtherTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void OralTxt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void PracticalTxt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void WritingTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }
    }
}
