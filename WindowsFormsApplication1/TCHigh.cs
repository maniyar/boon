﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using MySql.Data.MySqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Configuration;


namespace Boon
{
    public partial class TCHigh : Form
    {
        ErrorProvider errorProvider1 = new ErrorProvider();
        DataSet ds;
        MySqlConnection mysql;
        common c = new common();
        DataTable Dt = new DataTable();
        public TCHigh()
        {
            mysql = new MySqlConnection();
            mysql.ConnectionString = c.ConnectionString;
          //  mysql.Open();

           
            InitializeComponent();
        }
        public void LoadAcdYear()
        {
            try
            {
                AcdYearBox.Items.Clear();
                mysql.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = mysql;
                //Cmd.CommandText =
                Cmd.CommandText = "select Distinct(AcdYear) from Boon.tc_high where Std = '" + StdBox.Text + "' order by  AcdYear ";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["AcdYear"].ToString();
                    AcdYearBox.Items.Add(i);

                }
                dr.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                mysql.Close();
            }
        }
        private void GenrateTc()
        {
            try
            {
                string SelectQuery = "SELECT * from Boon.tc_high WHERE Name = '" + NameTxt.Text + "' AND Std ='" + StdBox.Text + "' ";
                Dt = c.SelectData(SelectQuery);
                if (Dt.Rows.Count > 0)
                {
                    MySqlCommand Cmd = new MySqlCommand();





                    Cmd.CommandText = "SELECT * from Boon.tc_high WHERE Name = '" + NameTxt.Text + "' AND Std ='" + StdBox.Text + "' ";
                  

                    Cmd.Connection = mysql;
                    ds = new DataSet();
                    MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                    mda.Fill(ds, "tc");
                    TCReport cr = new TCReport();
                  //  HighTCReport cr = new HighTCReport();
                    cr.SetDataSource(ds);
                    crystalReportViewer1.ReportSource = cr;
                }

                else
                {
                    MessageBox.Show("No record found", "Warnning");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GenrateTcByID()
        {
            try
            {
                if (IdTxtBox.Text != "")
                {
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.CommandText = "SELECT * from Boon.tc_high WHERE ID = '" + IdTxtBox.Text + "' AND Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";
                    //  Cmd.CommandText = "SELECT * from Boon.Tcapplication WHERE Name = '" + NameTxt.Text + "' AND Std ='" + comboBox1.Text + "' ";

                    Cmd.Connection = mysql;
                    ds = new DataSet();
                    MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                    mda.Fill(ds, "tc");
                  //  HighTCReport cr = new HighTCReport();
                    TCReport cr = new TCReport();
                    cr.SetDataSource(ds);
                    crystalReportViewer1.ReportSource = cr;
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void StudentID()
        {
            
            try
            {
                IdTxtBox.Items.Clear();
                mysql.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = mysql;

                Cmd.CommandText = "select ID from Boon.tc_high Where Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["ID"].ToString();
                    IdTxtBox.Items.Add(i);

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mysql.Close();
            }
            
        }


        private void StudentName()
        {
            try
            {
                mysql.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = mysql;
                Cmd.CommandText = "select Name from Boon.tc_high Where Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                MySqlDataReader dr1 = Cmd.ExecuteReader();
                AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                while (dr1.Read())
                {
                    data.Add(dr1.GetString(0));

                }
                NameTxt.AutoCompleteCustomSource = data;
                dr1.Close();
            }
            catch { }
            finally
            {
                mysql.Close();
            }
        }

        //private void StudentName()
        //{
        //    try
        //    {
        //        MySqlCommand Cmd = new MySqlCommand();
        //        Cmd.Connection = mysql;
        //        Cmd.CommandText = "select Name from Boon.students Where Std = '" + StdBox.Text + "' ";
        //        MySqlDataReader dr1 = Cmd.ExecuteReader();
        //        AutoCompleteStringCollection data = new AutoCompleteStringCollection();
        //        while (dr1.Read())
        //        {
        //            data.Add(dr1.GetString(0));

        //        }
        //        NameTxt.AutoCompleteCustomSource = data;
        //        dr1.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}
      
        private void TC_Load(object sender, EventArgs e)
        {
          
        }

        private void GenrateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (NameTxt.Text == "")
                {
                    GenrateTcByID();

                }
                else
                {
                    GenrateTc();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

     

        private void NameTxt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                IdTxtBox.Text = null;

                if (StdBox.Text == "" || AcdYearBox.Text == "")
                {
                    
                    StdBox.Focus();
                    MessageBox.Show("Please select standard and Acd.Year of student first");

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      

        private void IDTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (StdBox.Text == "")
            {

                errorProvider1.SetError(StdBox, "Select Std of student first ");
            }
            else
            {
                StudentID();
                StudentName();
            }
        }

        private void TCPrimary_KeyPress(object sender, KeyPressEventArgs e)
        {
           // Char ch = e.KeyChar;
          
            //if (ch == 0) 
            //{
            //   
            //    e.Handled = true;
            //}
            //if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            //{
            //    MessageBox.Show("Enter Letters Only", "Alert");
            //    e.Handled = true;
            //}
            //string chr = (Keys.P).ToString();

            //if (ch == char.Parse(chr))
            //{
            //    crystalReportViewer1.PrintReport();
            //    e.Handled = true;
            //}
            //else
            //{
            //    MessageBox.Show("xxxxxxxxxxxxxxxx", "Alert");
            //}
           
        }

        private void StdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAcdYear();
        }

        private void crystalReportViewer1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Char ch = e.KeyChar;

            //string chr = (Keys.P).ToString();

            //if (ch == char.Parse(chr))
            //{
            //    crystalReportViewer1.PrintReport();
            //    e.Handled = true;
            //}
            //else
            //{
            //    MessageBox.Show("xxxxxxxxxxxxxxxx", "Alert");
            //}
        }

        private void GetTc_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

            string chr = (Keys.P).ToString();

            if (ch == 80 || ch == 112)
            {
                crystalReportViewer1.PrintReport();
                e.Handled = true;
            }
        }

        private void NameTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }
        }

        private void IdTxtBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                NameTxt.Text = null;
            }
            catch { }
        }

       

    }
}
