﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Boon
{
    public partial class BookList : Form
    {
        common c= new common();
        NewBook obj = new NewBook();

        public Boolean condition = false;
        public int value;

        public BookList()
        {
         
            InitializeComponent();
            dataGridView1.Focus();
            Rectangle r = Screen.PrimaryScreen.WorkingArea;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Height);
        
        }
          public void load_grid()
        {
            try
            {
                this.dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
                String SelectQuery = "SELECT * from Boon.Library";
                DataTable table1 = new DataTable();
                table1 = c.SelectData(SelectQuery);
          
              
                dataGridView1.DataSource = table1;
                dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
                dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                dataGridView1.Columns[0].Width = 50;
                dataGridView1.Columns[1].Width = 150;
                dataGridView1.Columns[2].Width = 150;
                dataGridView1.Columns[3].Width = 150;
         
               // adpater.Dispose();
            }
              catch
            {
              }
          }


        private void list_Load(object sender, EventArgs e)
        {

            load_grid();

            }

        private void txtstudname_TextChanged(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("BookName LIKE '%{0}%'", SearchTxt.Text);
           
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }
       // BookList ob1 = new BookList();
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
                  
           }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                condition = true;
                e.SuppressKeyPress = true;
                this.Close();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        
        }
    }


//        using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Windows.Forms;

//namespace EasyTravelsExpanseManagerSoftware
//{
//    public partial class FormList : Form
//    {
//        CommonFunction common = new CommonFunction();
//        public Boolean condition = false;
//        public int value;
//        public FormList()
//        {
            
//          InitializeComponent();
//            Rectangle r = Screen.PrimaryScreen.WorkingArea;
//            this.StartPosition = FormStartPosition.Manual;
//            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Height);
//        }

//        private void FormList_Load(object sender, EventArgs e)
//        {
//            if(value==1)
//            {
//                lbltitle.Text = "Customer List";
//                DataTable dt = common.SelectData("select * from tbl_ReminderMaster where Status='Pending'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["remid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["custname"];
//                    }
//                }
//            }
//            else if (value == 2)
//            {
//                lbltitle.Text = "City List";
//                DataTable dt = common.SelectData("select * from tbl_CityMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["cityid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["cityname"];
//                    }
//                }
//            }
//            else if (value == 3)
//            {
//                lbltitle.Text = "Driver List";
//                DataTable dt = common.SelectData("select * from tbl_DriverMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["driverid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["drivername"];
//                    }
//                }
//            }
//            else if (value == 4)
//            {
//                lbltitle.Text = "Route List";
//                DataTable dt = common.SelectData("select * from tbl_RouteMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["routeid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["routename"];
//                    }
//                }
//            }
//            else if (value == 5)
//            {
//                lbltitle.Text = "City Point List";
//                DataTable dt = common.SelectData("select * from tbl_CityPointMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["citypointid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["citypointname"];
//                    }
//                }
//            }
//            else if (value == 6)
//            {
//                lbltitle.Text = "Expense Type List";
//                DataTable dt = common.SelectData("select * from tbl_ExpenseTypeMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["ExpenseTypeId"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["ExpenseTypeName"];
//                    }
//                }
//            }
//            else if (value == 7)
//            {
//                lbltitle.Text = "User List";
//                DataTable dt = common.SelectData("select * from tbl_UserDetails");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["empid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["empname"];
//                    }
//                }
//            }
//            else if (value == 8)
//            {
//                lbltitle.Text = "Customer List";
//                DataTable dt = common.SelectData("select * from tbl_ReminderMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["remid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["custname"];
//                    }
//                }
//            }
//            else if (value == 9)
//            {
//                lbltitle.Text = "Contact List";
//                DataTable dt = common.SelectData("select * from tbl_Contacts");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["id"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["name"];
//                    }
//                }
//            }
//            else if (value == 10)
//            {
//                lbltitle.Text = "Customer List";
//                DataTable dt = common.SelectData("select * from tbl_ParcelMaster");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["ParcelAlias"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["sendername"];
//                    }
//                }
//            }
//            else
//            {
//                lbltitle.Text = "Category List";
//                string loadstud = "Select CatId,CategoryName from tbl_CategoryDetails";
//                DataTable dt = new DataTable();
//                dt = common.SelectData(loadstud);
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["CatId"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["CategoryName"];
//                    }
//                }
//            }
//        }

//        private void FormList_KeyDown(object sender, KeyEventArgs e)
//        {
//            if (e.KeyCode == Keys.Escape)
//            {
//                this.Close();
//            }
//        }

//        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
//        {
//            if (e.KeyCode == Keys.Enter)
//            {
//                condition = true;
//                e.SuppressKeyPress = true;
//                this.Close();
//            }
//        }

//        private void BtnClose_Click(object sender, EventArgs e)
//        {
//            this.Close();
//        }

//        private void txtstudname_TextChanged(object sender, EventArgs e)
//        {
//            if (value == 1)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select * from tbl_ReminderEntryDetails where custname like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["remid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["custname"];
//                    }
//                }
//            }
//            else if(value == 2)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select * from tbl_CityMaster where cityname like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["cityid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["cityname"];
//                    }
//                }
//            }
//            else if (value == 3)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select driverid,drivername from tbl_DriverMaster where drivername like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["driverid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["drivername"];
//                    }
//                }
//            }
//            else if(value == 4)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select * from tbl_RouteMaster where routename like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["routeid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["routename"];
//                    }
//                }
//            }
//            else if (value == 5)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select * from tbl_CityPointMaster where citypointname like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["citypointid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["citypointname"];
//                    }
//                }
//            }
//            else if (value == 6)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select * from tbl_ExpenseTypeMaster where ExpenseTypeName like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["ExpenseTypeId"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["ExpenseTypeName"];
//                    }
//                }
//            }
//            else if (value == 7)
//            {
//                dataGridView1.Rows.Clear();
//                DataTable dt = common.SelectData("select * from tbl_UserDetails where empname like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["empid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["empname"];
//                    }
//                }
//            }
//            else if (value == 8)
//            {
//                dataGridView1.Rows.Clear();
//                lbltitle.Text = "Customer List";
//                DataTable dt = common.SelectData("select remid,custname from tbl_ReminderMaster where custname like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["remid"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["custname"];
//                    }
//                }
//            }
//            else if (value == 9)
//            {
//                dataGridView1.Rows.Clear();
//                lbltitle.Text = "Contact List";
//                DataTable dt = common.SelectData("select id,name from tbl_Contacts where name like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["id"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["name"];
//                    }
//                }
//            }
//            else if (value == 10)
//            {
//                dataGridView1.Rows.Clear();
//                lbltitle.Text = "Customer List";
//                DataTable dt = common.SelectData("select * from tbl_ParcelMaster where sendername like'" + txtstudname.Text + "%'");
//                if (dt.Rows.Count > 0)
//                {
//                    for (int i = 0; i < dt.Rows.Count; i++)
//                    {
//                        dataGridView1.Rows.Add();
//                        dataGridView1[0, i].Value = dt.Rows[i]["ParcelAlias"];
//                        dataGridView1[1, i].Value = dt.Rows[i]["sendername"];
//                    }
//                }
//            }
//            else
//            {
//                dataGridView1.Rows.Clear();
//                string searchbox = "Select CatId,CategoryName from tbl_CategoryDetails where CategoryName like'" + txtstudname.Text + "%'";
//                DataTable dt = new DataTable();
//                dt = common.SelectData(searchbox);
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    dataGridView1.Rows.Add();
//                    dataGridView1[0, i].Value = dt.Rows[i]["CatId"].ToString();
//                    dataGridView1[1, i].Value = dt.Rows[i]["CategoryName"].ToString();
//                }
//            }
//        }
//    }
//}

//    }
//}
