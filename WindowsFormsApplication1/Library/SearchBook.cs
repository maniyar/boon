﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DGVPrinterHelper;

namespace Boon
{
    public partial class SearchBook : Form
    {
        common c = new common();
        MySqlConnection connection;

        public SearchBook()
        {
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
      
        //public void openConnection()
        //{
        //    if (connection.State == ConnectionState.Closed)
        //    {
        //        connection.Open();
        //    }
        //}
        //public void closeConnection()
        //{
        //    if (connection.State == ConnectionState.Open)
        //    {
        //        connection.Close();
        //    }
        //}
        //public void executeQuery(String query)
        //{
        //    try
        //    {
        //        openConnection();
        //        command = new MySqlCommand(query, connection);


        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        closeConnection();
        //    }
    //    }
        public MySqlCommand command { get; set; }
        private void StudentNameLoad()
        {
            try
            {
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                Cmd.CommandText = "select BookName from Boon.library  ";
                MySqlDataReader dr1 = Cmd.ExecuteReader();
                AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                while (dr1.Read())
                {
                    data.Add(dr1.GetString(0));

                }
                NameTxt.AutoCompleteCustomSource = data;
                dr1.Close();
            }
            catch
            {

            }
            finally
            {
                connection.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            try
            {
                if (NameTxt.Text == "" && IdTxt.Text == "")
                {
                    MessageBox.Show("Please enter atleast one field  ", "Warnning");
                    NameTxt.Focus();

                }

                if (NameTxt.Text == "" && IdTxt.Text != "")
                {
                    String SelectQuery = "SELECT * from Boon.library WHERE ID ='" + IdTxt.Text + "';";
                    DataTable table = new DataTable();
                    //MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
                    //adpater.Fill(table);
                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["BookName"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Author"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["Price"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Quantity"].ToString();
                        dataGridView1.Rows[n].Cells[5].Value = item["OtherDetails"].ToString();

                    }

                }


                if (NameTxt.Text != "" && IdTxt.Text == "")
                {

                    String SelectQuery1 = "SELECT * from Boon.library WHERE BookName like '" + NameTxt.Text + "%' ";
                    DataTable table1 = new DataTable();

                    table1 = c.SelectData(SelectQuery1);
                    foreach (DataRow item in table1.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["BookName"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Author"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["Price"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Quantity"].ToString();
                        dataGridView1.Rows[n].Cells[5].Value = item["OtherDetails"].ToString();
                    }
                }
            }
            catch
            { }

           // dataGridView1.DataSource = table;
         
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            NameTxt.Text = null;
            IdTxt.Text = null;
        }

        private void SearchBook_Load(object sender, EventArgs e)
        {
            StudentNameLoad();
        }

        private void IdTxt_TextChanged(object sender, EventArgs e)
        {
            NameTxt.Text = null;
        }

        private void NameTxt_TextChanged(object sender, EventArgs e)
        {
            IdTxt.Text = null;
        }

        private void NameTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }
        }

        private void IdTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }
        }

        private void ListBtn_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            try
            {
                String SelectQuery1 = "SELECT * from Boon.library ";
                DataTable table1 = new DataTable();

                table1 = c.SelectData(SelectQuery1);
                foreach (DataRow item in table1.Rows)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                    dataGridView1.Rows[n].Cells[1].Value = item["BookName"].ToString();
                    dataGridView1.Rows[n].Cells[2].Value = item["Author"].ToString();
                    dataGridView1.Rows[n].Cells[3].Value = item["Price"].ToString();
                    dataGridView1.Rows[n].Cells[4].Value = item["Quantity"].ToString();
                    dataGridView1.Rows[n].Cells[5].Value = item["OtherDetails"].ToString();
                }
            }
            catch { }
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
        

        //private void BookListBtn_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        String SelectQuery1 = "SELECT * from Boon.library ";
        //        DataTable table1 = new DataTable();

        //        table1 = c.SelectData(SelectQuery1);
        //        foreach (DataRow item in table1.Rows)
        //        {
        //            int n = dataGridView1.Rows.Add();
        //            dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
        //            dataGridView1.Rows[n].Cells[1].Value = item["BookName"].ToString();
        //            dataGridView1.Rows[n].Cells[2].Value = item["Author"].ToString();
        //            dataGridView1.Rows[n].Cells[3].Value = item["Price"].ToString();
        //            dataGridView1.Rows[n].Cells[4].Value = item["Quantity"].ToString();
        //            dataGridView1.Rows[n].Cells[5].Value = item["OtherDetalis"].ToString();
        //        }
        //    }
        //    catch { }
        //}
    }
}
