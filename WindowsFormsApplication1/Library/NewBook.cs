﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class NewBook : Form
    {
        public NewBook()
        {
            InitializeComponent();
        }
        ErrorProvider error1 = new ErrorProvider();
        common c = new common();
        // MySqlConnection connection = new MySqlConnection(obj.ConnectionString);



        private void SaveBtn_Click(object sender, EventArgs e)
        {
            int i = Convert.ToInt32(QuantityTxt.Text);
            if (AuthorTxt.Text == "")
            {
                MessageBox.Show("Please Enter Author Name", "Alert");
                AuthorTxt.Focus();
            }
            else if (NameTxt.Text == "" || NameTxt.Text == "  ")
            {
                MessageBox.Show("Please Enter Name of Book", "Alert");
                NameTxt.Focus();
            }

            else if (i <= 0)
            {
                MessageBox.Show("Please Enter Quentity of Book", "Alert");
                QuantityTxt.Focus();

            }
            else
            {

                string insertQuery = "";

                insertQuery = "INSERT INTO Boon.Library (BookName,Author,Price,Quantity,OtherDetails) VALUES('" + NameTxt.Text + "','" + AuthorTxt.Text + "','" + PriceTxt.Text + "','" + QuantityTxt.Text + "','" + OtherTxt.Text + "');";

                if (c.InsertData(insertQuery))
                {

                    MessageBox.Show("Book Saved successfully");
                }
                else
                {
                    MessageBox.Show("failed");
                }

            }
        }
        public MySqlCommand command { get; set; }

        private void button2_Click(object sender, EventArgs e)
        {
            NameTxt.Text = null;
            AuthorTxt.Text = null;
            PriceTxt.Text = null;
            OtherTxt.Text = null;
            QuantityTxt.Text = null;
            errorProvider1.Clear();
        }

        private void PriceTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                errorProvider1.SetError(PriceTxt, "Enter Numbers Only");
                e.Handled = true;
            }
        }

        private void QuantityTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                errorProvider1.SetError(QuantityTxt, "Enter Numbers Only");
                e.Handled = true;
            }
        }



        private void PriceTxt_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();

            try
            {
                Double c = Convert.ToDouble(PriceTxt.Text);
            }
            catch
            {
                PriceTxt.Clear();
                errorProvider1.SetError(PriceTxt, "Use only one DOT '.' ");
            }
        }

        private void QuantityTxt_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }
        String id;
        private void ListBtn_Click(object sender, EventArgs e)
        {

            BookList l = new BookList();
            l.ShowDialog();
            if (l.condition)
            {
                id = l.dataGridView1.CurrentRow.Cells[0].Value.ToString();
                NameTxt.Text = l.dataGridView1.CurrentRow.Cells[1].Value.ToString();
                AuthorTxt.Text = l.dataGridView1.CurrentRow.Cells[2].Value.ToString();
                PriceTxt.Text = l.dataGridView1.CurrentRow.Cells[3].Value.ToString();
                QuantityTxt.Text = l.dataGridView1.CurrentRow.Cells[4].Value.ToString();
                OtherTxt.Text = l.dataGridView1.CurrentRow.Cells[5].Value.ToString();

            }
        }

        private void NameTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void NewBook_Load(object sender, EventArgs e)
        {
            QuantityTxt.Text = "0";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(id);

                if (i > 0)
                {
                    string DeleteQuery = "Delete From Boon.Library where ID = '" + id + "'";
                    if (c.InsertData(DeleteQuery))
                    {
                        MessageBox.Show("Book Deleted successfully");
                        NameTxt.Text = null;
                        AuthorTxt.Text = null;
                        PriceTxt.Text = null;
                        OtherTxt.Text = null;
                        QuantityTxt.Text = null;
                        errorProvider1.Clear();

                    }
                }
            }
            catch { }

        }
    }
}
