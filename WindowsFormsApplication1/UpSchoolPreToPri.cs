﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class UpSchoolPreToPri : Form
    {
        common c = new common();
        MySqlConnection connection;
        public UpSchoolPreToPri()
        {
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }      

        
        DataTable dt = new DataTable();

        String TutionFee, Fname, Mname, Religion, Cast, Gender, ID, Sub_Cast, Nationality, Address, Mtongue, B_place, Lschool = "BOON ENGLISH SCHOOL", D_O_A, Contact, DOB, FirstStd, Pre_Std, AcdYear, StudIdTxt, Aadhar, Category;
      
       // int Contact;

        private void GetListBtn_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
           // dataGridView1.DataSource =null;
            try
            {
                if (StdBox.Text == "")
                {
                    
                    
                    MessageBox.Show("Please select standard first");
                    StdBox.Focus();
                    return;

                }
                
                else 
                {

                    String SelectQuery = "SELECT * from Boon.Students WHERE Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";
                    DataTable table = new DataTable();
                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                    }
                }
            }
            catch
            {

            }
        }

        private void CheakAllBtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[5].Value = row.Cells[5].Value == null ? false : !(bool)row.Cells[5].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        public void LoadInfo()
        {
            try
            {
                String SelectQuery = "SELECT * from Boon.stud_p_primary WHERE ID = '" + StudIdTxt + "' ";
                DataTable table = new DataTable();
                //  MySqlDataAdapter da = new MySqlDataAdapter(SelectQuery, connection);

                table = c.SelectData(SelectQuery);
                //  da.Fill(table);
                //   Grid.DataSource = table;
                if (table.Rows.Count > 0)
                {
                    ID = table.Rows[0][0].ToString();
                    Name = table.Rows[0][1].ToString();
                    Address = table.Rows[0][2].ToString();
                    Contact = table.Rows[0][3].ToString();
                    Aadhar = table.Rows[0][4].ToString();
                    Lschool = table.Rows[0][5].ToString();
                    Gender = table.Rows[0][6].ToString();
                    DOB = table.Rows[0][7].ToString();
                    B_place = table.Rows[0][9].ToString();
                    Mname = table.Rows[0][11].ToString();
                    Fname = table.Rows[0][12].ToString();
                    StudIdTxt = table.Rows[0][13].ToString();
                    Cast = table.Rows[0][14].ToString();

                    Sub_Cast = table.Rows[0][15].ToString();
                    Category = table.Rows[0][16].ToString();
 
                    D_O_A = table.Rows[0][17].ToString();
                    Nationality = table.Rows[0][18].ToString();
                    Religion = table.Rows[0][24].ToString();
                    Mtongue = table.Rows[0][25].ToString();
                  //  Pre_Std = table.Rows[0][26].ToString();
                    Pre_Std = "Sr.KG";
                    FirstStd = table.Rows[0][27].ToString();
                    AcdYear = table.Rows[0][28].ToString();

                    //String SaveQuery = "INSERT INTO Boon.students (ID,Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place ,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,TutionFee) VALUES('" + ID + "','" + Name + "','" + Address + "','" + Contact + "','" + Aadhar + "','" + Lschool + "','" + Gender + "','" + DOB + "','" + ClassBox.Text + "','" + B_place + "','" + Mname + "','" + Fname + "','" + StudIdTxt + "','" + Cast + "','" + Sub_Cast + "','" + Category + "','" + D_O_A + "','" + Nationality + "','" + ClassBox.Text + "','" + StdBox.Text + "','" + Religion + "','" + Mtongue + "','" + AcdYearBox1.Text + "','" + TutionFee + "')";

                    //if (c.InsertData(SaveQuery))
                    //{

                    //}
                    // da.Dispose();
                }
                else
                {
                    MessageBox.Show("No record found", "Alert");
                    return;
                }
            }
            catch
            { }
        }


      

        private void UpdateClass_Load(object sender, EventArgs e)
        {
            StdBox.Focus();
           
        }
        public void PassValue()
        {
            
            try
            {
                int i = 0;
                int j = 0, Up= 0;
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {



                        var SelectedID = row.Cells[0].Value.ToString();
                        LoadInfo();
                     //   String Name = row.Cells[1].Value.ToString();
                     ////  var ab =  row.Cells[2].Value.ToString();
                     //   Address = row.Cells[2].Value.ToString();
                     //   Contact = long.Parse(row.Cells[3].Value.ToString());
                     //   var abc = row.Cells[3].Value.ToString();
                     //   if (Address == "")
                     //   {
                     //       Address = "_";
                     //   }

                     //  //  (ab != "" ) == true  ? Address =  row.Cells[2].Value.ToString()  : Address = "   " ;

                     //   if (abc == null )
                     //  {
                     //      Contact = 00;
                     //  }
                        //String UpdateQuery = "UPDATE Boon.students SET Std = '" + ClassBox.Text + "' , AcdYear = '" + AcdYearBox1.Text + "' ,TutionFee = '" + TutionFee + "' Where ID = '" + SelectedID + "' AND TutionFee = 0 ";
                        String SaveQuery = "INSERT INTO Boon.students (Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place ,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,TutionFee) VALUES('" + Name + "','" + Address + "','" + Contact + "','" + Aadhar + "','" + Lschool + "','" + Gender + "','" + DOB + "','" + ClassBox.Text + "','" + B_place + "','" + Mname + "','" + Fname + "','" + StudIdTxt + "','" + Cast + "','" + Sub_Cast + "','" + Category + "','" + D_O_A + "','" + Nationality + "','" + ClassBox.Text + "','" + StdBox.Text + "','" + Religion + "','" + Mtongue + "','" + AcdYearBox1.Text + "','" + TutionFee + "')";



                        if (c.InsertData(SaveQuery))
                        {
                            ++Up;
                        }
                        ++i;
                    }
                    else
                    {
                        ++j;
                    }


                    if (i + j == dataGridView1.Rows.Count)
                    {
                        MessageBox.Show("" + Up + " Student updated succsessfully", "Alert");
                    }
                    else
                    {
                      //  MessageBox.Show("Class is not updated", "Alert");
                    }
                  }
                }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        private void AddBtn_Click_1(object sender, EventArgs e)
        {
             int i1 = 0;

            if (AcdYearBox1.Text == "" || ClassBox.Text == "" )
            {
                MessageBox.Show("You have to select Acd.Year & class from update panel", "Alert");
                return;
            }
            LoadTutionFee();
             foreach (DataGridViewRow row in this.dataGridView1.Rows)
             {
                 //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                 if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                 {
                     i1++;
                     if (i1 == dataGridView1.Rows.Count)
                     {
                         MessageBox.Show("You have to select at least one student", "Alert");
                     }
                   
                 }
                 else
                 {

                     if (MessageBox.Show("Do you want to update students?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                     {

                         PassValue();
                        break;
                     }
                 }
                 
             }
           
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
           //foreach (DataGridViewRow row in this.dataGridView1.Rows)
           //     {
           //         //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
           //         if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
           //         {
           //             //row.Cells["Check"].c
           //         }
           // }
        }

        public void LoadTutionFee()
        {
            try
            {
                if (ClassBox.Text == "1 st")
                {
                    string SelecteQuery = "SELECT TutionFee FROM Boon.feesstructure where Class = '1 st' ";

                    dt = c.SelectData(SelecteQuery);
                    if (dt.Rows.Count > 0)
                    {
                        TutionFee = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        MessageBox.Show("You have to add fees structure of class", "Warnnig");

                    }
                }
                if (ClassBox.Text == "Jr.KG")
                {
                    string SelecteQuery = "SELECT TutionFee FROM Boon.FeesStructure where Class = 'Jr.KG' ";

                    dt = c.SelectData(SelecteQuery);
                    if (dt.Rows.Count > 0)
                    {
                        TutionFee = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        MessageBox.Show("You have to add fees structure of class", "Warnnig");

                    }
                }
                if (ClassBox.Text == "Sr.KG")
                {
                    string SelecteQuery = "SELECT TutionFee FROM Boon.FeesStructure where Class = 'Sr.KG' ";

                    dt = c.SelectData(SelecteQuery);
                    if (dt.Rows.Count > 0)
                    {
                        TutionFee = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        MessageBox.Show("You have to add fees structure of class", "Warnnig");

                    }
                }
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        
        }


        private void StdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               
                AcdYearBox.Items.Clear();
               
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                //Cmd.CommandText =
                Cmd.CommandText = "select Distinct(AcdYear) from Boon.students  WHERE Std = '" + StdBox.Text + "' ORDER BY AcdYear";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["AcdYear"].ToString();
                    AcdYearBox.Items.Add(i);

                }
                dr.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }

        }
    }
}
