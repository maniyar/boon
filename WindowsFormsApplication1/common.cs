﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    class common
    {

       public string ConnectionString = "datasource=localhost;port=3306;username=root;password=";
        MySqlConnection con;
        MySqlDataAdapter adpt;


        public DataTable SelectData(String query)
        {
            DataTable dt = new DataTable();
            try
            {
                con = new MySqlConnection(ConnectionString);
                con.Open();
                adpt = new MySqlDataAdapter(query, con);

                adpt.Fill(dt);
                // return dt;
                con.Close();
            }
            catch
            {
                // con.Close();
            }
            return dt;

        }



        public string Text;

        public override string ToString()
        {
            return this.Text;
        }

        public Boolean InsertData(string query)
        {
            int r = 1;

            try
            {
                con = new MySqlConnection(ConnectionString);
                con.Open();
                MySqlCommand cmd = new MySqlCommand(query, con);

                r = cmd.ExecuteNonQuery();
                {
                    return true;
                }

            }
            catch
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }
        static readonly string[] ones = new string[] { "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty", "Twenty First", "Twenty Second", "Twenty Third", "Twenty Fourth", "Twenty Fifth", "Twenty Sixth", "Twenty Seventh", "Twenty Eighth", "Twenty Ninth", "Thirty", "Thirty First" };

        static readonly string[] Month = new string[] { "", "January", "February", "March", "April", "May", "Jun", "July", "August", "September", "October", "November", "December" };
        static readonly string[] Year1 = new string[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };

        static readonly string[] one = new string[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };

        static readonly string[] teens = new string[] { "Twenty", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "" };

        static readonly string[] tens = new string[] { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        public static String DateToWords(String Date)
        {

            String DayInwords, MothInWords, LastYearWords;
            var YearInWords = "";
            string date1 = Date;

            string d = date1.Substring(0, 2);
            int n = Int16.Parse(d);
            DayInwords = ones[n];

            String m = date1.Substring(3, 2);
            int n1 = Int16.Parse(m);
            MothInWords = Month[n1];

            String y1 = date1.Substring(6, 2);
            String y2 = date1.Substring(8, 2);

            int n2 = Int16.Parse(y1);
            if (n2 == 19) { YearInWords = "Ninteen Hundred"; }
            if (n2 == 20) { YearInWords = "Two Thousand"; }

            int n3 = Int16.Parse(y2);
            if (n3 > 10 && n3 <= 20)
            {
                LastYearWords = teens[n3 % 10];
                Date = DayInwords + " " + MothInWords + " " + YearInWords + " " + LastYearWords;
            }
            else
            {
                LastYearWords = tens[n3 / 10];

                LastYearWords += " " + one[n3 % 10];
                Date = DayInwords + " " + MothInWords + " " + YearInWords + " " + LastYearWords;
            }


            return Date;
        }
    }
}

