﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using MySql.Data.MySqlClient;




namespace Boon
{
    public partial class MainForm : Form
    {
        MySqlConnection connection;
        common obj;
        public MainForm()
        {
            InitializeComponent();
            obj = new common();
            connection = new MySqlConnection(obj.ConnectionString);
            // fileToolStripMenuItem.MouseHover += fileToolStripMenuItem_MouseHover;
            // menuStrip1_MouseHover.MouseHover +=  menuStrip1_MouseHover.MouseHover ;
        }


        private void newAdmissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Student Open = new Student();
            //Open.MdiParent = this;
            //Open.Show();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeNew Op = new EmployeeNew();
            Op.MdiParent = this;
            Op.Show();
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SearchStudent Op = new SearchStudent();
            //Op.MdiParent = this;
            //Op.Show();
        }

        private void searchToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SearchEmployee OpE = new SearchEmployee();
            OpE.MdiParent = this;
            OpE.Show();
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }



        private void newBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewBook OpNb = new NewBook();
            OpNb.MdiParent = this;
            OpNb.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LogIn Exit = new LogIn();
            Exit.Close();

        }

        private void searchBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchBook b = new SearchBook();
            b.MdiParent = this;
            b.Show();
        }

        private void newReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void form1_Closing(object sender, CancelEventArgs e)
        {
            // _stopCounting = true;


        }

        private void updateEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateEmp abc = new UpdateEmp();
            abc.MdiParent = this;
            abc.Show();
        }

        private void tCToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void primToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TcAppliction vvv = new TcAppliction();
            vvv.MdiParent = this;
            vvv.Show();
        }

        private void bonafiteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void preSchoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //    BonfidePre nnn = new BonfidePre();
            //    nnn.MdiParent = this;
            //    nnn.Show();
        }

        private void primaryToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //Bonafide priya = new Bonafide();
            //priya.MdiParent = this;
            //priya.Show();

        }

        private void higherSecondaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //BonafideHigh ob = new BonafideHigh();
            //ob.MdiParent = this;
            //ob.Show ();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BonfidePre mona = new BonfidePre();
            mona.MdiParent = this;
            mona.Show();

        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void unitTestIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExamResult aa = new ExamResult();
            aa.MdiParent = this;
            aa.Show();
        }

        private void unitTestIIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExamMarksAdd bb = new ExamMarksAdd();
            bb.MdiParent = this;
            bb.Show();

        }

        private void primaeySchoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TCPrimary obj = new TCPrimary();
            obj.MdiParent = this;
            obj.Show();
        }

        private void newToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            NewTransactions obj1 = new NewTransactions();
            obj1.MdiParent = this;
            obj1.Show();

        }

        private void searchToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            SearchTransaction mm = new SearchTransaction();
            mm.MdiParent = this;
            mm.Show();
        }

        private void addmisionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BusAddmission nn = new BusAddmission();
            nn.MdiParent = this;
            nn.Show();
        }

        private void newToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Bus mm = new Bus();
            mm.MdiParent = this;
            mm.Show();

        }

        private void FeesStructureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FeesStructure mmm = new FeesStructure();
            mmm.MdiParent = this;
            mmm.Show(); ;
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BusDetails ob = new BusDetails();
            ob.MdiParent = this;
            ob.Show();
        }

        private void updateToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }



        private void MainForm_Load(object sender, EventArgs e)
        {
            this.menuStrip1.Items[0].MouseHover += new EventHandler(menuStrip1_MouseHover);

        }

        private void MainForm_MouseHover(object sender, EventArgs e)
        {
            //if (sender is ToolStripDropDownItem)
            //{

            //    MenuItem item = sender as MenuItem;
            //   // menuToolStripMenuItem it = sender as MenuItem;
            //    //ToolStripDropDownItem item = sender as ToolStripDropDownItem;
            //    if (item.HasDropDownItems && !item.DropDown.Visible)  //item.HasDropDownItems &&
            //    {
            //        item.ShowDropDown();
            //    }
            //}
        }

        private void menuStrip1_MouseHover(object sender, EventArgs e)
        {
            if (sender is ToolStripDropDownItem)
            {

                // MenuItem item = sender as MenuItem;
                //// menuToolStripMenuItem it = sender as MenuItem;
                ToolStripDropDownItem item = sender as ToolStripDropDownItem;
                if (item.HasDropDownItems && !item.DropDown.Visible)  //item.HasDropDownItems &&
                {
                    item.ShowDropDown();
                }
            }


        }

        private void searchReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IdSearchReceipt ob = new IdSearchReceipt();
            ob.MdiParent = this;
            ob.Show();
        }

        private void Backup()
        {

            //   MySqlConnection connection = new MySqlConnection(obj.ConnectionString);
            ////   string constring = "server=localhost;user=root;pwd=qwerty;database=test;";
            //   string file = "D:\\BonnFirst.sql";
            //   using (connection)
            //   {
            //       using (MySqlCommand cmd = new MySqlCommand())
            //       {
            //          // using (MySqlBackup mb = new MySqlBackup(cmd))
            //           {
            //               cmd.Connection = connection;
            //               connection.Open();
            //               mb.ExportToFile(file);
            //               connection.Close();
            //           }
            //       }
            //   }
        }



        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void examStructureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExamStuct ob = new ExamStuct();
            ob.MdiParent = this;
            ob.Show();
        }

        private void firstTermToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudMarks ob = new StudMarks();
            ob.MdiParent = this;
            ob.Show();
        }

        private void studentUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {

            UpdateStudent OpUs = new UpdateStudent();
            OpUs.MdiParent = this;
            OpUs.Show();
        }

        private void classUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateClassPPrimary OpUs1 = new UpdateClassPPrimary();
            OpUs1.MdiParent = this;
            OpUs1.Show();
        }

        private void sendSmsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void searchStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchFormPPrimary obj9 = new SearchFormPPrimary();
            obj9.MdiParent = this;
            obj9.Show();
        }

        private void newAddmisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudentPPrimary obj8 = new StudentPPrimary();
            obj8.MdiParent = this;
            obj8.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Student Open = new Student();
            Open.MdiParent = this;
            Open.Show();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            UpdateStudent obj7 = new UpdateStudent();
            obj7.MdiParent = this;
            obj7.Show();
        }

        private void updateStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateStudPPrimary obj6 = new UpdateStudPPrimary();
            obj6.MdiParent = this;
            obj6.Show();
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            UpdateClassHigh obj4 = new UpdateClassHigh();
            obj4.MdiParent = this;
            obj4.Show();
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            SearchFormHigh obj2 = new SearchFormHigh();
            obj2.MdiParent = this;
            obj2.Show();
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            StudAddHigh obj5 = new StudAddHigh();
            obj5.MdiParent = this;
            obj5.Show();

        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            UpdateStudentHigh obj1 = new UpdateStudentHigh();
            // UpdateClassHigh obj = new UpdateClassHigh();
            obj1.MdiParent = this;
            obj1.Show();
        }

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateClassPPrimary obj3 = new UpdateClassPPrimary();
            obj3.MdiParent = this;
            obj3.Show();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            UpdateClass obj55 = new UpdateClass();
            obj55.MdiParent = this;
            obj55.Show();
        }

        private void bonafideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BonfidePre onh = new BonfidePre();
            onh.MdiParent = this;
            onh.Show();
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            TcAppliction on = new TcAppliction();
            on.MdiParent = this;
            on.Show();
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            TCPrimary ll = new TCPrimary();
            ll.MdiParent = this;
            ll.Show();
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            BonfidePrimary kkk = new BonfidePrimary();
            kkk.MdiParent = this;
            kkk.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SearchFormPrimary k1 = new SearchFormPrimary();
            k1.MdiParent = this;
            k1.Show();
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            BonfideHigh k2 = new BonfideHigh();
            k2.MdiParent = this;
            k2.Show();
        }

        private void prePrimaryToPrimaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpSchoolPreToPri k3 = new UpSchoolPreToPri();
            k3.MdiParent = this;

            k3.Show();

        }

        private void toolStripMenuItem18_Click(object sender, EventArgs e)
        {
            HighTcAppliction k4 = new HighTcAppliction();
            k4.MdiParent = this;

            k4.Show();
        }

        private void toolStripMenuItem19_Click(object sender, EventArgs e)
        {
            TCHigh k5 = new TCHigh();
            k5.MdiParent = this;

            k5.Show();
        }

        private void primaryToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            NewReceipt Open = new NewReceipt();
            Open.MdiParent = this;
            Open.Show();
        }

        private void prePrimaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrePrimaryNewReceipt Open = new PrePrimaryNewReceipt();
            Open.MdiParent = this;
            Open.Show();
        }

        private void highschoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HighNewReceipt Open = new HighNewReceipt();
            Open.MdiParent = this;
            Open.Show();
        }

        private void updateTCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TcUpdateHigh Open = new TcUpdateHigh();
            Open.MdiParent = this;
            Open.Show();
        }

        private void toolStripMenuItem21_Click(object sender, EventArgs e)
        {
            TcUpdatePrimary Open = new TcUpdatePrimary();
            Open.MdiParent = this;
            Open.Show();
        }

        private void remainingFeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClasswiseFee Open = new ClasswiseFee();
            Open.MdiParent = this;
            Open.Show();
        }

        private void sMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendSms OpUs1 = new SendSms();
            OpUs1.MdiParent = this;
            OpUs1.Show();
        }

        private void searchTCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchTcPri OpUs1 = new SearchTcPri();
            OpUs1.MdiParent = this;
            OpUs1.Show();
        }

        private void searchTCToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SearchTcHigh OpUs1 = new SearchTcHigh();
            OpUs1.MdiParent = this;
            OpUs1.Show();
        }
    }
}