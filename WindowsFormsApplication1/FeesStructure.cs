﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace Boon
{
    public partial class FeesStructure : Form
    {
        common obj = new common();
        MySqlConnection connection;
        MySqlCommand command = new MySqlCommand();
        public FeesStructure()
        {
            connection = new MySqlConnection(obj.ConnectionString);
            InitializeComponent();
        }
      
     
        DataTable dt = new DataTable();

       DataTable table = new DataTable();
        common c = new common();
        private void FeesStructure_Load(object sender, EventArgs e)
        {
            LoadFee();
            AcdYearBox.Items.Clear();
            string SelectQuery = "Select Distinct(AcdYear) from Boon.FeesStructure ";
            DataTable dt = c.SelectData(SelectQuery);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    AcdYearBox.Items.Add(row["AcdYear"].ToString());
                }
            }
        }
        public void LoadFee()
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            ClassBox.Focus();
            try
            {
                dataGridView1.DataSource = null;
                String SelectQuery = "SELECT * from Boon.FeesStructure order By Class  ";
                table = c.SelectData(SelectQuery);
             
                if (table.Rows.Count > 0)
                {
                  //  dataGridView1.DataSource = table;
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["Class"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["FirstTerm"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["SecondTerm"].ToString();
                       
                        dataGridView1.Rows[n].Cells[3].Value = item["RegistrationFee"].ToString();

                        //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["TCFee"].ToString();
                        dataGridView1.Rows[n].Cells[5].Value = item["BonafideFee"].ToString();
                        dataGridView1.Rows[n].Cells[6].Value = item["ComputerFee"].ToString();
                        dataGridView1.Rows[n].Cells[7].Value = item["MarksMemoFee"].ToString();
                        dataGridView1.Rows[n].Cells[8].Value = item["OtherFee"].ToString();
                        dataGridView1.Rows[n].Cells[9].Value = item["TotalFee"].ToString();

                    }   
                }
            }
            catch
            {
            }
        }

        private void TutionTxt_TextChanged(object sender, EventArgs e)
        {
            if (ClassBox.Text == "")
            {
                string myStringVariable2 = string.Empty;
                ClassBox.Focus();
                MessageBox.Show("Select Standard first ");
            }
           
        }
        public void TotalAmount()
        {
            try
            {
                TotalTxt.Text = ((Double.Parse(TutionTxt.Text)) + (Double.Parse(ExamTxt.Text)) + (Double.Parse(RegTxt.Text)) + (Double.Parse(TcTxt.Text)) + (Double.Parse(BonTxt.Text)) + (Double.Parse(ComTxt.Text)) + (Double.Parse(OtherTxt.Text)) + (Double.Parse(MarkTxt.Text))).ToString();
            }
            catch { }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
            if (TutionTxt.Text == "")
            {
                MessageBox.Show("Enter 1st Term Tution Fee","Alert");
                TutionTxt.Focus();
                return;
            }
            else if (ExamTxt.Text == "")
            {
                MessageBox.Show("Enter 2nd Term Fee", "Alert");
                ExamTxt.Focus();
                return;
            }
            else if (RegTxt.Text == "")
            {
                MessageBox.Show("Enter Registration Fee", "Alert");
                RegTxt.Focus();
                return;
            }
            else if (TcTxt.Text == "")
            {
                MessageBox.Show("Enter TC Fee", "Alert");
                TcTxt.Focus();
                return;
            }
            else if (BonTxt.Text == "")
            {

                MessageBox.Show("Enter Bonafide Fee", "Alert");
                BonTxt.Focus();
                return;
            }
            else if (ComTxt.Text == "")
            {
                MessageBox.Show("Enter Computer Fee", "Alert");
                ComTxt.Focus();
                return;
            }
            else if (MarkTxt.Text == "")
            {
                MessageBox.Show("Enter Marks Memo  Fee", "Alert");
                MarkTxt.Focus();
                return;
            }
            else if (OtherTxt.Text == "")
            {
                string myStringVariable2 = string.Empty;
                MessageBox.Show("Enter Other Fee", "Alert");
                OtherTxt.Focus();
                return;
            }
            
            //   https://www.youtube.com/watch?v=9r1dcSsB8uw
            else if (MessageBox.Show("Do you want to save?", "Save Notification", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                TotalAmount();
                string SelectQuery = "SELECT Class From Boon.FeesStructure where Class = '" + ClassBox.Text + "' AND AcdYear= '"+AcdYearBox.Text+"'";
                Double fee1 = Double.Parse(TutionTxt.Text);
                Double fee2 = Double.Parse(ExamTxt.Text);
                Double totalfee = fee1 + fee2;
                dt = obj.SelectData(SelectQuery);
                if (dt.Rows.Count > 0)
                {
                    String UpdateQuery = "Update Boon.FeesStructure SET TutionFee ='" + totalfee + "', ExamFee = '" + ExamTxt.Text + "', RegistrationFee = '" + RegTxt.Text + "', TCFee ='" + TcTxt.Text + "' ,BonafideFee = '" + BonTxt.Text + "',ComputerFee = '" + ComTxt.Text + "', MarksMemoFee = '" + MarkTxt.Text + "', OtherFee ='" + OtherTxt.Text + "',TotalFee = '" + TotalTxt.Text + "',SecondTerm= '" + ExamTxt.Text + "', FirstTerm= '" + TutionTxt.Text + "' Where Class = '" + ClassBox.Text + "' AND AcdYear= '"+AcdYearBox.Text+"'";
                    if (obj.InsertData(UpdateQuery))
                    {
                        MessageBox.Show("Data Saved successfully", "Alert ");
                        LoadFee();
                    }
                }
                else
                {
                    String InsertQuery = "INSERT into Boon.FeesStructure (Class,TutionFee,ExamFee,RegistrationFee,TCFee,BonafideFee,ComputerFee,MarksMemoFee,OtherFee,TotalFee,FirstTerm,SecondTerm,AcdYear) VALUES ('" + ClassBox.Text + "', '" + totalfee + "','" + ExamTxt.Text + "','" + RegTxt.Text + "','" + TcTxt.Text + "','" + BonTxt.Text + "','" + ComTxt.Text + "','" + MarkTxt.Text + "','" + OtherTxt.Text + "','" + TotalTxt.Text + "','" + TutionTxt.Text + "','" + ExamTxt.Text + "','" + AcdYearBox.Text + "') ";
                    if (obj.InsertData(InsertQuery))
                    {                       
                        MessageBox.Show("Data Saved successfully", "Alert ");
                        LoadFee();                        
                    }
                }
            }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception ");
            }           
                
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //String SelectQuery = "SELECT * from Boon.FeesStructure where Class ='" + comboBox1.Text + "'";
            //DataTable table = new DataTable();
            //MySqlDataAdapter da = new MySqlDataAdapter(SelectQuery, connection);

            //da.Fill(table);
            //TotalTxt.Text = table.Rows[0][1].ToString();

            //da.Dispose(); 
            TotalAmount();
        }

        private void TutionTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only","Alert ");
                e.Handled = true;
            }
        }

        private void ExamTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void RegTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void TcTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void BonTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void ComTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void MarkTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void OtherTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers only", "Alert ");
                e.Handled = true;
            }
        }

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AcdYearBox.Text != "")
            {
                string SelectQuery = "Select * from Boon.FeesStructure where class='" + ClassBox.Text + "' AND AcdYear='" + AcdYearBox.Text + "'";
                DataTable dt = c.SelectData(SelectQuery);
                if (dt.Rows.Count > 0)
                {
                    TutionTxt.Text = dt.Rows[0][10].ToString();
                    ExamTxt.Text = dt.Rows[0][11].ToString();
                    RegTxt.Text = dt.Rows[0][3].ToString();
                    BonTxt.Text = dt.Rows[0][5].ToString();
                    ComTxt.Text = dt.Rows[0][6].ToString();
                    TcTxt.Text = dt.Rows[0][4].ToString();
                    MarkTxt.Text = dt.Rows[0][7].ToString();
                    OtherTxt.Text = dt.Rows[0][8].ToString();
                }
                else
                {
                    TutionTxt.Text = "";
                    ExamTxt.Text = "";
                    RegTxt.Text = "";
                    BonTxt.Text = "";
                    ComTxt.Text = "";
                    TcTxt.Text = "";
                    MarkTxt.Text = "";
                    OtherTxt.Text = "";
                    MessageBox.Show("No data found", "Alert");
                }
            }
            else {
                MessageBox.Show("Select AcdYear Please", "Alert");
            }
        }

        private void ExamTxt_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}
