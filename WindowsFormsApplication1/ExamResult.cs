﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DGVPrinterHelper;
using System.Net;

namespace Boon
{
    public partial class ExamResult : Form
    {
        public ExamResult()
        {
            InitializeComponent();
        }
        common c = new common();
        private void GetListBtn_Click(object sender, EventArgs e)
        {
            try
            {

               if (ClassBox.Text=="")
               {
                   MessageBox.Show("Select Class First");
                   ClassBox.Focus();
                   return;
               }
                dataGridView1.Rows.Clear();
                string d = Date1.Value.ToShortDateString();
                String SelectQuery = "SELECT * from Boon.Exam_primary WHERE Class = '" + ClassBox.Text + "'AND Sub = '" + SubBox.Text + "' AND   Date = '" + d + "' AND Ename = '"+NameTxt.Text+"' ";
                DataTable table = new DataTable();
                table = c.SelectData(SelectQuery);
                if (table.Rows.Count > 0)
                {
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["Class"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["SID"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["SName"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["EName"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Sub"].ToString();
                        dataGridView1.Rows[n].Cells[5].Value = item["Contact"].ToString();
                        dataGridView1.Rows[n].Cells[6].Value = item["Obtmarks"].ToString();
                        dataGridView1.Rows[n].Cells[7].Value = item["Tmarks"].ToString();
                      

                        TotalLbl.Text = "Total students : " + dataGridView1.Rows.Count;
                    }
                }
                else {
                    MessageBox.Show("No record found...","Alert");
                }
            }
            catch { }
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
        /*  dataGridView1.Rows[n].Cells[0].Value = item["Class"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["SID"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["SName"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["EName"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Sub"].ToString();
                        dataGridView1.Rows[n].Cells[5].Value = item["Contact"].ToString();
                        dataGridView1.Rows[n].Cells[6].Value = item["Obtmarks"].ToString();
                        dataGridView1.Rows[n].Cells[7].Value = item["Tmarks"].ToString();*/
        private void SendSmsBtn_Click(object sender, EventArgs e)
        {

            string username = "BOONSL";
            string pass = "Gateway@1";
            int i = 0, j = 0;
            string d = Date1.Value.ToShortDateString();
            try
            {
                if (MessageBox.Show("Do you want to send sms?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dataGridView1.Rows.Count > 0)
                    {
                        foreach (DataGridViewRow row in this.dataGridView1.Rows)
                        {
                            if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                            {
                                ++i;

                                string message = "Dear Parents, Test conducted on " + d + " of subject " + row.Cells[4].Value.ToString() + ", Marks Obtained by " + row.Cells[2].Value.ToString() + " are " + row.Cells[6].Value.ToString() + "/" + row.Cells[7].Value.ToString() + ".";

                                var number = row.Cells[5].Value.ToString();
                                String marks = row.Cells[6].Value.ToString();
                                if (marks != "A")
                                {

                                    //  //  StudID = row.Cells[0].Value.ToString();

                                    string no = ("+91" + number).ToString();

                                    UriBuilder urlBuilder = new UriBuilder();
                                    string createdURL = "http://mysms.gatewayinfotech.in/http-api.php?username=" + username + "&password=" + pass + "&senderid=BOONSL&route=2&number=" + no + "&message=" + message + "";
                                    HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(createdURL, false));
                                    httpReq.Timeout = 7200;
                                    HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse());

                                    httpReq.Abort();
                                }
                            }
                            else
                            { ++j; }
                        }

                        if (i + j == dataGridView1.Rows.Count)
                        {
                            MessageBox.Show("Successfully sent message");

                        }

                    }
                    else { MessageBox.Show("Select students first", "Alert"); }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                NameTxt.Text = null;

                String str = "select Distinct(Ename) from Boon.Exam_primary WHERE Class= '" + ClassBox.Text + "' ";
                AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                DataTable dt = c.SelectData(str);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        //c.Text = row["Ename"].ToString();
                        data.Add(row["Ename"].ToString());
                       
                    }
                    NameTxt.AutoCompleteCustomSource = data;
                }

            }
            catch { }
        }

        private void CheakAllBtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[8].Value = row.Cells[8].Value == null ? false : !(bool)row.Cells[8].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
    }
}
