﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
namespace Boon
{
    public partial class UpdateEmp : Form
    {
        MySqlConnection connection;
        common c = new common();
        public UpdateEmp()
        {
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
        String Gender;
        String SelectQuery;

        
      //  MySqlCommand command;
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        public void executeQuery(String query)
        {
            try
            {
                openConnection();
                command = new MySqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Data is Updated");
                }
                else { MessageBox.Show("Data is not Updated"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }
      
        
        
        private void load_grid()
        {
            try
            {
                this.dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
                SelectQuery = "SELECT * from Boon.emp";
                DataTable table1 = new DataTable();
                MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
                adpater.Fill(table1);
                dataGridView1.DataSource = table1;
                adpater.Dispose();
            }


            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
            }
            connection.Close();

        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to update the employee?", "Update Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (pictureBox1.Image == null)
                {
                    Bitmap bmp = Properties.Resources.avatar_159236_960_720;
                    Image newImg = bmp;
                    pictureBox1.Image = newImg;
                }

                
                {
                    MemoryStream ms = new MemoryStream();
                    pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                    byte[] Img = ms.ToArray();
                    MySqlCommand cmd1 = new MySqlCommand();

                    String UpdateQuery = "UPDATE Boon.emp SET Name=@Name,Address=@Address,Contact=@Contact,Aadhar=@Aadhar,Previous_school=@Previous_school,Gender=@Gender,Date_of_birth=@DOB,Date_of_joining=@DOJ,Profile=@Profile  WHERE ID ='" + IdTxt.Text + "' ";
                    connection.Open();

                    command = new MySqlCommand(UpdateQuery, connection);

                    command.Parameters.Add("@Name", MySqlDbType.VarChar, 45);
                    command.Parameters.Add("@Address", MySqlDbType.VarChar, 45);
                    command.Parameters.Add("@Contact", MySqlDbType.Decimal, 10);
                    command.Parameters.Add("@Aadhar", MySqlDbType.Decimal, 14);
                    command.Parameters.Add("@Previous_school", MySqlDbType.VarChar, 255);
                    command.Parameters.Add("@Gender", MySqlDbType.VarChar, 5);
                    command.Parameters.Add("@DOB", MySqlDbType.DateTime, 0);
                    command.Parameters.Add("@DOJ", MySqlDbType.DateTime, 0);
                    command.Parameters.Add("@B_Place", MySqlDbType.VarChar, 20);
                    command.Parameters.Add("@Profile", MySqlDbType.Blob);


                    command.Parameters["@Name"].Value = NameTxt.Text;

                    command.Parameters["@Address"].Value = AddressTxt.Text;

                    command.Parameters["@Contact"].Value = ContactTxt.Text;
                    command.Parameters["@Aadhar"].Value = AadharTxt.Text;

                    command.Parameters["@Previous_school"].Value = PriTxt.Text;
                    command.Parameters["@Gender"].Value = Gender;
                    command.Parameters["@DOB"].Value = DateTime1.Value;
                    command.Parameters["@DOJ"].Value = DateTime2.Value;
                    command.Parameters["@Profile"].Value = Img;

                    if (command.ExecuteNonQuery() == 1)
                    {
                        MessageBox.Show("Data saved");
                        SelectQuery = "SELECT * from Boon.emp";
                        DataTable table = new DataTable();
                        MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
                        adpater.Fill(table);
                        dataGridView1.DataSource = table;
                        connection.Close();
                    }
                    else
                    {
                        MessageBox.Show("Data is not saved plz cheak connections");

                    }

                    connection.Close();

                }
            }
        }
        
             public  void UpdateEmp_Load(object sender, EventArgs e)
        {

            load_grid();
       }

           
        private void button2_Click(object sender, EventArgs e)
        {
            IdTxt.Text = null;
            NameTxt.Text = null;
            AddressTxt.Text  = null;           
            ContactTxt.Text = null;
            AadharTxt.Text = null;          
            PriTxt.Text = null;
            DateTime1.Text  = null;
            DateTime2.Text  = null;
            this.FemaleRbtn.Checked = false;
            this.MaleRbtn.Checked = false;
        }

        private void MaleRbtn_CheckedChanged(object sender, EventArgs e)
        {
            Gender = "Male";
        }

        private void FemaleRbtn_CheckedChanged(object sender, EventArgs e)
        {
            Gender = "Female";
        }

      

        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[index];
                IdTxt.Text = selectedRow.Cells[0].Value.ToString();
                NameTxt.Text = selectedRow.Cells[1].Value.ToString();
                AddressTxt.Text = selectedRow.Cells[2].Value.ToString();
                ContactTxt.Text = selectedRow.Cells[3].Value.ToString();
                AadharTxt.Text = selectedRow.Cells[4].Value.ToString();
                PriTxt.Text = selectedRow.Cells[5].Value.ToString();
              var GenderTxt = selectedRow.Cells[6].Value.ToString();
                DateTime1.Text = selectedRow.Cells[7].Value.ToString();
                DateTime2.Text = selectedRow.Cells[8].Value.ToString();
                
                String selectQuery = "SELECT * FROM Boon.emp WHERE ID = '" + IdTxt.Text + "'";

                command = new MySqlCommand(selectQuery, connection);
                MySqlDataAdapter da1;
                da1 = new MySqlDataAdapter(command);

                DataTable table = new DataTable();

                da1.Fill(table);

                //   textBoxNAME.Text = table.Rows[0][1].ToString();
                //  textBoxDESC.Text = table.Rows[0][2].ToString();
                //   AddTxt.Text = table.Rows[0][4].ToString();

                byte[] img2 = (byte[])table.Rows[0][9];

                MemoryStream ms3 = new MemoryStream(img2);

                pictureBox1.Image = Image.FromStream(ms3);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                 
            
                if (GenderTxt == "Male")
                {
                    this.MaleRbtn.Checked = true;
                    this.FemaleRbtn.Checked = false;
                }
                else if (GenderTxt== "Female")
                {
                    this.MaleRbtn.Checked = false;
                    this.FemaleRbtn.Checked = true;
                }

                da1.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            connection.Close();

        }
       
        private void BrowseBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Choose Image(*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(opf.FileName);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void ClickMeBtn_Click(object sender, EventArgs e)
        {
            load_grid();
        }

        public void LoadGender()
        {
           
        }
       
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
       public MySqlCommand command { get; set; }

       private void GenderTxt_TextChanged(object sender, EventArgs e)
       {
           LoadGender();
       }

       private void ContactTxt_KeyPress(object sender, KeyPressEventArgs e)
       {
           Char ch = e.KeyChar;
           if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
           {
               e.Handled = true;
           }
       }

       private void AadharTxt_KeyPress(object sender, KeyPressEventArgs e)
       {
           Char ch = e.KeyChar;
           if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
           {
               e.Handled = true;
           }
       }
    }
}
