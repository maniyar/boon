﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class EmployeeNew : Form
    {
        String Gender;
        MySqlConnection connection;
        common obj;
        public EmployeeNew()
        {
            obj = new common();
            connection = new MySqlConnection(obj.ConnectionString);
            InitializeComponent();
        }
      
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        public void executeQuery(String query)
        {
            try
            {
                openConnection();
                command = new MySqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Data is saved");
                }
                else { MessageBox.Show("Data is not saved"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }




        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to save the employee?", "Save Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                MemoryStream ms = new MemoryStream();
                pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                byte[] Img = ms.ToArray();
                MySqlCommand cmd1 = new MySqlCommand();

                String SaveQuery = "INSERT INTO Boon.emp (Name,Address,Contact,Aadhar,Previous_school,Gender,Date_of_birth,Date_of_joining,Profile) VALUES( @Name, @Address, @Contact,@Aadhar,@Previous_school,@Gender,@DOB,@DOJ,@Profile)";
                connection.Open();
                command = new MySqlCommand(SaveQuery, connection);

                command.Parameters.Add("@Name", MySqlDbType.VarChar, 45);
                command.Parameters.Add("@Address", MySqlDbType.VarChar, 45);
                command.Parameters.Add("@Contact", MySqlDbType.Decimal, 10);
                command.Parameters.Add("@Aadhar", MySqlDbType.Decimal, 14);
                command.Parameters.Add("@Previous_school", MySqlDbType.VarChar, 255);
                command.Parameters.Add("@Gender", MySqlDbType.VarChar, 10);
                command.Parameters.Add("@DOB", MySqlDbType.DateTime, 0);
                command.Parameters.Add("@DOJ", MySqlDbType.DateTime, 0);
                command.Parameters.Add("@B_Place", MySqlDbType.VarChar, 20);
                command.Parameters.Add("@Profile", MySqlDbType.Blob);


                command.Parameters["@Name"].Value = NameTxt.Text;

                command.Parameters["@Address"].Value = AddressTxt.Text;

                command.Parameters["@Contact"].Value = ContactTxt.Text;
                command.Parameters["@Aadhar"].Value = AadharTxt.Text;

                command.Parameters["@Previous_school"].Value = PriTxt.Text;
                command.Parameters["@Gender"].Value = Gender;
                command.Parameters["@DOB"].Value = DateTime1.Value;
                command.Parameters["@DOJ"].Value = DateTime2.Value;
                command.Parameters["@Profile"].Value = Img;
                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Data saved");
                }
                else
                {
                    MessageBox.Show("Data is not saved plz cheak connections");
                }

                connection.Close();

            }
        }
        public MySqlCommand command { get; set; }

        private void button2_Click(object sender, EventArgs e)
        {
            NameTxt.Text = null;
            AddressTxt.Text = null;
            AadharTxt.Text = null;
            ContactTxt.Text = null;
            AadharTxt.Text = null;
          //  MaleRbtn.Text = null;
           // FemaleRbtn.Text = null;
            PriTxt.Text = null;
            DateTime1.Text = null;
            DateTime2.Text = null;
        }

        private void FemaleRbtn_CheckedChanged(object sender, EventArgs e)
        {
            Gender = "Female";
        }

        private void MaleRbtn_CheckedChanged(object sender, EventArgs e)
        {
            Gender = "Male";
        }

        private void OpenBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Choose Image(*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(opf.FileName);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void ContactTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void AadharTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }
    }
}
