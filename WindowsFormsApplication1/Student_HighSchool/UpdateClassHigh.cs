﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DGVPrinterHelper;

namespace Boon
{
    public partial class UpdateClassHigh : Form
    {
        common c = new common();
        MySqlConnection connection;
        public UpdateClassHigh()
        {
            MySqlConnection connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }

        DataTable table = new DataTable();

        DataTable dt = new DataTable();
        String TutionFee = "0", Fname, Mname, Religion, Cast, Gender, ID, Sub_Cast, Nationality, Address, Mtongue, B_place, Lschool = "BOON ENGLISH PRIMARY SCHOOL", D_O_A, Contact, DOB, FirstStd, Pre_Std, AcdYear, StudIdTxt, Aadhar, Category, NameOfStudent, RemaingFee, State;
        // int Contact;

        private void GetListBtn_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            StudCountTxt.Text = "Total students : 0";
            // dataGridView1.DataSource =null;
            try
            {
                if (StdBox.Text == "")
                {
                    MessageBox.Show("Please select standard first");
                    StdBox.Focus();
                    return;
                }
                else
                {
                    String SelectQuery = "SELECT * from Boon.students_high_school WHERE Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'Order By ID ";
                    DataTable table = new DataTable();
                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void CheakAllBtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[5].Value = row.Cells[5].Value == null ? false : !(bool)row.Cells[5].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }

        }

        private void UpdateClass_Load(object sender, EventArgs e)
        {
            StdBox.Focus();

        }
        public void PassValue()
        {
            try
            {
                int i = 0;
                int j = 0, Up = 0;
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {
                        var SelectedID = row.Cells[0].Value.ToString();
                        String SelectQuery = "SELECT  * FROM Boon.students_high_school WHERE ID =  '" + SelectedID + "' ";
                        table = c.SelectData(SelectQuery);

                        if (table.Rows.Count > 0)
                        {
                            ID = table.Rows[0][0].ToString();
                            NameOfStudent = table.Rows[0][1].ToString();
                            Address = table.Rows[0][2].ToString();
                            Contact = table.Rows[0][3].ToString();
                            Aadhar = table.Rows[0][4].ToString();
                            Lschool = table.Rows[0][5].ToString();
                            Gender = table.Rows[0][6].ToString();
                            DOB = table.Rows[0][7].ToString();
                            B_place = table.Rows[0][9].ToString();
                            Mname = table.Rows[0][11].ToString();
                            Fname = table.Rows[0][12].ToString();
                            StudIdTxt = table.Rows[0][13].ToString();
                            Cast = table.Rows[0][14].ToString();
                            Sub_Cast = table.Rows[0][15].ToString();
                            Category = table.Rows[0][16].ToString();
                            D_O_A = table.Rows[0][17].ToString();
                            //  D_O_A = DateTime.Today.ToString();
                            Nationality = table.Rows[0][18].ToString();
                            Religion = table.Rows[0][24].ToString();
                            Mtongue = table.Rows[0][25].ToString();
                            Pre_Std = table.Rows[0][26].ToString();
                            //Pre_Std = "7 th";
                            FirstStd = table.Rows[0][27].ToString();
                            AcdYear = table.Rows[0][28].ToString();
                            RemaingFee = table.Rows[0][33].ToString();
                            State = table.Rows[0][34].ToString();
                            String Tq = table.Rows[0][35].ToString();
                            String Dist = table.Rows[0][36].ToString();
                            String Country = table.Rows[0][37].ToString();
                            String AcdYearFirst = table.Rows[0][31].ToString();
                            int OldRemFees = Convert.ToInt32(table.Rows[0][19].ToString());

                            String SaveQuery = "INSERT INTO Boon.students_high_school_copy (ID,Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place ,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,RemaingFee,State,Tq,Dist,Country,AcdYearFirst) VALUES('" + SelectedID + "','" + NameOfStudent + "','" + Address + "','" + Contact + "','" + Aadhar + "','" + Lschool + "','" + Gender + "','" + DOB + "','" + StdBox.Text + "','" + B_place + "','" + Mname + "','" + Fname + "','" + StudIdTxt + "','" + Cast + "','" + Sub_Cast + "','" + Category + "','" + D_O_A + "','" + Nationality + "','" + FirstStd + "','" + Pre_Std + "','" + Religion + "','" + Mtongue + "','" + AcdYearBox.Text + "','" + RemaingFee + "','" + State + "','" + Tq + "','" + Dist + "','" + Country + "','" + AcdYearFirst + "')";
                            if (c.InsertData(SaveQuery))
                            {
                                String UpdateQuery = "UPDATE Boon.students_high_school SET Std = '" + ClassBox.Text + "' , AcdYear = '" + AcdYearBox1.Text + "' ,TutionFee = '" + TutionFee + "',RemaingFee ='" + TutionFee + "',OldRemFees= OldRemFees +'" + Convert.ToInt32(RemaingFee) + "' Where ID = '" + SelectedID + "' ";
                                if (c.InsertData(UpdateQuery))
                                {
                                    ++Up;
                                    connection.Close();
                                }
                            }
                            ++i;
                        }
                    }
                    else
                    {
                        ++j;
                    }
                    if (i + j == dataGridView1.Rows.Count)
                    {
                        MessageBox.Show("" + Up + " Student updated succsessfully", "Alert");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void AddBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                int i1 = 0;

                if (AcdYearBox1.Text == "" || ClassBox.Text == "")
                {
                    MessageBox.Show("You have to select Acd.Year & class from update panel", "Alert");
                    return;
                }
                LoadTutionFee();
                if (MessageBox.Show("Do you want to update students_high_school?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in this.dataGridView1.Rows)
                    {
                        //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                        if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                        {
                            i1++;
                            if (i1 == dataGridView1.Rows.Count)
                            {
                                MessageBox.Show("You have to select at least one student", "Alert");
                            }
                        }
                        else
                        {
                            PassValue();
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //foreach (DataGridViewRow row in this.dataGridView1.Rows)
            //     {
            //         //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
            //         if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
            //         {
            //             //row.Cells["Check"].c
            //         }
            // }
        }

        public void LoadTutionFee()
        {
            try
            {
                if (ClassBox.Text != "")
                {
                    string SelecteQuery = "SELECT TutionFee FROM Boon.feesstructure where Class = '" + ClassBox.Text + "' AND AcdYear= '" + AcdYearBox1.Text + "' ";

                    dt = c.SelectData(SelecteQuery);
                    if (dt.Rows.Count > 0)
                    {
                        TutionFee = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        MessageBox.Show("You have to add fees structure of class", "Warnnig");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }


        private void StdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();
                LoadTutionFee();
                String Str = "select Distinct(AcdYear) from Boon.students_high_school  WHERE Std = '" + StdBox.Text + "' ORDER BY AcdYear";
                DataTable tbl = c.SelectData(Str);
                if (tbl.Rows.Count > 0)
                {
                    foreach (DataRow row in tbl.Rows)
                    {
                        AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
