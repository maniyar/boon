﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class SearchStudPrimary : Form
    {
        MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=");

        DataTable Dt = new DataTable();
        common c = new common();
        public SearchStudPrimary()
        {
            InitializeComponent();
        }

        private void SearchStudPrePrimary_Load(object sender, EventArgs e)
        {
           
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (StudType.Text == "")
                {
                    
                    StudType.Focus();
                    MessageBox.Show("Please select type of student");
                    return;

                }
                else if (NameTxt.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    ClassBox.Focus();
                    MessageBox.Show("Please enter name of student");
                    return;

                }
                else if (ClassBox.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    ClassBox.Focus();
                    MessageBox.Show("Please select standard of student");
                    return;
                }
                else if (AcdYearBox.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    AcdYearBox.Focus();
                    MessageBox.Show("Please select Acd.Year of student");
                    return;

                }
                else if (StudType.Text == "New")
                {
                    String SelectQuery = "SELECT * from Boon.students_high_school WHERE Name like '" + NameTxt.Text + "%'  AND Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "';";
                    DataTable table = new DataTable();
                    MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
                    adpater.Fill(table);
                    dataGridView1.DataSource = table;
                    dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
                    dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                    dataGridView1.Columns[0].Width = 50;
                    dataGridView1.Columns[1].Width = 200;
                    // executeQuery(SelectQuery);
                }
                else if (StudType.Text == "Old")
                {
                    String SelectQuery = "SELECT * from Boon.students_high_school_copy WHERE Name like '" + NameTxt.Text + "%'  AND Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "';";
                    DataTable table = new DataTable();
                    MySqlDataAdapter adpater = new MySqlDataAdapter(SelectQuery, connection);
                    adpater.Fill(table);
                    dataGridView1.DataSource = table;
                    dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 10);
                    dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                    dataGridView1.Columns[0].Width = 50;
                    dataGridView1.Columns[1].Width = 200;
                    // executeQuery(SelectQuery);
                }
            }

               
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void StudentName()
        {
            try
            {
               
                    if (StudType.Text == "New")
                    {
                        connection.Open();
                        MySqlCommand Cmd = new MySqlCommand();
                        Cmd.Connection = connection;
                        Cmd.CommandText = "select Name from Boon.students_high_school Where Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                        MySqlDataReader dr1 = Cmd.ExecuteReader();
                        AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                        while (dr1.Read())
                        {
                            data.Add(dr1.GetString(0));

                        }
                        NameTxt.AutoCompleteCustomSource = data;
                        dr1.Close();

                    }
                    if (StudType.Text == "Old")
                    {
                        connection.Open();
                        MySqlCommand Cmd = new MySqlCommand();
                        Cmd.Connection = connection;
                        Cmd.CommandText = "select Name from Boon.students_high_school_copy Where Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                        MySqlDataReader dr1 = Cmd.ExecuteReader();
                        AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                        while (dr1.Read())
                        {
                            data.Add(dr1.GetString(0));

                        }
                        NameTxt.AutoCompleteCustomSource = data;
                        dr1.Close();
                    }

                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }
                
            }

            
        
        private void StudentID()
        {
            //comboBox1.Format.coustomSource();
            try
            {
               if (StudType.Text == "New")
               {
                IdBox.Items.Clear();
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                //Cmd.CommandText =
                Cmd.CommandText = "select ID from Boon.students_high_school Where Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["ID"].ToString();
                    IdBox.Items.Add(i);

                }
                dr.Close();
               
               }
               if (StudType.Text == "Old")
               {
               IdBox.Items.Clear();
                connection.Open();
                MySqlCommand Cmd = new MySqlCommand();
                Cmd.Connection = connection;
                //Cmd.CommandText =
                Cmd.CommandText = "select ID from Boon.students_high_school_copy Where Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                MySqlDataReader dr = Cmd.ExecuteReader();
                while (dr.Read())
                {
                    common i = new common();
                    i.Text = dr["ID"].ToString();
                    IdBox.Items.Add(i);

                }
                dr.Close();
               
               
               }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Alert");
            }
            finally
            {
                connection.Close();
            }
           
        }


       

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            //if (AcdYearBox.Text == "")
            //{
            //    MessageBox.Show("Select Acd.Year", "Alert");
                
            //    AcdYearBox.Focus();
            //    return;
            //}
            //else
            //{
            //    StudentID();
            //}
            try
            {
                NameTxt.Text = null;
                IdBox.Items.Clear();
                if (StudType.Text == "")
                {
                    MessageBox.Show("You have select Student type", "Alert");
                    StudType.Focus();
                    return;
                }
                if (StudType.Text == "New")
                {

                    AcdYearBox.Items.Clear();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students_high_school WHERE Std = '"+ClassBox.Text+"' ORDER BY AcdYear  ";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        AcdYearBox.Items.Add(i);

                    }
                    dr.Close();
                }
                if (StudType.Text == "Old")
                {

                    AcdYearBox.Items.Clear();
                    connection.Open();
                    MySqlCommand Cmd = new MySqlCommand();
                    Cmd.Connection = connection;
                    //Cmd.CommandText =
                    Cmd.CommandText = "select Distinct(AcdYear) from Boon.students_high_school_copy WHERE Std = '" + ClassBox.Text + "' ORDER BY AcdYear  ";
                    MySqlDataReader dr = Cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        common i = new common();
                        i.Text = dr["AcdYear"].ToString();
                        AcdYearBox.Items.Add(i);

                    }
                    dr.Close();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            finally
            {
                connection.Close();
            }
        }

        private void IdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (StudType.Text == "New")
                {
                    String SelectQuery = "select Name from Boon.students_high_school where ID = '" + IdBox.Text + "' AND Std = '" + ClassBox.Text + "' ";
                    Dt = c.SelectData(SelectQuery);

                    NameTxt.Text = Dt.Rows[0][0].ToString();

                }
                if (StudType.Text == "Old")
                {
                    String SelectQuery = "select Name from Boon.students_high_school_copy where ID = '" + IdBox.Text + "' AND Std = '" + ClassBox.Text + "' ";
                    Dt = c.SelectData(SelectQuery);

                    NameTxt.Text = Dt.Rows[0][0].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            try
            {

                NameTxt.Text = null;
                IdBox.Items.Clear();
                
                ClassBox.Text = null;
                this.dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void NameTxt_KeyPress(object sender, KeyPressEventArgs e)
        {

            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter letters only", "Alert");
                e.Handled = true;
            }
        }

        private void StudType_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameTxt.Text = null;
            IdBox.Items.Clear();
            AcdYearBox.Items.Clear();
        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ClassBox.Text == "")
            {
                MessageBox.Show("Select Class", "Alert");

                ClassBox.Focus();
                return;
            }
            else
            {
                StudentID();
                StudentName();
            }

        }
    }
}
