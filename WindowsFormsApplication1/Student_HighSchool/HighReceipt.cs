﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class HighReceipt : Form
    {
        MySqlConnection mysql;

        MySqlCommand Cmd = new MySqlCommand();
        NewReceipt obj = new NewReceipt();
        common c_obj;
        public HighReceipt()
        {
            c_obj = new common();
            mysql = new MySqlConnection(c_obj.ConnectionString); 
            InitializeComponent();
        }
       
        public string ID = "1";
        public void LoadReport()
        {
            try
            {
                MySqlCommand Cmd = new MySqlCommand();
               // String ShowQuery = "Select * from Boon.receipts_high order by ReceiptNo Desc LIMIT 1 ";
                String ShowQuery = "Select * from Boon.receipts_high where ReceiptNo ='"+ID+"' ";
                
                mysql.Open();

                Cmd = new MySqlCommand(ShowQuery, mysql);

                DataSet ds = new DataSet();
                MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                mda.Fill(ds, "Receipts");
                ReceiptReport cr = new ReceiptReport();
                cr.SetDataSource(ds);
                crystalReportViewer1.ReportSource = cr;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mysql.Close();
            }
        }

        private void Receipt_Load(object sender, EventArgs e)
        {
            LoadReport();
        }

    }
}
