﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;


namespace Boon
{
    public partial class StudAddHigh : Form
    {
        common c = new common();
        MySqlConnection connection;
        public StudAddHigh()
        {
            InitializeComponent();
            connection = new MySqlConnection(c.ConnectionString);
        }
       
        String Gender, SysDateCollect;
     
      
        DataTable dt = new DataTable();
        String ClassType, FromSchool, TutionFee;
        public void SysDate()
        {
            try
            {
                int a = int.Parse(DateTime.Now.Year.ToString());
                int b = a + 1;
                SysDateCollect = a + "-" + b;
            }
            catch { }
        }
        private void SchoolType()
        {
            if (FirstClassBox.Text == "Nursery" || FirstClassBox.Text == "LKG" || FirstClassBox.Text == "UKG" || FirstClassBox.Text == "SrKG" || FirstClassBox.Text == "LrKG")
            {
                ClassType = "Pre-Primary";

            }
            if (FirstClassBox.Text == "1 st " || FirstClassBox.Text == "2 nd" || FirstClassBox.Text == "3 rd" || FirstClassBox.Text == "4 th" || FirstClassBox.Text == "5 th" || FirstClassBox.Text == "6 th" || FirstClassBox.Text == "7 th")
            {
                ClassType = "Primary";

            }
            if (FirstClassBox.Text == "8 th " || FirstClassBox.Text == "9 th" || FirstClassBox.Text == "10 th")
            {
                ClassType = "Highschool";

            }

        }
        public void loadTutionFee()
        {

            string SelecteQuery = "SELECT TutionFee FROM Boon.FeesStructure where Class = '" + FirstClassBox.Text + "' and AcdYear = '" + AcdYearTxt.Text + "' ORDER BY id DESC LIMIT 1";

            dt = c.SelectData(SelecteQuery);
            if (dt.Rows.Count > 0)
            {
                TutionFee = dt.Rows[0][0].ToString();
            }
            else
            {
                MessageBox.Show("You to add fees structure of class", "Warnnig");

            }
        }

        public void LoadFromSchool()
        {

            if (SchoolCheked.Checked == true)
            {
                FromSchool = "Old";
            }
            else
            {
                FromSchool = "New";
            }


        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {

                //SysDate();
                if (Nametxt1.Text == "")
                {
                    MessageBox.Show("Enter Name of Student", "Warnings");
                    Nametxt1.Focus();
                    return;
                }
                else if (MotherTxt.Text == "")
                {
                    MessageBox.Show("Enter Mother's Name", "Warnings");
                    MotherTxt.Focus();
                    return;
                }
                else if (FatherTxt.Text == "")
                {
                    MessageBox.Show("Enter Father's Name", "Warnings");
                    FatherTxt.Focus();
                    return;
                }
                else if (FirstClassBox.Text == "")
                {
                    MessageBox.Show("Select Class of Student", "Warnings");
                    FirstClassBox.Focus();
                    return;
                }
                else if (AcdYearTxt.Text == "")
                {
                    MessageBox.Show("Enter Acd. Year", "Warnings");
                    AcdYearTxt.Focus();
                    return;
                }

                else if (MessageBox.Show("Do you want to save the student?", "Save Student", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    loadTutionFee();

                    if (pictureBox1.Image == null)
                    {
                        Bitmap bmp = Properties.Resources.user_male_512;
                        Image newImg = bmp;
                        pictureBox1.Image = newImg;
                        this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    }
                    {



                        MemoryStream ms = new MemoryStream();
                        pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                        byte[] Img = ms.ToArray();


                        if (SchoolCheked.Checked == true)
                        {


                            String insertQuery = "INSERT INTO Boon.students_high_school (ID,Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,OldId,TcDate,AcdYearFirst,TutionFee,RemaingFee,State,Tq,Dist,Country,OldTcNo) VALUES ('" + IDTxt.Text + "','" + Nametxt1.Text + "','" + Addresstxt2.Text + "','" + Contacttxt3.Text + "','" + Aadhartxt4.Text + "','" + Privioustxt5.Text + "','" + Gender + "','" + DateTime1.Value + "','" + FirstClassBox.Text + "','" + PlaceTxt.Text + "','" + MotherTxt.Text + "','" + FatherTxt.Text + "','" + StudentIDTxt.Text + "','" + CastBox.Text + "','" + SubCastTxt.Text + "','" + CategoryBox.Text + "','" + DateTime2.Value + "','" + NationalityBox.Text + "','" + FirstClassBox.Text + "','" + PreClassBox.Text + "','" + ReligionTxt.Text + "','" + MotherTongueTxt.Text + "','" + AcdYearTxt.Text + "','" + OldGenTxt.Text + "','" + DateTime3.Value + "','" + AcdYearTxt.Text + "','" + TutionFee + "','" + TutionFee + "','" + StateBox.Text + "','" + TqTxt.Text + "','" + DistricTxt.Text + "','" + CountryTxt.Text + "','" + OldTcTxt.Text + "')";

                            if (c.InsertData(insertQuery))
                            {
                                connection.Open();

                                String UpdateQuery = "UPDATE Boon.students_high_school set Profile=@Profile WHERE ID ='" + IDTxt.Text + "'";


                                command = new MySqlCommand(UpdateQuery, connection);
                                command.Parameters.Add("@Profile", MySqlDbType.Blob);
                                //  command.Parameters.Add("@TutionFee", MySqlDbType.Decimal);
                                command.Parameters["@Profile"].Value = Img;    // ,From = @From
                                //  command.Parameters["@TutionFee"].Value = TutionFee;

                                if (command.ExecuteNonQuery() == 1)
                                {

                                    MessageBox.Show("Data saved succesfully...", "Alert");
                                    LoadID();
                                    ClearAllText();
                                }
                                else
                                {
                                    MessageBox.Show("Data is not saved plz check connections");
                                }

                                connection.Close();
                            }

                            else
                            {
                                MessageBox.Show("Data is not saved plz check connections");
                            }
                        }
                        else
                        {
                            String insertQuery = "INSERT INTO Boon.students_high_school(ID,Name,Address,Contact,Aadhar,Gender,DOB,Std,Profile,B_Place,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Religion,MotherTongue,AcdYear,AcdYearFirst,TutionFee,RemaingFee,State,Tq,Dist,Country) VALUES ('" + IDTxt.Text + "','" + Nametxt1.Text + "','" + Addresstxt2.Text + "','" + Contacttxt3.Text + "','" + Aadhartxt4.Text + "','" + Gender + "','" + DateTime1.Value + "','" + FirstClassBox.Text + "','" + Img + "','" + PlaceTxt.Text + "','" + MotherTxt.Text + "','" + FatherTxt.Text + "','" + StudentIDTxt.Text + "','" + CastBox.Text + "','" + SubCastTxt.Text + "','" + CategoryBox.Text + "','" + DateTime2.Value + "','" + NationalityBox.Text + "','" + FirstClassBox.Text + "','" + ReligionTxt.Text + "','" + MotherTongueTxt.Text + "','" + AcdYearTxt.Text + "','" + AcdYearTxt.Text + "','" + TutionFee + "','" + TutionFee + "','" + StateBox.Text + "','" + TqTxt.Text + "','" + DistricTxt.Text + "','" + CountryTxt.Text + "')";
                            if (c.InsertData(insertQuery))
                            {
                                connection.Open();
                                String UpdateQuery = "UPDATE Boon.students_high_school SET Profile=@Profile WHERE ID ='" + IDTxt.Text + "' ";
                                command = new MySqlCommand(UpdateQuery, connection);
                                command.Parameters.Add("@Profile", MySqlDbType.Blob);
                                command.Parameters["@Profile"].Value = Img;

                                if (command.ExecuteNonQuery() == 1)
                                {
                                    MessageBox.Show("Data saved succesfully...", "Alert");
                                    LoadID();
                                    ClearAllText();
                                }
                                else
                                {
                                    MessageBox.Show("Data is not saved plz check connections");
                                }

                                connection.Close();
                            }
                            else
                            {
                                MessageBox.Show("Data is not saved plz check connections");
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public MySqlCommand command { get; set; }
        public void ClearAllText()
        {
            StateBox.Text = null;
            PreClassBox.Text = null;
            FirstClassBox.Text = null;
            MotherTongueTxt.Text = null;
            ReligionTxt.Text = null;
            TqTxt.Text = null;
            DistricTxt.Text = null;
            NationalityBox.Text = null;
            Nametxt1.Text = null;
            Addresstxt2.Text = null;
            Aadhartxt4.Text = null;
            Contacttxt3.Text = null;
            Privioustxt5.Text = null;
            PlaceTxt.Text = null;
            DateTime1.Text = null;
            pictureBox1.Image = null;
            pictureBox1.Invalidate();
            MotherTxt.Text = null;
            StudentIDTxt.Text = null;
            CastBox.Text = null;
            SubCastTxt.Text = null;
            DateTime2.Text = null;
            CategoryBox.Text = null;
            FatherTxt.Text = null;
            this.FemaleRbtn.Checked = false;
            this.MaleRbtn.Checked = false;
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {

            ClearAllText();


        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Gender = "Male";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Gender = "Female";
        }

        private void OpenBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Choose Image(*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(opf.FileName);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
        public void LoadID()
        {
            try
            {
                String SelectQuery = "select Max(ID)+1 from Boon.students_high_school ";

                dt = c.SelectData(SelectQuery);
                if (dt.Rows.Count > 0)
                {
                    IDTxt.Text = dt.Rows[0][0].ToString();
                }
                else
                {
                    IDTxt.Text = "1";
                }
            }
            catch { }

        }
        private void Student_Load(object sender, EventArgs e)
        {
            SchoolCheked.Checked = false;
            NoCheckBox.Checked = false;
            CheackedEvent();
            LoadID();

        }



        private void Aadhartxt4_KeyPress(object sender, KeyPressEventArgs e)
        {

            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }

        }

        private void Nametxt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void Nametxt1_TextChanged(object sender, EventArgs e)
        {


        }

        private void MotherTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void FatherTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void DateTime1_KeyPress(object sender, KeyPressEventArgs e)
        {



        }

        private void PlaceTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }


        }

        private void MotherTongueTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void TqTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void DistricTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void NationalityBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void ReligionTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }

        }

        private void CastBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void SubCastTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;


        }

        private void CategoryBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void Contacttxt3_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }


        }

        private void Addresstxt2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void Privioustxt5_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            //if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            //{
            //    MessageBox.Show("Enter Letters Only", "Alert");
            //    e.Handled = true;
            //}

        }

        private void PreClassBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void StudentIDTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void FirstClassBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;


        }

        private void DateTime2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;


        }

        private void SaveBtn_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;


        }

        private void UploadBtn_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        public void CheackedEvent()
        {
            if (SchoolCheked.Checked == false)
            {
                PreClassBox.Visible = false;
                Privioustxt5.Visible = false;
                DateTime3.Visible = false;
                OldGenTxt.Visible = false;
                school.Visible = false;
                reg.Visible = false;
                std.Visible = false;
                tc.Visible = false;
                groupBox1.Visible = false;

            }
            else
            {
                PreClassBox.Visible = true;
                Privioustxt5.Visible = true;
                DateTime3.Visible = true;
                OldGenTxt.Visible = true;
                school.Visible = true;
                reg.Visible = true;
                std.Visible = true;
                tc.Visible = true;
                groupBox1.Visible = true;

            }
        }

        private void SchoolCheked_CheckedChanged(object sender, EventArgs e)
        {

            CheackedEvent();
            NoCheckBox.Checked = false;
            FromSchool = "Old";
        }

        private void NoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // SchoolCheked.Checked =false;
        }

        private void SchoolCheked_Click(object sender, EventArgs e)
        {
            if (NoCheckBox.Checked == true)
            {
                CheackedEvent();
                NoCheckBox.Checked = false;
                FromSchool = "Old";
            }
        }

        private void NoCheckBox_Click(object sender, EventArgs e)
        {
            if (SchoolCheked.Checked == true)
            {
                SchoolCheked.Checked = false;
                NoCheckBox.Checked = true;
            }
        }

        private void Nolabel_Click(object sender, EventArgs e)
        {
            if (SchoolCheked.Checked == true)
            {
                SchoolCheked.Checked = false;
                NoCheckBox.Checked = true;
            }
        }

        private void Yeslabel_Click(object sender, EventArgs e)
        {
            if (NoCheckBox.Checked == true)
            {
                CheackedEvent();
                NoCheckBox.Checked = false;
                SchoolCheked.Checked = true;
            }
        }

        private void FirstClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SchoolType();
            // LoadFromSchool();
        }

        private void FirstClassBox_MouseClick(object sender, MouseEventArgs e)
        {
            SchoolType();
        }

        private void MaleRbtn_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (ch == 9) // for tab key 
            {
                FemaleRbtn.Focus();
            }
        }

        private void AcdYearTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 45)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }

        }
    }

}


