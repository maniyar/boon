﻿namespace Boon
{
    partial class HighTcAppliction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ProgressTxt = new System.Windows.Forms.ComboBox();
            this.ConductTxt = new System.Windows.Forms.ComboBox();
            this.RemarkTxt = new System.Windows.Forms.ComboBox();
            this.ReasonTxt = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.DateOfLeaving = new System.Windows.Forms.DateTimePicker();
            this.DateOfApplication = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.label11 = new System.Windows.Forms.Label();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.GenNoBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.TdateTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.SaveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Image = global::Boon.Properties.Resources.save;
            this.SaveBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveBtn.Location = new System.Drawing.Point(397, 319);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(102, 39);
            this.SaveBtn.TabIndex = 7;
            this.SaveBtn.Text = "        Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(70, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 18);
            this.label4.TabIndex = 31;
            this.label4.Text = "Reason for Leaving :";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.AutoSize = true;
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.TdateTxt);
            this.groupBox2.Controls.Add(this.ProgressTxt);
            this.groupBox2.Controls.Add(this.ConductTxt);
            this.groupBox2.Controls.Add(this.RemarkTxt);
            this.groupBox2.Controls.Add(this.ReasonTxt);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.comboBox3);
            this.groupBox2.Controls.Add(this.DateOfLeaving);
            this.groupBox2.Controls.Add(this.DateOfApplication);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.SaveBtn);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(388, 236);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(588, 379);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Other details :";
            // 
            // ProgressTxt
            // 
            this.ProgressTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProgressTxt.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProgressTxt.FormattingEnabled = true;
            this.ProgressTxt.Items.AddRange(new object[] {
            "Satisfactory",
            "Good",
            "Excellent"});
            this.ProgressTxt.Location = new System.Drawing.Point(208, 175);
            this.ProgressTxt.Name = "ProgressTxt";
            this.ProgressTxt.Size = new System.Drawing.Size(291, 25);
            this.ProgressTxt.TabIndex = 85;
            // 
            // ConductTxt
            // 
            this.ConductTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConductTxt.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConductTxt.FormattingEnabled = true;
            this.ConductTxt.Items.AddRange(new object[] {
            "Satisfactory",
            "Good",
            "Excellent"});
            this.ConductTxt.Location = new System.Drawing.Point(208, 209);
            this.ConductTxt.Name = "ConductTxt";
            this.ConductTxt.Size = new System.Drawing.Size(291, 25);
            this.ConductTxt.TabIndex = 84;
            // 
            // RemarkTxt
            // 
            this.RemarkTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RemarkTxt.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemarkTxt.FormattingEnabled = true;
            this.RemarkTxt.Items.AddRange(new object[] {
            "She is eligible to get admission in class",
            "He is eligible to get admission in class"});
            this.RemarkTxt.Location = new System.Drawing.Point(208, 72);
            this.RemarkTxt.Name = "RemarkTxt";
            this.RemarkTxt.Size = new System.Drawing.Size(291, 25);
            this.RemarkTxt.TabIndex = 83;
            // 
            // ReasonTxt
            // 
            this.ReasonTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ReasonTxt.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReasonTxt.FormattingEnabled = true;
            this.ReasonTxt.Items.AddRange(new object[] {
            "Due to parents application",
            "Due to rejected by school"});
            this.ReasonTxt.Location = new System.Drawing.Point(208, 38);
            this.ReasonTxt.Name = "ReasonTxt";
            this.ReasonTxt.Size = new System.Drawing.Size(291, 25);
            this.ReasonTxt.TabIndex = 82;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(71, 211);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 18);
            this.label12.TabIndex = 80;
            this.label12.Text = "Conduct :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(71, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 18);
            this.label5.TabIndex = 78;
            this.label5.Text = "Progress :";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "8 th",
            "9 th",
            "10 th"});
            this.comboBox3.Location = new System.Drawing.Point(208, 140);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(291, 26);
            this.comboBox3.TabIndex = 3;
            // 
            // DateOfLeaving
            // 
            this.DateOfLeaving.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateOfLeaving.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateOfLeaving.Location = new System.Drawing.Point(208, 243);
            this.DateOfLeaving.Name = "DateOfLeaving";
            this.DateOfLeaving.Size = new System.Drawing.Size(120, 25);
            this.DateOfLeaving.TabIndex = 6;
            // 
            // DateOfApplication
            // 
            this.DateOfApplication.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateOfApplication.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateOfApplication.Location = new System.Drawing.Point(208, 106);
            this.DateOfApplication.Name = "DateOfApplication";
            this.DateOfApplication.Size = new System.Drawing.Size(120, 25);
            this.DateOfApplication.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(70, 248);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 18);
            this.label10.TabIndex = 36;
            this.label10.Text = "Date of Leaving :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(70, 142);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 18);
            this.label9.TabIndex = 35;
            this.label9.Text = "Running Std :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(70, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 18);
            this.label8.TabIndex = 34;
            this.label8.Text = "Date of Application :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(70, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 18);
            this.label7.TabIndex = 33;
            this.label7.Text = "Remark :";
            // 
            // NameTxt
            // 
            this.NameTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.NameTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.NameTxt.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTxt.Location = new System.Drawing.Point(398, 78);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.ReadOnly = true;
            this.NameTxt.Size = new System.Drawing.Size(359, 25);
            this.NameTxt.TabIndex = 28;
            this.NameTxt.TextChanged += new System.EventHandler(this.NameTxt_TextChanged_1);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(165, 34);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(71, 18);
            this.label.TabIndex = 34;
            this.label.Text = "Select Std :";
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "8 th",
            "9 th",
            "10 th"});
            this.ClassBox.Location = new System.Drawing.Point(242, 30);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 26);
            this.ClassBox.TabIndex = 0;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(232, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 18);
            this.label6.TabIndex = 75;
            this.label6.Text = "Enter Name of Student :";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1362, 741);
            this.shapeContainer1.TabIndex = 76;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 84;
            this.lineShape1.X2 = 1269;
            this.lineShape1.Y1 = 212;
            this.lineShape1.Y2 = 210;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(661, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 18);
            this.label11.TabIndex = 80;
            this.label11.Text = "Gen.No. :";
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.AcdYearBox.BackColor = System.Drawing.SystemColors.Window;
            this.AcdYearBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Location = new System.Drawing.Point(484, 30);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(121, 26);
            this.AcdYearBox.TabIndex = 1;
            this.AcdYearBox.SelectedIndexChanged += new System.EventHandler(this.AcdYearBox_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(407, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 18);
            this.label13.TabIndex = 81;
            this.label13.Text = "Acd.Year :";
            // 
            // GenNoBox
            // 
            this.GenNoBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.GenNoBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.GenNoBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GenNoBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenNoBox.FormattingEnabled = true;
            this.GenNoBox.Location = new System.Drawing.Point(730, 30);
            this.GenNoBox.Name = "GenNoBox";
            this.GenNoBox.Size = new System.Drawing.Size(121, 26);
            this.GenNoBox.TabIndex = 2;
            this.GenNoBox.SelectedIndexChanged += new System.EventHandler(this.GenNoBox_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.NameTxt);
            this.panel1.Controls.Add(this.GenNoBox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.AcdYearBox);
            this.panel1.Controls.Add(this.label);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.ClassBox);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(183, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1018, 135);
            this.panel1.TabIndex = 84;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(91, 741);
            this.panel2.TabIndex = 86;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1271, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(91, 741);
            this.panel3.TabIndex = 87;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(201, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 21);
            this.label1.TabIndex = 85;
            this.label1.Text = "Select student :";
            // 
            // TdateTxt
            // 
            this.TdateTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TdateTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TdateTxt.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TdateTxt.Location = new System.Drawing.Point(208, 277);
            this.TdateTxt.Name = "TdateTxt";
            this.TdateTxt.Size = new System.Drawing.Size(120, 25);
            this.TdateTxt.TabIndex = 86;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(69, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 18);
            this.label2.TabIndex = 87;
            this.label2.Text = "T.C Issuing Date:";
            // 
            // HighTcAppliction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.shapeContainer1);
            this.Name = "HighTcAppliction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "High School Tc Appliction form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.TcAppliction_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.DateTimePicker DateOfLeaving;
        private System.Windows.Forms.DateTimePicker DateOfApplication;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Label label6;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox GenNoBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox ReasonTxt;
        private System.Windows.Forms.ComboBox RemarkTxt;
        private System.Windows.Forms.ComboBox ConductTxt;
        private System.Windows.Forms.ComboBox ProgressTxt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TdateTxt;

    }
}