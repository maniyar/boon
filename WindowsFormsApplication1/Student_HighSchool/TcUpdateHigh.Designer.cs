﻿namespace Boon
{
    partial class TcUpdateHigh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Nametxt = new System.Windows.Forms.TextBox();
            this.AddressTxt = new System.Windows.Forms.TextBox();
            this.ContactTxt = new System.Windows.Forms.TextBox();
            this.AadharTxt = new System.Windows.Forms.TextBox();
            this.MaleRbtn = new System.Windows.Forms.RadioButton();
            this.FemaleRbtn = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.PlaceTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.MotherTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CastBox = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.SubCastTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.CategoryBox = new System.Windows.Forms.ComboBox();
            this.NationalityBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.FatherTxt = new System.Windows.Forms.TextBox();
            this.ReligionTxt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TqTxt = new System.Windows.Forms.TextBox();
            this.DistricTxt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.MotherTongueTxt = new System.Windows.Forms.TextBox();
            this.StateBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CountryTxt = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.reg = new System.Windows.Forms.Label();
            this.FirstClassBox = new System.Windows.Forms.ComboBox();
            this.OldGenTxt = new System.Windows.Forms.TextBox();
            this.PreClassBox = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tc = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.PriviousTxt = new System.Windows.Forms.TextBox();
            this.std = new System.Windows.Forms.Label();
            this.school = new System.Windows.Forms.Label();
            this.OldTcTxt = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.StudentIDTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtTcDate = new System.Windows.Forms.TextBox();
            this.TxtDOAD = new System.Windows.Forms.TextBox();
            this.TxtDOB = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.TdateTxt = new System.Windows.Forms.TextBox();
            this.DateOfLeaving = new System.Windows.Forms.TextBox();
            this.DateOfApplication = new System.Windows.Forms.TextBox();
            this.ProgressTxt = new System.Windows.Forms.ComboBox();
            this.ConductTxt = new System.Windows.Forms.ComboBox();
            this.RemarkTxt = new System.Windows.Forms.ComboBox();
            this.ReasonTxt = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.GenNoBox = new System.Windows.Forms.ComboBox();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.label = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(33, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 22);
            this.label1.TabIndex = 86;
            this.label1.Text = "Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(33, 391);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 22);
            this.label2.TabIndex = 88;
            this.label2.Text = "Address :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(477, 348);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 22);
            this.label3.TabIndex = 89;
            this.label3.Text = "Cont. No. :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(33, 348);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 22);
            this.label4.TabIndex = 91;
            this.label4.Text = "Aadhar No. :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(33, 259);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 22);
            this.label5.TabIndex = 94;
            this.label5.Text = "Date of Birth :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(33, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 22);
            this.label6.TabIndex = 95;
            this.label6.Text = "Gender :";
            // 
            // Nametxt
            // 
            this.Nametxt.BackColor = System.Drawing.SystemColors.Window;
            this.Nametxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nametxt.Location = new System.Drawing.Point(157, 37);
            this.Nametxt.Name = "Nametxt";
            this.Nametxt.Size = new System.Drawing.Size(618, 29);
            this.Nametxt.TabIndex = 90;
            // 
            // AddressTxt
            // 
            this.AddressTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AddressTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressTxt.Location = new System.Drawing.Point(157, 388);
            this.AddressTxt.Name = "AddressTxt";
            this.AddressTxt.Size = new System.Drawing.Size(618, 29);
            this.AddressTxt.TabIndex = 112;
            // 
            // ContactTxt
            // 
            this.ContactTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ContactTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ContactTxt.Location = new System.Drawing.Point(571, 345);
            this.ContactTxt.Name = "ContactTxt";
            this.ContactTxt.Size = new System.Drawing.Size(204, 29);
            this.ContactTxt.TabIndex = 111;
            // 
            // AadharTxt
            // 
            this.AadharTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AadharTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AadharTxt.Location = new System.Drawing.Point(157, 345);
            this.AadharTxt.Name = "AadharTxt";
            this.AadharTxt.Size = new System.Drawing.Size(204, 29);
            this.AadharTxt.TabIndex = 110;
            // 
            // MaleRbtn
            // 
            this.MaleRbtn.AutoSize = true;
            this.MaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaleRbtn.Location = new System.Drawing.Point(157, 121);
            this.MaleRbtn.Name = "MaleRbtn";
            this.MaleRbtn.Size = new System.Drawing.Size(62, 26);
            this.MaleRbtn.TabIndex = 96;
            this.MaleRbtn.Text = "Male";
            this.MaleRbtn.UseVisualStyleBackColor = true;
            this.MaleRbtn.CheckedChanged += new System.EventHandler(this.MaleRbtn_CheckedChanged);
            // 
            // FemaleRbtn
            // 
            this.FemaleRbtn.AutoSize = true;
            this.FemaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FemaleRbtn.Location = new System.Drawing.Point(251, 122);
            this.FemaleRbtn.Name = "FemaleRbtn";
            this.FemaleRbtn.Size = new System.Drawing.Size(77, 26);
            this.FemaleRbtn.TabIndex = 97;
            this.FemaleRbtn.Text = "Female";
            this.FemaleRbtn.UseVisualStyleBackColor = true;
            this.FemaleRbtn.CheckedChanged += new System.EventHandler(this.FemaleRbtn_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(299, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 22);
            this.label8.TabIndex = 113;
            this.label8.Text = "Place of birth :";
            // 
            // PlaceTxt
            // 
            this.PlaceTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PlaceTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceTxt.Location = new System.Drawing.Point(409, 256);
            this.PlaceTxt.Name = "PlaceTxt";
            this.PlaceTxt.Size = new System.Drawing.Size(112, 29);
            this.PlaceTxt.TabIndex = 105;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(33, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 22);
            this.label11.TabIndex = 114;
            this.label11.Text = "Mother\'s Name :";
            // 
            // MotherTxt
            // 
            this.MotherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTxt.Location = new System.Drawing.Point(157, 79);
            this.MotherTxt.Name = "MotherTxt";
            this.MotherTxt.Size = new System.Drawing.Size(204, 29);
            this.MotherTxt.TabIndex = 92;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(579, 163);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 22);
            this.label13.TabIndex = 116;
            this.label13.Text = "Caste :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CastBox
            // 
            this.CastBox.BackColor = System.Drawing.SystemColors.Window;
            this.CastBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CastBox.FormattingEnabled = true;
            this.CastBox.Items.AddRange(new object[] {
            "Muslim",
            "Crishshan",
            "Hindu",
            "Sikh",
            "Buddh",
            "Jain",
            "Other"});
            this.CastBox.Location = new System.Drawing.Point(663, 159);
            this.CastBox.Name = "CastBox";
            this.CastBox.Size = new System.Drawing.Size(112, 30);
            this.CastBox.TabIndex = 100;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(33, 210);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 22);
            this.label14.TabIndex = 117;
            this.label14.Text = "Sub Caste :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SubCastTxt
            // 
            this.SubCastTxt.BackColor = System.Drawing.SystemColors.Window;
            this.SubCastTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubCastTxt.Location = new System.Drawing.Point(157, 205);
            this.SubCastTxt.Name = "SubCastTxt";
            this.SubCastTxt.Size = new System.Drawing.Size(112, 29);
            this.SubCastTxt.TabIndex = 101;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(325, 210);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 22);
            this.label16.TabIndex = 118;
            this.label16.Text = "Category :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CategoryBox
            // 
            this.CategoryBox.BackColor = System.Drawing.SystemColors.Window;
            this.CategoryBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategoryBox.FormattingEnabled = true;
            this.CategoryBox.Items.AddRange(new object[] {
            "Open",
            "OBC",
            "NT",
            "NT3",
            "SC",
            "ST",
            "VJNT",
            "Other"});
            this.CategoryBox.Location = new System.Drawing.Point(409, 206);
            this.CategoryBox.Name = "CategoryBox";
            this.CategoryBox.Size = new System.Drawing.Size(112, 30);
            this.CategoryBox.TabIndex = 102;
            // 
            // NationalityBox
            // 
            this.NationalityBox.BackColor = System.Drawing.SystemColors.Window;
            this.NationalityBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NationalityBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NationalityBox.FormattingEnabled = true;
            this.NationalityBox.Items.AddRange(new object[] {
            "Indian",
            "Other"});
            this.NationalityBox.Location = new System.Drawing.Point(663, 206);
            this.NationalityBox.Name = "NationalityBox";
            this.NationalityBox.Size = new System.Drawing.Size(112, 30);
            this.NationalityBox.TabIndex = 103;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(559, 306);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 22);
            this.label18.TabIndex = 120;
            this.label18.Text = "Country :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(416, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(117, 22);
            this.label19.TabIndex = 121;
            this.label19.Text = "Father\'s Name :";
            // 
            // FatherTxt
            // 
            this.FatherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.FatherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FatherTxt.Location = new System.Drawing.Point(571, 81);
            this.FatherTxt.Name = "FatherTxt";
            this.FatherTxt.Size = new System.Drawing.Size(204, 29);
            this.FatherTxt.TabIndex = 93;
            // 
            // ReligionTxt
            // 
            this.ReligionTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ReligionTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReligionTxt.Location = new System.Drawing.Point(409, 160);
            this.ReligionTxt.Name = "ReligionTxt";
            this.ReligionTxt.Size = new System.Drawing.Size(112, 29);
            this.ReligionTxt.TabIndex = 99;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(333, 163);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 22);
            this.label20.TabIndex = 122;
            this.label20.Text = "Religion :";
            // 
            // TqTxt
            // 
            this.TqTxt.BackColor = System.Drawing.SystemColors.Window;
            this.TqTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TqTxt.Location = new System.Drawing.Point(663, 256);
            this.TqTxt.Name = "TqTxt";
            this.TqTxt.Size = new System.Drawing.Size(112, 29);
            this.TqTxt.TabIndex = 106;
            // 
            // DistricTxt
            // 
            this.DistricTxt.BackColor = System.Drawing.SystemColors.Window;
            this.DistricTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DistricTxt.Location = new System.Drawing.Point(157, 303);
            this.DistricTxt.Name = "DistricTxt";
            this.DistricTxt.Size = new System.Drawing.Size(112, 29);
            this.DistricTxt.TabIndex = 107;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(599, 259);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 22);
            this.label21.TabIndex = 123;
            this.label21.Text = "Tq :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(33, 306);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 22);
            this.label22.TabIndex = 124;
            this.label22.Text = "Dist. :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(33, 163);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(123, 22);
            this.label24.TabIndex = 125;
            this.label24.Text = "Mother Tongue :";
            // 
            // MotherTongueTxt
            // 
            this.MotherTongueTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTongueTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTongueTxt.Location = new System.Drawing.Point(157, 160);
            this.MotherTongueTxt.Name = "MotherTongueTxt";
            this.MotherTongueTxt.Size = new System.Drawing.Size(112, 29);
            this.MotherTongueTxt.TabIndex = 98;
            // 
            // StateBox
            // 
            this.StateBox.BackColor = System.Drawing.SystemColors.Window;
            this.StateBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StateBox.FormattingEnabled = true;
            this.StateBox.Items.AddRange(new object[] {
            "Andhra Pradesh",
            "Arunachal Pradesh",
            "Assam",
            "Bihar",
            "Chhattisgarh",
            "Goa",
            "Gujarat",
            "Haryana",
            "Himachal Pradesh",
            "Jammu & Kashmir",
            "Jharkhand",
            "Karnataka",
            "Kerala",
            "Madhya Pradesh",
            "Maharashtra",
            "Manipur",
            "Meghalaya",
            "Mizoram",
            "Nagaland",
            "Odisha",
            "Punjab",
            "Rajasthan",
            "Sikkim",
            "Tamil Nadu",
            "Telangana",
            "Tripura",
            "Uttarakhand",
            "Uttar Pradesh",
            "West Bengal"});
            this.StateBox.Location = new System.Drawing.Point(409, 302);
            this.StateBox.Name = "StateBox";
            this.StateBox.Size = new System.Drawing.Size(112, 30);
            this.StateBox.TabIndex = 108;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(354, 306);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 22);
            this.label7.TabIndex = 126;
            this.label7.Text = "State :";
            // 
            // CountryTxt
            // 
            this.CountryTxt.BackColor = System.Drawing.SystemColors.Window;
            this.CountryTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CountryTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CountryTxt.FormattingEnabled = true;
            this.CountryTxt.Items.AddRange(new object[] {
            "India",
            "Other"});
            this.CountryTxt.Location = new System.Drawing.Point(663, 302);
            this.CountryTxt.Name = "CountryTxt";
            this.CountryTxt.Size = new System.Drawing.Size(112, 30);
            this.CountryTxt.TabIndex = 109;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(539, 210);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 22);
            this.label23.TabIndex = 127;
            this.label23.Text = "Nationality :";
            // 
            // reg
            // 
            this.reg.AutoSize = true;
            this.reg.BackColor = System.Drawing.Color.Transparent;
            this.reg.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reg.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.reg.Location = new System.Drawing.Point(333, 531);
            this.reg.Name = "reg";
            this.reg.Size = new System.Drawing.Size(73, 22);
            this.reg.TabIndex = 139;
            this.reg.Text = "Reg.No. :";
            // 
            // FirstClassBox
            // 
            this.FirstClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.FirstClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FirstClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstClassBox.FormattingEnabled = true;
            this.FirstClassBox.Items.AddRange(new object[] {
            "8 th",
            "9 th",
            "10 th"});
            this.FirstClassBox.Location = new System.Drawing.Point(157, 486);
            this.FirstClassBox.Name = "FirstClassBox";
            this.FirstClassBox.Size = new System.Drawing.Size(112, 30);
            this.FirstClassBox.TabIndex = 128;
            // 
            // OldGenTxt
            // 
            this.OldGenTxt.BackColor = System.Drawing.SystemColors.Window;
            this.OldGenTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OldGenTxt.Location = new System.Drawing.Point(420, 528);
            this.OldGenTxt.Name = "OldGenTxt";
            this.OldGenTxt.Size = new System.Drawing.Size(113, 29);
            this.OldGenTxt.TabIndex = 132;
            // 
            // PreClassBox
            // 
            this.PreClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.PreClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PreClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreClassBox.FormattingEnabled = true;
            this.PreClassBox.ItemHeight = 22;
            this.PreClassBox.Items.AddRange(new object[] {
            "7 th",
            "8 th",
            "9 th",
            "10 th"});
            this.PreClassBox.Location = new System.Drawing.Point(157, 532);
            this.PreClassBox.Name = "PreClassBox";
            this.PreClassBox.Size = new System.Drawing.Size(112, 30);
            this.PreClassBox.TabIndex = 130;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(270, 490);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(141, 22);
            this.label15.TabIndex = 137;
            this.label15.Text = "Date of Admission :";
            // 
            // tc
            // 
            this.tc.AutoSize = true;
            this.tc.BackColor = System.Drawing.Color.Transparent;
            this.tc.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tc.Location = new System.Drawing.Point(553, 531);
            this.tc.Name = "tc";
            this.tc.Size = new System.Drawing.Size(81, 22);
            this.tc.TabIndex = 140;
            this.tc.Text = "T.C. Date :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(33, 490);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 22);
            this.label9.TabIndex = 136;
            this.label9.Text = "Admitted Std. :";
            // 
            // PriviousTxt
            // 
            this.PriviousTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PriviousTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PriviousTxt.Location = new System.Drawing.Point(157, 437);
            this.PriviousTxt.Name = "PriviousTxt";
            this.PriviousTxt.Size = new System.Drawing.Size(622, 29);
            this.PriviousTxt.TabIndex = 129;
            // 
            // std
            // 
            this.std.AutoSize = true;
            this.std.BackColor = System.Drawing.Color.Transparent;
            this.std.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.std.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.std.Location = new System.Drawing.Point(33, 540);
            this.std.Name = "std";
            this.std.Size = new System.Drawing.Size(107, 22);
            this.std.TabIndex = 138;
            this.std.Text = "Prevoius Std. :";
            // 
            // school
            // 
            this.school.AutoSize = true;
            this.school.BackColor = System.Drawing.Color.Transparent;
            this.school.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.school.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.school.Location = new System.Drawing.Point(33, 444);
            this.school.Name = "school";
            this.school.Size = new System.Drawing.Size(63, 22);
            this.school.TabIndex = 135;
            this.school.Text = "School :";
            // 
            // OldTcTxt
            // 
            this.OldTcTxt.BackColor = System.Drawing.SystemColors.Window;
            this.OldTcTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OldTcTxt.Location = new System.Drawing.Point(663, 483);
            this.OldTcTxt.Name = "OldTcTxt";
            this.OldTcTxt.Size = new System.Drawing.Size(116, 29);
            this.OldTcTxt.TabIndex = 133;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(564, 490);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 22);
            this.label25.TabIndex = 141;
            this.label25.Text = "T.C No. :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(33, 585);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 22);
            this.label12.TabIndex = 143;
            this.label12.Text = "Online ID :";
            // 
            // StudentIDTxt
            // 
            this.StudentIDTxt.BackColor = System.Drawing.SystemColors.Window;
            this.StudentIDTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentIDTxt.Location = new System.Drawing.Point(157, 585);
            this.StudentIDTxt.Name = "StudentIDTxt";
            this.StudentIDTxt.Size = new System.Drawing.Size(204, 29);
            this.StudentIDTxt.TabIndex = 142;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtTcDate);
            this.groupBox1.Controls.Add(this.TxtDOAD);
            this.groupBox1.Controls.Add(this.TxtDOB);
            this.groupBox1.Controls.Add(this.StudentIDTxt);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.OldTcTxt);
            this.groupBox1.Controls.Add(this.school);
            this.groupBox1.Controls.Add(this.std);
            this.groupBox1.Controls.Add(this.PriviousTxt);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tc);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.PreClassBox);
            this.groupBox1.Controls.Add(this.OldGenTxt);
            this.groupBox1.Controls.Add(this.FirstClassBox);
            this.groupBox1.Controls.Add(this.reg);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.CountryTxt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.StateBox);
            this.groupBox1.Controls.Add(this.MotherTongueTxt);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.DistricTxt);
            this.groupBox1.Controls.Add(this.TqTxt);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.ReligionTxt);
            this.groupBox1.Controls.Add(this.FatherTxt);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.NationalityBox);
            this.groupBox1.Controls.Add(this.CategoryBox);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.SubCastTxt);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.CastBox);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.MotherTxt);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.PlaceTxt);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.FemaleRbtn);
            this.groupBox1.Controls.Add(this.MaleRbtn);
            this.groupBox1.Controls.Add(this.AadharTxt);
            this.groupBox1.Controls.Add(this.ContactTxt);
            this.groupBox1.Controls.Add(this.AddressTxt);
            this.groupBox1.Controls.Add(this.Nametxt);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(27, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(830, 628);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Student Info :";
            // 
            // TxtTcDate
            // 
            this.TxtTcDate.BackColor = System.Drawing.SystemColors.Window;
            this.TxtTcDate.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTcDate.Location = new System.Drawing.Point(663, 528);
            this.TxtTcDate.Name = "TxtTcDate";
            this.TxtTcDate.Size = new System.Drawing.Size(116, 29);
            this.TxtTcDate.TabIndex = 146;
            // 
            // TxtDOAD
            // 
            this.TxtDOAD.BackColor = System.Drawing.SystemColors.Window;
            this.TxtDOAD.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOAD.Location = new System.Drawing.Point(420, 487);
            this.TxtDOAD.Name = "TxtDOAD";
            this.TxtDOAD.Size = new System.Drawing.Size(112, 29);
            this.TxtDOAD.TabIndex = 145;
            // 
            // TxtDOB
            // 
            this.TxtDOB.BackColor = System.Drawing.SystemColors.Window;
            this.TxtDOB.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOB.Location = new System.Drawing.Point(157, 256);
            this.TxtDOB.Name = "TxtDOB";
            this.TxtDOB.Size = new System.Drawing.Size(112, 29);
            this.TxtDOB.TabIndex = 144;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.AutoSize = true;
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.TdateTxt);
            this.groupBox2.Controls.Add(this.DateOfLeaving);
            this.groupBox2.Controls.Add(this.DateOfApplication);
            this.groupBox2.Controls.Add(this.ProgressTxt);
            this.groupBox2.Controls.Add(this.ConductTxt);
            this.groupBox2.Controls.Add(this.RemarkTxt);
            this.groupBox2.Controls.Add(this.ReasonTxt);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.SaveBtn);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(875, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(449, 433);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Other details :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(24, 302);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(107, 18);
            this.label32.TabIndex = 110;
            this.label32.Text = "T.C Issuing Date:";
            // 
            // TdateTxt
            // 
            this.TdateTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TdateTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TdateTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TdateTxt.Location = new System.Drawing.Point(158, 299);
            this.TdateTxt.Name = "TdateTxt";
            this.TdateTxt.Size = new System.Drawing.Size(120, 29);
            this.TdateTxt.TabIndex = 109;
            // 
            // DateOfLeaving
            // 
            this.DateOfLeaving.BackColor = System.Drawing.SystemColors.Window;
            this.DateOfLeaving.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateOfLeaving.Location = new System.Drawing.Point(158, 254);
            this.DateOfLeaving.Name = "DateOfLeaving";
            this.DateOfLeaving.Size = new System.Drawing.Size(270, 29);
            this.DateOfLeaving.TabIndex = 108;
            // 
            // DateOfApplication
            // 
            this.DateOfApplication.BackColor = System.Drawing.SystemColors.Window;
            this.DateOfApplication.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateOfApplication.Location = new System.Drawing.Point(158, 126);
            this.DateOfApplication.Name = "DateOfApplication";
            this.DateOfApplication.Size = new System.Drawing.Size(270, 29);
            this.DateOfApplication.TabIndex = 107;
            // 
            // ProgressTxt
            // 
            this.ProgressTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProgressTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProgressTxt.FormattingEnabled = true;
            this.ProgressTxt.Items.AddRange(new object[] {
            "Satisfactory",
            "Good",
            "Excellent"});
            this.ProgressTxt.Location = new System.Drawing.Point(158, 168);
            this.ProgressTxt.Name = "ProgressTxt";
            this.ProgressTxt.Size = new System.Drawing.Size(270, 30);
            this.ProgressTxt.TabIndex = 85;
            // 
            // ConductTxt
            // 
            this.ConductTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConductTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConductTxt.FormattingEnabled = true;
            this.ConductTxt.Items.AddRange(new object[] {
            "Satisfactory",
            "Good",
            "Excellent"});
            this.ConductTxt.Location = new System.Drawing.Point(158, 211);
            this.ConductTxt.Name = "ConductTxt";
            this.ConductTxt.Size = new System.Drawing.Size(270, 30);
            this.ConductTxt.TabIndex = 84;
            // 
            // RemarkTxt
            // 
            this.RemarkTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RemarkTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemarkTxt.FormattingEnabled = true;
            this.RemarkTxt.Items.AddRange(new object[] {
            "She is eligible to get admission in class",
            "He is eligible to get admission in class"});
            this.RemarkTxt.Location = new System.Drawing.Point(158, 83);
            this.RemarkTxt.Name = "RemarkTxt";
            this.RemarkTxt.Size = new System.Drawing.Size(270, 30);
            this.RemarkTxt.TabIndex = 83;
            // 
            // ReasonTxt
            // 
            this.ReasonTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ReasonTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReasonTxt.FormattingEnabled = true;
            this.ReasonTxt.Items.AddRange(new object[] {
            "Due to parents application",
            "Due to rejected by school"});
            this.ReasonTxt.Location = new System.Drawing.Point(158, 40);
            this.ReasonTxt.Name = "ReasonTxt";
            this.ReasonTxt.Size = new System.Drawing.Size(270, 30);
            this.ReasonTxt.TabIndex = 82;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(24, 219);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 18);
            this.label10.TabIndex = 80;
            this.label10.Text = "Conduct :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(24, 176);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 18);
            this.label17.TabIndex = 78;
            this.label17.Text = "Progress :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(24, 262);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(106, 18);
            this.label26.TabIndex = 36;
            this.label26.Text = "Date of Leaving :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(24, 133);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(129, 18);
            this.label28.TabIndex = 34;
            this.label28.Text = "Date of Application :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(24, 90);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 18);
            this.label29.TabIndex = 33;
            this.label29.Text = "Remark :";
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.SaveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveBtn.Location = new System.Drawing.Point(332, 348);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(96, 30);
            this.SaveBtn.TabIndex = 7;
            this.SaveBtn.Text = "Update";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(24, 47);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(128, 18);
            this.label30.TabIndex = 31;
            this.label30.Text = "Reason for Leaving :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.GenNoBox);
            this.panel1.Controls.Add(this.AcdYearBox);
            this.panel1.Controls.Add(this.label);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.ClassBox);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Location = new System.Drawing.Point(244, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(874, 63);
            this.panel1.TabIndex = 85;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(741, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 26);
            this.button1.TabIndex = 82;
            this.button1.Text = "Get ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.GetBtn_Click);
            // 
            // GenNoBox
            // 
            this.GenNoBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.GenNoBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GenNoBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenNoBox.FormattingEnabled = true;
            this.GenNoBox.Location = new System.Drawing.Point(604, 17);
            this.GenNoBox.Name = "GenNoBox";
            this.GenNoBox.Size = new System.Drawing.Size(121, 26);
            this.GenNoBox.TabIndex = 2;
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.AcdYearBox.BackColor = System.Drawing.SystemColors.Window;
            this.AcdYearBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Location = new System.Drawing.Point(376, 17);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(121, 26);
            this.AcdYearBox.TabIndex = 1;
            this.AcdYearBox.SelectedIndexChanged += new System.EventHandler(this.AcdYearBox_SelectedIndexChanged);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(43, 19);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(84, 22);
            this.label.TabIndex = 34;
            this.label.Text = "Select Std :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(280, 19);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(80, 22);
            this.label27.TabIndex = 81;
            this.label27.Text = "Acd.Year :";
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "8 th",
            "9 th",
            "10 th"});
            this.ClassBox.Location = new System.Drawing.Point(143, 17);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 26);
            this.ClassBox.TabIndex = 0;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(513, 19);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(75, 22);
            this.label31.TabIndex = 80;
            this.label31.Text = "Gen.No. :";
            // 
            // TcUpdateHigh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TcUpdateHigh";
            this.Text = "Tc Update High School";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Nametxt;
        private System.Windows.Forms.TextBox AddressTxt;
        private System.Windows.Forms.TextBox ContactTxt;
        private System.Windows.Forms.TextBox AadharTxt;
        private System.Windows.Forms.RadioButton MaleRbtn;
        private System.Windows.Forms.RadioButton FemaleRbtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox PlaceTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox MotherTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox CastBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox SubCastTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox CategoryBox;
        private System.Windows.Forms.ComboBox NationalityBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox FatherTxt;
        private System.Windows.Forms.TextBox ReligionTxt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TqTxt;
        private System.Windows.Forms.TextBox DistricTxt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox MotherTongueTxt;
        private System.Windows.Forms.ComboBox StateBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CountryTxt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label reg;
        private System.Windows.Forms.ComboBox FirstClassBox;
        private System.Windows.Forms.TextBox OldGenTxt;
        private System.Windows.Forms.ComboBox PreClassBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label tc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox PriviousTxt;
        private System.Windows.Forms.Label std;
        private System.Windows.Forms.Label school;
        private System.Windows.Forms.TextBox OldTcTxt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox StudentIDTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ProgressTxt;
        private System.Windows.Forms.ComboBox ConductTxt;
        private System.Windows.Forms.ComboBox RemarkTxt;
        private System.Windows.Forms.ComboBox ReasonTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox GenNoBox;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TxtTcDate;
        private System.Windows.Forms.TextBox TxtDOAD;
        private System.Windows.Forms.TextBox TxtDOB;
        private System.Windows.Forms.TextBox DateOfApplication;
        private System.Windows.Forms.TextBox DateOfLeaving;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox TdateTxt;


    }
}