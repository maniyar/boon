﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DGVPrinterHelper;

namespace Boon
{
    public partial class UpdateClassPPrimary : Form
    {
        MySqlConnection connection;
        common c = new common();
        public UpdateClassPPrimary()
        {
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
      
        String TutionFee = "0", Fname, Mname, Religion, Cast, Gender, ID, Sub_Cast, Nationality, Address, Mtongue, B_place, Lschool = "BOON ENGLISH PRIMARY SCHOOL", D_O_A, Contact, DOB, FirstStd, Pre_Std, AcdYear, StudIdTxt, Aadhar, State, RemaingFee, Category, NameOfStudent;
        
        DataTable dt = new DataTable();

        DataTable table = new DataTable();
        // String  TutionFee = "0";
        // int Contact;

        private void GetListBtn_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            StudCountTxt.Text = "Total students : 0";
            // dataGridView1.DataSource =null;
            try
            {
                if (StdBox.Text == "")
                {
                    MessageBox.Show("Please select standard first");
                    StdBox.Focus();
                    return;

                }

                else
                {
                    //AND RemaingFee = 0
                    String SelectQuery = "SELECT * from Boon.stud_p_primary WHERE Std = '" + StdBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'   Order BY ID";
                    DataTable table = new DataTable();
                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = item["Address"].ToString();
                        dataGridView1.Rows[n].Cells[3].Value = item["Contact"].ToString();
                        dataGridView1.Rows[n].Cells[4].Value = item["Std"].ToString();
                        StudCountTxt.Text = ("Total students :" + dataGridView1.Rows.Count).ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void CheakAllBtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    row.Cells[5].Value = row.Cells[5].Value == null ? false : !(bool)row.Cells[5].Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }

        }

        private void UpdateClass_Load(object sender, EventArgs e)
        {
            StdBox.Focus();

        }
        public void PassValue()
        {
            try
            {
                int i = 0;
                int j = 0, Up = 0, a = 0;
                LoadTutionFee();
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                    if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
                    {
                        ++i;
                        var SelectedID = row.Cells[0].Value.ToString();

                        if (ClassBox.Text == "1 st")
                        {
                            a = 1;
                        }
                        switch (a)
                        {
                            case 0:
                                {
                                    String SelectQuery = "SELECT  * FROM Boon.stud_p_primary WHERE ID =  '" + SelectedID + "' ";
                                    table = c.SelectData(SelectQuery);
                                    if (table.Rows.Count > 0)
                                    {
                                        ID = table.Rows[0][0].ToString();
                                        NameOfStudent = table.Rows[0][1].ToString();
                                        Address = table.Rows[0][2].ToString();
                                        Contact = table.Rows[0][3].ToString();
                                        Aadhar = table.Rows[0][4].ToString();
                                        Lschool = table.Rows[0][5].ToString();
                                        Gender = table.Rows[0][6].ToString();
                                        DOB = table.Rows[0][7].ToString();
                                        B_place = table.Rows[0][9].ToString();
                                        Mname = table.Rows[0][11].ToString();
                                        Fname = table.Rows[0][12].ToString();
                                        StudIdTxt = table.Rows[0][13].ToString();
                                        Cast = table.Rows[0][14].ToString();
                                        Sub_Cast = table.Rows[0][15].ToString();
                                        Category = table.Rows[0][16].ToString();
                                        D_O_A = table.Rows[0][17].ToString();
                                        //  D_O_A = DateTime.Today.ToString();
                                        Nationality = table.Rows[0][18].ToString();
                                        int OldRemFees = Convert.ToInt32(table.Rows[0][19].ToString());
                                        Religion = table.Rows[0][24].ToString();
                                        Mtongue = table.Rows[0][25].ToString();
                                        Pre_Std = table.Rows[0][26].ToString();
                                        // Pre_Std = "";
                                        FirstStd = table.Rows[0][27].ToString();
                                        AcdYear = table.Rows[0][28].ToString();
                                        State = table.Rows[0][34].ToString();
                                        RemaingFee = table.Rows[0][33].ToString();
                                        String Tq = table.Rows[0][35].ToString();
                                        String Dist = table.Rows[0][36].ToString();
                                        String Country = table.Rows[0][37].ToString();
                                        String AcdYearFirst = table.Rows[0][31].ToString();
                                        String SaveQuery = "INSERT INTO Boon.stud_p_primary_copy (ID,Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,State,RemaingFee,Tq,Dist,Country,AcdYearFirst) VALUES('" + SelectedID + "','" + NameOfStudent + "','" + Address + "','" + Contact + "','" + Aadhar + "','" + Lschool + "','" + Gender + "','" + DOB + "','" + StdBox.Text + "','" + B_place + "','" + Mname + "','" + Fname + "','" + StudIdTxt + "','" + Cast + "','" + Sub_Cast + "','" + Category + "','" + D_O_A + "','" + Nationality + "','" + FirstStd + "','" + Pre_Std + "','" + Religion + "','" + Mtongue + "','" + AcdYearBox.Text + "','" + State + "','" + RemaingFee + "','" + Tq + "','" + Dist + "','" + Country + "','" + AcdYearFirst + "')";

                                        if (c.InsertData(SaveQuery))
                                        {
                                            String UpdateQuery = "UPDATE Boon.stud_p_primary SET Std = '" + ClassBox.Text + "' , AcdYear = '" + AcdYearBox1.Text + "',TutionFee = '" + TutionFee + "',RemaingFee ='" + TutionFee + "',OldRemFees= OldRemFees +'" + Convert.ToInt32(RemaingFee) + "' Where ID = '" + SelectedID + "' ";   // AND RemaingFee = 0 
                                            if (c.InsertData(UpdateQuery))
                                            {
                                                ++Up;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ++j;
                                    }

                                    if (i + j == dataGridView1.Rows.Count)
                                    {
                                        // MessageBox.Show("" + Up + " Student updated succsessfully", "Alert");
                                    }

                                    break;
                                }
                            case 1:
                                {
                                    String SelectQuery = "SELECT  * FROM Boon.stud_p_primary WHERE ID =  '" + SelectedID + "' ";
                                    table = c.SelectData(SelectQuery);
                                    if (table.Rows.Count > 0)
                                    {
                                        ID = table.Rows[0][0].ToString();
                                        NameOfStudent = table.Rows[0][1].ToString();
                                        Address = table.Rows[0][2].ToString();
                                        Contact = table.Rows[0][3].ToString();
                                        Aadhar = table.Rows[0][4].ToString();
                                        Lschool = "Boon English Pre-Primary School Latur";
                                        Gender = table.Rows[0][6].ToString();
                                        DOB = table.Rows[0][7].ToString();
                                        B_place = table.Rows[0][9].ToString();
                                        Mname = table.Rows[0][11].ToString();
                                        Fname = table.Rows[0][12].ToString();
                                        StudIdTxt = table.Rows[0][13].ToString();
                                        Cast = table.Rows[0][14].ToString();
                                        Sub_Cast = table.Rows[0][15].ToString();
                                        Category = table.Rows[0][16].ToString();
                                        //  D_O_A = table.Rows[0][17].ToString();
                                        D_O_A = DateTime.Today.ToString();
                                        Nationality = table.Rows[0][18].ToString();
                                        Religion = table.Rows[0][24].ToString();
                                        Mtongue = table.Rows[0][25].ToString();
                                        //  Pre_Std = table.Rows[0][26].ToString();
                                        Pre_Std = "Sr.KG";
                                        FirstStd = table.Rows[0][27].ToString();
                                        AcdYear = table.Rows[0][28].ToString();
                                        State = table.Rows[0][34].ToString();
                                        RemaingFee = table.Rows[0][33].ToString();

                                        String Tq = table.Rows[0][35].ToString();
                                        String Dist = table.Rows[0][36].ToString();
                                        String Country = table.Rows[0][37].ToString();
                                        String AcdYearFirst = table.Rows[0][31].ToString();
                                        String SaveQuery = "INSERT INTO Boon.students(Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place ,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,TutionFee,State,RemaingFee,Tq,Dist,Country,AcdYearFirst,OldRemFees) VALUES('" + NameOfStudent + "','" + Address + "','" + Contact + "','" + Aadhar + "','" + Lschool + "','" + Gender + "','" + DOB + "','" + ClassBox.Text + "','" + B_place + "','" + Mname + "','" + Fname + "','" + StudIdTxt + "','" + Cast + "','" + Sub_Cast + "','" + Category + "','" + D_O_A + "','" + Nationality + "','" + ClassBox.Text + "','" + Pre_Std + "','" + Religion + "','" + Mtongue + "','" + AcdYearBox1.Text + "','" + TutionFee + "','" + State + "','" + TutionFee + "','" + Tq + "','" + Dist + "','" + Country + "','" + AcdYearFirst + "','" + RemaingFee + "')";

                                        if (c.InsertData(SaveQuery))
                                        {
                                            Pre_Std = table.Rows[0][26].ToString();
                                            Lschool = table.Rows[0][5].ToString();

                                            String SaveQuery1 = "INSERT INTO Boon.stud_p_primary_copy (ID,Name,Address,Contact,Aadhar,Previous_school,Gender,DOB,Std,B_Place ,Mother_Name,Father_Name,StudentId,Cast,SubCast,Category,D_O_A,Nationality,FirstStd,Pre_Std,Religion,MotherTongue,AcdYear,Tq,Dist,Country,AcdYearFirst,State) VALUES('" + SelectedID + "','" + NameOfStudent + "','" + Address + "','" + Contact + "','" + Aadhar + "','" + Lschool + "','" + Gender + "','" + DOB + "','" + StdBox.Text + "','" + B_place + "','" + Mname + "','" + Fname + "','" + StudIdTxt + "','" + Cast + "','" + Sub_Cast + "','" + Category + "','" + D_O_A + "','" + Nationality + "','" + FirstStd + "','" + Pre_Std + "','" + Religion + "','" + Mtongue + "','" + AcdYearBox.Text + "','" + Tq + "','" + Dist + "','" + Country + "','" + AcdYearFirst + "','" + State + "')";
                                            if (c.InsertData(SaveQuery1))
                                            {
                                                ++Up;
                                            }
                                        }
                                    }
                                    break;
                                }
                        }
                    }
                    else
                    {
                        ++j;
                    }
                    if (i + j == dataGridView1.Rows.Count)
                    {
                        MessageBox.Show("" + Up + " Student updated succsessfully", "Alert");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }


        private void AddBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                int i1 = 0;

                if (AcdYearBox1.Text == "" || ClassBox.Text == "")
                {
                    MessageBox.Show("You have to select Acd.Year & class from update panel", "Alert");
                    return;
                }
                LoadTutionFee();
                if (MessageBox.Show("Do you want to update students?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in this.dataGridView1.Rows)
                    {
                        //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
                        if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
                        {
                            i1++;
                            if (i1 == dataGridView1.Rows.Count)
                            {
                                MessageBox.Show("You have to select at least one student", "Alert");
                            }
                        }
                        else
                        {
                            PassValue();
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //foreach (DataGridViewRow row in this.dataGridView1.Rows)
            //     {
            //         //CheckBox means Check Box Column Name in Datagridview1   // AND AcdYear = '"+AcdYearBox.Text+"'
            //         if (Convert.ToBoolean(row.Cells["Check"].Value) == false)
            //         {
            //             //row.Cells["Check"].c
            //         }
            // }
        }

        public void LoadTutionFee()
        {
            try
            {
                if (ClassBox.Text != "")
                {
                    string SelecteQuery = "SELECT TutionFee FROM Boon.feesstructure where Class = '" + ClassBox.Text + "' AND AcdYear= '" + AcdYearBox1.Text + "' ";

                    dt = c.SelectData(SelecteQuery);
                    if (dt.Rows.Count > 0)
                    {
                        TutionFee = dt.Rows[0][0].ToString();
                    }
                    else
                    {
                        MessageBox.Show("You have to add fees structure of class", "Warnnig");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }


        private void StdBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();
                string SelectQuery = "select Distinct(AcdYear) from Boon.stud_p_primary  WHERE Std = '" + StdBox.Text + "' ORDER BY AcdYear";
                DataTable dt = c.SelectData(SelectQuery);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AcdYearBox.Items.Add(row["AcdYear"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    DGVPrinter printer = new DGVPrinter();
                    printer.PrintDataGridView(dataGridView1);
                }
                else
                {
                    MessageBox.Show("List is empty", "Alert");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }
    }
}
