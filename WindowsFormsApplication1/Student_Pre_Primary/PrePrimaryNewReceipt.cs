﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections;

namespace Boon
{
    public partial class PrePrimaryNewReceipt : Form
    {
        common c = new common();

        string studID;
        ErrorProvider error1 = new ErrorProvider();
        DataTable Dt = new DataTable();
        Double tot, tot1 = 0;
        string TypeBox = "Student Fees";
        String OpBal, Clbal;
        string Name_of_account = "Pre-Primary";
        public PrePrimaryNewReceipt()
        {
            //   ClassBox.Focus();
            InitializeComponent();
        }



        public void LoadTextBoxes()
        {
            try
            {
                String SelectQuery = "SELECT * from Boon.feesstructure WHERE Class = '" + ClassBox.Text + "' AND AcdYear='" + AcdYearBox.Text + "' ";
                Dt = c.SelectData(SelectQuery);
                if (dt.Rows.Count > 0)
                {
                    Double tuion = Double.Parse((Dt.Rows[0][10].ToString()));
                    Double exam = Double.Parse((Dt.Rows[0][11].ToString()));
                    TutionTxt.Text = tuion.ToString();
                    ExamTxt.Text = exam.ToString();
                    RegTxt.Text = Dt.Rows[0][3].ToString();
                    TcTxt.Text = Dt.Rows[0][4].ToString();
                    BonTxt.Text = Dt.Rows[0][5].ToString();
                    ComTxt.Text = Dt.Rows[0][6].ToString();
                    MarkTxt.Text = Dt.Rows[0][7].ToString();
                    OtherTxt.Text = Dt.Rows[0][8].ToString();
                    Total.Text = Dt.Rows[0][9].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void StudentNameLoad()
        {
            try
            {

                NameTxt.Items.Clear();
                string str = "select Name from Boon.stud_p_primary Where Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' Order By Name ";
                dt = c.SelectData(str);

                AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                foreach (DataRow row in dt.Rows)
                {
                    // data.Add(row["Name"].ToString());
                    c.Text = row["Name"].ToString();
                    NameTxt.Items.Add(c);


                }
                //NameTxt.AutoCompleteCustomSource = data;
            }
            catch
            {

            }

        }


        public void FeeMode()
        {
            try
            {
                if (TutionCheckBox.Checked)
                {
                    if (FeeModeBox.Text == "Full")
                    {
                        error1.Clear();
                        Double tufee = ((Double.Parse(TutionTxt.Text)));
                        //tot = Double.Parse(TotalTxt.Text);
                        tot = tufee;
                    }
                    else if (FeeModeBox.Text == "Half")
                    {
                        error1.Clear();
                        Double tufee1 = ((Double.Parse(TutionTxt.Text)) / 2);
                        // tot = Double.Parse(TotalTxt.Text);
                        tot = tufee1;
                        // ((Double.Parse(TutionTxt.Text)) ).ToString();((Double.Parse(TotalTxt.Text)) / 2).ToString();
                    }
                    else if (FeeModeBox.Text == "Quarter")
                    {
                        error1.Clear();
                        Double tufee12 = ((Double.Parse(TutionTxt.Text)) / 4);
                        //tot = Double.Parse(TotalTxt.Text);
                        tot = tufee12;
                    }
                    else if (FeeModeBox.Text == "")
                    {
                        error1.Clear();
                        //     Double tufee12 = ((Double.Parse(TutionTxt.Text)) / 4);
                        //tot = Double.Parse(TotalTxt.Text);
                        tot = 0;
                    }
                }
                else
                {
                    tot = Double.Parse(OldFeeTxt.Text);
                    AmountTxt.Text = tot.ToString();
                }
            }
            catch
            {

            }
        }
        public void ClearTxt()
        {
            try
            {
                IdBox.Text = null;
                FeeTxt.Text = null;
                ReamingTxt.Text = null;
                // TutionTxt.Text = null;
                //  ExamTxt.Text = null;
                //RegTxt.Text = null;
                //TcTxt.Text = null;
                //BonTxt.Text = null;
                //ComTxt.Text = null;
                //MarkTxt.Text = null;
                //OtherTxt.Text = null;
                //Total.Text = null;
                //AmountTxt.Text = null;
                NameTxt.Text = null;
                ReamingTxt.Text = null;
                FeeTxt.Text = null;
                PaidTxt.Text = null;
            }
            catch { }
        }
        public void ExamFee()
        {
            try
            {
                if (ExamCheckBox.Checked)
                {
                    if (ExamFeeBox.Text == "Full")
                    {
                        error1.Clear();
                        //TotalTxt.Text = ((Double.Parse(ExamTxt.Text))).ToString();
                        tot1 = Double.Parse(ExamTxt.Text);


                    }
                    else if (ExamFeeBox.Text == "Half")
                    {
                        error1.Clear();
                        String Total = ((Double.Parse(ExamTxt.Text)) / 2).ToString();
                        // ((Double.Parse(TutionTxt.Text)) ).ToString();((Double.Parse(TotalTxt.Text)) / 2).ToString();
                        tot1 = Double.Parse(ExamTxt.Text) / 2;
                    }
                    else if (ExamFeeBox.Text == "Quarter")
                    {
                        error1.Clear();
                        // TotalTxt.Text = ((Double.Parse(ExamTxt.Text)) / 4).ToString();
                        // ((Double.Parse(TutionTxt.Text)) ).ToString();((Double.Parse(TotalTxt.Text)) / 2).ToString();
                        tot1 = Double.Parse(ExamTxt.Text) / 4;
                    }
                    else if (ExamFeeBox.Text == "")
                    {
                        error1.Clear();
                        //TotalTxt.Text = ((Double.Parse(ExamTxt.Text)) / 4).ToString();
                        // ((Double.Parse(TutionTxt.Text)) ).ToString();((Double.Parse(TotalTxt.Text)) / 2).ToString();
                        tot1 = 0;
                    }
                }
            }
            catch { }
        }
        private void FeeModeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TutionCheckBox.Checked)
            {
                FeeMode();
                // Count();
            }

        }


        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                NameTxt.Text = null;
                FeeTxt.Text = null;
                ReamingTxt.Text = null;
                LoadAcdYear();
                LoadID();
                ClearTxt();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void LoadAC()
        {
            try
            {
                //   Name_of_account = ClassBox.Text=="" ? "Primary" : "Pre-Primary";
                String SelectQuery = "SELECT Max(ID) from Boon.accounts where Account_name = '" + Name_of_account + "'";
                DataTable table = new DataTable();

                table = c.SelectData(SelectQuery);
                if (table.Rows.Count > 0)
                {

                    string tid = table.Rows[0][0].ToString();
                    String selectedata = "Select * from Boon.accounts where ID = '" + tid + "'";
                    DataTable dt2 = new DataTable();
                    dt2 = c.SelectData(selectedata);
                    if (dt2.Rows.Count > 0)
                    {
                        OpBal = dt2.Rows[0][2].ToString();
                        Clbal = dt2.Rows[0][3].ToString();
                    }
                    else
                    {
                        OpBal = "0";
                        Clbal = "0";
                    }
                }
                else
                {
                    OpBal = "0";
                    Clbal = "0";
                    MessageBox.Show("No data");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void SaveTransaction()
        {
            try
            {
                LoadAC();
                string insertQuery = "";
                float amt = float.Parse(AmountTxt.Text);
                float closBal = float.Parse(Clbal);
                float total = closBal + amt;
                String Description = "(Receipt No.: " + IdTxt.Text + ") " + NameTxt.Text;
                string Mode = "Deposited";
                insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + Name_of_account + "','" + Clbal + "' ,'" + total + "','" + DateTime.Value + "','" + total + "')";
                if (c.InsertData(insertQuery))
                {
                    LoadAC();
                    String SaveQuery = "INSERT INTO Boon.transaction_pre(Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('Student Fees','" + Name_of_account + "','" + AmountTxt.Text + "','" + Description + "','" + DateTime.Value + "','" + Mode + "','" + OpBal + "','" + Clbal + "')";

                    if (c.InsertData(SaveQuery))
                    {
                        if (OldTutioncheck.Checked == true)
                        {
                            String UpdateQuery1 = "UPDATE Boon.stud_p_primary SET RemaingFee =  RemaingFee -'" + OldFeeTxt.Text + "'  WHERE ID ='" + studID + "' ";
                            c.InsertData(UpdateQuery1);

                        }
                        else if (TutionCheckBox.Checked == true)
                        {
                            String UpdateQuery1 = "UPDATE Boon.stud_p_primary SET RemaingFee =  RemaingFee -'" + tot + "'  WHERE ID ='" + studID + "' ";
                            if (c.InsertData(UpdateQuery1)) { } else { MessageBox.Show("less fees remaining"); return; }
                        }
                        if (ExamCheckBox.Checked == true)
                        {
                            String UpdateQuery1 = "UPDATE Boon.stud_p_primary SET RemaingFee =  RemaingFee -'" + tot1 + "'  WHERE ID ='" + studID + "' ";
                            if (c.InsertData(UpdateQuery1)) { } else { MessageBox.Show("less fees remaining"); return; }
                        }
                        if (PreRemCheckBox.Checked == true)
                        {
                            String UpdateQuery1 = "UPDATE Boon.stud_p_primary SET OldRemFees = '0'  WHERE ID ='" + studID + "' ";
                            if (c.InsertData(UpdateQuery1)) { } else { MessageBox.Show("less fees remaining"); return; }
                        }

                    }

                }
                else
                {
                    MessageBox.Show("failed");
                    return;
                }

            }
            catch { }
        }
        DataTable dt;
        private void LoadAcdYear()
        {
            try
            {
                AcdYearBox.Items.Clear();

                String str = "select Distinct(AcdYear) from Boon.stud_p_primary WHERE Std='" + ClassBox.Text + "' ORDER BY AcdYear ";
                dt = c.SelectData(str);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        c.Text = row["AcdYear"].ToString();
                        AcdYearBox.Items.Add(c);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }

        }



        private void TypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // loadValue();
        }


        public void Count()
        {
            if (TutionCheckBox.Checked == true)
            {

                Double a = tot;
                Double b = Double.Parse(ReamingTxt.Text);

                if (b < a)
                {
                    MessageBox.Show("Tution fee is less remaining", "Alert ");
                    AmountTxt.Text = null;
                    return;
                }
                else
                {

                }
            }
        }

        private void Print_Click(object sender, EventArgs e)
        {
            int Count1 = 0;
            if (AmountTxt.Text == "" || AmountTxt.Text == "0" || AmountTxt.Text == null)
            {
                MessageBox.Show("You have to select at least one fee", "Warnning");
                return;

            }
            else if (TutionCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (ExamCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (RegCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (TcCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (BonCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (ComCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (MarkCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (OtherCheckBox.Checked)
            {
                Count1 += 1;
            }
            else if (OldTutioncheck.Checked)
            {
                Count1 += 1;
            }
            else if (PreRemCheckBox.Checked)
            {
                Count1 += 1;
            }
            if (Count1 == 0)
            {
                MessageBox.Show("You have to select at least one fee", "Warnning");
                return;
            }
            else if (PreRemCheckBox.Checked == false)
            {
                if (OldTutioncheck.Checked == true && studID != "")
                {
                    SaveTransaction();
                    String InserQuery = "Insert into Boon.receipts_pri_primary (ReceiptNo,Type,StudName,Class,ID,Date,TotalFee,AcdYear) Values ('" + IdTxt.Text + "','" + TypeBox + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + IdBox.Text + "','" + DateTime.Value + "','" + AmountTxt.Text + "','" + AcdYearBox.Text + "')";

                    if (c.InsertData(InserQuery))
                    {

                        String UpdateQuery = "Update Boon.receipts_pri_primary Set EducationFee = '" + AmountTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                        if (c.InsertData(UpdateQuery))
                        {
                            PrePrimaryReceipt obj = new PrePrimaryReceipt();
                            obj.ID = IdTxt.Text;
                            obj.ShowDialog();
                        }
                    }


                }
                else
                {
                    SaveTransaction();
                    Count();
                    TotalFeeCount();
                    String InserQuery = "Insert into Boon.receipts_pri_primary (ReceiptNo,Type,StudName,Class,ID,Date,TotalFee,AcdYear) Values ('" + IdTxt.Text + "','" + TypeBox + "','" + NameTxt.Text + "','" + ClassBox.Text + "','" + IdBox.Text + "','" + DateTime.Value + "','" + AmountTxt.Text + "','" + AcdYearBox.Text + "')";

                    if (c.InsertData(InserQuery))
                    {
                        if (TutionCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set EducationFee = '" + tot + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (ExamCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set ExamFee = '" + tot1 + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (RegCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set RegFee = '" + RegTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (TcCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set TCFee = '" + TcTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (BonCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set BonFee = '" + BonTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (ComCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set ComFee = '" + ComTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (MarkCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set MarkFee = '" + MarkTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }
                        if (OtherCheckBox.Checked)
                        {
                            String UpdateQuery = "Update Boon.receipts_pri_primary Set OtherFee = '" + OtherTxt.Text + "'  where ReceiptNo = '" + IdTxt.Text + "'";
                            c.InsertData(UpdateQuery);
                        }

                        PrePrimaryReceipt obj = new PrePrimaryReceipt();
                        obj.ID = IdTxt.Text;
                        obj.ShowDialog();

                    }
                    else
                    {
                        MessageBox.Show("Unable to print data not stored", "Print Error");
                    }
                }
            }
            else if (PreRemCheckBox.Checked == true && OldTutioncheck.Checked == false && TutionCheckBox.Checked == false && ExamCheckBox.Checked == false)
            {
                SaveTransaction();
                double FTerm = 0;
                double STerm = 0;
                var Acd = AcdYearBox.Text;
                int Fyear = Convert.ToInt32((Acd).Substring(2, 2).ToString());
                string AcdYear = ("20" + (Fyear - 1) + "-" + Fyear);
                String SelectQuery = "Select Class,Sum(EducationFee),SUM(ExamFee) from Boon.receipts_pri_primary where ID='" + IdBox.Text + "' AND AcdYear ='" + AcdYear + "'";
                DataTable tbl = c.SelectData(SelectQuery);
                if (tbl.Rows.Count > 0)
                {
                    string std = tbl.Rows[0][0].ToString();
                    DataTable dt = c.SelectData("Select FirstTerm,SecondTerm from Boon.feesstructure where Class ='" + std + "' AND AcdYear = '" + AcdYear + "'");
                    if (dt.Rows.Count > 0)
                    {
                        string a = dt.Rows[0][0].ToString();
                        string b = dt.Rows[0][1].ToString();
                        if (a != "" && a != " " && a != null)
                        {
                            FTerm = Convert.ToDouble(a);
                        }
                        if (b != "" && b != " " && b != null)
                        {
                            STerm = Convert.ToDouble(b);
                        }
                        string f1 = tbl.Rows[0][1].ToString();
                        string f2 = tbl.Rows[0][2].ToString();
                        if (f1 != "" && f1 != " " && f1 != null)
                        {
                            double FirstTerm = Convert.ToDouble(f1);
                            if (FTerm > 0)
                            {
                                FTerm = FTerm - FirstTerm;
                            }
                        }
                        if (f2 != "" && f2 != " " && f2 != null)
                        {
                            double SecondTerm = Convert.ToDouble(f2);
                            if (STerm > 0)
                            {
                                STerm = STerm - SecondTerm;
                            }
                        }
                        if (STerm == 0 && FTerm != 0)
                        {
                            String InserQuery = "Insert into Boon.receipts_pri_primary (ReceiptNo,Type,StudName,Class,ID,Date,TotalFee,AcdYear,EducationFee) Values ('" + IdTxt.Text + "','" + TypeBox + "','" + NameTxt.Text + "','" + tbl.Rows[0][0].ToString() + "','" + IdBox.Text + "','" + DateTime.Value + "','" + AmountTxt.Text + "','" + AcdYear + "','" + PreRemTxt.Text + "')";
                            if (c.InsertData(InserQuery))
                            {
                                PrePrimaryReceipt obj = new PrePrimaryReceipt();
                                obj.ID = IdTxt.Text;
                                obj.ShowDialog();
                            }
                        }
                        else
                        {
                            String InserQuery = "Insert into Boon.receipts_pri_primary (ReceiptNo,Type,StudName,Class,ID,Date,TotalFee,AcdYear,ExamFee) Values ('" + IdTxt.Text + "','" + TypeBox + "','" + NameTxt.Text + "','" + tbl.Rows[0][0].ToString() + "','" + IdBox.Text + "','" + DateTime.Value + "','" + AmountTxt.Text + "','" + AcdYear + "','" + PreRemTxt.Text + "')";
                            if (c.InsertData(InserQuery))
                            {
                                PrePrimaryReceipt obj = new PrePrimaryReceipt();
                                obj.ID = IdTxt.Text;
                                obj.ShowDialog();
                            }
                        }
                    }
                    else
                    {
                        string stdclass = "";
                        if (ClassBox.Text != "")
                        {
                            ArrayList classStd = new ArrayList();
                            classStd.Add("Nursery");
                            classStd.Add("Jr.KG");
                            classStd.Add("Sr.KG");
                            stdclass = (classStd[(classStd.IndexOf(ClassBox.Text)) - 1]).ToString();
                        }
                        DataTable dt1 = c.SelectData("Select FirstTerm,SecondTerm from Boon.feesstructure where Class ='" + stdclass + "' AND AcdYear = '" + AcdYear + "'");
                        if (dt1.Rows.Count > 0)
                        {
                            string a = dt1.Rows[0][0].ToString();
                            string b = dt1.Rows[0][1].ToString();
                            if (a != "" && a != " " && a != null)
                            {
                                FTerm = Convert.ToDouble(a);
                            }
                            if (b != "" && b != " " && b != null)
                            {
                                STerm = Convert.ToDouble(b);
                            }
                            String InserQuery = "Insert into Boon.receipts_pri_primary (ReceiptNo,Type,StudName,Class,ID,Date,TotalFee,AcdYear,EducationFee,ExamFee) Values ('" + IdTxt.Text + "','" + TypeBox + "','" + NameTxt.Text + "','" + stdclass + "','" + IdBox.Text + "','" + DateTime.Value + "','" + AmountTxt.Text + "','" + AcdYear + "','" + FTerm + "','" + STerm + "')";
                            if (c.InsertData(InserQuery))
                            {
                                PrePrimaryReceipt obj = new PrePrimaryReceipt();
                                obj.ID = IdTxt.Text;
                                obj.ShowDialog();
                            }
                        }
                    }
                }
            }
        }


        private void IdBox_Leave(object sender, EventArgs e)
        {

        }

        private void AmountTxt_TextChanged(object sender, EventArgs e)
        {

            error1.Clear();

            try
            {
                Double c = Convert.ToDouble(AmountTxt.Text);
                Count();
            }
            catch (Exception ex)
            {
                AmountTxt.Clear();
                error1.SetError(AmountTxt, "Enter Numbers Only");
                MessageBox.Show(ex.Message);

            }
        }

        public void TotalFeeCount()
        {
            try
            {
                Double amount = 0;

                if (TutionCheckBox.Checked)
                {
                    OldTutioncheck.Checked = false;
                    if (FeeModeBox.Text == "")
                    {
                        MessageBox.Show("Select Fee mode ");
                        FeeModeBox.Focus();
                        FeeModeBox.SelectedIndex = 0;
                        // return;
                    }
                    // var v1 = Double.Parse(AmountTxt.Text).ToString();

                    //  Double ob = Double.Parse(v1);
                    Double ob = tot;
                    amount = amount + ob;
                    // TypeBox = "Tution fee";

                }
                if (ExamCheckBox.Checked)
                {
                    OldTutioncheck.Checked = false;
                    if (ExamFeeBox.Text == "")
                    {

                        MessageBox.Show("Select Fee mode ", "Alert");
                        ExamFeeBox.Focus();
                        ExamFeeBox.SelectedIndex = 0;
                        //   return;
                    }
                    //   var v2 = Double.Parse(ExamTxt.Text).ToString();
                    //  Double o1 =  Double.Parse(v2);
                    Double o1 = tot1;
                    amount = amount + o1;
                    //TypeBox =  "Exam fee";
                }
                if (RegCheckBox.Checked)
                {
                    var v3 = Double.Parse(RegTxt.Text).ToString();
                    Double o2 = Double.Parse(v3);
                    amount = amount + o2;
                }
                if (TcCheckBox.Checked)
                {
                    var v4 = Double.Parse(TcTxt.Text).ToString();
                    Double o3 = Double.Parse(v4);
                    amount = amount + o3;
                }
                if (BonCheckBox.Checked)
                {
                    var v4 = Double.Parse(BonTxt.Text).ToString();
                    Double o3 = Double.Parse(v4);
                    amount = amount + o3;
                }
                if (ComCheckBox.Checked)
                {
                    var v5 = Double.Parse(ComTxt.Text).ToString();
                    Double o4 = Double.Parse(v5);
                    amount = amount + o4;
                }
                if (MarkCheckBox.Checked)
                {
                    var v6 = Double.Parse(MarkTxt.Text).ToString();
                    Double o4 = Double.Parse(v6);
                    amount = amount + o4;
                }
                if (OtherCheckBox.Checked)
                {
                    var v7 = Double.Parse(OtherTxt.Text).ToString();
                    Double o5 = Double.Parse(v7);
                    amount = amount + o5;
                }
                if (OldTutioncheck.Checked)
                {
                    var v7 = Double.Parse(OldFeeTxt.Text).ToString();
                    Double o5 = Double.Parse(v7);
                    amount = amount + o5;
                }

                AmountTxt.Text = amount.ToString();
            }
            catch { }

        }

        private void AmountTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Count();
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                error1.SetError(AmountTxt, "Enter Numbers Only");
                e.Handled = true;
            }
        }

        public void LoadID()
        {
            try
            {

                String SelectQuery = "select Max(ReceiptNo)+1 from Boon.receipts_pri_primary";
                DataTable table = new DataTable();
                table = c.SelectData(SelectQuery);
                var id = table.Rows[0][0].ToString();

                if (table.Rows.Count > 0)
                {
                    IdTxt.Text = table.Rows[0][0].ToString();

                }
                else
                {
                    IdTxt.Text = "1";
                }
            }

            catch (Exception ex)
            {


            }
        }


        private void StudentFees_Load(object sender, EventArgs e)
        {
            LoadID();

            //   TotalTxt.Location = new Point(100, 100);

        }

        private void TutionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OldTutioncheck.Checked == true)
            {
                OldTutioncheck.Checked = false;
            }

            TotalFeeCount();

        }

        private void TotalBtn_Click(object sender, EventArgs e)
        {
            TotalFeeCount();
        }



        private void ExamFeeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExamFee();
        }

        private void ExamCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (OldTutioncheck.Checked == true)
                {
                    OldTutioncheck.Checked = false;
                }

                TotalFeeCount();

            }
            catch { }

        }

        public void IdSend()
        {
            var ReceiptID = Double.Parse(IdTxt.Text).ToString();
        }

        private void FeeTxt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (FeeTxt.Text != "0" || FeeTxt.Text != "")
                {
                    Double tution = Double.Parse(TutionTxt.Text);
                    Double exam = Double.Parse(ExamTxt.Text);
                    Double fee = Double.Parse(FeeTxt.Text);

                    if (fee != (exam + exam))
                    {
                        OldTutioncheck.Visible = true;
                        TutionCheckBox.Visible = false;
                        ExamCheckBox.Visible = false;
                    }
                    else
                    {
                        OldTutioncheck.Visible = false;
                        TutionCheckBox.Visible = true;
                        ExamCheckBox.Visible = true;
                    }
                }
                else
                {
                    ExamCheckBox.Visible = true;
                    OldTutioncheck.Visible = true;
                    TutionCheckBox.Visible = true;
                }
            }
            catch
            {
            }
        }

        private void IdBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        private void NameTxt_TextChanged(object sender, EventArgs e)
        {


        }

        private void AcdYearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ClassBox.Text == "")
                {
                    MessageBox.Show("You have select Class", "Warnning");
                    ClassBox.Focus();
                }
                //StudentID();
                LoadTextBoxes();
                StudentNameLoad();
                LoadID();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void NameTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32 && ch != 46)
            {
                MessageBox.Show("Enter Letters Only", "Alert");
                e.Handled = true;
            }
            if (ch == 9) // for tab key 
            {
                //MotherTxt.Focus();
            }
        }

        private void ReamingTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (OldTutioncheck.Checked == true)
                {
                    ExamCheckBox.Checked = false;
                    TutionCheckBox.Checked = false;
                    oldTutuionPanal.Visible = true;
                    ExamCheckBox.Visible = false;
                    TutionCheckBox.Visible = false;

                }
                else
                {
                    ExamCheckBox.Checked = true;
                    TutionCheckBox.Checked = true;
                    oldTutuionPanal.Visible = false;
                    OldTutioncheck.Checked = false;
                }
            }
            catch
            { }
        }

        private void NameTxt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadID();
                IdBox.Text = null;
                FeeTxt.Text = null;
                ReamingTxt.Text = null;
                PreRemTxt.Text = null;
                String SelectQuery = "select ID,TutionFee,RemaingFee,OldRemFees from Boon.stud_p_primary where Name = '" + NameTxt.Text + "' AND Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'  ";

                DataTable table = c.SelectData(SelectQuery);
                if (table.Rows.Count > 0)
                {
                    IdBox.Text = table.Rows[0][0].ToString();
                    studID = table.Rows[0][0].ToString();
                    FeeTxt.Text = table.Rows[0][1].ToString();
                    ReamingTxt.Text = table.Rows[0][2].ToString();
                    PreRemTxt.Text = Dt.Rows[0][3].ToString();
                    double total = double.Parse(FeeTxt.Text);
                    double rem = double.Parse(ReamingTxt.Text);
                    double paid = total - rem;
                    PaidTxt.Text = paid.ToString();
                    table.Rows.Clear();
                    table = c.SelectData("Select Sum(EducationFee),SUM(ExamFee) from Boon.receipts_pri_primary where ID='" + IdBox.Text + "' AND AcdYear ='" + AcdYearBox.Text + "'");
                    double Edu = double.Parse(table.Rows[0][0].ToString());
                    double Exam = double.Parse(table.Rows[0][1].ToString());
                    double TxtEdu = double.Parse(TutionTxt.Text);
                    double TxtExam = double.Parse(ExamTxt.Text);
                    if (Edu == TxtEdu)
                    {
                        TutionCheckBox.Checked = false;
                        TutionCheckBox.Visible = false;
                    }
                    if (Exam == TxtExam)
                    {
                        ExamCheckBox.Checked = false;
                        ExamCheckBox.Visible = false;
                    }
                }
            }
            catch
            {

            }
        }


        private void NameTxt_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                IdBox.Text = null;
                FeeTxt.Text = null;
                ReamingTxt.Text = null;
                String SelectQuery = "select ID from Boon.stud_p_primary where Name = '" + NameTxt.Text + "' AND Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'  ";

                DataTable table = c.SelectData(SelectQuery);
                if (table.Rows.Count > 0)
                {
                    studID = table.Rows[0][0].ToString();
                }

            }
            catch { }
        }

        private void OldFeeTxt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double ab = double.Parse(OldFeeTxt.Text);
                double ab1 = double.Parse(ReamingTxt.Text);
                //  AmountTxt.Text = (ab1 - ab).ToString();
                AmountTxt.Text = OldFeeTxt.Text;
                if (ab > ab1)
                {
                    AmountTxt.Text = null;
                    MessageBox.Show("Less fess is remaining", "Warnning");
                    OldFeeTxt.Text = null;
                }
            }
            catch { }
        }

        private void IdBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void TutionCheckBox_VisibleChanged(object sender, EventArgs e)
        {

            //   OldTutioncheck.Visible = true;
        }

        private void NewReceiptBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTxt();
                LoadID();
            }
            catch { }
        }

        private void PreRemTxt_TextChanged(object sender, EventArgs e)
        {
            if (PreRemTxt.Text == "0")
            {
                PreRemCheckBox.Checked = false;
                PreRemCheckBox.Visible = false;
            }
            else
            {
                PreRemCheckBox.Visible = true;
            }
        }

        private void IdBox_TextChanged_1(object sender, EventArgs e)
        {
            studID = (IdBox.Text).ToString();
            try
            {

                FeeTxt.Text = null;
                ReamingTxt.Text = null;

                String SelectQuery = "select Name,TutionFee,RemaingFee,OldRemFees from Boon.stud_p_primary where ID = '" + IdBox.Text + "' AND Std = '" + ClassBox.Text + "' ";
                Dt = c.SelectData(SelectQuery);
                if (Dt.Rows.Count > 0)
                {

                    NameTxt.Text = Dt.Rows[0][0].ToString();

                    var Amount = Dt.Rows[0][1].ToString();

                    if (Amount.ToString() == null || Amount.ToString() == "NULL" || Amount.ToString() == "")
                    {
                        FeeTxt.Text = "0";
                    }
                    else
                    {
                        FeeTxt.Text = Dt.Rows[0][1].ToString();
                        ReamingTxt.Text = Dt.Rows[0][2].ToString();
                        PreRemTxt.Text = Dt.Rows[0][3].ToString();
                    }
                }
            }
            catch
            {
                // MessageBox.Show(ex.Message);
            }
        }

        private void PreRemCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (PreRemCheckBox.Checked)
            {
                AmountTxt.Text = PreRemTxt.Text;
                ExamCheckBox.Checked = false;
                TutionCheckBox.Checked = false;
                ExamCheckBox.Visible = false;
                TutionCheckBox.Visible = false;
            }
            else
            {
                AmountTxt.Text = "0";
                //ExamCheckBox.Checked = false;
                //TutionCheckBox.Checked = false;
                ExamCheckBox.Visible = true;
                TutionCheckBox.Visible = true;
            }
        }
    }

}
