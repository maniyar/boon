﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;


namespace Boon
{
    public partial class UpdateStudPPrimary : Form
    {
        common c = new common();
        MySqlConnection connection;
        public UpdateStudPPrimary()
        {
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
        String Gender;
        String SelectQuery;

        public MySqlCommand command { get; set; }
        private void MaleRbtn_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void FemaleRbtn_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        public void ClearAll()
        {
            this.dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();

            PreClassBox.Text = null;
            FirstClassBox.Text = null;
            MotherTongueTxt.Text = null;
            ReligionTxt.Text = null;
            //TqTxt.Text = null;
            //DistricTxt.Text = null;
            NationalityBox.Text = null;
            NameTxt.Text = null;
            AddressTxt.Text = null;
            AadharTxt.Text = null;
            ContactTxt.Text = null;
            PriviousTxt.Text = null;
            PlaceTxt.Text = null;
            DateTime1.Text = null;
            pictureBox1.Image = null;
            pictureBox1.Invalidate();
            MotherTxt.Text = null;
            StudentIDTxt.Text = null;
            CastBox.Text = null;
            SubCastTxt.Text = null;
            DateTime2.Text = null;
            CategoryBox.Text = null;
            FatherTxt.Text = null;
            this.FemaleRbtn.Checked = false;
            this.MaleRbtn.Checked = false;

        }

        private void GetListBtn_Click(object sender, EventArgs e)
        {
            try
            {

                if (ClassBox.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    ClassBox.Focus();
                    MessageBox.Show("Please select standard of student first ", "Alert");

                }
                if (AcdYearBox.Text == "")
                {
                    string myStringVariable2 = string.Empty;
                    ClassBox.Focus();
                    MessageBox.Show("Please select Acd. year of student first ", "Alert");

                }
                else
                {
                    ClearAll();
                    this.dataGridView2.DataSource = null;
                    dataGridView2.Rows.Clear();
                    DataTable table1 = new DataTable();
                    SelectQuery = "SELECT ID, Name from Boon.stud_p_primary WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";

                    table1 = c.SelectData(SelectQuery);


                    dataGridView2.DataSource = table1;
                    dataGridView2.DefaultCellStyle.Font = new Font("Tahoma", 10);
                    dataGridView2.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
                    dataGridView2.Columns[0].Width = 50;
                    dataGridView2.Columns[1].Width = 200;
                }
            }
            catch { }

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void OpenImg_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox1.Invalidate();
            OpenFileDialog opf1 = new OpenFileDialog();
            opf1.Filter = "Choose Image(*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif";
            if (opf1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(opf1.FileName);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            //da.Dispose();
        }

        private void UpdateStudent_Load(object sender, EventArgs e)
        {

        }


        private void UpdateBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to update student?", "Update Student", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (pictureBox1.Image == null)
                    {
                        Bitmap bmp = Properties.Resources.user_male_512;
                        Image newImg = bmp;
                        pictureBox1.Image = newImg;
                        this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    }

                    MemoryStream ms = new MemoryStream();
                    pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                    byte[] Img = ms.ToArray();

                    String UpdateQuery = "UPDATE  Boon.stud_p_primary SET Name = '" + NameTxt.Text + "',Address = '" + AddressTxt.Text + "',Contact = '" + ContactTxt.Text + "',Aadhar = '" + AadharTxt.Text + "',Previous_school = '" + PriviousTxt.Text + "',Gender = '" + Gender + "',DOB = '" + DateTime1.Value + "',Std = '" + ClassBox.Text + "',B_Place = '" + PlaceTxt.Text + "',Profile = '" + Img + "' ,Mother_Name = '" + MotherTxt.Text + "',Father_Name = '" + FatherTxt.Text + "',StudentId = '" + StudentIDTxt.Text + "',Cast = '" + CastBox.Text + "',SubCast = '" + SubCastTxt.Text + "',Category = '" + CategoryBox.Text + "',D_O_A = '" + DateTime2.Value + "',Nationality = '" + NationalityBox.Text + "',FirstStd = '" + FirstClassBox.Text + "',Pre_Std = '" + PreClassBox.Text + "',Religion = '" + ReligionTxt.Text + "',MotherTongue = '" + MotherTongueTxt.Text + "',State='" + StateBox.Text + "' ,Tq='" + TqTxt.Text + "',Dist='" + DistricTxt.Text + "',Country = '" + CountryTxt.Text + "'WHERE ID = '" + IdTxt.Text + "' AND Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "' ";
                    if (c.InsertData(UpdateQuery))
                    {
                        connection.Open();

                        String UpdateQuery1 = "UPDATE Boon.stud_p_primary SET Profile=@Profile WHERE ID ='" + IdTxt.Text + "'  AND AcdYear = '" + AcdYearBox.Text + "' ";

                        command = new MySqlCommand(UpdateQuery1, connection);
                        command.Parameters.Add("@Profile", MySqlDbType.Blob);
                        command.Parameters["@Profile"].Value = Img;

                        if (command.ExecuteNonQuery() == 1)
                        {

                            MessageBox.Show("Data updated succesfully...", "Alert");

                        }
                        else
                        {
                            MessageBox.Show("Data is not updated plz check connection");
                        }

                        connection.Close();
                    }
                    else
                    {
                        MessageBox.Show("Check connections", "Error");
                        return;
                    }


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }

        }

        private void ClearBtn1_Click(object sender, EventArgs e)
        {

            // this.dataGridView2.DataSource = null;
            // dataGridView2.Rows.Clear(); 
            IdTxt.Text = null;
            NameTxt.Text = null;
            AddressTxt.Text = null;
            ContactTxt.Text = null;
            AadharTxt.Text = null;
            PriviousTxt.Text = null;
            PlaceTxt.Text = null;
            DateTime1.Text = null;
            DateTime2.Text = null;
            //GenderTxt.Text = null;
            pictureBox1.Image = null;
            pictureBox1.Invalidate();
            NationalityBox.Text = null;
            MotherTxt.Text = null;
            FatherTxt.Text = null;
            //  ClassBox.Text = null;
            this.FemaleRbtn.Checked = false;
            this.MaleRbtn.Checked = false;
            CastBox.Text = null;
            SubCastTxt.Text = null;
            CategoryBox.Text = null;
            MotherTongueTxt.Text = null;
            ReligionTxt.Text = null;
            FirstClassBox.Text = null;
            PreClassBox.Text = null;
            StudentIDTxt.Text = null;
            StateBox.Text = null;
            TqTxt.Text = null;
            DistricTxt.Text = null;
            CountryTxt.Text = null;
        }

        private void SearchTxt1_TextChanged(object sender, EventArgs e)
        {
            if (ClassBox.Text == "")
            {

                ClassBox.Focus();
                MessageBox.Show("Please select standard of student first ");

            }
            else
            {
                (dataGridView2.DataSource as DataTable).DefaultView.RowFilter = string.Format("Name LIKE '%{0}%'", SearchTxt1.Text);
            }
        }


        private void GenderTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaleRbtn_CheckedChanged_1(object sender, EventArgs e)
        {
            Gender = "Male";
        }

        private void FemaleRbtn_CheckedChanged_1(object sender, EventArgs e)
        {
            Gender = "Female";
        }



        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();

                String Str = "select Distinct(AcdYear) from Boon.stud_p_primary where Std = '" + ClassBox.Text + "' ORDER BY AcdYear ";
                DataTable dt = c.SelectData(Str);

                foreach (DataRow row in dt.Rows)
                {
                    AcdYearBox.Items.Add(row["AcdYear"].ToString());
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }

        }

        private void dataGridView2_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.MaleRbtn.Checked = false;
                this.FemaleRbtn.Checked = false;

                int index = e.RowIndex;
                DataGridViewRow selectedRow = dataGridView2.Rows[index];
                IdTxt.Text = selectedRow.Cells[0].Value.ToString();
                NameTxt.Text = selectedRow.Cells[1].Value.ToString();

                DataTable table = new DataTable();
                SelectQuery = "SELECT * from Boon.stud_p_primary WHERE ID = '" + IdTxt.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'";
                table = c.SelectData(SelectQuery);
                AddressTxt.Text = table.Rows[0][2].ToString();
                ContactTxt.Text = table.Rows[0][3].ToString();
                AadharTxt.Text = table.Rows[0][4].ToString();
                PriviousTxt.Text = table.Rows[0][5].ToString();
                //GenderTxt.Text = selectedRow.Cells[6].Value.ToString();
                //dateTimePicker1.Text = selectedRow.Cells[7].Value.ToString();
                //Place.Text = selectedRow.Cells[9].Value.ToString();
                var GetGender = table.Rows[0][6].ToString();

                DateTime1.Text = table.Rows[0][7].ToString();

                // var Bplace = table.Rows[0][9].ToString();
                PlaceTxt.Text = table.Rows[0][9].ToString();


                MotherTxt.Text = table.Rows[0][11].ToString();
                FatherTxt.Text = table.Rows[0][12].ToString();
                StudentIDTxt.Text = table.Rows[0][13].ToString();
                CastBox.Text = table.Rows[0][14].ToString();
                SubCastTxt.Text = table.Rows[0][15].ToString();
                CategoryBox.Text = table.Rows[0][16].ToString();
                DateTime2.Text = table.Rows[0][17].ToString();
                NationalityBox.Text = table.Rows[0][18].ToString();
                ReligionTxt.Text = table.Rows[0][24].ToString();
                MotherTongueTxt.Text = table.Rows[0][25].ToString();
                PreClassBox.Text = table.Rows[0][26].ToString();
                FirstClassBox.Text = table.Rows[0][27].ToString();
                StateBox.Text = table.Rows[0][34].ToString();
                TqTxt.Text = table.Rows[0][35].ToString();
                DistricTxt.Text = table.Rows[0][36].ToString();
                CountryTxt.Text = table.Rows[0][37].ToString();



                byte[] img1 = (byte[])table.Rows[0][10];

                MemoryStream ms1 = new MemoryStream(img1);

                pictureBox1.Image = Image.FromStream(ms1);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                if (GetGender.ToString() != "")
                {
                    if (GetGender.ToString() == "Male")
                    {
                        this.FemaleRbtn.Checked = false;
                        this.MaleRbtn.Checked = true;
                    }
                    else
                    {
                        this.MaleRbtn.Checked = false;
                        this.FemaleRbtn.Checked = true;
                    }
                }
                else { this.FemaleRbtn.Checked = true; }
            }
            catch { }
        }

        private void ContactTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }
        }

        private void AadharTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                MessageBox.Show("Enter Numbers Only", "Alert");
                e.Handled = true;
            }
        }
    }
}

