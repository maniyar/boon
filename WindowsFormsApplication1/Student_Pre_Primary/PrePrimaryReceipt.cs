﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class PrePrimaryReceipt : Form
    {
        MySqlConnection mysql;
        common c = new common();
        public PrePrimaryReceipt()
        {
            mysql = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
        
        public string ID = "1";
        MySqlCommand Cmd = new MySqlCommand();
        NewReceipt obj = new NewReceipt();
       
        public void LoadReport()
        {
            try
            {
                MySqlCommand Cmd = new MySqlCommand();


               // String ShowQuery = "Select * from Boon.receipts_pri_primary order by ReceiptNo Desc LIMIT 1 ";
                String ShowQuery = "Select * from Boon.receipts_pri_primary where ReceiptNo ='"+ID+"' ";
                mysql.Open();

                Cmd = new MySqlCommand(ShowQuery, mysql);

                DataSet ds = new DataSet();
                MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                mda.Fill(ds, "Receipts");
                PrePrimaryReceiptReport cr = new PrePrimaryReceiptReport();
                cr.SetDataSource(ds);
                crystalReportViewer1.ReportSource = cr;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mysql.Close();
            }
        }

        private void Receipt_Load(object sender, EventArgs e)
        {
            LoadReport();
        }

    }
}
