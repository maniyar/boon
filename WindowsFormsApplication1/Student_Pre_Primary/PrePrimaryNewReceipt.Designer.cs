﻿namespace Boon
{
    partial class PrePrimaryNewReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrePrimaryNewReceipt));
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FeeModeBox = new System.Windows.Forms.ComboBox();
            this.TotalTxt = new System.Windows.Forms.TextBox();
            this.OtherTxt = new System.Windows.Forms.TextBox();
            this.MarkTxt = new System.Windows.Forms.TextBox();
            this.ComTxt = new System.Windows.Forms.TextBox();
            this.BonTxt = new System.Windows.Forms.TextBox();
            this.TcTxt = new System.Windows.Forms.TextBox();
            this.RegTxt = new System.Windows.Forms.TextBox();
            this.ExamTxt = new System.Windows.Forms.TextBox();
            this.TutionTxt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.AmountTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ExamFeeBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.TextBox();
            this.OtherCheckBox = new System.Windows.Forms.CheckBox();
            this.TcCheckBox = new System.Windows.Forms.CheckBox();
            this.MarkCheckBox = new System.Windows.Forms.CheckBox();
            this.RegCheckBox = new System.Windows.Forms.CheckBox();
            this.ExamCheckBox = new System.Windows.Forms.CheckBox();
            this.ComCheckBox = new System.Windows.Forms.CheckBox();
            this.TutionCheckBox = new System.Windows.Forms.CheckBox();
            this.BonCheckBox = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.FeeTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DateTime = new System.Windows.Forms.DateTimePicker();
            this.Print = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PreRemCheckBox = new System.Windows.Forms.CheckBox();
            this.PreRemTxt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.NameTxt = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.PaidTxt = new System.Windows.Forms.TextBox();
            this.ReamingTxt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TotalBtn = new System.Windows.Forms.Button();
            this.IdTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.OldTutioncheck = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.oldTutuionPanal = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.OldFeeTxt = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.IdBox = new System.Windows.Forms.TextBox();
            this.NewReceiptBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.oldTutuionPanal.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.Color.White;
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "Nursery",
            "Jr.KG",
            "Sr.KG"});
            this.ClassBox.Location = new System.Drawing.Point(250, 16);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 30);
            this.ClassBox.TabIndex = 0;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(176, 20);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 23);
            this.label10.TabIndex = 42;
            this.label10.Text = "Class :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(443, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 22);
            this.label1.TabIndex = 43;
            this.label1.Text = "Mode :";
            // 
            // FeeModeBox
            // 
            this.FeeModeBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.FeeModeBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.FeeModeBox.BackColor = System.Drawing.Color.White;
            this.FeeModeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FeeModeBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FeeModeBox.FormattingEnabled = true;
            this.FeeModeBox.Items.AddRange(new object[] {
            "Full",
            "Half",
            "Quarter"});
            this.FeeModeBox.Location = new System.Drawing.Point(502, 24);
            this.FeeModeBox.Name = "FeeModeBox";
            this.FeeModeBox.Size = new System.Drawing.Size(105, 30);
            this.FeeModeBox.TabIndex = 6;
            this.FeeModeBox.SelectedIndexChanged += new System.EventHandler(this.FeeModeBox_SelectedIndexChanged);
            // 
            // TotalTxt
            // 
            this.TotalTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.TotalTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TotalTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.TotalTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalTxt.Location = new System.Drawing.Point(877, 788);
            this.TotalTxt.Margin = new System.Windows.Forms.Padding(4);
            this.TotalTxt.Name = "TotalTxt";
            this.TotalTxt.ReadOnly = true;
            this.TotalTxt.Size = new System.Drawing.Size(183, 29);
            this.TotalTxt.TabIndex = 53;
            // 
            // OtherTxt
            // 
            this.OtherTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.OtherTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.OtherTxt.BackColor = System.Drawing.Color.White;
            this.OtherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OtherTxt.Location = new System.Drawing.Point(771, 168);
            this.OtherTxt.Margin = new System.Windows.Forms.Padding(4);
            this.OtherTxt.Name = "OtherTxt";
            this.OtherTxt.ReadOnly = true;
            this.OtherTxt.Size = new System.Drawing.Size(183, 29);
            this.OtherTxt.TabIndex = 52;
            // 
            // MarkTxt
            // 
            this.MarkTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.MarkTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.MarkTxt.BackColor = System.Drawing.Color.White;
            this.MarkTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MarkTxt.Location = new System.Drawing.Point(771, 119);
            this.MarkTxt.Margin = new System.Windows.Forms.Padding(4);
            this.MarkTxt.Name = "MarkTxt";
            this.MarkTxt.ReadOnly = true;
            this.MarkTxt.Size = new System.Drawing.Size(183, 29);
            this.MarkTxt.TabIndex = 51;
            // 
            // ComTxt
            // 
            this.ComTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ComTxt.BackColor = System.Drawing.Color.White;
            this.ComTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComTxt.Location = new System.Drawing.Point(771, 70);
            this.ComTxt.Margin = new System.Windows.Forms.Padding(4);
            this.ComTxt.Name = "ComTxt";
            this.ComTxt.ReadOnly = true;
            this.ComTxt.Size = new System.Drawing.Size(183, 29);
            this.ComTxt.TabIndex = 50;
            // 
            // BonTxt
            // 
            this.BonTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.BonTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.BonTxt.BackColor = System.Drawing.Color.White;
            this.BonTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BonTxt.Location = new System.Drawing.Point(771, 21);
            this.BonTxt.Margin = new System.Windows.Forms.Padding(4);
            this.BonTxt.Name = "BonTxt";
            this.BonTxt.ReadOnly = true;
            this.BonTxt.Size = new System.Drawing.Size(183, 29);
            this.BonTxt.TabIndex = 49;
            // 
            // TcTxt
            // 
            this.TcTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.TcTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TcTxt.BackColor = System.Drawing.Color.White;
            this.TcTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TcTxt.Location = new System.Drawing.Point(224, 174);
            this.TcTxt.Margin = new System.Windows.Forms.Padding(4);
            this.TcTxt.Name = "TcTxt";
            this.TcTxt.ReadOnly = true;
            this.TcTxt.Size = new System.Drawing.Size(183, 29);
            this.TcTxt.TabIndex = 48;
            // 
            // RegTxt
            // 
            this.RegTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.RegTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.RegTxt.BackColor = System.Drawing.Color.White;
            this.RegTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegTxt.Location = new System.Drawing.Point(224, 124);
            this.RegTxt.Margin = new System.Windows.Forms.Padding(4);
            this.RegTxt.Name = "RegTxt";
            this.RegTxt.ReadOnly = true;
            this.RegTxt.Size = new System.Drawing.Size(183, 29);
            this.RegTxt.TabIndex = 47;
            // 
            // ExamTxt
            // 
            this.ExamTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ExamTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ExamTxt.BackColor = System.Drawing.Color.White;
            this.ExamTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExamTxt.Location = new System.Drawing.Point(224, 74);
            this.ExamTxt.Margin = new System.Windows.Forms.Padding(4);
            this.ExamTxt.Name = "ExamTxt";
            this.ExamTxt.ReadOnly = true;
            this.ExamTxt.Size = new System.Drawing.Size(183, 29);
            this.ExamTxt.TabIndex = 46;
            // 
            // TutionTxt
            // 
            this.TutionTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.TutionTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.TutionTxt.BackColor = System.Drawing.Color.White;
            this.TutionTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TutionTxt.Location = new System.Drawing.Point(224, 24);
            this.TutionTxt.Margin = new System.Windows.Forms.Padding(4);
            this.TutionTxt.Name = "TutionTxt";
            this.TutionTxt.ReadOnly = true;
            this.TutionTxt.Size = new System.Drawing.Size(183, 29);
            this.TutionTxt.TabIndex = 45;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(28, 30);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(172, 22);
            this.label9.TabIndex = 62;
            this.label9.Text = "Tution Fee ( 1 st Term ) :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(635, 24);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 22);
            this.label8.TabIndex = 61;
            this.label8.Text = "Bonafide Fee :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(635, 215);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 22);
            this.label7.TabIndex = 60;
            this.label7.Text = "Total Fee :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(635, 170);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 22);
            this.label6.TabIndex = 59;
            this.label6.Text = "Other Fee :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(634, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 22);
            this.label5.TabIndex = 58;
            this.label5.Text = "Marks Memo Fee :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(28, 177);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 22);
            this.label4.TabIndex = 57;
            this.label4.Text = "TC Fee :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(28, 128);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 22);
            this.label3.TabIndex = 56;
            this.label3.Text = "Registration Fee :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(635, 70);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 22);
            this.label11.TabIndex = 55;
            this.label11.Text = "Computer Fee :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(28, 77);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(179, 22);
            this.label12.TabIndex = 54;
            this.label12.Text = "Tution Fee ( 2 nd Term ) :";
            // 
            // AmountTxt
            // 
            this.AmountTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.AmountTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.AmountTxt.BackColor = System.Drawing.Color.White;
            this.AmountTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AmountTxt.Location = new System.Drawing.Point(942, 560);
            this.AmountTxt.Margin = new System.Windows.Forms.Padding(4);
            this.AmountTxt.Multiline = true;
            this.AmountTxt.Name = "AmountTxt";
            this.AmountTxt.Size = new System.Drawing.Size(183, 39);
            this.AmountTxt.TabIndex = 4;
            this.AmountTxt.TextChanged += new System.EventHandler(this.AmountTxt_TextChanged);
            this.AmountTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AmountTxt_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox1.Controls.Add(this.ExamFeeBox);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.Total);
            this.groupBox1.Controls.Add(this.OtherCheckBox);
            this.groupBox1.Controls.Add(this.TcCheckBox);
            this.groupBox1.Controls.Add(this.MarkCheckBox);
            this.groupBox1.Controls.Add(this.RegCheckBox);
            this.groupBox1.Controls.Add(this.ExamCheckBox);
            this.groupBox1.Controls.Add(this.ComCheckBox);
            this.groupBox1.Controls.Add(this.TutionCheckBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.BonCheckBox);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.FeeModeBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.BonTxt);
            this.groupBox1.Controls.Add(this.OtherTxt);
            this.groupBox1.Controls.Add(this.TutionTxt);
            this.groupBox1.Controls.Add(this.MarkTxt);
            this.groupBox1.Controls.Add(this.ExamTxt);
            this.groupBox1.Controls.Add(this.ComTxt);
            this.groupBox1.Controls.Add(this.RegTxt);
            this.groupBox1.Controls.Add(this.TcTxt);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(171, 271);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1014, 258);
            this.groupBox1.TabIndex = 64;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fees Structure";
            // 
            // ExamFeeBox
            // 
            this.ExamFeeBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ExamFeeBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExamFeeBox.BackColor = System.Drawing.Color.White;
            this.ExamFeeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ExamFeeBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExamFeeBox.FormattingEnabled = true;
            this.ExamFeeBox.Items.AddRange(new object[] {
            "Full",
            "Half",
            "Quarter"});
            this.ExamFeeBox.Location = new System.Drawing.Point(502, 74);
            this.ExamFeeBox.Name = "ExamFeeBox";
            this.ExamFeeBox.Size = new System.Drawing.Size(105, 30);
            this.ExamFeeBox.TabIndex = 8;
            this.ExamFeeBox.SelectedIndexChanged += new System.EventHandler(this.ExamFeeBox_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label18.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(443, 80);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 22);
            this.label18.TabIndex = 73;
            this.label18.Text = "Mode :";
            // 
            // Total
            // 
            this.Total.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.Total.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Total.BackColor = System.Drawing.Color.White;
            this.Total.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Total.Location = new System.Drawing.Point(771, 212);
            this.Total.Margin = new System.Windows.Forms.Padding(4);
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Size = new System.Drawing.Size(183, 29);
            this.Total.TabIndex = 71;
            // 
            // OtherCheckBox
            // 
            this.OtherCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.OtherCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.OtherCheckBox.Location = new System.Drawing.Point(961, 172);
            this.OtherCheckBox.Name = "OtherCheckBox";
            this.OtherCheckBox.Size = new System.Drawing.Size(25, 20);
            this.OtherCheckBox.TabIndex = 14;
            this.OtherCheckBox.UseVisualStyleBackColor = false;
            // 
            // TcCheckBox
            // 
            this.TcCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.TcCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TcCheckBox.Location = new System.Drawing.Point(414, 179);
            this.TcCheckBox.Name = "TcCheckBox";
            this.TcCheckBox.Size = new System.Drawing.Size(22, 20);
            this.TcCheckBox.TabIndex = 10;
            this.TcCheckBox.UseVisualStyleBackColor = false;
            // 
            // MarkCheckBox
            // 
            this.MarkCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.MarkCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MarkCheckBox.Location = new System.Drawing.Point(961, 125);
            this.MarkCheckBox.Name = "MarkCheckBox";
            this.MarkCheckBox.Size = new System.Drawing.Size(25, 20);
            this.MarkCheckBox.TabIndex = 13;
            this.MarkCheckBox.UseVisualStyleBackColor = false;
            // 
            // RegCheckBox
            // 
            this.RegCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.RegCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RegCheckBox.Location = new System.Drawing.Point(414, 129);
            this.RegCheckBox.Name = "RegCheckBox";
            this.RegCheckBox.Size = new System.Drawing.Size(22, 20);
            this.RegCheckBox.TabIndex = 9;
            this.RegCheckBox.UseVisualStyleBackColor = false;
            // 
            // ExamCheckBox
            // 
            this.ExamCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ExamCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ExamCheckBox.Location = new System.Drawing.Point(414, 79);
            this.ExamCheckBox.Name = "ExamCheckBox";
            this.ExamCheckBox.Size = new System.Drawing.Size(22, 20);
            this.ExamCheckBox.TabIndex = 7;
            this.ExamCheckBox.UseVisualStyleBackColor = false;
            this.ExamCheckBox.CheckedChanged += new System.EventHandler(this.ExamCheckBox_CheckedChanged);
            // 
            // ComCheckBox
            // 
            this.ComCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ComCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ComCheckBox.Location = new System.Drawing.Point(961, 74);
            this.ComCheckBox.Name = "ComCheckBox";
            this.ComCheckBox.Size = new System.Drawing.Size(25, 22);
            this.ComCheckBox.TabIndex = 12;
            this.ComCheckBox.UseVisualStyleBackColor = false;
            // 
            // TutionCheckBox
            // 
            this.TutionCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.TutionCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TutionCheckBox.Location = new System.Drawing.Point(414, 24);
            this.TutionCheckBox.Name = "TutionCheckBox";
            this.TutionCheckBox.Size = new System.Drawing.Size(22, 26);
            this.TutionCheckBox.TabIndex = 5;
            this.TutionCheckBox.UseVisualStyleBackColor = false;
            this.TutionCheckBox.CheckedChanged += new System.EventHandler(this.TutionCheckBox_CheckedChanged);
            this.TutionCheckBox.VisibleChanged += new System.EventHandler(this.TutionCheckBox_VisibleChanged);
            // 
            // BonCheckBox
            // 
            this.BonCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BonCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BonCheckBox.Location = new System.Drawing.Point(961, 24);
            this.BonCheckBox.Name = "BonCheckBox";
            this.BonCheckBox.Size = new System.Drawing.Size(25, 23);
            this.BonCheckBox.TabIndex = 11;
            this.BonCheckBox.UseVisualStyleBackColor = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(585, 20);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 23);
            this.label13.TabIndex = 66;
            this.label13.Text = "Gen.Reg.No. :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(39, 41);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 22);
            this.label14.TabIndex = 69;
            this.label14.Text = "Name :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label17.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label17.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(37, 18);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 22);
            this.label17.TabIndex = 73;
            this.label17.Text = "Yearly Fee :";
            // 
            // FeeTxt
            // 
            this.FeeTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.FeeTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.FeeTxt.BackColor = System.Drawing.Color.White;
            this.FeeTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FeeTxt.Location = new System.Drawing.Point(158, 15);
            this.FeeTxt.Margin = new System.Windows.Forms.Padding(4);
            this.FeeTxt.Name = "FeeTxt";
            this.FeeTxt.ReadOnly = true;
            this.FeeTxt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FeeTxt.Size = new System.Drawing.Size(79, 29);
            this.FeeTxt.TabIndex = 8;
            this.FeeTxt.TextChanged += new System.EventHandler(this.FeeTxt_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(39, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 22);
            this.label2.TabIndex = 74;
            this.label2.Text = "Date :";
            // 
            // DateTime
            // 
            this.DateTime.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTime.Location = new System.Drawing.Point(155, 91);
            this.DateTime.Name = "DateTime";
            this.DateTime.Size = new System.Drawing.Size(184, 29);
            this.DateTime.TabIndex = 4;
            // 
            // Print
            // 
            this.Print.BackColor = System.Drawing.Color.Aquamarine;
            this.Print.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Print.Image = ((System.Drawing.Image)(resources.GetObject("Print.Image")));
            this.Print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Print.Location = new System.Drawing.Point(1002, 623);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(183, 39);
            this.Print.TabIndex = 16;
            this.Print.Text = "    Save and Print";
            this.Print.UseVisualStyleBackColor = false;
            this.Print.Click += new System.EventHandler(this.Print_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Location = new System.Drawing.Point(0, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(108, 753);
            this.panel2.TabIndex = 73;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1248, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(108, 741);
            this.panel3.TabIndex = 74;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PreRemCheckBox);
            this.groupBox2.Controls.Add(this.PreRemTxt);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.NameTxt);
            this.groupBox2.Controls.Add(this.DateTime);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(171, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(704, 155);
            this.groupBox2.TabIndex = 76;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Information to fill";
            // 
            // PreRemCheckBox
            // 
            this.PreRemCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.PreRemCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PreRemCheckBox.Location = new System.Drawing.Point(644, 92);
            this.PreRemCheckBox.Name = "PreRemCheckBox";
            this.PreRemCheckBox.Size = new System.Drawing.Size(22, 26);
            this.PreRemCheckBox.TabIndex = 85;
            this.PreRemCheckBox.UseVisualStyleBackColor = false;
            this.PreRemCheckBox.CheckedChanged += new System.EventHandler(this.PreRemCheckBox_CheckedChanged);
            // 
            // PreRemTxt
            // 
            this.PreRemTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.PreRemTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.PreRemTxt.BackColor = System.Drawing.Color.White;
            this.PreRemTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreRemTxt.Location = new System.Drawing.Point(506, 91);
            this.PreRemTxt.Margin = new System.Windows.Forms.Padding(4);
            this.PreRemTxt.Name = "PreRemTxt";
            this.PreRemTxt.ReadOnly = true;
            this.PreRemTxt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PreRemTxt.Size = new System.Drawing.Size(121, 29);
            this.PreRemTxt.TabIndex = 84;
            this.PreRemTxt.TextChanged += new System.EventHandler(this.PreRemTxt_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(353, 94);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(150, 22);
            this.label15.TabIndex = 83;
            this.label15.Text = "Pre. Remaining Fee :";
            // 
            // NameTxt
            // 
            this.NameTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.NameTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.NameTxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NameTxt.FormattingEnabled = true;
            this.NameTxt.Location = new System.Drawing.Point(155, 38);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(511, 30);
            this.NameTxt.TabIndex = 3;
            this.NameTxt.SelectedIndexChanged += new System.EventHandler(this.NameTxt_SelectedIndexChanged);
            this.NameTxt.TextChanged += new System.EventHandler(this.NameTxt_TextChanged);
            this.NameTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NameTxt_KeyDown);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.PaidTxt);
            this.panel1.Controls.Add(this.ReamingTxt);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.FeeTxt);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Location = new System.Drawing.Point(907, 93);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(278, 145);
            this.panel1.TabIndex = 77;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label24.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label24.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(37, 59);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 22);
            this.label24.TabIndex = 78;
            this.label24.Text = "Paid Fee :";
            // 
            // PaidTxt
            // 
            this.PaidTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.PaidTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.PaidTxt.BackColor = System.Drawing.Color.White;
            this.PaidTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaidTxt.Location = new System.Drawing.Point(158, 56);
            this.PaidTxt.Margin = new System.Windows.Forms.Padding(4);
            this.PaidTxt.Name = "PaidTxt";
            this.PaidTxt.ReadOnly = true;
            this.PaidTxt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PaidTxt.Size = new System.Drawing.Size(79, 29);
            this.PaidTxt.TabIndex = 77;
            // 
            // ReamingTxt
            // 
            this.ReamingTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ReamingTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ReamingTxt.BackColor = System.Drawing.Color.White;
            this.ReamingTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReamingTxt.Location = new System.Drawing.Point(158, 97);
            this.ReamingTxt.Margin = new System.Windows.Forms.Padding(4);
            this.ReamingTxt.Name = "ReamingTxt";
            this.ReamingTxt.ReadOnly = true;
            this.ReamingTxt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ReamingTxt.Size = new System.Drawing.Size(79, 29);
            this.ReamingTxt.TabIndex = 76;
            this.ReamingTxt.TextChanged += new System.EventHandler(this.ReamingTxt_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label20.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label20.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(37, 100);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(119, 22);
            this.label20.TabIndex = 75;
            this.label20.Text = "Remaining Fee :";
            // 
            // TotalBtn
            // 
            this.TotalBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.TotalBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.TotalBtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalBtn.Location = new System.Drawing.Point(810, 560);
            this.TotalBtn.Name = "TotalBtn";
            this.TotalBtn.Size = new System.Drawing.Size(118, 39);
            this.TotalBtn.TabIndex = 15;
            this.TotalBtn.Text = "Total Amount ";
            this.TotalBtn.UseVisualStyleBackColor = false;
            this.TotalBtn.Click += new System.EventHandler(this.TotalBtn_Click);
            // 
            // IdTxt
            // 
            this.IdTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.IdTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.IdTxt.BackColor = System.Drawing.Color.White;
            this.IdTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdTxt.Location = new System.Drawing.Point(944, 17);
            this.IdTxt.Margin = new System.Windows.Forms.Padding(4);
            this.IdTxt.Name = "IdTxt";
            this.IdTxt.ReadOnly = true;
            this.IdTxt.Size = new System.Drawing.Size(105, 29);
            this.IdTxt.TabIndex = 77;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label19.Font = new System.Drawing.Font("Palatino Linotype", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(825, 20);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(105, 23);
            this.label19.TabIndex = 78;
            this.label19.Text = "Receipt No. :";
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.AcdYearBox.BackColor = System.Drawing.Color.White;
            this.AcdYearBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Location = new System.Drawing.Point(450, 16);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(121, 30);
            this.AcdYearBox.TabIndex = 1;
            this.AcdYearBox.SelectedIndexChanged += new System.EventHandler(this.AcdYearBox_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label21.Font = new System.Drawing.Font("Palatino Linotype", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(385, 20);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 23);
            this.label21.TabIndex = 80;
            this.label21.Text = "Year :";
            // 
            // OldTutioncheck
            // 
            this.OldTutioncheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.OldTutioncheck.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.OldTutioncheck.Location = new System.Drawing.Point(397, 554);
            this.OldTutioncheck.Name = "OldTutioncheck";
            this.OldTutioncheck.Size = new System.Drawing.Size(22, 20);
            this.OldTutioncheck.TabIndex = 15;
            this.OldTutioncheck.UseVisualStyleBackColor = false;
            this.OldTutioncheck.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label16.Font = new System.Drawing.Font("Palatino Linotype", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(175, 553);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(218, 21);
            this.label16.TabIndex = 82;
            this.label16.Text = "Is student has old tution fee ?";
            // 
            // oldTutuionPanal
            // 
            this.oldTutuionPanal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oldTutuionPanal.Controls.Add(this.label22);
            this.oldTutuionPanal.Controls.Add(this.OldFeeTxt);
            this.oldTutuionPanal.Location = new System.Drawing.Point(177, 602);
            this.oldTutuionPanal.Name = "oldTutuionPanal";
            this.oldTutuionPanal.Size = new System.Drawing.Size(418, 65);
            this.oldTutuionPanal.TabIndex = 83;
            this.oldTutuionPanal.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label22.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(27, 20);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 22);
            this.label22.TabIndex = 73;
            this.label22.Text = "Enter fee :";
            // 
            // OldFeeTxt
            // 
            this.OldFeeTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.OldFeeTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.OldFeeTxt.BackColor = System.Drawing.Color.White;
            this.OldFeeTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OldFeeTxt.Location = new System.Drawing.Point(146, 17);
            this.OldFeeTxt.Margin = new System.Windows.Forms.Padding(4);
            this.OldFeeTxt.Name = "OldFeeTxt";
            this.OldFeeTxt.Size = new System.Drawing.Size(243, 29);
            this.OldFeeTxt.TabIndex = 0;
            this.OldFeeTxt.TextChanged += new System.EventHandler(this.OldFeeTxt_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label23.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(389, 577);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(33, 22);
            this.label23.TabIndex = 85;
            this.label23.Text = "Yes";
            // 
            // IdBox
            // 
            this.IdBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdBox.Location = new System.Drawing.Point(711, 17);
            this.IdBox.Name = "IdBox";
            this.IdBox.ReadOnly = true;
            this.IdBox.Size = new System.Drawing.Size(100, 29);
            this.IdBox.TabIndex = 86;
            this.IdBox.TextChanged += new System.EventHandler(this.IdBox_TextChanged_1);
            // 
            // NewReceiptBtn
            // 
            this.NewReceiptBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.NewReceiptBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.NewReceiptBtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewReceiptBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NewReceiptBtn.Location = new System.Drawing.Point(1063, 12);
            this.NewReceiptBtn.Name = "NewReceiptBtn";
            this.NewReceiptBtn.Size = new System.Drawing.Size(118, 39);
            this.NewReceiptBtn.TabIndex = 87;
            this.NewReceiptBtn.Text = "New Receipt";
            this.NewReceiptBtn.UseVisualStyleBackColor = false;
            this.NewReceiptBtn.Click += new System.EventHandler(this.NewReceiptBtn_Click);
            // 
            // PrePrimaryNewReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1356, 741);
            this.Controls.Add(this.NewReceiptBtn);
            this.Controls.Add(this.IdBox);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.OldTutioncheck);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.AcdYearBox);
            this.Controls.Add(this.TotalTxt);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.IdTxt);
            this.Controls.Add(this.TotalBtn);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ClassBox);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.Print);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.AmountTxt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.oldTutuionPanal);
            this.Name = "PrePrimaryNewReceipt";
            this.Text = "Pre-Primary School New Receipt";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.StudentFees_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.oldTutuionPanal.ResumeLayout(false);
            this.oldTutuionPanal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox FeeModeBox;
        private System.Windows.Forms.TextBox TotalTxt;
        private System.Windows.Forms.TextBox OtherTxt;
        private System.Windows.Forms.TextBox MarkTxt;
        private System.Windows.Forms.TextBox ComTxt;
        private System.Windows.Forms.TextBox BonTxt;
        private System.Windows.Forms.TextBox TcTxt;
        private System.Windows.Forms.TextBox RegTxt;
        private System.Windows.Forms.TextBox ExamTxt;
        private System.Windows.Forms.TextBox TutionTxt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox AmountTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button Print;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DateTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox FeeTxt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox OtherCheckBox;
        private System.Windows.Forms.CheckBox MarkCheckBox;
        private System.Windows.Forms.CheckBox ComCheckBox;
        private System.Windows.Forms.CheckBox BonCheckBox;
        private System.Windows.Forms.CheckBox TcCheckBox;
        private System.Windows.Forms.CheckBox RegCheckBox;
        private System.Windows.Forms.CheckBox ExamCheckBox;
        private System.Windows.Forms.CheckBox TutionCheckBox;
        private System.Windows.Forms.Button TotalBtn;
        private System.Windows.Forms.TextBox Total;
        private System.Windows.Forms.ComboBox ExamFeeBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox IdTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox ReamingTxt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox OldTutioncheck;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel oldTutuionPanal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox OldFeeTxt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox NameTxt;
        private System.Windows.Forms.TextBox IdBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox PaidTxt;
        private System.Windows.Forms.Button NewReceiptBtn;
        private System.Windows.Forms.CheckBox PreRemCheckBox;
        private System.Windows.Forms.TextBox PreRemTxt;
        private System.Windows.Forms.Label label15;
    }
}