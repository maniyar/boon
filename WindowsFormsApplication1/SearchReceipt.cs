﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Boon
{
    public partial class IdSearchReceipt : Form
    {
        common c = new common();
        DataTable table = new DataTable();
        MySqlConnection connection;
        public IdSearchReceipt()
        {
            InitializeComponent();
            connection = new MySqlConnection(c.ConnectionString);
        }
   
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            //Std = '" + comboBox1.Text +   OR ID = '" + IdTxt.Text + "' OR  ReceiptNo = '" + ReceiptsNoTxt.Text + "'                 String SelectQuery = "SELECT * from Boon.receipts WHERE ReceiptNo = '" + ReceiptsNoTxt.Text + "'   ";
            try
            {
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    if (ReceiptsNoTxt.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_pri_primary WHERE ReceiptNo = '" + ReceiptsNoTxt.Text + "'   ";
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {

                    if (ReceiptsNoTxt.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts WHERE ReceiptNo = '" + ReceiptsNoTxt.Text + "'   ";
                        
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {

                    if (ReceiptsNoTxt.Text != "")
                    {
                        
                        String SelectQuery = "SELECT * from Boon.receipts_high WHERE ReceiptNo = '" + ReceiptsNoTxt.Text + "'   ";
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                
                
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
        }
        public void ClearText()
        {
            IdTxt.Text = null;
            NameTxt.Text = null;
            ReceiptsNoTxt.Text = null;
            dataGridView1.Rows.Clear();
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            //ClearText();
        }

        private void StudentNameLoad()
        {
            try
            {
                //connection.Open();
                //MySqlCommand Cmd = new MySqlCommand();
                //Cmd.Connection = connection;
                //Cmd.CommandText = "select Name from Boon.students Where Std = '" + ClassBox.Text + "' OR ID = '" + IdTxt.Text + "'";
                //MySqlDataReader dr1 = Cmd.ExecuteReader();
                //AutoCompleteStringCollection data = new AutoCompleteStringCollection();
                //while (dr1.Read())
                //{
                //    data.Add(dr1.GetString(0));

                //}
                //NameTxt.AutoCompleteCustomSource = data;
                //dr1.Close();
                string SelectQuery = "select Name from Boon.students Where Std = '" + ClassBox.Text + "' OR ID = '" + IdTxt.Text + "'";

            }
            catch
            {

            }
            connection.Close();
        }


        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //StudentNameLoad();
        }

        private void GenSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    if (IdTxt.Text != "" && NameTxt.Text == "" && ReceiptsNoTxt.Text == "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_pri_primary WHERE ID = '" + IdTxt.Text + "'   ";
                        table = c.SelectData(SelectQuery);

                        var a = table.Rows[0][0].ToString();

                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();

                            }
                        }

                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                } // pre loop end here
                
                // primary loop start from here
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    if (IdTxt.Text != "" && NameTxt.Text == "" && ReceiptsNoTxt.Text == "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts WHERE ID = '" + IdTxt.Text + "'   ";
                        table = c.SelectData(SelectQuery);

                        var a = table.Rows[0][0].ToString();

                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();

                            }
                        }

                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }//pri loop end here

                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    if (IdTxt.Text != "" && NameTxt.Text == "" && ReceiptsNoTxt.Text == "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_high WHERE ID = '" + IdTxt.Text + "'   ";
                        table = c.SelectData(SelectQuery);

                        var a = table.Rows[0][0].ToString();

                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();

                            }
                        }

                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }

            }  // try loop end
            catch { }  
        }

        private void NameSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    if (NameTxt.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_pri_primary WHERE StudName LIKE '" + NameTxt.Text + "%'  ";
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {

                    if (NameTxt.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts WHERE StudName LIKE '" + NameTxt.Text + "%'  ";
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {

                    if (NameTxt.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_high WHERE StudName LIKE '" + NameTxt.Text + "%'  ";
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }

            }// try loop
            catch { }
        }

        private void ClassBtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    if (ClassBox.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_pri_primary WHERE Class = '" + ClassBox.Text + "'   AND Date Between '" + dateTimePicker1.Value + "' AND '" + dateTimePicker2.Value + "' "; 
                        table = c.SelectData(SelectQuery);
                  
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {

                    if (ClassBox.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts WHERE Class = '" + ClassBox.Text + "'   AND Date Between '" + dateTimePicker1.Value + "' AND '" + dateTimePicker2.Value + "' "; 
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {

                    if (ClassBox.Text != "")
                    {
                        String SelectQuery = "SELECT * from Boon.receipts_high WHERE Class = '" + ClassBox.Text + "'   AND Date Between '" + dateTimePicker1.Value + "' AND '" + dateTimePicker2.Value + "' "; 
                        table = c.SelectData(SelectQuery);
                        //    connection.Open();
                        if (table.Rows.Count > 0)
                        {
                            foreach (DataRow item in table.Rows)
                            {
                                int n = dataGridView1.Rows.Add();
                                dataGridView1.Rows[n].Cells[0].Value = item["ReceiptNo"].ToString();
                                dataGridView1.Rows[n].Cells[1].Value = item["StudName"].ToString();
                                dataGridView1.Rows[n].Cells[2].Value = item["Class"].ToString();
                                dataGridView1.Rows[n].Cells[3].Value = item["Date"].ToString();

                                //  dataGridView1.Rows[n].Cells[4].Value = item["Date"].ToString();
                                dataGridView1.Rows[n].Cells[4].Value = item["EducationFee"].ToString();
                                dataGridView1.Rows[n].Cells[5].Value = item["ExamFee"].ToString();
                                dataGridView1.Rows[n].Cells[6].Value = item["RegFee"].ToString();
                                dataGridView1.Rows[n].Cells[7].Value = item["TCFee"].ToString();
                                dataGridView1.Rows[n].Cells[8].Value = item["BonFee"].ToString();
                                dataGridView1.Rows[n].Cells[9].Value = item["ComFee"].ToString();
                                dataGridView1.Rows[n].Cells[10].Value = item["MarkFee"].ToString();
                                dataGridView1.Rows[n].Cells[11].Value = item["OtherFee"].ToString();
                                dataGridView1.Rows[n].Cells[12].Value = item["TotalFee"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Data Not Available  ");
                            return;
                        }
                    }
                }
                
            }
            catch { }
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[index];
                String IdTxt = selectedRow.Cells[0].Value.ToString();
                if (ClassBox.Text == "Nursery" || ClassBox.Text == "Sr.KG" || ClassBox.Text == "Jr.KG")
                {
                    PrePrimaryReceipt obj = new PrePrimaryReceipt();
                    obj.ID = IdTxt;
                    obj.ShowDialog();
                }
                if (ClassBox.Text == "8 th" || ClassBox.Text == "9 th" || ClassBox.Text == "10 th")
                {
                    HighReceipt obj = new HighReceipt();
                    obj.ID = IdTxt;
                    obj.ShowDialog();
                }
                if (ClassBox.Text == "1 st" || ClassBox.Text == "2 nd" || ClassBox.Text == "3 rd" || ClassBox.Text == "4 th" || ClassBox.Text == "5 th" || ClassBox.Text == "6 th" || ClassBox.Text == "7 th")
                {
                    Receipt obj = new Receipt();
                    obj.ID = IdTxt;
                    obj.ShowDialog();
                }
            }
            catch { }
        } 

    }
}
