﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Boon
{
    public partial class ExamMarksAdd : Form
    {
        public ExamMarksAdd()
        {
            InitializeComponent();
        }
        common c = new common();
        private void GetListBtn_Click(object sender, EventArgs e)
        {
            try {
                if (ClassBox.Text == "" || AcdYearBox.Text == "")
                {
                    MessageBox.Show("Class & Acd. Year should be entered ", "Warnning");
                    AcdYearBox.Focus();
                    return;
                }
                dataGridView1.Rows.Clear();
                String SelectQuery = "SELECT * from Boon.students WHERE Std = '" + ClassBox.Text + "' AND AcdYear = '" + AcdYearBox.Text + "'AND Contact>10 Order BY ID";
                    DataTable table = new DataTable();
                    table = c.SelectData(SelectQuery);
                    foreach (DataRow item in table.Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = item["ID"].ToString();
                        dataGridView1.Rows[n].Cells[1].Value = item["Name"].ToString();
                        dataGridView1.Rows[n].Cells[2].Value = "A";
                        TotalLbl.Text = "Total students : " + dataGridView1.Rows.Count;
                    }
            
            }
            catch { }
        }
        //Ename,Date,Sub,Tmarks,Obtmarks,Class,Year,SID,Sname,Contact

        private void ClassBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AcdYearBox.Items.Clear();

                String str = "select Distinct(AcdYear) from Boon.students WHERE Std='" + ClassBox.Text + "'  ORDER BY AcdYear ";
               DataTable dt = c.SelectData(str);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        c.Text = row["AcdYear"].ToString();
                        AcdYearBox.Items.Add(c);
                    }
                }


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {

                if (NameTxt.Text == "" || NameTxt.Text == "  " || NameTxt.Text == " ")
                {
                    MessageBox.Show("Exam name should be entered ", "Warnning");
                    NameTxt.Focus();
                    return;
                }
                if (OutOfMarksTxt.Text == "" || SubBox.Text == "")
                {
                    MessageBox.Show("Exam marks & subject should be entered ", "Warnning");
                    OutOfMarksTxt.Focus();
                    return;
                }
                if (MessageBox.Show("Do you want to save exam marks?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    int i = 0;
                    int j = 0;
                    if (dataGridView1.Rows.Count > 0)
                    {
                        foreach (DataGridViewRow row in this.dataGridView1.Rows)
                        {
                            var SelectedID = row.Cells[0].Value.ToString();
                            var name = row.Cells[1].Value.ToString();
                            var mark = row.Cells[2].Value.ToString();
                            string SelecetQuery = "Select Contact From Boon.Students Where ID ='" + SelectedID + "' ";
                            DataTable dt = c.SelectData(SelecetQuery);
                            if (dt.Rows.Count > 0)
                            {
                                String Contact = dt.Rows[0][0].ToString();
                                String InsertQuery = "Insert Into Boon.Exam_primary(Ename,Date,Sub,Tmarks,Obtmarks,Class,Year,SID,Sname,Contact) values ('" + NameTxt.Text + "','" + dateTimePicker1.Value + "','" + SubBox.Text + "','" + OutOfMarksTxt.Text + "','" + mark + "','" + ClassBox.Text + "','" + AcdYearBox.Text + "','" + SelectedID + "','" + name + "','" + Contact + "')";
                                if (c.InsertData(InsertQuery))
                                {
                                    ++i;

                                }
                                else
                                {
                                    ++j;
                                }
                            }
                            if (i + j == dataGridView1.Rows.Count)
                            {
                                MessageBox.Show("Exam Marks Saved", "Alert");
                            }
                        }
                    }
                    else { MessageBox.Show("Select students first", "Alert"); }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
        }

        private void OutOfMarksTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try {
                Char ch = e.KeyChar;
                if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
                {
                    MessageBox.Show("Enter Numbers Only", "Alert");
                    e.Handled = true;
                }
            }
            catch { }
        }
    }
}
