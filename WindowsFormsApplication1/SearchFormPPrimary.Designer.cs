﻿namespace Boon
{
    partial class SearchFormPPrimary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.StudType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StudCountTxt = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.RemFeeTxt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TutionFeeTxt = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.DateTime2 = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.StudentIDTxt = new System.Windows.Forms.TextBox();
            this.FirstClassBox = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.PreClassBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.CategoryBox = new System.Windows.Forms.ComboBox();
            this.PriviousTxt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.SNameTxt = new System.Windows.Forms.TextBox();
            this.MotherTxt = new System.Windows.Forms.TextBox();
            this.FatherTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.FemaleRbtn = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.MotherTongueTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.AddressTxt = new System.Windows.Forms.TextBox();
            this.ReligionTxt = new System.Windows.Forms.TextBox();
            this.AadharTxt = new System.Windows.Forms.TextBox();
            this.SubCastTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ContactTxt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.MaleRbtn = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.CastBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.NationalityBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.DateTime1 = new System.Windows.Forms.DateTimePicker();
            this.PlaceTxt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.ClassBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.AcdYearBox);
            this.panel1.Location = new System.Drawing.Point(453, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 54);
            this.panel1.TabIndex = 119;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(284, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 22);
            this.label4.TabIndex = 0;
            this.label4.Text = "Acd. Year :";
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "Nursery",
            "Jr.KG",
            "Sr.KG"});
            this.ClassBox.Location = new System.Drawing.Point(144, 14);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 26);
            this.ClassBox.TabIndex = 0;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(54, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 22);
            this.label3.TabIndex = 24;
            this.label3.Text = "Select Std :";
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AcdYearBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Location = new System.Drawing.Point(374, 14);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(121, 26);
            this.AcdYearBox.TabIndex = 1;
            this.AcdYearBox.SelectedIndexChanged += new System.EventHandler(this.AcdYearBox_SelectedIndexChanged);
            // 
            // NameTxt
            // 
            this.NameTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.NameTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.NameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTxt.Location = new System.Drawing.Point(1023, 129);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(282, 26);
            this.NameTxt.TabIndex = 3;
            this.NameTxt.TextChanged += new System.EventHandler(this.NameTxt_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.SearchBtn);
            this.panel2.Location = new System.Drawing.Point(1028, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(185, 52);
            this.panel2.TabIndex = 118;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.button2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(94, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 38);
            this.button2.TabIndex = 1;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // SearchBtn
            // 
            this.SearchBtn.BackColor = System.Drawing.Color.Lime;
            this.SearchBtn.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.SearchBtn.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBtn.Location = new System.Drawing.Point(8, 6);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(80, 38);
            this.SearchBtn.TabIndex = 0;
            this.SearchBtn.Text = "Search";
            this.SearchBtn.UseVisualStyleBackColor = false;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.AutoSize = true;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.StudType);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Location = new System.Drawing.Point(145, 30);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(283, 52);
            this.panel3.TabIndex = 120;
            // 
            // StudType
            // 
            this.StudType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.StudType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.StudType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StudType.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudType.FormattingEnabled = true;
            this.StudType.Items.AddRange(new object[] {
            "New",
            "Old"});
            this.StudType.Location = new System.Drawing.Point(137, 14);
            this.StudType.Name = "StudType";
            this.StudType.Size = new System.Drawing.Size(121, 26);
            this.StudType.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(22, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 22);
            this.label5.TabIndex = 116;
            this.label5.Text = "Student Type :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(957, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 22);
            this.label1.TabIndex = 15;
            this.label1.Text = "Name :";
            // 
            // StudCountTxt
            // 
            this.StudCountTxt.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.StudCountTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StudCountTxt.Location = new System.Drawing.Point(12, 135);
            this.StudCountTxt.Name = "StudCountTxt";
            this.StudCountTxt.ReadOnly = true;
            this.StudCountTxt.Size = new System.Drawing.Size(154, 15);
            this.StudCountTxt.TabIndex = 153;
            this.StudCountTxt.Text = "Total students :";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(44, 171);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1275, 519);
            this.tabControl1.TabIndex = 154;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage1.Controls.Add(this.PrintBtn);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1267, 490);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "List";
            // 
            // PrintBtn
            // 
            this.PrintBtn.BackColor = System.Drawing.Color.Lime;
            this.PrintBtn.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintBtn.Image = global::Boon.Properties.Resources.printicon;
            this.PrintBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PrintBtn.Location = new System.Drawing.Point(1098, 430);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(136, 45);
            this.PrintBtn.TabIndex = 156;
            this.PrintBtn.Text = "Print";
            this.PrintBtn.UseVisualStyleBackColor = false;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(133, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1000, 408);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "List of Students :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.Location = new System.Drawing.Point(14, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(972, 358);
            this.dataGridView1.TabIndex = 144;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1267, 490);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Student Info";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.RemFeeTxt);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.TutionFeeTxt);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.DateTime2);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.StudentIDTxt);
            this.panel6.Controls.Add(this.FirstClassBox);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.PreClassBox);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.CategoryBox);
            this.panel6.Controls.Add(this.PriviousTxt);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Location = new System.Drawing.Point(629, 31);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(622, 428);
            this.panel6.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label27.Location = new System.Drawing.Point(29, 240);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(119, 22);
            this.label27.TabIndex = 185;
            this.label27.Text = "Remaining Fee :";
            // 
            // RemFeeTxt
            // 
            this.RemFeeTxt.BackColor = System.Drawing.SystemColors.Window;
            this.RemFeeTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemFeeTxt.Location = new System.Drawing.Point(181, 235);
            this.RemFeeTxt.Name = "RemFeeTxt";
            this.RemFeeTxt.ReadOnly = true;
            this.RemFeeTxt.Size = new System.Drawing.Size(159, 29);
            this.RemFeeTxt.TabIndex = 184;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(355, 238);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 22);
            this.label22.TabIndex = 183;
            this.label22.Text = "Tution Fee :";
            // 
            // TutionFeeTxt
            // 
            this.TutionFeeTxt.BackColor = System.Drawing.SystemColors.Window;
            this.TutionFeeTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TutionFeeTxt.Location = new System.Drawing.Point(463, 235);
            this.TutionFeeTxt.Name = "TutionFeeTxt";
            this.TutionFeeTxt.ReadOnly = true;
            this.TutionFeeTxt.Size = new System.Drawing.Size(130, 29);
            this.TutionFeeTxt.TabIndex = 182;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label26.Location = new System.Drawing.Point(29, 85);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(92, 22);
            this.label26.TabIndex = 181;
            this.label26.Text = "Student ID :";
            // 
            // DateTime2
            // 
            this.DateTime2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime2.Location = new System.Drawing.Point(181, 183);
            this.DateTime2.Name = "DateTime2";
            this.DateTime2.Size = new System.Drawing.Size(159, 29);
            this.DateTime2.TabIndex = 179;
            this.DateTime2.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(29, 188);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(141, 22);
            this.label20.TabIndex = 180;
            this.label20.Text = "Date of Admission :";
            // 
            // StudentIDTxt
            // 
            this.StudentIDTxt.BackColor = System.Drawing.SystemColors.Window;
            this.StudentIDTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentIDTxt.Location = new System.Drawing.Point(181, 78);
            this.StudentIDTxt.Name = "StudentIDTxt";
            this.StudentIDTxt.ReadOnly = true;
            this.StudentIDTxt.Size = new System.Drawing.Size(159, 29);
            this.StudentIDTxt.TabIndex = 178;
            // 
            // FirstClassBox
            // 
            this.FirstClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.FirstClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FirstClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstClassBox.FormattingEnabled = true;
            this.FirstClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th"});
            this.FirstClassBox.Location = new System.Drawing.Point(181, 130);
            this.FirstClassBox.Name = "FirstClassBox";
            this.FirstClassBox.Size = new System.Drawing.Size(159, 30);
            this.FirstClassBox.TabIndex = 139;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(29, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(146, 22);
            this.label25.TabIndex = 138;
            this.label25.Text = "First Admitted Std. :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(350, 85);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 22);
            this.label23.TabIndex = 140;
            this.label23.Text = "Previous Std. :";
            // 
            // PreClassBox
            // 
            this.PreClassBox.BackColor = System.Drawing.SystemColors.Window;
            this.PreClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PreClassBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreClassBox.FormattingEnabled = true;
            this.PreClassBox.Items.AddRange(new object[] {
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th",
            "Nursery",
            "Jr.KG",
            "Sr.KG"});
            this.PreClassBox.Location = new System.Drawing.Point(463, 78);
            this.PreClassBox.Name = "PreClassBox";
            this.PreClassBox.Size = new System.Drawing.Size(130, 30);
            this.PreClassBox.TabIndex = 137;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(355, 135);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 22);
            this.label16.TabIndex = 177;
            this.label16.Text = "Category :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CategoryBox
            // 
            this.CategoryBox.BackColor = System.Drawing.SystemColors.Window;
            this.CategoryBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategoryBox.FormattingEnabled = true;
            this.CategoryBox.Items.AddRange(new object[] {
            "Open",
            "OBC",
            "NT",
            "NT3",
            "SC",
            "ST",
            "VJNT",
            "Other"});
            this.CategoryBox.Location = new System.Drawing.Point(463, 130);
            this.CategoryBox.Name = "CategoryBox";
            this.CategoryBox.Size = new System.Drawing.Size(130, 30);
            this.CategoryBox.TabIndex = 169;
            // 
            // PriviousTxt
            // 
            this.PriviousTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PriviousTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PriviousTxt.Location = new System.Drawing.Point(181, 26);
            this.PriviousTxt.Name = "PriviousTxt";
            this.PriviousTxt.ReadOnly = true;
            this.PriviousTxt.Size = new System.Drawing.Size(412, 29);
            this.PriviousTxt.TabIndex = 136;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(29, 29);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(122, 22);
            this.label21.TabIndex = 135;
            this.label21.Text = "Previous school :";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.SNameTxt);
            this.panel4.Controls.Add(this.MotherTxt);
            this.panel4.Controls.Add(this.FatherTxt);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.FemaleRbtn);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.MotherTongueTxt);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.AddressTxt);
            this.panel4.Controls.Add(this.ReligionTxt);
            this.panel4.Controls.Add(this.AadharTxt);
            this.panel4.Controls.Add(this.SubCastTxt);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.ContactTxt);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.MaleRbtn);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.CastBox);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.NationalityBox);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.DateTime1);
            this.panel4.Controls.Add(this.PlaceTxt);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Location = new System.Drawing.Point(16, 31);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(607, 428);
            this.panel4.TabIndex = 4;
            // 
            // SNameTxt
            // 
            this.SNameTxt.BackColor = System.Drawing.SystemColors.Window;
            this.SNameTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SNameTxt.Location = new System.Drawing.Point(155, 22);
            this.SNameTxt.Name = "SNameTxt";
            this.SNameTxt.ReadOnly = true;
            this.SNameTxt.Size = new System.Drawing.Size(425, 29);
            this.SNameTxt.TabIndex = 152;
            // 
            // MotherTxt
            // 
            this.MotherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTxt.Location = new System.Drawing.Point(155, 66);
            this.MotherTxt.Name = "MotherTxt";
            this.MotherTxt.ReadOnly = true;
            this.MotherTxt.Size = new System.Drawing.Size(135, 29);
            this.MotherTxt.TabIndex = 155;
            // 
            // FatherTxt
            // 
            this.FatherTxt.BackColor = System.Drawing.SystemColors.Window;
            this.FatherTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FatherTxt.Location = new System.Drawing.Point(419, 66);
            this.FatherTxt.Name = "FatherTxt";
            this.FatherTxt.ReadOnly = true;
            this.FatherTxt.Size = new System.Drawing.Size(161, 29);
            this.FatherTxt.TabIndex = 157;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(296, 73);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(117, 22);
            this.label19.TabIndex = 179;
            this.label19.Text = "Father\'s Name :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(24, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 22);
            this.label11.TabIndex = 174;
            this.label11.Text = "Mother\'s Name :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(25, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 22);
            this.label12.TabIndex = 151;
            this.label12.Text = "Name :";
            // 
            // FemaleRbtn
            // 
            this.FemaleRbtn.AutoSize = true;
            this.FemaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FemaleRbtn.Location = new System.Drawing.Point(254, 105);
            this.FemaleRbtn.Name = "FemaleRbtn";
            this.FemaleRbtn.Size = new System.Drawing.Size(77, 26);
            this.FemaleRbtn.TabIndex = 161;
            this.FemaleRbtn.Text = "Female";
            this.FemaleRbtn.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(24, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 22);
            this.label6.TabIndex = 160;
            this.label6.Text = "Gender :";
            // 
            // MotherTongueTxt
            // 
            this.MotherTongueTxt.BackColor = System.Drawing.SystemColors.Window;
            this.MotherTongueTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotherTongueTxt.Location = new System.Drawing.Point(422, 139);
            this.MotherTongueTxt.Name = "MotherTongueTxt";
            this.MotherTongueTxt.ReadOnly = true;
            this.MotherTongueTxt.Size = new System.Drawing.Size(158, 29);
            this.MotherTongueTxt.TabIndex = 163;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(300, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 22);
            this.label7.TabIndex = 180;
            this.label7.Text = "Religion :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(293, 146);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(123, 22);
            this.label24.TabIndex = 181;
            this.label24.Text = "Mother Tongue :";
            // 
            // AddressTxt
            // 
            this.AddressTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AddressTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressTxt.Location = new System.Drawing.Point(155, 376);
            this.AddressTxt.Name = "AddressTxt";
            this.AddressTxt.ReadOnly = true;
            this.AddressTxt.Size = new System.Drawing.Size(425, 29);
            this.AddressTxt.TabIndex = 172;
            // 
            // ReligionTxt
            // 
            this.ReligionTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ReligionTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReligionTxt.Location = new System.Drawing.Point(419, 231);
            this.ReligionTxt.Name = "ReligionTxt";
            this.ReligionTxt.ReadOnly = true;
            this.ReligionTxt.Size = new System.Drawing.Size(161, 29);
            this.ReligionTxt.TabIndex = 166;
            // 
            // AadharTxt
            // 
            this.AadharTxt.BackColor = System.Drawing.SystemColors.Window;
            this.AadharTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AadharTxt.Location = new System.Drawing.Point(155, 327);
            this.AadharTxt.Name = "AadharTxt";
            this.AadharTxt.ReadOnly = true;
            this.AadharTxt.Size = new System.Drawing.Size(135, 29);
            this.AadharTxt.TabIndex = 170;
            // 
            // SubCastTxt
            // 
            this.SubCastTxt.BackColor = System.Drawing.SystemColors.Window;
            this.SubCastTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubCastTxt.Location = new System.Drawing.Point(419, 281);
            this.SubCastTxt.Name = "SubCastTxt";
            this.SubCastTxt.ReadOnly = true;
            this.SubCastTxt.Size = new System.Drawing.Size(161, 29);
            this.SubCastTxt.TabIndex = 168;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(26, 379);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 22);
            this.label10.TabIndex = 153;
            this.label10.Text = "Address :";
            // 
            // ContactTxt
            // 
            this.ContactTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ContactTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ContactTxt.Location = new System.Drawing.Point(419, 326);
            this.ContactTxt.Name = "ContactTxt";
            this.ContactTxt.ReadOnly = true;
            this.ContactTxt.Size = new System.Drawing.Size(161, 29);
            this.ContactTxt.TabIndex = 171;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(26, 234);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 22);
            this.label18.TabIndex = 178;
            this.label18.Text = "Nationality :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(296, 283);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 22);
            this.label14.TabIndex = 176;
            this.label14.Text = "Sub Caste :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MaleRbtn
            // 
            this.MaleRbtn.AutoSize = true;
            this.MaleRbtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaleRbtn.Location = new System.Drawing.Point(155, 104);
            this.MaleRbtn.Name = "MaleRbtn";
            this.MaleRbtn.Size = new System.Drawing.Size(62, 26);
            this.MaleRbtn.TabIndex = 159;
            this.MaleRbtn.Text = "Male";
            this.MaleRbtn.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(26, 330);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 22);
            this.label8.TabIndex = 156;
            this.label8.Text = "Aadhar No. :";
            // 
            // CastBox
            // 
            this.CastBox.BackColor = System.Drawing.SystemColors.Window;
            this.CastBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CastBox.FormattingEnabled = true;
            this.CastBox.Items.AddRange(new object[] {
            "Muslim",
            "Crishshan",
            "Hindu",
            "Sikh",
            "Buddh",
            "Jain",
            "Other"});
            this.CastBox.Location = new System.Drawing.Point(155, 280);
            this.CastBox.Name = "CastBox";
            this.CastBox.Size = new System.Drawing.Size(135, 30);
            this.CastBox.TabIndex = 167;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(300, 333);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 22);
            this.label9.TabIndex = 154;
            this.label9.Text = "Contact No. :";
            // 
            // NationalityBox
            // 
            this.NationalityBox.BackColor = System.Drawing.SystemColors.Window;
            this.NationalityBox.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NationalityBox.FormattingEnabled = true;
            this.NationalityBox.Items.AddRange(new object[] {
            "Indian",
            "Other"});
            this.NationalityBox.Location = new System.Drawing.Point(155, 231);
            this.NationalityBox.Name = "NationalityBox";
            this.NationalityBox.Size = new System.Drawing.Size(135, 30);
            this.NationalityBox.TabIndex = 165;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(26, 283);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 22);
            this.label13.TabIndex = 175;
            this.label13.Text = "Caste :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(24, 148);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 22);
            this.label15.TabIndex = 158;
            this.label15.Text = "Date of Birth :";
            // 
            // DateTime1
            // 
            this.DateTime1.CalendarMonthBackground = System.Drawing.Color.NavajoWhite;
            this.DateTime1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime1.Location = new System.Drawing.Point(155, 139);
            this.DateTime1.Name = "DateTime1";
            this.DateTime1.Size = new System.Drawing.Size(135, 29);
            this.DateTime1.TabIndex = 162;
            this.DateTime1.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            // 
            // PlaceTxt
            // 
            this.PlaceTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PlaceTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceTxt.Location = new System.Drawing.Point(155, 187);
            this.PlaceTxt.Name = "PlaceTxt";
            this.PlaceTxt.ReadOnly = true;
            this.PlaceTxt.Size = new System.Drawing.Size(425, 29);
            this.PlaceTxt.TabIndex = 164;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(24, 190);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 22);
            this.label17.TabIndex = 173;
            this.label17.Text = "Place of birth";
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 3;
            this.lineShape1.X2 = 1354;
            this.lineShape1.Y1 = 101;
            this.lineShape1.Y2 = 100;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1362, 741);
            this.shapeContainer1.TabIndex = 155;
            this.shapeContainer1.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Location = new System.Drawing.Point(950, 120);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(369, 45);
            this.panel5.TabIndex = 156;
            // 
            // SearchFormPPrimary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.StudCountTxt);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NameTxt);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.shapeContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SearchFormPPrimary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Student Pre-Primary";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox StudType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox StudCountTxt;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox RemFeeTxt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TutionFeeTxt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker DateTime2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox StudentIDTxt;
        private System.Windows.Forms.ComboBox FirstClassBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox PreClassBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox CategoryBox;
        private System.Windows.Forms.TextBox PriviousTxt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox SNameTxt;
        private System.Windows.Forms.TextBox MotherTxt;
        private System.Windows.Forms.TextBox FatherTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton FemaleRbtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox MotherTongueTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox AddressTxt;
        private System.Windows.Forms.TextBox ReligionTxt;
        private System.Windows.Forms.TextBox AadharTxt;
        private System.Windows.Forms.TextBox SubCastTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ContactTxt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton MaleRbtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CastBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox NationalityBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker DateTime1;
        private System.Windows.Forms.TextBox PlaceTxt;
        private System.Windows.Forms.Label label17;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Panel panel5;
    }
}