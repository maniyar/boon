﻿namespace Boon
{
    partial class ClasswiseFee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.NewReceiptBtn = new System.Windows.Forms.Button();
            this.AcdYearBox = new System.Windows.Forms.ComboBox();
            this.ClassBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.StudCountTxt = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstTerm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STerm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreRemFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.NewReceiptBtn);
            this.panel1.Controls.Add(this.AcdYearBox);
            this.panel1.Controls.Add(this.ClassBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(314, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(735, 58);
            this.panel1.TabIndex = 0;
            // 
            // NewReceiptBtn
            // 
            this.NewReceiptBtn.BackColor = System.Drawing.Color.Aquamarine;
            this.NewReceiptBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.NewReceiptBtn.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewReceiptBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NewReceiptBtn.Location = new System.Drawing.Point(551, 13);
            this.NewReceiptBtn.Name = "NewReceiptBtn";
            this.NewReceiptBtn.Size = new System.Drawing.Size(86, 30);
            this.NewReceiptBtn.TabIndex = 88;
            this.NewReceiptBtn.Text = "Get List";
            this.NewReceiptBtn.UseVisualStyleBackColor = false;
            this.NewReceiptBtn.Click += new System.EventHandler(this.GetListBtn_Click);
            // 
            // AcdYearBox
            // 
            this.AcdYearBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.AcdYearBox.BackColor = System.Drawing.Color.White;
            this.AcdYearBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcdYearBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcdYearBox.FormattingEnabled = true;
            this.AcdYearBox.Location = new System.Drawing.Point(397, 15);
            this.AcdYearBox.Name = "AcdYearBox";
            this.AcdYearBox.Size = new System.Drawing.Size(121, 26);
            this.AcdYearBox.TabIndex = 3;
            // 
            // ClassBox
            // 
            this.ClassBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ClassBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClassBox.BackColor = System.Drawing.Color.White;
            this.ClassBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassBox.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassBox.FormattingEnabled = true;
            this.ClassBox.Items.AddRange(new object[] {
            "Nursery",
            "Jr.KG",
            "Sr.KG",
            "1 st",
            "2 nd",
            "3 rd",
            "4 th",
            "5 th",
            "6 th",
            "7 th",
            "8 th",
            "9 th",
            "10 th"});
            this.ClassBox.Location = new System.Drawing.Point(156, 15);
            this.ClassBox.Name = "ClassBox";
            this.ClassBox.Size = new System.Drawing.Size(121, 26);
            this.ClassBox.TabIndex = 2;
            this.ClassBox.SelectedIndexChanged += new System.EventHandler(this.ClassBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Class :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(313, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Acd.Year";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Location = new System.Drawing.Point(137, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1089, 450);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.SName,
            this.Class,
            this.FirstTerm,
            this.STerm,
            this.PreRemFee,
            this.Total});
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1081, 442);
            this.dataGridView1.TabIndex = 0;
            // 
            // PrintBtn
            // 
            this.PrintBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PrintBtn.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintBtn.Image = global::Boon.Properties.Resources.printicon;
            this.PrintBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PrintBtn.Location = new System.Drawing.Point(1090, 555);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(136, 45);
            this.PrintBtn.TabIndex = 156;
            this.PrintBtn.Text = "Print";
            this.PrintBtn.UseVisualStyleBackColor = false;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // StudCountTxt
            // 
            this.StudCountTxt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.StudCountTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StudCountTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudCountTxt.Location = new System.Drawing.Point(141, 78);
            this.StudCountTxt.Name = "StudCountTxt";
            this.StudCountTxt.ReadOnly = true;
            this.StudCountTxt.Size = new System.Drawing.Size(154, 15);
            this.StudCountTxt.TabIndex = 157;
            this.StudCountTxt.Text = "Total students :";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(108, 741);
            this.panel3.TabIndex = 158;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1254, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(108, 741);
            this.panel4.TabIndex = 159;
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ID.HeaderText = "Gen.Reg.No.";
            this.ID.Name = "ID";
            this.ID.Width = 120;
            // 
            // SName
            // 
            this.SName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SName.HeaderText = "Student Name";
            this.SName.Name = "SName";
            // 
            // Class
            // 
            this.Class.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Class.HeaderText = "Class";
            this.Class.Name = "Class";
            this.Class.Width = 71;
            // 
            // FirstTerm
            // 
            this.FirstTerm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.FirstTerm.HeaderText = "First Term";
            this.FirstTerm.Name = "FirstTerm";
            this.FirstTerm.Width = 101;
            // 
            // STerm
            // 
            this.STerm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.STerm.HeaderText = "Second Term";
            this.STerm.Name = "STerm";
            this.STerm.Width = 123;
            // 
            // PreRemFee
            // 
            this.PreRemFee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PreRemFee.HeaderText = "Pre.Rem. Fee";
            this.PreRemFee.Name = "PreRemFee";
            this.PreRemFee.Width = 125;
            // 
            // Total
            // 
            this.Total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Total.HeaderText = "Total Remaing";
            this.Total.Name = "Total";
            this.Total.Width = 129;
            // 
            // ClasswiseFee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.StudCountTxt);
            this.Controls.Add(this.PrintBtn);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ClasswiseFee";
            this.Text = "Classwise Fee Pre-primary";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox AcdYearBox;
        private System.Windows.Forms.ComboBox ClassBox;
        private System.Windows.Forms.Button NewReceiptBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.TextBox StudCountTxt;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Class;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstTerm;
        private System.Windows.Forms.DataGridViewTextBoxColumn STerm;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreRemFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;

    }
}