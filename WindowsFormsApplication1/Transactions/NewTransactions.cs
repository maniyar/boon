﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using System.Text.RegularExpressions;

namespace Boon
{
    public partial class NewTransactions : Form
    {
        common c = new common();
        MySqlConnection connection;
        public NewTransactions()
        {
            // LoadBalance();
            connection = new MySqlConnection(c.ConnectionString);
            InitializeComponent();
        }
      
       
       MySqlCommand command = new MySqlCommand();
       String OpBal, Clbal; 
       public void openConnection()
       {
           if (connection.State == ConnectionState.Closed)
           {
               connection.Open();
           }
       }
       public void closeConnection()
       {
           if (connection.State == ConnectionState.Open)
           {
               connection.Close();
           }
       }
       public void executeQuery(String query)
       {
           try
           {
               openConnection();
               command = new MySqlCommand(query, connection);
               if (command.ExecuteNonQuery() == 1)
               {
                  // MessageBox.Show("Data is saved");
               }
               else { MessageBox.Show("Cheak network"); }
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
           finally
           {
               closeConnection();
           }
       }
      
//        Pre-Primary
//Primary
//High_school
//Other
       private void Depositbtn_Click(object sender, EventArgs e)

       {
           try
           {
               if (MessageBox.Show("Do you want to deposit the amount?", "Deposit amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
               {
                   if (comboBox1.Text == "Pre-Primary" && tnametxt.Text != "")
                   {
                       LoadAC();
                       string ttype = "Deposited";
                       float amt = float.Parse(amttext.Text);
                       float closBal = float.Parse(Clbal);
                       float total = closBal + amt;

                       if (closBal + amt >= 0)
                       {
                           String insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + comboBox1.Text + "','" + Clbal + "' ,'" + total + "','" + datetext.Value + "','" + total + "')";
                           if (c.InsertData(insertQuery))
                           {
                               LoadAC();
                               String SaveQuery = "INSERT INTO Boon.transaction_Pre(ID,Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('" + idTxt1.Text + "','" + tnametxt.Text + "','" + comboBox1.Text + "' ,'" + amttext.Text + "','" + dnametxt.Text + "' ,'" + datetext.Value + "','" + ttype + "','" + OpBal + "','" + Clbal + "')";
                               if (c.InsertData(SaveQuery))
                               {
                                   MessageBox.Show("Data is saved");
                                   clearText();
                                   LoadID();
                               }
                               else
                               {
                                   MessageBox.Show("Data is not saved plz check connections");
                               }
                           }
                       }
                   }

                   if (comboBox1.Text == "Primary" && tnametxt.Text != "")
                   {
                       LoadAC();
                       string ttype = "Deposited";
                       float amt = float.Parse(amttext.Text);
                       float closBal = float.Parse(Clbal);
                       float total = closBal + amt;

                       if (closBal + amt >= 0)
                       {
                           String insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + comboBox1.Text + "','" + Clbal + "' ,'" + total + "','" + datetext.Value + "','" + total + "')";
                           if (c.InsertData(insertQuery))
                           {
                               LoadAC();
                               String SaveQuery = "INSERT INTO Boon.transaction_Pri(ID,Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('" + idTxt1.Text + "','" + tnametxt.Text + "','" + comboBox1.Text + "' ,'" + amttext.Text + "','" + dnametxt.Text + "' ,'" + datetext.Value + "','" + ttype + "','" + OpBal + "','" + Clbal + "')";
                               if (c.InsertData(SaveQuery))
                               {
                                   MessageBox.Show("Data is saved");
                                   clearText();
                                   LoadID();
                               }
                               else
                               {
                                   MessageBox.Show("Data is not saved plz check connections");
                               }
                           }
                       }
                   }
                   if (comboBox1.Text == "High_school" && tnametxt.Text != "")
                   {
                       LoadAC();
                       string ttype = "Deposited";
                       float amt = float.Parse(amttext.Text);
                       float closBal = float.Parse(Clbal);
                       float total = closBal + amt;

                       if (closBal + amt >= 0)
                       {
                           String insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + comboBox1.Text + "','" + Clbal + "' ,'" + total + "','" + datetext.Value + "','" + total + "')";
                           if (c.InsertData(insertQuery))
                           {
                               LoadAC();
                               String SaveQuery = "INSERT INTO Boon.transaction_High(ID,Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('" + idTxt1.Text + "','" + tnametxt.Text + "','" + comboBox1.Text + "' ,'" + amttext.Text + "','" + dnametxt.Text + "' ,'" + datetext.Value + "','" + ttype + "','" + OpBal + "','" + Clbal + "')";
                               if (c.InsertData(SaveQuery))
                               {
                                   MessageBox.Show("Data is saved");
                                   clearText();
                                   LoadID();
                               }
                               else
                               {
                                   MessageBox.Show("Data is not saved plz check connections");
                               }
                           }
                       }
                   }
               }
           }
           catch { }
           }
          
//Primary
//High_school
//Other
       public void LoadID()
       {
           if (comboBox1.Text == "Pre-Primary")
           {
               idTxt1.Text = "1";
               String str = "select Max(ID)+1 from Boon.transaction_Pre ";
               DataTable dt1 = c.SelectData(str);
               if (dt1.Rows.Count > 0)
               {
                   if (dt1.Rows[0][0].ToString() == null || dt1.Rows[0][0].ToString() == "NULL" || dt1.Rows[0][0].ToString() == "")
                   {
                       idTxt1.Text = "1";
                   }
                   else
                   {
                       idTxt1.Text = dt1.Rows[0][0].ToString();
                   }

               }

           }
           if (comboBox1.Text == "Primary")
           {
               idTxt1.Text = "1";
               String str = "select Max(ID)+1 from Boon.transaction_Pri ";
               DataTable dt1 = c.SelectData(str);
               if (dt1.Rows.Count > 0)
               {
                   if (dt1.Rows[0][0].ToString() == null || dt1.Rows[0][0].ToString() == "NULL" || dt1.Rows[0][0].ToString() == "")
                   {
                       idTxt1.Text = "1";
                   }
                   else
                   {
                       idTxt1.Text = dt1.Rows[0][0].ToString();
                   }

               }

           }
           if (comboBox1.Text == "High_school")
           {
               idTxt1.Text = "1";
               String str = "select Max(ID)+1 from Boon.transaction_High ";
               DataTable dt1 = c.SelectData(str);
               if (dt1.Rows.Count > 0)
               {
                   if (dt1.Rows[0][0].ToString() == null || dt1.Rows[0][0].ToString() == "NULL" || dt1.Rows[0][0].ToString() == "")
                   {
                       idTxt1.Text = "1";
                   }
                   else
                   {
                       idTxt1.Text = dt1.Rows[0][0].ToString();
                   }

               }

           }
       }

        //public void LoadBalance()
        //{
        //    try
        //    {
        //        String SelectQuery = "SELECT Balance from Boon.Accounts where Account_name ='Total'";
        //        DataTable table = new DataTable();
        //        MySqlDataAdapter da = new MySqlDataAdapter(SelectQuery, connection);

        //        da.Fill(table);
        //        TotalTxt.Text = table.Rows[0][0].ToString();

        //        da.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //    MessageBox.Show(ex.Message);
        //    }
        //}
      
       public void LoadAC()
       {
           try
           {
               String SelectQuery = "SELECT Max(ID) from Boon.accounts where Account_name ='" + comboBox1.Text + "'";
               DataTable table = new DataTable();

               table = c.SelectData(SelectQuery);
               if (table.Rows.Count > 0)
               {

                   string tid = table.Rows[0][0].ToString();
                   String selectedata = "Select * from Boon.accounts where ID = '" + tid + "'";
                   DataTable dt2 = new DataTable();
                   dt2 = c.SelectData(selectedata);
                   if (dt2.Rows.Count > 0)
                   {
                       OpBal = dt2.Rows[0][2].ToString();
                       Clbal = dt2.Rows[0][3].ToString();


                   }
                   else {
                       OpBal ="0";
                       Clbal = "0";
                   
                   }

               }
               else
               {
                   OpBal = "0";
                   Clbal = "0";
                   MessageBox.Show("No data");
               }


           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }
        public void clearText()
        {
            idTxt1.Text = null;
            amttext.Text = null;
            dnametxt.Text = null;
            datetext.Text = null;
            tnametxt.Text = null;
            
        }
        private void Transactions_Load(object sender, EventArgs e)
        {
        // LoadBalance();
        // LoadID();
        }

        private void comboBox1_Leave(object sender, EventArgs e)
        {
           // LoadAC();
            
        }
        


        private void WithdrawalBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to Withdrawal the amount?", "Withdrawal amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (comboBox1.Text == "Pre-Primary" && tnametxt.Text != "")
                    {
                        LoadAC();

                        float amt = float.Parse(amttext.Text);
                        float closBal = float.Parse(Clbal);
                        float total = closBal - amt;

                        if (closBal - amt >= 0)
                        {
                            string ttype = "Withdrawal";
                            String insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + comboBox1.Text + "','" + Clbal + "' ,'" + total + "','" + datetext.Value + "','" + total + "')";
                            if (c.InsertData(insertQuery))
                            {

                                LoadAC();
                                String SaveQuery = "INSERT INTO Boon.transaction_pre(ID,Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('" + idTxt1.Text + "','" + tnametxt.Text + "','" + comboBox1.Text + "' ,'" + amttext.Text + "','" + dnametxt.Text + "' ,'" + datetext.Value + "','" + ttype + "','" + OpBal + "','" + Clbal + "')";
                                if (c.InsertData(SaveQuery))
                                {
                                    MessageBox.Show("Data is saved");
                                    clearText();
                                    LoadID();
                                }
                                else
                                {

                                }
                            }
                            else { MessageBox.Show("Data is not saved plz check connections"); }

                        }
                        else
                        {
                            MessageBox.Show("Account don't have that much amount");
                        }
                    }
                    if (comboBox1.Text == "Primary" && tnametxt.Text != "")
                    {
                        LoadAC();

                        float amt = float.Parse(amttext.Text);
                        float closBal = float.Parse(Clbal);
                        float total = closBal - amt;

                        if (closBal - amt >= 0)
                        {
                            string ttype = "Withdrawal";
                            String insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + comboBox1.Text + "','" + Clbal + "' ,'" + total + "','" + datetext.Value + "','" + total + "')";
                            if (c.InsertData(insertQuery))
                            {

                                LoadAC();
                                String SaveQuery = "INSERT INTO Boon.transaction_pri(ID,Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('" + idTxt1.Text + "','" + tnametxt.Text + "','" + comboBox1.Text + "' ,'" + amttext.Text + "','" + dnametxt.Text + "' ,'" + datetext.Value + "','" + ttype + "','" + OpBal + "','" + Clbal + "')";
                                if (c.InsertData(SaveQuery))
                                {
                                    MessageBox.Show("Data is saved");
                                    clearText();
                                    LoadID();
                                }
                                else
                                {

                                }



                            }
                            else { MessageBox.Show("Data is not saved plz check connections"); }

                        }
                        else
                        {
                            MessageBox.Show("Account don't have that much amount");
                        }
                    }
                    if (comboBox1.Text == "High_school" && tnametxt.Text != "")
                    {
                        LoadAC();

                        float amt = float.Parse(amttext.Text);
                        float closBal = float.Parse(Clbal);
                        float total = closBal - amt;

                        if (closBal - amt >= 0)
                        {
                            string ttype = "Withdrawal";
                            String insertQuery = "Insert into Boon.accounts (Account_Name,OpBalance,ClBalance,Tdate,Balance) Values ('" + comboBox1.Text + "','" + Clbal + "' ,'" + total + "','" + datetext.Value + "','" + total + "')";
                            if (c.InsertData(insertQuery))
                            {
                                LoadAC();
                                String SaveQuery = "INSERT INTO Boon.transaction_high(ID,Name_of_transaction,Name_of_account,Amount,Description,Date,Mode,OpBalance,ClBalance) VALUES('" + idTxt1.Text + "','" + tnametxt.Text + "','" + comboBox1.Text + "' ,'" + amttext.Text + "','" + dnametxt.Text + "' ,'" + datetext.Value + "','" + ttype + "','" + OpBal + "','" + Clbal + "')";
                                if (c.InsertData(SaveQuery))
                                {
                                    MessageBox.Show("Data is saved");
                                    clearText();
                                    LoadID();
                                }
                                else
                                {

                                }
                            }
                            else { MessageBox.Show("Data is not saved plz check connections"); }

                        }
                        else
                        {
                            MessageBox.Show("Account don't have that much amount");
                        }
                    }
                }
            }
            catch { }
        }

        private void amttext_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                errorProvider1.SetError(amttext, "Enter Numbers Only");
                e.Handled = true;
            }
        }
        ErrorProvider errorProvider1 = new ErrorProvider();
        
        private void amttext_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();

            try
            {
                Double c = Convert.ToDouble(amttext.Text);
            }
            catch
            {
                amttext.Clear();
                errorProvider1.SetError(amttext, "Use correct format");
            }
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadID();
            //if (comboBox1.Text == "High_school")
            //{
            //    String S = "High School";
            //    AcLbl.Text = S;
            //}
            //else
            //{
            //    AcLbl.Text =  comboBox1.SelectedItem.ToString();
            //}
        }

       
        }
    }

