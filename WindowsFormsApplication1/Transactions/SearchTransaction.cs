﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using MySql.Data.MySqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Configuration;

namespace Boon
{
    public partial class SearchTransaction : Form
    {
        MySqlConnection mysql;
        common Obj = new common();
        public SearchTransaction()
        {
            mysql = new MySqlConnection(Obj.ConnectionString);
            InitializeComponent();
        }
      
        MySqlCommand Cmd = new MySqlCommand();
       
        DataTable Dt = new DataTable();
        String OpBal, Clbal;

        private void SearchTransaction_Load(object sender, EventArgs e)
        {
            
        }
        public void LoadAmount()
        {

            try
            {
                String SelectQuery = "Select SUM(Amount) from Boon.transaction WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                Dt = Obj.SelectData(SelectQuery);
                if (Dt.Rows.Count > 0)
                {
                    LoadAmountTxt.Text = Dt.Rows[0][0].ToString();
                }
            }
            catch
            {
            }
        }
        public void LoadAC()
        {
            try
            {
                String SelectQuery = "SELECT SUM(OpBalance) from Boon.accounts where Account_name ='" + comboBox1.Text + "' AND Tdate Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                DataTable table = new DataTable();

                table = Obj.SelectData(SelectQuery);
                if (table.Rows.Count > 0)
                {

                    //string tid = table.Rows[0][0].ToString();
                    //String selectedata = "Select * from Boon.accounts where ID = '" + tid + "'";
                    //DataTable dt2 = new DataTable();
                    //dt2 = Obj.SelectData(selectedata);
                    //if (dt2.Rows.Count > 0)
                    //{
                    OpBal = table.Rows[0][0].ToString();
                    //Clbal = table.Rows[0][3].ToString();


                    //}
                    //else { }

                }
                else
                {

                    MessageBox.Show("No data");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
      



        private void ShowBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "Pre-Primary")
                {
                    MySqlCommand Cmd = new MySqlCommand();

                    if (TransactionNameTxt.Text == "All")
                    {
                        //String SelectQuery = "Select SUM(Amount) from Boon.transaction WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pre WHERE Name_of_account ='" + comboBox1.Text + "' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' order by Date ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pre WHERE Name_of_account = '" + comboBox1.Text + "'  AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' order by Date  ";
                            mysql.Open();

                            Cmd = new MySqlCommand(ShowQuery, mysql);

                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }

                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pre WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pre WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'  AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date ";

                            mysql.Open();

                            Cmd = new MySqlCommand(ShowQuery, mysql);

                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                }
                if (comboBox1.Text == "Primary")
                {
                    MySqlCommand Cmd = new MySqlCommand();

                    if (TransactionNameTxt.Text == "All")
                    {
                        //String SelectQuery = "Select SUM(Amount) from Boon.transaction WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pri WHERE Name_of_account ='" + comboBox1.Text + "' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pri WHERE Name_of_account = '" + comboBox1.Text + "'  AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date  ";
                            mysql.Open();

                            Cmd = new MySqlCommand(ShowQuery, mysql);

                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }

                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pri WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date  ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pri WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'  AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date  ";

                            mysql.Open();

                            Cmd = new MySqlCommand(ShowQuery, mysql);

                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                }
                if (comboBox1.Text == "High_school")
                {
                    MySqlCommand Cmd = new MySqlCommand();

                    if (TransactionNameTxt.Text == "All")
                    {
                        //String SelectQuery = "Select SUM(Amount) from Boon.transaction WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_high WHERE Name_of_account ='" + comboBox1.Text + "' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_high WHERE Name_of_account = '" + comboBox1.Text + "'  AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date  ";
                            mysql.Open();

                            Cmd = new MySqlCommand(ShowQuery, mysql);

                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }

                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_high WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'   AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_high WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%'  AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date  ";

                            mysql.Open();

                            Cmd = new MySqlCommand(ShowQuery, mysql);

                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mysql.Close();
            }


        }

        private void ShowDeBtn_Click(object sender, EventArgs e)
        {
            if (TransactionNameTxt.Text == "")
            {
                MessageBox.Show("Select Type of transaction","Notification ");
                TransactionNameTxt.Focus();
                return;
            }
            try
            {
             if (comboBox1.Text == "Pre-Primary")
                {
                    MySqlCommand Cmd = new MySqlCommand();
                    MySqlDataAdapter da = new MySqlDataAdapter(Cmd);
                    DataTable table = new DataTable();
                    if (TransactionNameTxt.Text != "All")
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pre WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        //String ShowQuery = "Select * from Boon.transaction WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pre WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pre WHERE Name_of_account ='" + comboBox1.Text + "' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        //String ShowQuery = "Select * from Boon.transaction WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pre WHERE Name_of_account = '" + comboBox1.Text + "'AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }

                    }
               }
                if (comboBox1.Text == "Primary")
                {
                    MySqlCommand Cmd = new MySqlCommand();
                    MySqlDataAdapter da = new MySqlDataAdapter(Cmd);
                    DataTable table = new DataTable();
                    if (TransactionNameTxt.Text != "All")
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pri WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        //String ShowQuery = "Select * from Boon.transaction WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pri WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pri WHERE Name_of_account ='" + comboBox1.Text + "' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date ";
                        //String ShowQuery = "Select * from Boon.transaction WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pri WHERE Name_of_account = '" + comboBox1.Text + "'AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }

                    }
                }

                if (comboBox1.Text == "High_school")
                {
                    MySqlCommand Cmd = new MySqlCommand();
                    MySqlDataAdapter da = new MySqlDataAdapter(Cmd);
                    DataTable table = new DataTable();
                    if (TransactionNameTxt.Text != "All")
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_high WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        //String ShowQuery = "Select * from Boon.transaction WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_high WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_high WHERE Name_of_account ='" + comboBox1.Text + "' AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        //String ShowQuery = "Select * from Boon.transaction WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";
                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_high WHERE Name_of_account = '" + comboBox1.Text + "'AND Mode = 'Deposited' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mysql.Close();
            }


        }

        private void ShowWeBtn_Click(object sender, EventArgs e)
        {
               if (TransactionNameTxt.Text == "")
            {
                MessageBox.Show("Select Type of transaction","Notification ");
                TransactionNameTxt.Focus();
                return;
            }
            try
            {

                MySqlCommand Cmd = new MySqlCommand();
                MySqlDataAdapter da = new MySqlDataAdapter(Cmd);
                DataTable table = new DataTable();
                


                if (comboBox1.Text == "Pre-Primary")
                {
                    if (TransactionNameTxt.Text != "All")
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pre WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";

                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pre WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pre WHERE Name_of_account ='" + comboBox1.Text + "' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";

                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pre WHERE Name_of_account = '" + comboBox1.Text + "'AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
               }
                if (comboBox1.Text == "Primary")
                {
                    if (TransactionNameTxt.Text != "All")
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pri WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";

                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pri WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "'  Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_pri WHERE Name_of_account ='" + comboBox1.Text + "' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";

                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_pri WHERE Name_of_account = '" + comboBox1.Text + "'AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                }
                if (comboBox1.Text == "High_school")
                {
                    if (TransactionNameTxt.Text != "All")
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_high WHERE Name_of_account ='" + comboBox1.Text + "' AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";

                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_high WHERE Name_of_account = '" + comboBox1.Text + "'AND Name_of_transaction Like '" + TransactionNameTxt.Text + "%' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date  ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                    else
                    {
                        String SelectQuery = "Select SUM(Amount) from Boon.transaction_high WHERE Name_of_account ='" + comboBox1.Text + "' AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' ";

                        Dt = Obj.SelectData(SelectQuery);

                        if (Dt.Rows.Count > 0)
                        {
                            LoadAmountTxt.Text = Dt.Rows[0][0].ToString();

                            String ShowQuery = "Select * from Boon.transaction_high WHERE Name_of_account = '" + comboBox1.Text + "'AND Mode = 'Withdrawal' AND Date Between '" + Date1.Value + "' AND'" + Date2.Value + "' Order By Date ";
                            Cmd = new MySqlCommand(ShowQuery, mysql);
                            mysql.Open();
                            DataSet ds = new DataSet();
                            MySqlDataAdapter mda = new MySqlDataAdapter(Cmd);
                            mda.Fill(ds, "transaction");
                            SearchTransactionReport cr = new SearchTransactionReport();
                            cr.SetDataSource(ds);
                            crystalReportViewer1.ReportSource = cr;
                        }
                        else
                        {
                            MessageBox.Show("No data found", "Alert");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mysql.Close();
            }

        
        
        }

     

        private void TransactionNameTxt_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
        }

    }
}
