﻿namespace Boon
{
    partial class SearchTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Date1 = new System.Windows.Forms.DateTimePicker();
            this.Date2 = new System.Windows.Forms.DateTimePicker();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.ShowBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TransactionNameTxt = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ShowDeBtn = new System.Windows.Forms.Button();
            this.ShowWeBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LoadAmountTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Date1
            // 
            this.Date1.CalendarFont = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date1.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Date1.Location = new System.Drawing.Point(686, 22);
            this.Date1.Name = "Date1";
            this.Date1.Size = new System.Drawing.Size(130, 28);
            this.Date1.TabIndex = 2;
            // 
            // Date2
            // 
            this.Date2.CalendarFont = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date2.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Date2.Location = new System.Drawing.Point(900, 22);
            this.Date2.Name = "Date2";
            this.Date2.Size = new System.Drawing.Size(130, 28);
            this.Date2.TabIndex = 3;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Location = new System.Drawing.Point(2, 203);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(1365, 535);
            this.crystalReportViewer1.TabIndex = 2;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBox1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Pre-Primary",
            "Primary",
            "High_school",
            "Other"});
            this.comboBox1.Location = new System.Drawing.Point(122, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(136, 26);
            this.comboBox1.TabIndex = 0;
            // 
            // ShowBtn
            // 
            this.ShowBtn.BackColor = System.Drawing.Color.Lime;
            this.ShowBtn.Location = new System.Drawing.Point(1072, 36);
            this.ShowBtn.Name = "ShowBtn";
            this.ShowBtn.Size = new System.Drawing.Size(83, 45);
            this.ShowBtn.TabIndex = 0;
            this.ShowBtn.Text = "Show All";
            this.ShowBtn.UseVisualStyleBackColor = false;
            this.ShowBtn.Click += new System.EventHandler(this.ShowBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(557, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "Select Date From :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Select Account :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(826, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "To Date :";
            // 
            // TransactionNameTxt
            // 
            this.TransactionNameTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.TransactionNameTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TransactionNameTxt.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransactionNameTxt.FormattingEnabled = true;
            this.TransactionNameTxt.Items.AddRange(new object[] {
            "All",
            "Student Fees",
            "Staff Salary",
            "Xerox Exp",
            "Stationary Exp",
            "Printing Exp",
            "Exam Exp",
            "Cultural Prog",
            "Educational Equipment Exp",
            "Building Rent"});
            this.TransactionNameTxt.Location = new System.Drawing.Point(327, 23);
            this.TransactionNameTxt.Name = "TransactionNameTxt";
            this.TransactionNameTxt.Size = new System.Drawing.Size(220, 26);
            this.TransactionNameTxt.TabIndex = 1;
            this.TransactionNameTxt.SelectedIndexChanged += new System.EventHandler(this.TransactionNameTxt_SelectedIndexChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(268, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Type  :";
            // 
            // ShowDeBtn
            // 
            this.ShowDeBtn.BackColor = System.Drawing.Color.Lime;
            this.ShowDeBtn.Location = new System.Drawing.Point(1161, 36);
            this.ShowDeBtn.Name = "ShowDeBtn";
            this.ShowDeBtn.Size = new System.Drawing.Size(83, 45);
            this.ShowDeBtn.TabIndex = 1;
            this.ShowDeBtn.Text = "Show Diposited";
            this.ShowDeBtn.UseVisualStyleBackColor = false;
            this.ShowDeBtn.Click += new System.EventHandler(this.ShowDeBtn_Click);
            // 
            // ShowWeBtn
            // 
            this.ShowWeBtn.BackColor = System.Drawing.Color.Lime;
            this.ShowWeBtn.Location = new System.Drawing.Point(1250, 36);
            this.ShowWeBtn.Name = "ShowWeBtn";
            this.ShowWeBtn.Size = new System.Drawing.Size(83, 45);
            this.ShowWeBtn.TabIndex = 2;
            this.ShowWeBtn.Text = "Show Withdrawal";
            this.ShowWeBtn.UseVisualStyleBackColor = false;
            this.ShowWeBtn.Click += new System.EventHandler(this.ShowWeBtn_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.RosyBrown;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(1062, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 71);
            this.panel1.TabIndex = 12;
            // 
            // LoadAmountTxt
            // 
            this.LoadAmountTxt.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadAmountTxt.Location = new System.Drawing.Point(137, 17);
            this.LoadAmountTxt.Name = "LoadAmountTxt";
            this.LoadAmountTxt.ReadOnly = true;
            this.LoadAmountTxt.Size = new System.Drawing.Size(220, 29);
            this.LoadAmountTxt.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 18);
            this.label5.TabIndex = 14;
            this.label5.Text = "Total  :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Tan;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.LoadAmountTxt);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(20, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(402, 64);
            this.panel2.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Tan;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.Date2);
            this.panel3.Controls.Add(this.Date1);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.TransactionNameTxt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(20, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1036, 71);
            this.panel3.TabIndex = 16;
            // 
            // SearchTransaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.ShowWeBtn);
            this.Controls.Add(this.ShowDeBtn);
            this.Controls.Add(this.ShowBtn);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Name = "SearchTransaction";
            this.Text = "Search Transaction";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SearchTransaction_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker Date1;
        private System.Windows.Forms.DateTimePicker Date2;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button ShowBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox TransactionNameTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ShowDeBtn;
        private System.Windows.Forms.Button ShowWeBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox LoadAmountTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}